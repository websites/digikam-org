
## Usage:
Clone the repository to your site's `themes` directory. Refer to [`exampleSite/config.toml`](https://github.com/htkoca/hugo-theme-foundation6-blog/blob/master/exampleSite/config.toml) for optional configuration values. A few suggestions to help you get a good looking site quickly:

* Keep news posts in the `content/news` directory, for example: `content/news/my-news-item.md`.
* Keep static pages in the `content` directory, for example: `content/about.md`.
* Keep media like images in the `static` directory, for example: `static/2016/10/screenshot.png`.
* If you want an image to be shown when you share a post on social media, specify at least one image in the post's front matter, for example: `images: ["/2016/10/screenshot.png"]`.
* Use the `<!--more-->` tag in posts to control how much of a post is shown on summary pages.

## Building, for developers: (Optional)
This theme uses gulp build scripts modified from [Foundation Zurb Template](https://github.com/zurb/foundation-zurb-template/). OS must have `node` and `bower` installed.

Command | Description
:-- | :--
**Install:** | **Run from `/scripts` directory to download all dependencies.**
`npm install` | Installs node dependencies.
`bower install` | Installs bower dependencies.
**Build Tasks:** | **Choose one to build from scss / js files in `/source` to `/static`.**
`npm run build:debug` | Build human readable `debug` files.
`npm run build:prod` | Build minified, sourcemapped files.
`npm run build:css` | Builds `debug` css only.
`npm run build:js` | Builds `debug` js only.
**Hugo Tasks:** | **Theme must be installed in a working or fresh hugo installation.**
`npm run build:hugo` | Builds `debug` css / js, deletes `/public`, builds `/public`, lint `/public`.
`npm run server` | Builds `debug` css / js, deletes `/public`, runs hugo server, start css / js / lint watch task.

## Attribution:

### This repository is maintained by:
* [Mica](https://silentumbrella.com) - [GPL 3](https://tldrlegal.com/license/gnu-general-public-license-v3-(gpl-3))
### This repository was originally cloned from:
* [Tony Ko](https://github.com/htkoca) - [GPL 3](https://tldrlegal.com/license/gnu-general-public-license-v3-(gpl-3)) - [License file](https://github.com/htkoca/hugo-theme-foundation6-blog/blob/master/license.txt)

### Which was originally cloned from:
* [Alan Orth - hugo-theme-bootstrap4-blog](https://github.com/alanorth/hugo-theme-bootstrap4-blog/) - [GPL 3](https://tldrlegal.com/license/gnu-general-public-license-v3-(gpl-3)) - [License file](https://github.com/alanorth/hugo-theme-bootstrap4-blog/blob/master/LICENSE.txt)

### And contains the code of:
* [Foundation Zurb Template](https://github.com/zurb/foundation-zurb-template/) - [MIT license](https://tldrlegal.com/license/mit-license)
