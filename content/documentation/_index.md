---
date: "2017-03-21"
title: "Documentation"
author: "digiKam Team"
description: "digiKam Documentation and Resources"
category: "documentation"
aliases: "/docs"
menu: "navbar"
---

### Documentation and Useful Resources

![Online Handbook](/img/content/documentation/handbook_HTML.webp)

The digiKam documentation can be [read online here](https://docs.digikam.org/en/index.html) or downloaded in [EPUB format here](https://docs.digikam.org/en/epub/DigikamManual.epub). 

[digiKam tutorials](https://userbase.kde.org/Digikam/Tutorials) wiki offers tutorials that cover various digiKam tools and functionality.

### API Documentation for Developers and Database Schema

![API Documentation](/img/content/documentation/apidoc_HTML.webp)

If you plan contribute as a developer to digiKam, you might find the following links useful:

* [digiKam API](https://files.kde.org/digikam/api/)
* [Database schema](https://invent.kde.org/graphics/digikam/-/raw/master/project/documents/DBSCHEMA.ODS)

Uncompress the API tarball on your computer to read the developer documentation with your prefered Web-browser. The entry of the API is the **html/index.html** file.

To explore the SQlite database files contents, you can use the [SQliteBrowser application](https://sqlitebrowser.org/).

![SQlite Browser ](/img/content/documentation/sqlite_browser.webp)

### Contribute

We encourage you to be involved in the existing documentation by submitting comments, corrections, and patches. 
If you'd like to help to the documentation, have a look at the [Contribute](/contribute#documentation) page.
