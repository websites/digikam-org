---
date: "2017-03-22T13:14:13-06:00"
title: "Overview"
author: "digiKam Team"
description: "A brief description of the digiKam sotware."
category: "about"
aliases: "/about/overview"
---

![digiKam Pack](/img/content/about/overview_digikam_pack.png)

### A digiKam Overview

digiKam is an advanced digital **photo management application**, which makes importing and organizing digital photos a "snap". 
The photos are organized in albums which can be sorted chronologically, by folder layout or by custom collections.

[![](/img/content/about/overview_tagsview.png "Tags View")](/img/content/about/overview_tagsview.png)

Tired of the folder constraints? Don’t worry, digiKam also provides **tagging**. You tag your images which can be spread out across 
multiple folders, and digiKam provides fast and intuitive ways to browse these tagged images. You can also add comments to your images. 
digiKam makes use of a fast and robust database to store these meta-informations which makes adding and editing of comments and tags very reliable.

digiKam include also many tools dedicated to import and export contents to remote web-services, as Flickr, GPhoto, ImgUr, Facebook, etc.

[![](/img/content/about/overview_import.png "Camera Import")](/img/content/about/overview_import.png)

An easy-to-use interface is provided that enables you to connect to your camera and preview, download and/or delete your images. 
Basic auto-transformations can be deployed on the fly during image downloading.

Another tool, which most artists and photographers will be familiar with, is a **Light Table**. This tool assists artists and photographers 
with reviewing their work ensuring the highest quality only. A classical light table will show the artist the place on the images to touch up. 
Well in digiKam, the light table function provides the user a similar experience. You can import a photo, drag it onto the light table, 
and touch up only the areas that need it.

[![](/img/content/about/overview_lighttable.png "Light Table")](/img/content/about/overview_lighttable.png)

The digiKam **Image Editor** provide some common tools e.g. red eye correction or colors correction. Additional tools are provided with the main application 
to process advanced corrections on image like color management, noise reduction, or special effects. digiKam Image Editor supports all camera RAW file formats, 
16 bits color depth, Exif/Makernote/IPTC/GPS/XMP metadata, Color management, tagging/rating/comments pictures, etc.

[![](/img/content/about/overview_imageeditor.png "Image Editor")](/img/content/about/overview_imageeditor.png)

A stand-alone image editor version named **ShowFoto** is also available. It runs without digiKam images database support, but provides all Image Editor functions.

Finally, digiKam include also a queue manager to batch operations on a set of images. This tool is perfect to play common photographer workflow previously 
configured by end-user. The operations available on **Batch Queue Manager** are the same than Image Editor, but operations can be chained to process more than 
one file at the same time.

[![](/img/content/about/overview_bqm.png "Batch Queue Manager")](/img/content/about/overview_bqm.png)

You want to know more about digiKam? Continue to [Features page](/about/features/)
