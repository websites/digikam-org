---
date: "2017-03-21"
title: "Events"
author: "digiKam Team"
description: "A list of events related to digiKam."
category: "events"
aliases: "/node/354"
---

<div class="content"><h3>2008</h3>

<p>
</p><ul>
  <li><b><a href="http://www.libregraphicsmeeting.org">Linux Graphic Meeting</a></b>: Wroclaw, Poland - 8-11th May - <a href="http://www.digikam.org/drupal/node/315">See details here</a></li>

  <img src="http://farm4.static.flickr.com/3162/2513282729_2d5ba64b0f_m.jpg" width="160" height="240"><br>

  <li><b><a href="http://akademy.kde.org">Akademy</a></b>: Sint-Katelijne-Waver, Belgium - 9-10th August - <a href="http://www.digikam.org/drupal/node/366">See details here</a></li>

  <img src="https://lh3.googleusercontent.com/-RqdFzbHqyHk/Tmdw_0gkGYI/AAAAAAAAAqo/KIaZ0Y3C2P8/s604/n1117543793_30072033_763.jpg" width="240" height="160"><br>

  <li><b>KDE Graphics Coding Sprint</b>:<br>
         Planed at <a href="http://snipurl.com/3zpqa">Genoa, Italia</a><br>
         Date: 31 october - 2 november<br>
         Organized by Angelo Naselli<br>
         Hosted by <a href="http://www.alid.it">Alid Association</a><br>
         Sponsored by <a href="http://ev.kde.org">KDE-ev</a>
  </li>

  <img src="https://lh3.googleusercontent.com/-sFJ2a3QUxXA/TmdxeQP2_LI/AAAAAAAAAsE/smaFjwXy3Vo/s604/n1117543793_30091022_7013.jpg" width="240" height="160"><br>

</ul>
<p></p>

<h3>2009</h3>

<p>
</p><ul>
  <li><b>KDE Graphics Coding Sprint</b>:<br>
         Planed at <a href="http://maps.google.de/maps?f=q&amp;source=s_q&amp;hl=de&amp;geocode=&amp;q=Bernhardstra%C3%9Fe+12,+Essen&amp;sll=51.392833,7.024748&amp;sspn=0.006494,0.019119&amp;ie=UTF8&amp;hq=&amp;hnear=Bernhardstra%C3%9Fe+12,+Fischlaken+45239+Essen,+Nordrhein-Westfalen&amp;ll=51.391681,7.026787&amp;spn=0.006494,0.019119&amp;z=16">Essen, Germany</a><br>
         Date: 13 - 15 november<br>
         Organized and hosted by Marcel Wiesweg<br>
         Sponsored by <a href="http://ev.kde.org">KDE-ev</a><br>
         <a href="http://techbase.kde.org/Projects/Digikam/CodingSprint2009">Wiki page</a><br>
         <a href="http://dot.kde.org/2009/11/22/digikam-and-kipi-sprint">See event resume details here</a>
  </li>

<a href="http://www.flickr.com/photos/digikam/4107577420/" title="PICT2891.jpg by digiKam team, on Flickr"><img src="http://farm3.static.flickr.com/2604/4107577420_71301d1155_m.jpg" alt="PICT2891.jpg" width="240" height="160"></a>

<a href="http://www.flickr.com/photos/digikam/4104932135/" title="PICT2892.jpg by digiKam team, on Flickr"><img src="http://farm3.static.flickr.com/2602/4104932135_3cb3f69536_m.jpg" alt="PICT2892.jpg" width="240" height="160"></a>

</ul>
<p></p>

<h3>2010</h3>

<p>
</p><ul>
  <li><b>KDE Graphics Coding Sprint</b>:<br>
         Planed at <a href="http://maps.google.com/maps?f=q&amp;hl=en&amp;q=1%2C%20Rue%20Emile%20Tavan%2C%2013100%20%2C%20Aix-en-Provence%2C%20France">Maison des associations Tavan, Aix en provence, France</a><br>
         Date: 27 - 29 august<br>
         Organized by digiKam team<br>
         Sponsored by <a href="http://ev.kde.org">KDE-ev</a><br>
         <a href="http://techbase.kde.org/Projects/Digikam/CodingSprint2010">Wiki page</a><br>
         <a href="http://www.digikam.org/drupal/node/538">See event resume details here</a>
  </li>

<a href="http://www.flickr.com/photos/digikam/4941067580/" title="DSC01577.JPG by digiKam team, on Flickr"><img src="http://farm5.static.flickr.com/4142/4941067580_017df32ccd_m.jpg" alt="DSC01577.JPG" width="160" height="240"></a>

<a href="http://www.flickr.com/photos/50387249@N04/4936994695/" title="P1030859 by zzzahu, on Flickr"><img src="http://farm5.static.flickr.com/4097/4936994695_e80a8f2e1e_m.jpg" alt="P1030859" width="240" height="180"></a>

</ul>
<p></p>

<h3>2012</h3>

<p>
</p><ul>
  <li><b>KDE Graphics Coding Sprint</b>:<br>
         Planed at Genoa<br>
         Date: 13 - 15 january 2012<br>
         Organized by Angelo Naselli<br>
         Hosted by <a href="http://www.alid.it">Alid Association</a><br>
         Sponsored by <a href="http://ev.kde.org">KDE-ev</a><br>
         <a href="http://techbase.kde.org/Projects/Digikam/CodingSprint2011">Wiki page</a><br>
         <a href="http://dot.kde.org/2012/02/22/digikam-team-meets-genoa-italy">See event resume details here</a>
  </li>

<a href="https://lh3.googleusercontent.com/-0_Nc31NkByE/TxHVtpHP8EI/AAAAAAAAAmc/fo7MyE7mUm0/w284-h270-k/DSC00499_v1.JPG" title="DSC00499_v1.JPG by digiKam team"><img src="https://lh3.googleusercontent.com/-0_Nc31NkByE/TxHVtpHP8EI/AAAAAAAAAmc/fo7MyE7mUm0/w284-h270-k/DSC00499_v1.JPG" alt="DSC00499_v1.JPG" width="284" height="270"></a>

</ul>
<p></p>

<h3>2014</h3>

<p>
</p><ul>
  <li><b>digiKam Coding Sprint</b>:<br>
         Planed at Berlin<br>
         Date: 14 - 16 november 2014<br>
         Organized by Tobias Hunger<br>
         Hosted by <a href="http://www.digia.com/en/Contact/">Digia Germany GmbH</a><br>
         Sponsored by <a href="http://ev.kde.org">KDE-ev</a><br>
         <a href="https://sprints.kde.org/sprint/248">Wiki page</a><br>
         <a href="https://dot.kde.org/2015/03/05/digikam-sprint-2014">See event resume details here</a>
  </li>

<a href="https://www.flickr.com/photos/digikam/15772576686"><img src="https://farm6.staticflickr.com/5615/15772576686_e5ea277655_n.jpg" width="320" height="213"></a>
</ul>
<p></p>


<h3>2015</h3>

<p>
</p><ul>
  <li><b>digiKam Coding Sprint</b>:<br>
         Planed while KDE Randa Reunion<br>
         Date: 09 - 13 september 2015<br>
         Organized by Mario Fux<br>
         Hosted at <a href="http://randa-meetings.ch/">Randa, Switzerland </a><br>
         Sponsored by <a href="http://ev.kde.org">KDE-ev</a><br>
         <a href="https://sprints.kde.org/sprint/271">Wiki page</a><br>
         <a href="https://dot.kde.org/2015/12/07/randa-meetings-2015-huge-success-again">See event resume details here</a>
  </li>

<a href="https://www.flickr.com/photos/digikam/21363200575/in/album-72157658453239225/" title="DSC08226"><img src="https://farm1.staticflickr.com/632/21363200575_4cd510654b_n.jpg" alt="DSC08226" width="320" height="213"></a>
</ul>
<p></p>

<p>
</p><ul>
  <li><b>digiKam Coding Sprint</b>:<br>
         Planed while KDE Randa Reunion<br>
         Date: 10 - 16 september 2017<br>
         Organized by Mario Fux<br>
         Hosted at <a href="http://randa-meetings.ch/">Randa, Switzerland </a><br>
         Sponsored by <a href="http://ev.kde.org">KDE-ev</a><br>
  </li>

</ul>
<p></p>

</div>
