---
date: "2017-03-22T13:13:45-06:00"
title: "Installing a tarball"
author: "digiKam Team"
description: "Build and install digiKam from a tarball."
category: "download"
aliases: "/download/tarball"
---

<div class="content"><p>Installing a tarball will get you the most up-to-date stable version of digiKam. But it can happen that you run into all kinds of problems related to compiling. </p>
<p>So, if your distribution is running behind a lot, you can install a tarball.</p>
<p>Before installing a tarball, it is better to prevent conflicts to uninstall the digiKam package provided by your distribution. </p>
<p>First you have to download digiKam from :</p>
<p>- This repository for <a href="http://download.kde.org/stable/digikam/">stable releases</a>.</p>
<p>- For older releases deprecated and not maintained <a href="http://download.kde.org/Attic/digikam/">look here</a>.</p>
<p>Second you need to install all digiKam <a href="/download/dependencies">dependencies</a>.</p>
<p>Extract the tarball via <code>tar -xvJf filename.xz</code>, enter the extracted directory and then you need to issue a set of commands. You need to specify an installation prefix. This will be the base path of the installation. To determine the correct installation path, use <code>`kf5-config --prefix`</code> and provide it as a parameter for cmake, as shown in the example commands below:</p>
<p><code><br>
bootstrap.linux<br>
cd build<br>
make<br>
su<br>
make install<br>
</code></p>
<p>Notes: </p>
<p>- A bootstrap script is also available to configure for OSX through Macports.<br>
- A bootstrap script is also available to configure for Windows through MXE (cross compiling with MinGW). Forget a native compilation using Microsoft Visual C++. It's a waste of time as this compiler is a big weird puzzle...</p>
</div>
