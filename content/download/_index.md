---
date: "2017-03-21"
title: "Download"
author: "digiKam Team"
description: "Where to obtain digiKam."
category: "download"
aliases: "/download"
---

- All files are signed with GnuPG and can be verified with [this public key](https://keys.openpgp.org/vks/v1/by-fingerprint/D1CF2444A7858C5F2FB095B74A77747BC2386E50)
- The AppImage bundle can be integrated in your Linux desktop using [AppImageLauncher](https://github.com/TheAssassin/AppImageLauncher)
- The bundles to use first are the Qt6 versions. The Qt5 versions are prior bundles provided for compatibity.

### Weekly Pre-Release Snapshots

These are compiled weekly versions provided by the team for testing purposes only.

This is the easiest way to try the current implementation that hasn't been released yet.
This includes a universal Linux AppImage, macOS package, and Windows installer.

[Download the latest weekly pre-release snapshots](https://files.kde.org/digikam/).

### Package By Distribution

[Install a package](/download/binary/) that is made available through your distribution.

Keep in mind that distro-provided versions of digiKam could be out of date.
macOS and Windows users should refer to the [official bundles](#official-bundles) listed above.

### Compile the Official Release

This is an option for those that are comfortable with compiling the project themselves.

Get the source for the [latest official release](http://download.kde.org/stable/digikam/) and build.

### Compile Latest Development Version

This is more complicated, but is as recent as possible.
This is for when you want to help with finding bugs or to hack on the project as a contributor.

Get the latest code [from our git repository](/download/git/).

You can choose the one which fits you best. We provide detailed instructions for each of them.

### Older-Releases

All previous stable versions are archived.
This includes a universal Linux AppImage, macOS package, and Windows installer.

[Download the older stable release](https://download.kde.org/Attic/digikam/).
