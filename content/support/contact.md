---
date: "2017-03-21"
title: "Contact"
author: "digiKam Team"
description: "Various contact methods for digiKam"
category: "support"
aliases: "/contact"
---

### IRC CHANNEL

You can find us on the **irc.libera.chat** network, server: #digikam

You can use [web chat](https://web.libera.chat/)
to be connected on \#digikam IRC channel using a web interface.

### TEAM

See the list of current [digiKam team
members](https://invent.kde.org/graphics/digikam/-/blob/master/AUTHORS).
