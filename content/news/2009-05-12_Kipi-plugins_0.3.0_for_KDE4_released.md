---
date: "2009-05-12T07:14:00Z"
title: "Kipi-plugins 0.3.0 for KDE4 released"
author: "digiKam"
description: "Dear all digiKam fans and users! Kipi-plugins 0.3.0 maintenance release for KDE4 is out. kipi-plugins 0.3.0 tarball can be downloaded from SourceForge at this url"
category: "news"
aliases: "/node/449"

---

<p>Dear all digiKam fans and users!</p>

<p>Kipi-plugins 0.3.0 maintenance release for KDE4 is out.</p>

<a href="http://www.flickr.com/photos/digikam/3524879470/" title="23exportplugin by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3341/3524879470_b820484561.jpg" width="500" height="400" alt="23exportplugin"></a>

<p>kipi-plugins 0.3.0 tarball can be downloaded from SourceForge <a href="http://sourceforge.net/project/showfiles.php?group_id=149779&amp;package_id=165761">at this url</a></p>

<p>Kipi-plugins will be also available for Windows. Precompiled packages can be donwloaded with KDE-Windows installer. See <a href="http://windows.kde.org">KDE-Windows project</a> for details.</p>

<p>See below the list of new features and bugs-fix coming with this release:</p>

<b>General</b>         : Fix compilation under MSVC 9.<br>
<b>PicasaWebExport</b> : Support protected albums (sign-in required to view).<br>
<b>PrintImages</b>     : Added per photo print management.<br>
<b>PrintImages</b>     : Added Atkins page layout printing.<br><br>

001 ==&gt; 186981 : FlickrExport       : Flickr on plasma crashes on "next photo".<br>
002 ==&gt; 187512 : PicasaWebExport    : Album list does not show proper titles.<br>
003 ==&gt; 185010 : FaceBook           : Export to facebook gone in KIPI.<br>
004 ==&gt; 150737 : PicasaWebExport    : Tool does not use correct album name if special characters present.<br>
005 ==&gt; 169695 : PicasaWebExport    : digiKam crashes with cancel in Export to Picasaweb.<br>
006 ==&gt; 187512 : PicasaWebExport    : PicasaWeb GUI shows no picasa album list.<br>
007 ==&gt; 175221 : PicasaWebExport    : Photos are always rewritten to JPEG before upload (and not only for RAW and rescale).<br>
008 ==&gt; 187536 : PicasaWebExport    : Support new type of albums (sign-in required to view).<br>
009 ==&gt; 187809 : PicasaWebExport    : Crash on Start upload when no album selected (issue error instead).<br>
010 ==&gt; 185909 : AdvancedSlideshow  : Crash on startup (KIPI plugin Advanced Slideshow).<br>
011 ==&gt; 141535 : PrintImages        : Suggestion for photo layout algorithm.<br>
012 ==&gt; 157173 : LibKexiv2          : Crash by adding gps data to pictures.<br>
013 ==&gt; 190964 : SendImages         : "send pictures" is modal in digiKam.<br>
014 ==&gt; 189636 : BatchProcessImages : Application crashes when closing completed batch resize or convert.<br>
015 ==&gt; 191351 : GPSSync            : digiKam crashes in Ubuntu (NOT Kubuntu) 9.04 when Konqueror removed.<br>
016 ==&gt; 187756 : BatchProcessImages : Crash normalizing image colors batch processing.<br>
017 ==&gt; 161492 : RawConverter       : Crash when converting Nikon RAW (NEF) image to anything.<br>

<div class="legacy-comments">

  <a id="comment-18532"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/449#comment-18532" class="active">"kipi-plugins 0.2.0 tarball</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Tue, 2009-05-12 11:36.</div>
    <div class="content">
     <p>"kipi-plugins 0.2.0 tarball can be downloaded..."</p>
<p>I suppose you wanted to say "kipi-plugins 0.3.0".</p>
<p>Congrats for the release!</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18533"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/449#comment-18533" class="active">Right...</a></h3>    <div class="submitted">Submitted by digiKam on Tue, 2009-05-12 11:44.</div>
    <div class="content">
     <p>Right... fixed</p>
         </div>
    <div class="links">» </div>
  </div>
</div>
</div>
