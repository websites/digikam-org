---
date: "2008-10-30T21:05:00Z"
title: "digiKam 0.10.0-beta5 release for KDE4"
author: "digiKam"
description: "Dear all digiKam fans and users! The digiKam development team is happy to release the 5th beta release dedicated to KDE4. The digiKam tarball can"
category: "news"
aliases: "/node/381"

---

<p>Dear all digiKam fans and users!</p>

<p>The digiKam development team is happy to release the 5th beta release dedicated to KDE4. The digiKam tarball can be downloaded from <a href="http://sourceforge.net/project/showfiles.php?group_id=42641">SourceForge</a>.</p>

<a href="http://www.flickr.com/photos/digikam/2987596628/" title="showfoto-win32 by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3137/2987596628_fa92e2e580.jpg" width="500" height="375" alt="showfoto-win32"></a>

<p>With this new release, digiKam is now compilable under Windows using MinGW and Microsoft Visual C++. Precompiled packages are available with KDE-Windows installer. See <a href="http://windows.kde.org">KDE-Windows project</a> for details.</p>

<p>Take care, it's always a BETA code with many of new bugs not yet fixed... Do not use yet in production...</p>
 
<p>KDE3 version still the stable release and have been published <a href="http://www.digikam.org/drupal/node/359">this summer</a>.</p>

<p>To compile digiKam for KDE4, you need <b>libkexiv2</b>, <b>libkdcraw</b>, and <b>libkipi</b> for KDE4. These libraries are now included in KDE core but for the moment, digiKam require libkdcraw and libkexiv2 from svn trunk to compile fine (These versions of libraries will be released with KDE 4.2). To extract libraries source code from svn, look at bottom of <a href="http://www.digikam.org/drupal/download?q=download/svn">this page</a> for details</p>

<p>For others depencies, consult the <a href="http://websvn.kde.org/trunk/extragear/graphics/digikam/README?view=markup">README file</a>. There are also optional depencies to enable some new features as lensfun for lens auto-correction, and marble for geolocation.</p>
 
<p>The digikam.org web site has been redesigned for the new KDE4 series. All <a href=" http://www.digikam.org/drupal/node/323">screenshots</a> have been updated and a lots of <a href="  http://www.digikam.org/drupal/tour">video demos</a> have been added to present new features included in this version.</p>
 
<h5>NEW FEATURES (since 0.9.x series):</h5><br>

<b>General</b> : Ported to CMake/Qt4/KDE4.<br>
<b>General</b> : Thumbs KIO-Slave removed. digiKam now use multi-threading to generate thumnails.<br>
<b>General</b> : Removed all X11 library dependencies. Code is now portable under MACOS-X and Win32.<br>
<b>General</b> : Support of XMP metadata (require <a href="http://www.exiv2.org">Exiv2 library</a> &gt;= 0.16).<br>
<b>General</b> : Hardware handling using KDE4 Solid interface.<br>
<b>General</b> : Preview of Video and Audio files using KDE4 Phonon interface.<br>
<b>General</b> : Database file can be stored on a customized place to support remote album library path.<br>
<b>General</b> : New database schema to host more photo and collection informations.<br>
<b>General</b> : Database interface fully re-written using Qt4 SQL plugin.<br>
<b>General</b> : Support of multiple roots album paths.<br>
<b>General</b> : Physical root albums are managed as real album.<br>
<b>General</b> : New option in Help menu to list all RAW file formats supported.<br>
<b>General</b> : Geolocation of pictures from sidebars is now delegate to KDE4 Marble widget.<br>
<b>General</b> : New option in Help menu to list all main components/libraries used by digiKam.<br>
<b>General</b> : libkdcraw dependency updated to 0.3.0.<br>
<b>General</b> : Raw metadata can be edited, changed, added to TIFF/EP like RAW file formats (require Exiv2 &gt;= 0.18). Currently DNG, NEF, and PEF raw files are supported. More will be added in the future.<br>
<b>General</b> : digiKam can be compiled natively under Microsoft Windows.<br>
<b>General</b> : libkdcraw dependency updated to 0.4.0.<br>
<b>General</b> : libgphoto2 dependency is now optional to be able to compile digiKam under operating systems not supported by GPhoto2.<br><br>

<b>CameraGUI</b> : New design for camera interface.<br>
<b>CameraGUI</b> : New Capture tool.<br>
<b>CameraGUI</b> : New bargraph to display camera media free-space.<br><br>
 
<b>AlbumGUI</b> : Added Thumbbar with Preview mode to easy navigate between pictures.<br>
<b>AlbumGUI</b> : Integration of Simple Text Search tool to left sidebar as Amarok.<br>
<b>AlbumGUI</b> : New advanced Search tools. Re-design of Search backend, based on XML. Re-design of search dialog for a better usability. Searches based on metadata and image properties are now possible.<br>
<b>AlbumGUI</b> : New fuzzy Search tools based on sketch drawing template. Fuzzy searches backend use an Haar wevelet interface. You simply draw a rough sketch of what you want to find and digiKam displays for you a thumbnail view of the best matches.<br>
<b>AlbumGUI</b> : New Search tools based on marble widget to find pictures over a map.<br>
<b>AlbumGUI</b> : New Search tools to find similar images against a reference image.<br>
<b>AlbumGUI</b> : New Search tools to find duplicates images around whole collections.<br><br>
 
<b>ImageEditor</b> : Added Thumbbar to easy navigate between pictures.<br>
<b>ImageEditor</b> : New plugin based on <a href="http://lensfun.berlios.de">LensFun library</a> to correct automaticaly lens aberrations.<br>
<b>ImageEditor</b> : LensDistortion and AntiVignetting are now merged with LensFun plugin.<br>
<b>ImageEditor</b> : All image plugin tool settings provide default buttons to reset values.<br>
<b>ImageEditor</b> : New Raw import tool to handle Raw pictures with customized decoding settings.<br>
<b>ImageEditor</b> : All image plugin dialogs are removed. All tools are embedded in editor window.<br>

<h5>BUGS FIXES:</h5><br>

001 ==&gt; 146864 : Lesser XMP support in digiKam.<br>
002 ==&gt; 145096 : Request: acquire mass storage from printer as from camera. Change menu "Camera" to "Acquire".<br>
003 ==&gt; 134206 : Rethink about: Iptc.Application2.Urgency digiKam Rating.<br>
004 ==&gt; 149966 : Alternative IPTC Keyword Separator (dot notation).<br>
005 ==&gt; 129437 : Album could point to network path. Now it's impossible to view photos from shared network drive.<br>
006 ==&gt; 137694 : Allow album pictures to be stored on network devices.<br>
007 ==&gt; 114682 : About library path.<br>
008 ==&gt; 122516 : Album Path cannot be on Network device (Unmounted).<br>
009 ==&gt; 107871 : Allow multiple album library path.<br>
010 ==&gt; 105645 : Impossible to not copy images in ~/Pictures.<br>
011 ==&gt; 132697 : Metadata list has no scrollbar.<br>
012 ==&gt; 148502 : Show rating in embedded preview / slideshow.<br>
013 ==&gt; 155408 : Thumbbar in the album view.<br>
014 ==&gt; 138290 : GPSSync plugin integration in the side bar.<br>
015 ==&gt; 098651 : Image Plugin filter based on clens.<br>
016 ==&gt; 147426 : Search for non-voted pics.<br>
017 ==&gt; 149555 : Always present search box instead of search by right-clicking and selecting simple or advanced search.<br>
018 ==&gt; 139283 : IPTC Caption comment in search function.<br>
019 ==&gt; 150265 : Avanced search filter is missing search in comment / description.<br>
020 ==&gt; 155735 : Make it possible to search on IPTC-text.<br>
021 ==&gt; 147636 : GUI error in advanced searches: lots of free space.<br>
022 ==&gt; 158866 : Advanced Search on Tags a mess.<br>
023 ==&gt; 149026 : Search including sub-albums.<br>
024 ==&gt; 153070 : Search for image by geo-location.<br>
025 ==&gt; 154764 : Pictures saved into root album folder are not shown.<br>
026 ==&gt; 162678 : digiKam crashed while loading.<br>
027 ==&gt; 104067 : Duplicate image finder should offer more actions on duplicate images found.<br>
028 ==&gt; 107095 : Double image removal: Use trashcan.<br>
029 ==&gt; 112473 : findimages shows only small thumbnails.<br>
030 ==&gt; 150077 : Find Duplicate Images tool quite unusable on many images (a couple of issues).<br>
031 ==&gt; 161858 : Find Duplicate Image fails with Canon Raw Files.<br>
032 ==&gt; 162152 : Batch Duplicate Image Management.<br>
033 ==&gt; 164418 : GPS window zoom possibility.<br>
034 ==&gt; 117287 : Search albums on read only album path.<br>
035 ==&gt; 164600 : No picture in view pane.<br>
036 ==&gt; 164973 : Showfoto crashed at startup.<br>
037 ==&gt; 165275 : build-failure - imageresize.cpp - 'KToolInvocation' has not been declared.<br>
038 ==&gt; 165292 : albumwidgetstack.cpp can't find Phonon/MediaObject.<br>
039 ==&gt; 165341 : Crash when changing histogram channel when welcome page is shown.<br>
040 ==&gt; 165318 : digiKam doesn't start because of SQL.<br>
041 ==&gt; 165342 : Crash when changing album sort modus.<br>
042 ==&gt; 165338 : Right sidebar initally too big.<br>
043 ==&gt; 165280 : Sqlite2 component build failure.<br>
044 ==&gt; 165769 : adjustcurves.cpp - can't find version.h.<br>
045 ==&gt; 166472 : Thumbnail bar gone in image editor when switching back from fullscreen.<br>
046 ==&gt; 166663 : Tags not showing pictures without "Include Tag Subtree".<br>
047 ==&gt; 166616 : Filmstrip mode in View.<br>
048 ==&gt; 165885 : Thumbnails and images are NOT displayed in the main view center pane.<br>
049 ==&gt; 167139 : Crash if Exif.GPSInfo.GPSAltitude is empty.<br>
050 ==&gt; 167168 : Timeline view shows redundant extra sections.<br>
051 ==&gt; 166440 : Removing images from Light Table is not working properly.<br>
052 ==&gt; 166484 : digiKam crashes when changing some settings and using "find similar" after the changes made.<br>
053 ==&gt; 167124 : Timeline view not updated when changing selected time range.<br>
054 ==&gt; 166564 : Display of *already* *created* thumbnails is slow.<br>
055 ==&gt; 166483 : Error in "not enough disk space" error message.<br>
056 ==&gt; 167379 : Image selection for export plugin.<br>
057 ==&gt; 166622 : Confusing Add Images use.<br>
058 ==&gt; 166576 : digiKam quits when running "Rebuild all Thumbnail Images" twice.<br>
059 ==&gt; 167562 : Image Editor Shortcut Keys under Edit redo/undo are missing in GUI.<br>
060 ==&gt; 167561 : Crash when moving albums.<br>
061 ==&gt; 167529 : Image Editor not working correctly after setting "use horizontal thumbbar" option.<br>
062 ==&gt; 167621 : digiKam crashes when trying to remove a tag with the context menu.<br>
063 ==&gt; 165348 : Problems when trying to import KDE3 data.<br>
064 ==&gt; 166424 : Crash when editing Caption with Digikam4 SVN.<br>
065 ==&gt; 166310 : Preview not working in image effect dialogs.<br>
066 ==&gt; 167571 : Unnatural order when removing images from light table.<br>
067 ==&gt; 167139 : Crash if Exif.GPSInfo.GPSAltitude is empty.<br>
068 ==&gt; 167778 : Assigning ratings in image editor via shortcuts or toolbar not working.<br>
069 ==&gt; 168567 : Unable to edit or remove iptc and xmp info.<br>
070 ==&gt; 168846 : crash after playing with tags (seems to be rather a crash in Qt or kdelibs).<br>
071 ==&gt; 166671 : Image filter dialog buttons are much too big.<br>
072 ==&gt; 134486 : Keywords are not written to raw files even though they do embed iptc/exif.<br>
073 ==&gt; 168839 : Digikam crashed after tagging.<br>
074 ==&gt; 167085 : Color selection in sketch search tool looks plain black.<br>
075 ==&gt; 168852 : Crash on profile application.<br>
076 ==&gt; 167867 : Album view is reset by changing settings.<br>
077 ==&gt; 168461 : Info in Properties panel not updated after moving to other image.<br>
078 ==&gt; 169704 : Crash during RAW import.<br>
079 ==&gt; 166437 : Deleting images in Image Editor not working properly.<br>
080 ==&gt; 169814 : Compilation error: no exp2().<br>
081 ==&gt; 165345 : Selecting images from bottom right to top left does not work.<br>
082 ==&gt; 170693 : Enable geotagging of Olympus orf images.<br>
083 ==&gt; 170711 : Write single Keywords instead of Hirachy into IPTC.<br>
084 ==&gt; 168569 : New Album Folders are not detected in displayed in GUI until restart.<br>
085 ==&gt; 165337 : Text overlapping box on welcome page.<br>
086 ==&gt; 171310 : No preview photos shown clicking on imported albums.<br>
087 ==&gt; 171778 : digiKam does not load photos in its database.<br>
088 ==&gt; 171736 : Make transferring of metadata between machines easier.<br>
089 ==&gt; 170929 : Compilation of SVN version fails with gcc 4.3.2 unless debug option is activated.<br>
090 ==&gt; 171989 : Can't set gps photo location on .nef images.<br>
091 ==&gt; 172018 : Initial cursor in the "sharpen" dialog is on the "cancel" button.<br>
092 ==&gt; 172033 : Unable to draw rectangle from right to left.<br>
093 ==&gt; 171953 : Using create "Date-based sub-albums" makes import fail.<br>
094 ==&gt; 118318 : Make (lib)gphoto support a configure flag.<br>
095 ==&gt; 172269 : digiKam hangs for around 20-60 seconds when starting.<br>
096 ==&gt; 172648 : Thumbnail click does not display embedded picture preview.<br>
097 ==&gt; 172650 : No kipi plugins available.<br>
098 ==&gt; 172651 : Send image by email missing.<br>
099 ==&gt; 168005 : Week view broken in timeline.<br>
100 ==&gt; 172295 : digiKam has a hard dependency on Jasper library.<br>
101 ==&gt; 172733 : Crash on Synchronize images with database.<br>
102 ==&gt; 172776 : Combine Edit and Album menus, and other ideas.<br>
103 ==&gt; 172836 : No menu item for lensfun.<br>
104 ==&gt; 172912 : "Slide" becomes "Slideshow" text on button in Main window.<br>
105 ==&gt; 172882 : Setup verbiage patches.<br>
106 ==&gt; 172884 : Welcome/First-Run verbiage patches.<br>
107 ==&gt; 172892 : Tips verbiage changes.<br>
108 ==&gt; 172895 : Setup verbiage patches for slideshow and albumview.<br>
109 ==&gt; 172898 : Verbiage changes for Util-ImageEditor-ImageResize.cpp.<br>
110 ==&gt; 172899 : Verbiage changes for digikamapp.cpp.<br>
111 ==&gt; 167320 : Filmstrip in album view is not synchronized correctly.<br>
112 ==&gt; 172911 : Album File lister does not have an "All Image Files" filter.<br>
113 ==&gt; 167777 : Thumbnails in Album not shown at first start of digiKam.<br>
<div class="legacy-comments">

  <a id="comment-17900"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/381#comment-17900" class="active">niiiice! really cool!
there's</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Thu, 2008-10-30 21:59.</div>
    <div class="content">
     <p>niiiice! really cool!</p>
<p>there's only one thing I dislike at the screenshot and thats the look of the histogram graph. Do you have any plans to apply some sort of anti-alising to make it look more "professional"?</p>
<p>Keep up the good work!</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-17901"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/381#comment-17901" class="active">Screenshot shows beta6 ;)</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Thu, 2008-10-30 22:13.</div>
    <div class="content">
     <p>Screenshot shows beta6 ;)</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-17932"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/381#comment-17932" class="active">right...</a></h3>    <div class="submitted">Submitted by digiKam on Mon, 2008-11-03 14:48.</div>
    <div class="content">
     <p>I have bumped version id just before to take the shot... In fact it's really beta5 code (:=)))</p>
<p>digiKam</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-17904"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/381#comment-17904" class="active">Antialias?</a></h3>    <div class="submitted">Submitted by Fri13 (not verified) on Fri, 2008-10-31 17:18.</div>
    <div class="content">
     <p>The non-smooth result is not because of the graph, but because of the data on the file. If you have JPEG file and it is 8bit, it has 255 bars then on it. Every bar is showing correctly then. You can not twist the important image information with antialias, because it looks "bad" ;-)</p>
<p>When you use 16bit photo, you get even more bars to histrogram and you need to see as real information from it as possible ;-)</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18095"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/381#comment-18095" class="active">Thanks for all your wonderful</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Sat, 2009-01-03 19:54.</div>
    <div class="content">
     <p>Thanks for all your wonderful comments and support! I really appreciate it.</p>
<p>Thanks again!</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-17930"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/381#comment-17930" class="active">Please use option buttons for</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Sun, 2008-11-02 20:21.</div>
    <div class="content">
     <p>Please use option buttons for choosing histogram colours instead of list field.</p>
<p>This would mean one click instead of two - better usability.</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-17902"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/381#comment-17902" class="active">.par image files?</a></h3>    <div class="submitted">Submitted by Johannes Freimann (not verified) on Fri, 2008-10-31 01:22.</div>
    <div class="content">
     <p>Is digiKam able to read .par image files? I get these from photo scanning at my local photoshop store. Normally i use gm convert to convert them into .jpg but it would be nice to open them directly from the dvds.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-17903"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/381#comment-17903" class="active">Please disregard the</a></h3>    <div class="submitted">Submitted by Johannes Freimann (not verified) on Fri, 2008-10-31 14:52.</div>
    <div class="content">
     <p>Please disregard the question, I found the simple solution. In the config dialog one can enter more filetypes than it appears, so I just put par and p00 to p99 into there and it works. Sorry to bother you.</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-17906"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/381#comment-17906" class="active">DNG Converter</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Fri, 2008-10-31 20:36.</div>
    <div class="content">
     <p>Does this beta version come with the stand alone DNG converter?</p>
<p>Thanks,</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-17926"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/381#comment-17926" class="active">Not yet as end user application for beta3</a></h3>    <div class="submitted">Submitted by digiKam on Sun, 2008-11-02 01:52.</div>
    <div class="content">
     <p>I have just set DngConverter plugin as stand alone program in current implementation. The program name is dngconverter and will be available with the next beta release.</p>
<p>digiKam</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-17924"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/381#comment-17924" class="active">Nepomuk</a></h3>    <div class="submitted">Submitted by Raul (not verified) on Sat, 2008-11-01 17:04.</div>
    <div class="content">
     <p>Is there any plans for integrating the rating system with nepomuk? Gwenview is developing it, and it would be nice to be able to share the ratings between digikam and gwenview</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-17925"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/381#comment-17925" class="active">Nepomuk &lt;&gt; DigiKam</a></h3>    <div class="submitted">Submitted by Mikko (not verified) on Sat, 2008-11-01 19:50.</div>
    <div class="content">
     <p>This is my own personal opinion: I dont think that Gwenview is on right tracks currently. It should be simple image viewer and not a photo management application, what is the purpose for DigiKam to exist. Gwenview developers should try to work together with DigiKam and use the metadata information itself for a better photo management, if they really want it.</p>
<p>DigiKam exports the photo ratings and other information as XMP metadata for file itself, so it can be easily readed from any other application, without need to understand a Nepomuk. </p>
<p>I suggest for Gwenview developers to forget the ratings etc information if it does not write all the ratings and comment data to metadata itself, so it is readed by photoshop or lightroom if photo gets opened there. Currently it looks like all the data is stored to nepomuk itself, what is one bad thing.</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-17927"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/381#comment-17927" class="active">Yes, it's planed, but later 0.10.0 final release.</a></h3>    <div class="submitted">Submitted by digiKam on Sun, 2008-11-02 01:58.</div>
    <div class="content">
     <p>Because a lots of new codes have been done in digiKam for KDE4, we need to stabilize current implementations before to play with Nepomuk. Also, the new Database interface from Marcel which is already very stable need to be polished before to be interfaced with Nepomuk. So, it's something planed for 0.11.0 release.</p>
<p>digiKam</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-17928"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/381#comment-17928" class="active">I have got a question:
Now,</a></h3>    <div class="submitted">Submitted by Benni (not verified) on Sun, 2008-11-02 02:24.</div>
    <div class="content">
     <p>I have got a question:</p>
<p>Now, when one selects an "enhancement" tool to edit a picture, the  settings of these tools are shown up sideways on the right.</p>
<p>But way must one still select the tools through the menu? </p>
<p>Why is it not possible, to select the enhancement tools from the toolbar at the right side?</p>
<p>The reason for this proposal is the following:</p>
<p>When editing a raw picture, one must often take many enhancement tools into account. Maybe sharpening, noise, colors, and this repeatedly.</p>
<p>When each plugin would be stored on tabs or on the toolbar on the right side, the user could reach the plugins with one click only.</p>
<p>Since in digikam, all enhancement plugins must be selected from a manu, one first has to make one click at the menu "enhancement" and then at the appropriate plugin.</p>
<p>Say, to edit a raw picture, one needs the use of these tools 10 times (since one must see, how the combined effect looks).<br>
If a user has 50 Photos to edit, this makes, with 2 clicks for each tool 1000 clicks. </p>
<p>In case the tools could be selected not from the menu, but from the toolbar on the right, or with tabs on the right after one click, the user would only have to do 500 clicks. </p>
<p>So this is an important point of usability.</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-17931"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/381#comment-17931" class="active">I have GIF images in my</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Sun, 2008-11-02 20:24.</div>
    <div class="content">
     <p>I have GIF images in my collection, but they don't display. Could this be improved? (dK 0.9.3)</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-17933"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/381#comment-17933" class="active">This depand of Qt...</a></h3>    <div class="submitted">Submitted by digiKam on Mon, 2008-11-03 14:53.</div>
    <div class="content">
     <p>This depand of your Qt3 compiled with GIF support. There is no GIF image loader in digiKam. We use Qt image loader for this format.</p>
<p>Like Gif have been (is always?) a patented format, it's possible that your distro disable Gif support in Qt to prevent any legal problems.</p>
<p>Notes:<br>
1/ PNG replace GIF prefectly. I recommend to switch.<br>
2/ For me GIF is not a photo format (256 color limitation). I considerate this format as fully out of digiKam goal.</p>
<p>digiKam</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-17969"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/381#comment-17969" class="active">macos x build</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Sat, 2008-11-15 04:15.</div>
    <div class="content">
     <p>is there any place, where a precompiled build for macos x can be downloaded?</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-17970"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/381#comment-17970" class="active">Look KDE for MAC project</a></h3>    <div class="submitted">Submitted by digiKam on Sat, 2008-11-15 08:27.</div>
    <div class="content">
     <p>Like for KDE Windows project, there is a <a href="http://mac.kde.org">KDE Mac page</a></p>

<p>I'm sure that digiKam compile under Mac. All have been ported by a KDE core developer in the pass.</p>

         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-17972"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/381#comment-17972" class="active">mac os x</a></h3>    <div class="submitted">Submitted by jonathan (not verified) on Sat, 2008-11-15 17:25.</div>
    <div class="content">
     <p>ok, so what do i need to do? install kde4 mac, then download  digikam sources, and then compile them? how, do i need apple devtools installed?</p>
<p>sorry, im a bit of a noob when it comes to macos, but since there is no decent free photomanagement on mac (since there is no picasa on mac), i thought i would have a look.</p>
<p>thanks in advance</p>
<p>jonathan</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-17973"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/381#comment-17973" class="active">Contact KDE Mac team...</a></h3>    <div class="submitted">Submitted by digiKam on Sat, 2008-11-15 17:41.</div>
    <div class="content">
     <p>&gt;ok, so what do i need to do? install kde4 mac, then download digikam sources, and then compile them?<br>
&gt;how, do i need apple devtools installed?</p>
<p>KDE Mac team do not provide a repository with digiKam pre-compiled (as KDE Windows do).</p>
<p>Note: You can contact KDE MAC team by IRC on irc.freenode.net with chanel #kde-mac</p>
<p>&gt;sorry, im a bit of a noob when it comes to macos, but since there is no decent free photomanagement on mac<br>
&gt;(since there is no picasa on mac), i thought i would have a look.</p>
<p>Same for me. I don't have a Mac to guide you i still try to find a way to install MACOS-X for i386 unde VirtualBox...</p>
<p>digiKam</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18297"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/381#comment-18297" class="active">I suggest for Gwenview</a></h3>    <div class="submitted">Submitted by Youms (not verified) on Tue, 2009-03-03 08:40.</div>
    <div class="content">
     <p>I suggest for Gwenview developers to forget the ratings etc information if it does not write all the ratings and comment data to metadata itself, so it is readed by photoshop or lightroom if photo gets opened there. Currently it looks like all the data is stored to nepomuk itself, what is one bad thing.<br>
regards.</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div></div></div>
</div>
