---
date: "2011-03-30T13:39:00Z"
title: "digiKam Software Collection 2.0.0 beta4 is out..."
author: "digiKam"
description: "Dear all digiKam fans and users! digiKam team is proud to announce the 4th digiKam Software Collection 2.0.0 beta release! With this release, digiKam include"
category: "news"
aliases: "/node/591"

---

<p>Dear all digiKam fans and users!</p>

<p>digiKam team is proud to announce the 4th digiKam Software Collection 2.0.0 beta release!</p>

<p>With this release, digiKam include <b>Group of items feature</b> and <b>MediaWiki export tool</b>.</p>

<a href="http://www.flickr.com/photos/digikam/5564467832/" title="wikimediatool-windows by digiKam team, on Flickr"><img src="http://farm6.static.flickr.com/5062/5564467832_98ec55be99_z.jpg" width="640" height="211" alt="wikimediatool-windows"></a>

<a href="http://www.flickr.com/photos/digikam/5488098135/" title="digiKam-groupitems by digiKam team, on Flickr"><img src="http://farm6.static.flickr.com/5179/5488098135_17e6307452_z.jpg" width="640" height="360" alt="digiKam-groupitems"></a>

<p>digiKam include since 2.0.0 beta3 <b>Color Labels</b> and <b>Pick Labels</b> feature to simplify image cataloging operations in your photograph workflow.</p>

<a href="http://www.flickr.com/photos/digikam/5453984332/" title="digikampicklabel2 by digiKam team, on Flickr"><img src="http://farm6.static.flickr.com/5013/5453984332_bba7f267da_z.jpg" width="640" height="256" alt="digikampicklabel2"></a>

<p>Also, digiKam include since 2.0.0 beta2 the <b>Tags Keyboard Shorcuts</b> feature to simplify tagging operations in your photograph workflow.</p>

<a href="http://www.flickr.com/photos/digikam/5389657319/" title="tagshortcuts2 by digiKam team, on Flickr"><img src="http://farm6.static.flickr.com/5218/5389657319_edb75c8ccb_z.jpg" width="640" height="256" alt="tagshortcuts2"></a>

<p>digiKam software collection 2.0.0 include all Google Summer of Code 2010 projects, as <b>XMP sidecar support</b>, <b>Face Recognition</b>, <b>Image Versioning</b>, and <b>Reverse Geocoding</b>. All this works have been processed during <a href="http://techbase.kde.org/Projects/Digikam/CodingSprint2010">Coding Sprint 2010</a>. You can find a resume of this event <a href="http://www.digikam.org/drupal/node/538">at this page</a>.</p>

<a href="http://www.flickr.com/photos/digikam/5323487542/" title="digikam2.0.0-19 by digiKam team, on Flickr"><img src="http://farm6.static.flickr.com/5008/5323487542_fdea5ef240.jpg" width="500" height="400" alt="digikam2.0.0-19"></a>

<p>This beta release is not yet stable. Do not use yet in production. Please report all bugs to KDE bugzilla following indications <a href="http://www.digikam.org/drupal/support">from this page</a>. The release plan can be seen <a href="http://www.digikam.org/drupal/about/releaseplan">at this url</a>.</p>

<a href="http://www.flickr.com/photos/digikam/5319746295/" title="digikam2.0.0-05 by digiKam team, on Flickr"><img src="http://farm6.static.flickr.com/5130/5319746295_df7bba3672_z.jpg" width="640" height="256" alt="digikam2.0.0-05"></a>

<p>See also <a href="https://projects.kde.org/projects/extragear/graphics/digikam/repository/revisions/a332ec6a1bb3d4207598f3f59badd5ea0c35a78b/raw/NEWS">the list of file closed</a> with this release into KDE bugzilla.</p>

<p>digiKam software collection tarball can be downloaded from <a href="http://sourceforge.net/projects/digikam/files">SourceForge Repository</a></p>

<a href="http://www.flickr.com/photos/digikam/5319746291/" title="digikam2.0.0-04 by digiKam team, on Flickr"><img src="http://farm6.static.flickr.com/5249/5319746291_60bcdc0b28_z.jpg" width="640" height="256" alt="digikam2.0.0-04"></a>

<p>Happy digiKam testing...</p>
<div class="legacy-comments">

  <a id="comment-19869"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/591#comment-19869" class="active">Great job!</a></h3>    <div class="submitted">Submitted by <a href="http://inigoalonso.com/" rel="nofollow">Iñigo Alonso</a> (not verified) on Wed, 2011-03-30 14:40.</div>
    <div class="content">
     <p>This new release is coming along nicely, thank you very much to all the involved ;)</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-19870"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/591#comment-19870" class="active">Face detection works nicely,</a></h3>    <div class="submitted">Submitted by uetsah (not verified) on Wed, 2011-03-30 16:29.</div>
    <div class="content">
     <p>Face detection works nicely, but I don't see any face recognition happening anywhere, even after manually associating lots of "Unknown" faces with people tags. Or am I just looking in the wrong place in the UI?</p>
<p>Btw, I found it very confusing that the list of detected but unknown faces is tied to a pre-created "People/Unknown" tag. Not only is it confusing (because, logically, a detected face doesn't really have anything to do with tags, until it is actually tagged by the user). And not only is it confusing, but also:<br>
 1) being a tag like any other. it also shows up in the normal tag view, but it's always empty there, so it's just additional UI clutter...<br>
 2) when you accidentally delete it, there doesn't seem to be a way to get it back (manually re-creating a tag with the same name doesn't seem to work)...<br>
I think it would be much more logical to have a dedicated hard-coded "unknown faces" list in the People view, without "abusing" a tag for this.</p>
<p>Otherwise: Nice work!</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19871"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/591#comment-19871" class="active">please report on Bugzilla...</a></h3>    <div class="submitted">Submitted by digiKam on Wed, 2011-03-30 17:30.</div>
    <div class="content">
     <p>http://www.digikam.org/support</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19873"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/591#comment-19873" class="active">So does this mean that face</a></h3>    <div class="submitted">Submitted by uetsah (not verified) on Wed, 2011-03-30 18:21.</div>
    <div class="content">
     <p>So does this mean that face recognition is actually supposed to work in this beta, and I'm merely encountering some kind of bug?</p>
<p>How exactly is it supposed to work?<br>
Is it supposed to just tag faces it thinks it recognizes, as if the user himself had tagged them?<br>
Or is it supposed to propose suggestions for individual faces so that you can accept/deny them with a single click (like in Picasa)? If so, where (in which view)?<br>
I couldn't observe either, so far.</p>
<p>That's why I didn't go straight to the bug tracker, because I don't even know what the expected behavior should be... ;-)</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div><a id="comment-19872"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/591#comment-19872" class="active">face tagging with example pictures</a></h3>    <div class="submitted">Submitted by peter (not verified) on Wed, 2011-03-30 18:05.</div>
    <div class="content">
     <p>It would be nice, to show each person-tag with an (choosable) example face-picture. see how it is done in picasa. that would be in the 4th screenshot on the left a foto-icon instead of the icon, and also in the context menu on the bottom right. perhaps the icons could then be a little larger.</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-19874"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/591#comment-19874" class="active">Version for Windows?</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Thu, 2011-03-31 07:17.</div>
    <div class="content">
     <p>Thanks again for this wonderful application !!!</p>
<p>I saw that one screenshot was made on a windows machine. Is there any hope to get an installer for digiKam on windows? The KDE Windows installer is not yet updated and I would really like to test the new features on windows to give some feedback.</p>
<p>Thanks,<br>
 Daniel</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19884"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/591#comment-19884" class="active">Windows and Mac?? PLEASE!</a></h3>    <div class="submitted">Submitted by GZ (not verified) on Fri, 2011-04-29 18:04.</div>
    <div class="content">
     <p>I know digikam on Linux, and it's a killer application. </p>
<p>But if you had that on Windows (Win XP and Win 7 64 bit) and Mac OSX (without the awful X environemnt that Gimp for Mac needs) it will be a real killer for several commercial products and the majority of shareware/opensource/freeware products. I know most applications in this business, believe me, this wonderful program would become number one within months.</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-19875"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/591#comment-19875" class="active">Grouping, Versioning and XMP Sidecars</a></h3>    <div class="submitted">Submitted by Jeff (not verified) on Wed, 2011-04-06 13:41.</div>
    <div class="content">
     <p>The grouping, versioning and XMP sidecar support are features that I am really looking forward to. I have a quick question about how they would fit into my typical workflow and I hope one of the developers could help me understand how those features will work for me.</p>
<p>Most cameras that support a raw format also support RAW+JPEG shooting, where two files are created for each shot. I often shoot RAW+JPEG when conditions are difficult. Most of the time, the JPEG is good enough, but it is handy to have the RAW file available for some adjustments in post processing. Shooting the two together saves conversion time and gives me two files with the same name and different file extensions. To all intents and purposes, these are the same image, so tags that apply to one will apply equally to the other.</p>
<p>What will happen with the XMP sidecar support in this case? As the images are tagged individually, if I add tags A, B and C to the RAW file and the XMP sidecar is updated, will these tags appear for the JPEG file, or do I need to apply them again to that file to have them reflected in the database? What if I apply tags A and B to the JPEG file and forget to add tag C, will the sidecar be overwritten and the C tag (added for the RAW image) be removed?</p>
<p>Will the grouping feature automatically group my RAW and JPEG images into one, or do I have to do that manually? The files have the same name, but different extensions.</p>
<p>Say I modify a RAW file and a new version is created in JPEG format. Now I make a modification to the original companion JPEG image. Will the version of the JPEG image overwrite the version of the RAW image (saved in JPEG format), as the files will probably have the same name (Xv1.jpg, or something like that)?</p>
<p>It is little details like this that will make or break these features for me. I haven't been able to find any information about these use cases anywhere.</p>
<p>Looking forward to 2.0,<br>
Jeff.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19876"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/591#comment-19876" class="active">No need to reply</a></h3>    <div class="submitted">Submitted by Jeff (not verified) on Mon, 2011-04-11 21:13.</div>
    <div class="content">
     <p>I see from some of the more active bug reports that the developers never properly considered any of these possibilities. Never mind, I'll wait for some sort of usable implementation in 3.0.</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-19889"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/591#comment-19889" class="active">Good</a></h3>    <div class="submitted">Submitted by <a href="http://www.batikentevdenevenakliyat.com" rel="nofollow">nakliyat ankara</a> (not verified) on Tue, 2011-05-10 13:58.</div>
    <div class="content">
     <p>this paper is accurate to be useful. Thanks to the author.</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-19883"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/591#comment-19883" class="active">Great Product</a></h3>    <div class="submitted">Submitted by <a href="http://www.webtaming/Blog/" rel="nofollow">Joanna</a> (not verified) on Tue, 2011-04-26 15:23.</div>
    <div class="content">
     <p>Thank you for spending so much time creating this 'must-have' for all web designers and even budding photographers.  I was pointed in your direction by someone who just 'happened' upon your site.  </p>
<p>Since I am a full-time web designer and often need tools to help me cut down on the time that I spend manipulating photos, I was absolutely delighted to try your comprehensive software.</p>
<p>I was very pleasantly surprised, firstly that it was so very flexible and interactive.  Secondly, that you have not attached a HUGE price tag to such a great product.</p>
<p>To make a long story short, I so impressed today that I dedicated my blog to your efforts at<br>
http://wp.me/p1vFKo-27.</p>
<p>I am very proud to give all bragging rights to digiKam today!  Thank you.</p>
<p>Joanna (Joanna's Blog)</p>
         </div>
    <div class="links">» </div>
  </div>

</div>
