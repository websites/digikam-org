---
date: "2006-12-11T18:07:00Z"
title: "digiKam and digiKamPlugins RC2 released !"
author: "fabien"
description: "The digiKam team is proud to announce the second release candidate of digiKam 0.9.0 and digiKamimageplugins 0.9.0. The 0.9 series has been under heavy development"
category: "news"
aliases: "/node/200"

---

<p>The digiKam team is proud to announce the second release candidate of digiKam 0.9.0 and digiKamimageplugins 0.9.0.</p>
<p>The 0.9 series has been under heavy development since November 2005. After having released three beta versions and a first release candidate, we are now entering the last phase of fixing critical bugs and testing.</p>
<p>So, download, compile and try it ! Help us to make the final release a very reliable version !</p>
<p>The source for digiKam and DigikamImagePlugins 0.9.0-rc1 is available <a href="http://sourceforge.net/project/showfiles.php?group_id=42641">at this url</a></p>
<p>All digiKam 0.9-rc1 features can be seen <a href="http://www.digikam.org/?q=about/features09x">at this url</a></p>
<p>New features and bug fixes since rc1 are listed below:</p>
<ul>
<li>General: Exiv2 dependency fixed to 0.12 release</li>
<li>General: Fix broken compilation using "./configure -enable-final" option</li>
<li>ImageEditor: Fixed and improved Color Management View workflow</li>
<li>115125 : remaining endianess issue on PowerPC with White Balance tool</li>
<li>137495 : showfoto crashes when doing any modification to a loaded directory</li>
<li>137845 : Album Header cut off (squeezed) when entering two or more lines in Album comment</li>
<li>137886 : When a tag is moved in the left panel it's position is not updated in the right panel</li>
<li>135834 : Lowercasing camera filenames only works for first imported file/li&gt;
</li><li>134391 : digiKam camera gui dialog crashes if there is a filename without extension/li&gt;
</li><li>131947 : digiKam complains about invalid ICC profiles path</li>
<li>130176 : Typos in digiKam.po and one plugin</li>
<li>138252 : Display is not updates when switching color managed view on/off</li>
<li>138253 : Keyboard shortcut for turning color managed display on/off</li>
</ul>

<div class="legacy-comments">

</div>