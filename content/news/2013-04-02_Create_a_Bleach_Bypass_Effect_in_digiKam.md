---
date: "2013-04-02T09:43:00Z"
title: "Create a Bleach Bypass Effect in digiKam"
author: "Dmitri Popov"
description: "Using tools available in digiKam, you can simulate a number of effects, including bleach bypass. This effect produces a desaturated high-contrast image. Creating the bleach"
category: "news"
aliases: "/node/689"

---

<p>Using tools available in digiKam, you can simulate a number of effects, including bleach bypass. This effect produces a desaturated high-contrast image. Creating the bleach bypass effect in digiKam is done in two simple steps.</p>
<p><img src="http://scribblesandsnaps.files.wordpress.com/2013/02/digikam-bleachbypass.png?w=500" width="500" height="400"></p>
<p><a href="http://scribblesandsnaps.com/2013/04/02/create-bleach-bypass-effect-in-digikam/">Continue to read</a></p>

<div class="legacy-comments">

</div>