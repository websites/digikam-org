---
date: "2013-08-06T09:07:00Z"
title: "digiKam Software Collection 3.3.0 released..."
author: "digiKam"
description: "Dear all digiKam fans and users! digiKam team is proud to announce the release of digiKam Software Collection 3.3.0. This version include a new core"
category: "news"
aliases: "/node/701"

---

<a href="http://www.flickr.com/photos/digikam/9450640994/" title="digiKam3.3.0 by digiKam team, on Flickr"><img src="http://farm6.staticflickr.com/5457/9450640994_192af36e0b_z.jpg" width="640" height="512" alt="digiKam3.3.0"></a>

<p>Dear all digiKam fans and users!</p>

<p>digiKam team is proud to announce the release of digiKam Software Collection 3.3.0.
This version include a new core implementation to manage faces, especially face recognition feature which have never been completed with previous release. <a href="http://en.wikipedia.org/wiki/Face_detection">Face detection</a> feature still always here and work as expected.</p>

<p><a href="https://plus.google.com/113704327590506304403/posts">Mahesh Hegde</a> who has work on <a href="http://en.wikipedia.org/wiki/Facial_recognition_system">Face Recognition</a> implementation has published a proof of concept demo on YouTube.</p>

<iframe width="560" height="315" src="http://www.youtube.com/embed/iaFGy0n0R-g" frameborder="0" allowfullscreen=""></iframe>

<p>At the same time, another student who have been selected to GSoC 2013 has improved the <a href="http://community.kde.org/Digikam/GSoC2013#Read_face_from_Picasa_metadata_to_populate_digiKam_database">Face Tags interoperability</a> with <a href="http://picasa.google.com/intl/en">Picasa photo manager</a>.</p>


<p>See <a href="https://bugs.kde.org/buglist.cgi?f1=cf_versionfixedin&amp;o1=equals&amp;query_format=advanced&amp;bug_status=RESOLVED&amp;bug_status=NEEDSINFO&amp;bug_status=VERIFIED&amp;bug_status=CLOSED&amp;v1=3.3.0&amp;product=digikam&amp;product=digikamimageplugins&amp;product=kipiplugins&amp;product=showfoto">the list of files closed</a> with this release into KDE bugzilla.</p>

<p>digiKam software collection tarball can be downloaded from <a href="http://download.kde.org/stable/digikam/digikam-3.3.0.tar.bz2.mirrorlist">KDE repository</a></p>

<p>Next main release 3.4.0 will be a bugfix release, as we are currently busy to mentor <a href="http://community.kde.org/Digikam/GSoC2013#digiKam_Google_Summer_of_Code_2013_Projects_list">all GSoC 2013 projects</a>. Look in Release plan <a href="http://www.digikam.org/about/releaseplan">for details...</a></p>

<p>Happy digiKam</p>
<div class="legacy-comments">

  <a id="comment-20602"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/701#comment-20602" class="active">Kipi</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Tue, 2013-08-06 11:20.</div>
    <div class="content">
     <p>Any info about KIPI plugins? Roadmap with changes etc. ? :)</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20603"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/701#comment-20603" class="active">kipi is included to digiKam now...</a></h3>    <div class="submitted">Submitted by digiKam on Tue, 2013-08-06 11:34.</div>
    <div class="content">
     <p>...and list of bugzilla entries fixed are listed with link given on announcement.</p>
<p>You can always check NEW file from kipi for details :</p>
<p>https://projects.kde.org/projects/extragear/graphics/kipi-plugins/repository/revisions/d032ccaa7524e2866571793097d755a498092f1a/entry/project/NEWS.3.3.0</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-20604"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/701#comment-20604" class="active">digikam 3.3.0 windows installer</a></h3>    <div class="submitted">Submitted by Günther (not verified) on Tue, 2013-08-06 12:32.</div>
    <div class="content">
     <p>When do you provide a windows installer version for 3.3.0?</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-20606"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/701#comment-20606" class="active">Thank you...</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Thu, 2013-08-08 08:16.</div>
    <div class="content">
     <p>... for the great work! I am very excited to see the new face recognition feature!<br>
Thank you very much.</p>
<p>Greetings, Malte.</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-20607"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/701#comment-20607" class="active">When do you provide a windows</a></h3>    <div class="submitted">Submitted by wQuick (not verified) on Thu, 2013-08-08 14:13.</div>
    <div class="content">
     <p>When do you provide a windows installer version for 3.3.0?</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-20608"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/701#comment-20608" class="active">DigiKam 3.3 frozen right after launch</a></h3>    <div class="submitted">Submitted by Tomas (not verified) on Tue, 2013-08-13 23:35.</div>
    <div class="content">
     <p>Hi guys, more than happy to hear DigiKam 3.3 is out with Face Recognition now. Unfortunatelly, DigiKam 3.3 freezes for me right after it is launched (application is available with first few photos and then it stops responding and looses its the window content later, consuming 1 CPU core since the beginning). Is it possible to workaround somehow?</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20619"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/701#comment-20619" class="active">try this:</a></h3>    <div class="submitted">Submitted by Romain (not verified) on Mon, 2013-08-26 01:40.</div>
    <div class="content">
     <p>try this: <a href="https://bbs.archlinux.org/viewtopic.php?id=168429">https://bbs.archlinux.org/viewtopic.php?id=168429</a></p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20620"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/701#comment-20620" class="active">it helped!</a></h3>    <div class="submitted">Submitted by Tomas (not verified) on Thu, 2013-08-29 22:04.</div>
    <div class="content">
     <p>Many thanks :-)</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div><a id="comment-20609"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/701#comment-20609" class="active">Support "older" systems?</a></h3>    <div class="submitted">Submitted by <a href="http://spiele.j-crew.de/" rel="nofollow">Thomas Bleher</a> (not verified) on Wed, 2013-08-14 21:31.</div>
    <div class="content">
     <p>I wanted to try digikam 3.3.0, but the current Ubuntu stable release (13.04 aka Raring) is apparently already too old. CMake aborts because libkface wants OpenCV 2.4.5, while Ubuntu "only" has 2.4.2. From the Git log I see that apparently the new face recognition really needs the new version. Still, this makes it really hard to test and contribute to Digikam, when even a four month old distribution is already too old to run the software. Maybe the library requirements can be made such that the current stable releases of major distributions can run the software without additional installs? That would be really great!<br>
Anyway, enough ranting, thanks for producing this great software!</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20610"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/701#comment-20610" class="active">Kubuntu 12.10 user</a></h3>    <div class="submitted">Submitted by Tomas (not verified) on Thu, 2013-08-15 09:25.</div>
    <div class="content">
     <p>http://linuxg.net/how-to-install-digikam-3-3-0-on-ubuntu-linux-mint-debian-and-derivates/</p>
<p>Damn, this captcha is hard.</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-20611"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/701#comment-20611" class="active">Missing albums in Ubuntu 12.04</a></h3>    <div class="submitted">Submitted by Tom Rank (not verified) on Mon, 2013-08-19 12:37.</div>
    <div class="content">
     <p>I've upgraded to this version successfully and it looks fine. BUT all my albums are now shown as having 0 images. The photos are still there, but I can't use digiKam to access them. The nearest to an answer I can find online is this:<br>
"It looked like Python was not up to date and update was failing for some dependency problem. After finding out how to succesfully update Python all is back on track."<br>
http://digikam.1695700.n4.nabble.com/Lost-photos-td4655288.html</p>
<p>However, the post doesn't help with updating Python. I've checked Ubuntu 12.04, which seems to use Python 2.7, so I've now updated to Python 3.2, but I'm not sure that this is being used as default. </p>
<p>Can you help, or send me somewhere for an answer, please? I love using digiKam so this is very frustrating!</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20612"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/701#comment-20612" class="active">Missing albums issue solved!</a></h3>    <div class="submitted">Submitted by Tom Rank (not verified) on Mon, 2013-08-19 13:03.</div>
    <div class="content">
     <p>Sorry - have now solved this by re-booting the system! Should have been obvious, I know, but as digiKam 3.3.0 ran without complaint I'd not thought I needed to re-boot. </p>
<p>Silly me! anyway, thanks for the upgrade: I look forward to exploring the new features.</p>
<p>Tom</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-20618"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/701#comment-20618" class="active">Is a Debian version coming</a></h3>    <div class="submitted">Submitted by Robin (not verified) on Thu, 2013-08-22 23:37.</div>
    <div class="content">
     <p>Is a Debian version coming too?  It's sad to see such a great improvement and have only v. 3.1 available.</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-20625"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/701#comment-20625" class="active">Thank you</a></h3>    <div class="submitted">Submitted by heron (not verified) on Fri, 2013-09-06 15:20.</div>
    <div class="content">
     <p>This is a very good piece of software! :) A real killer application for linux and kde</p>
         </div>
    <div class="links">» </div>
  </div>

</div>
