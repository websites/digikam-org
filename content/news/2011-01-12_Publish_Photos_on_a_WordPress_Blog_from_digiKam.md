---
date: "2011-01-12T09:55:00Z"
title: "Publish Photos on a WordPress Blog from digiKam"
author: "Dmitri Popov"
description: "While digiKam doesn’t allow you to publish photos directly to a WordPress blog, you can work around this limitation. Enable the Post by Email feature"
category: "news"
aliases: "/node/563"

---

<p>While digiKam doesn’t allow you to publish photos directly to a WordPress blog, you can work around this limitation. Enable the Post by Email feature on your WordPress blog, and you can use the SendImages Kipi plugin in digiKam to email photos directly to your blog. <a href="http://scribblesandsnaps.wordpress.com/2011/01/12/publish-photos-on-a-wordpress-blog-from-digikam/">Continue to read</a></p>

<div class="legacy-comments">

</div>