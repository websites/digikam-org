---
date: "2017-04-30T22:27:50"
title: "The new digiKam web presence"
author: "Mica Semrick"
description: "digiKam gets a brand new static website."
category: "news"
---

Notice something a bit different? 

So do we!

The folks from the [PIXLS.US](https://pixls.us) community saw that digiKam needed an update to the old website (previously running on an older Drupal instance) for security reasons and they wanted to help out.
So after several weeks of hard work, the digiKam team has deployed a new look-and-feel for the website that is easier to navigate and mobile-friendly.
Don't worry, all the content from the old website has been preserved so all the old links should be fine.  You can find the links to this content in the footer of the new site.

There are still things in the new design that need a bit of polish, but none of these things are important enough to keep the new site from our users.

Here is a summary of changes:

- replaced Drupal 6 with [Hugo](https://gohugo.io)
- posts and pages are now authored in markdown
- website source and content stored in git and hosted on KDE's infrastructure
- continuous integration builds and deploys the site on new commits

You can find out more about the new site on the [meta](/documentation/meta/) and [writing](/documentation/meta/writing/) pages.

You also open issues for the website using [digiKam bugzilla ][] or by sending email to the [mailing list][].

We hope you enjoy the new site!

<!-- pld -->
[digiKam bugzilla ]: https://bugs.kde.org/buglist.cgi?component=Website&product=digikam&resolution=---
[mailing list]: https://mail.kde.org/mailman/listinfo/digikam-users
