---
date: "2013-09-29T17:43:00Z"
title: "digiKam Software Collection 3.3.0 and 3.4.0 for Windows Released"
author: "Ananta Palani"
description: "I am pleased to announce the release of digiKam 3.3.0 and 3.4.0 for Windows built against KDE 4.10.2 that can be downloaded from the KDE"
category: "news"
aliases: "/node/705"

---

<p><img src="http://lh5.googleusercontent.com/-ZQoHDqhopcw/UkNZO8KNe8I/AAAAAAAABeU/Tqzod0tekSo/w1002-h564-no/digikamWindows3.4.0.png" width="640" height="361"></p>
<p>I am pleased to announce the release of digiKam <a href="http://download.kde.org/stable/digikam/digiKam-installer-3.3.0-win32.exe">3.3.0</a> and <a href="http://download.kde.org/stable/digikam/digiKam-installer-3.4.0-win32.exe">3.4.0</a> for Windows built against KDE 4.10.2 that can be downloaded from the <a href="http://download.kde.org/stable/digikam/">KDE repository</a>. The build platform is the same as for 3.1.0 and 3.0.0. If you encounter any problems please file a very detailed <a href="https://bugs.kde.org/enter_bug.cgi?product=digikam&amp;op_sys=MS%20Windows&amp;rep_platform=MS%20Windows&amp;version=3.4.0&amp;format=guided">bug report</a>.</p>
<p>Bug fixes include those completed for the digiKam 3.3.0 and 3.4.0 source releases.</p>
<p>Known Windows problems:</p>
<p>1. After rotating an image using the rotate icons in the thumbnail view, the user may have to press F5 to see the change<br>
2. Bugs listed on <a href="https://bugs.kde.org/buglist.cgi?bug_status=UNCONFIRMED&amp;bug_status=CONFIRMED&amp;bug_status=ASSIGNED&amp;bug_status=REOPENED&amp;list_id=676982&amp;op_sys=MS%20Windows&amp;product=digikam&amp;product=digikamimageplugins&amp;product=showfoto&amp;query_format=advanced&amp;query_based_on=&amp;columnlist=bug_severity%2Cpriority%2Copendate%2Cbug_status%2Cresolution%2Ccomponent%2Cop_sys%2Crep_platform%2Cshort_desc">KDE Bugzilla</a></p>
<p>Enjoy!</p>

<div class="legacy-comments">

  <a id="comment-20638"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/705#comment-20638" class="active">Thanks!</a></h3>    <div class="submitted">Submitted by Calle (not verified) on Sun, 2013-09-29 21:55.</div>
    <div class="content">
     <p>Thank you very much, Ananta! It's great that DigiKam is shown a little Windows love! Much appreciated!</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20639"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/705#comment-20639" class="active">Yes, thank you very much for</a></h3>    <div class="submitted">Submitted by Mori (not verified) on Mon, 2013-09-30 10:30.</div>
    <div class="content">
     <p>Yes, thank you very much for the Windows release :)</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-20640"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/705#comment-20640" class="active">Beta? Alpha?</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Mon, 2013-09-30 20:44.</div>
    <div class="content">
     <p>Might it be that 3.4.0 is up to now only a beta or alpha release? It keeps crashing while face recognition and confirmation of tags.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20642"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/705#comment-20642" class="active">Pressing Enter or clicking Check?</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Tue, 2013-10-01 05:28.</div>
    <div class="content">
     <p>Are you pressing Enter to confirm the name or clicking the Check below the name? Clicking the check sometimes crashes digikam for me, but pressing the enter key never does.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20643"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/705#comment-20643" class="active">Crash</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Wed, 2013-10-02 12:10.</div>
    <div class="content">
     <p>I does not only crash during confirmation but also during searching for matches/face recognition.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20646"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/705#comment-20646" class="active">digiKam 3.4 Windows crashes</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Sat, 2013-10-12 16:11.</div>
    <div class="content">
     <p>I have same problems. Uninstalling and reinstalling digiKam does not help. :-((</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div></div><a id="comment-20641"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/705#comment-20641" class="active">¡Genial!</a></h3>    <div class="submitted">Submitted by jhduarte (not verified) on Mon, 2013-09-30 23:19.</div>
    <div class="content">
     <p>Gracias por liberar esta versión...desde hace buen rato la extrañaba.<br>
Tan pronto termine la descarga, actualizo.<br>
Un saludo<br>
:D</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-20656"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/705#comment-20656" class="active">digiKam 3.5 ?</a></h3>    <div class="submitted">Submitted by Dirk Michelsen (not verified) on Fri, 2013-11-22 14:02.</div>
    <div class="content">
     <p>Hello Ananta, could you do the same trick for digiKam 3.5?</p>
<p>Best wishes and many thanks in advance<br>
Dirk</p>
         </div>
    <div class="links">» </div>
  </div>

</div>