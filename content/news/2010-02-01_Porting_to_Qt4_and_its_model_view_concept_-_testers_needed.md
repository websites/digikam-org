---
date: "2010-02-01T12:52:00Z"
title: "Porting to Qt4 and its model view concept - testers needed"
author: "languitar"
description: "During the last two months Marcel and I ported all tree views in digiKam from Qt3 to Qt4 and its model view concept. These changes"
category: "news"
aliases: "/node/498"

---

<a href="http://www.flickr.com/photos/digikam/4321468889/" title="digiKam1.2.0 by digiKam team, on Flickr"><img src="http://farm5.static.flickr.com/4017/4321468889_199fa9123f.jpg" width="500" height="400" alt="digiKam1.2.0"></a>

<p>During the last two months Marcel and I ported all tree views in digiKam from Qt3 to Qt4 and its model view concept. These changes are now included in the svn trunk. The new code still needs some serious testing and we would appreciate your help on this.</p>

<p>Some parts of the current digiKam release still rely on old Qt3 support classes. Most prominent are the various tree views in the main view, e.g. for selecting an album to show in the central icon view or filtering by tag. My job since the coding sprint was to port these tree views to Qt4 using the model view concept introduced there. Marcel already wrote the models some time ago so that I could start my work on a solid foundation. This work was done in a separate feature branch for the last two months. Now, after the 1.1.0 release, this branch is merged back into trunk and ready to be included in the next release of digiKam.</p>

<p>But as usual with GUI changes this can introduce some regressions as user interfaces are hard to test automatically. Therefore we would really like you to test the current trunk code including these changes before releasing the next version of digiKam. Try to find as many bugs as you can that we have introduced here. ;)</p>

<p>If you are not yet familiar on how to test current development code, <a href="http://www.digikam.org/drupal/download/KDE4">this page</a> gives you a short introduction how to get and compile digiKam from source. For reporting bugs please use the normal <a href="https://bugs.kde.org/">KDE bug tracker</a>.</p>


<div class="legacy-comments">

  <a id="comment-19013"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/498#comment-19013" class="active">Filter by tags, album, time, etc. all at the same time?</a></h3>    <div class="submitted">Submitted by <a href="http://jakob.malms.org" rel="nofollow">Jakob Malm</a> (not verified) on Tue, 2010-02-02 14:40.</div>
    <div class="content">
     <p>Is it possible to filter on tags, album, time, etc. all at the same time, using the tree views? E.g., say I would like to find all photos of Joe, taken December 2009, in Istanbul. I would then first select the tag "Joe", then limit to December 2009 using the calendar, and then refine again with the map tool, to limit to Istanbul.</p>
<p>I know I can "search" for all of these at the same time, but it would be nice to also be able to do the process step by step, e.g. if I'm looking for one photo among many photos, but don't know at the start exactly which search will find the photo.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19015"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/498#comment-19015" class="active">The search</a></h3>    <div class="submitted">Submitted by Fri13 on Tue, 2010-02-02 17:15.</div>
    <div class="content">
     <p>Thats why the advanced search was added that if you need to use very specific search classes you can do so. The filter bar is just for filtering current view and not for searching. And thats why we have the sidebars where we can enable some filtering as well fast. Like first the date from left and then the tags from right and then rating from bottom. </p>
<p>And if wanted, you can always just testrun the search.</p>
<p>I myself believe that advanced search should be think again if it could be implented without own window (or if there is need for such reimplentation!).</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19017"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/498#comment-19017" class="active">Search is not bad, but...</a></h3>    <div class="submitted">Submitted by Stefan (not verified) on Tue, 2010-02-02 18:00.</div>
    <div class="content">
     <p>Advanced search is not bad. It could be improved.</p>
<p>However what he is talking about is better solution.</p>
<p>For example if you want to find photos that were taken 2008 in Istanbul, by Jon Doe and/or Jane Doe, rated 3 or 5 stars maybe, tags: wedding, rain, size 1280, 1024, black and white, with faces (face recognition filter). All of this criteria could be used in search, virtual folders ("smart albums"), for filtering current view.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19018"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/498#comment-19018" class="active">To get this going it would be</a></h3>    <div class="submitted">Submitted by languitar on Tue, 2010-02-02 21:03.</div>
    <div class="content">
     <p>To get this going it would be good if you create a wish in the kde bug tracker.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19020"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/498#comment-19020" class="active">Yes, I know. I haven't found</a></h3>    <div class="submitted">Submitted by Stefan (not verified) on Tue, 2010-02-02 21:16.</div>
    <div class="content">
     <p>Yes, I know. I haven't found time to do it yet. Plus I have a lot of wishes. Up to digikam 2.0. :)))</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-19023"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/498#comment-19023" class="active">But I can already make</a></h3>    <div class="submitted">Submitted by Fri13 on Wed, 2010-02-03 17:06.</div>
    <div class="content">
     <p>But I can already make "virtual folders" (on search panel) with my wanted search criteria. I do not want albums view to have entries what does not exist in real. That is one great thing on digiKam that albums are real folders and not virtual.</p>
<p>And filtering should always filter only the current tool. Like you can filter albums in the album view. Then filter tags on tag view. Rating on the thumbnail view what depends from the other views what you have included. </p>
<p>For current thumbnail view I dont know is there need to have a one shown super-mega-filter panel to search with keywords from everywhere because it throws away many other great ideas.</p>
<p>Right now the user does not need to remember all the information but can just search them itself if wanted and make wanted filters. This helps a lot because you can see the metadata library easily and still being able to filter that to build own kind searches ("virtual folders").</p>
<p>Now the stuff what is missing from that wish is face recognizing.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19025"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/498#comment-19025" class="active">"But I can already make</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Wed, 2010-02-03 20:02.</div>
    <div class="content">
     <p>"But I can already make "virtual folders" (on search panel) with my wanted search criteria. I do not want albums view to have entries what does not exist in real. That is one great thing on digiKam that albums are real folders and not virtual."</p>
<p>Yes. I wasn't proposing anything. </p>
<p>"And filtering should always filter only the current tool. Like you can filter albums in the album view. Then filter tags on tag view. Rating on the thumbnail view what depends from the other views what you have included."</p>
<p>Yes, and no. Search is very good, but I think that sometimes using filters is faster. You don't have to launch search and set criteria, with filters you can just select... Maybe I am wrong, but to me it's better when you want to quickly find something. Different situations - different solutions... </p>
<p>Currently if you want to see images rated with 5 stars you just click on 5th star on status bar, but it would be nice if you could do that with other criteria. This solution is used in one very popular Win application.</p>
<p>Off-course there are other solutions, for some problems that I have currently with it...</p>
<p>"Now the stuff what is missing from that wish is face recognizing."</p>
<p>And "color filter".</p>
<p>////////</p>
<p>And as I said, I am not proposing anything. I still don't have clear vision how it should work or look, particularly if you add face recognition and 'color filter'...</p>
<p>//////////////</p>
<p>Tag Filters could be improved. It should show only tags that can be used. Currently if you select tag in Tag Filters and there is no image with that tag in currently selected album you get no results. Or maybe I am missing something.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19050"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/498#comment-19050" class="active">Bibble 5 has a really neat</a></h3>    <div class="submitted">Submitted by Dan Welch (not verified) on Tue, 2010-02-16 13:53.</div>
    <div class="content">
     <p>Bibble 5 has a really neat approach to this. You choose a filter, then click to lock it, and go on to the next filter, and so on - it's intuitive and feels better than a separate search function.</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div></div></div></div><a id="comment-19070"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/498#comment-19070" class="active">Nifty little program</a></h3>    <div class="submitted">Submitted by <a href="http://www.jeffkolodnyphotography.com/" rel="nofollow">Miami Wedding Photographer</a> (not verified) on Sat, 2010-03-06 12:12.</div>
    <div class="content">
     <p>Looks like a better alternative to Lightroom and Photoshop.</p>
         </div>
    <div class="links">» </div>
  </div>

</div>
