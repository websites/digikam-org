---
date: "2010-12-24T07:50:00Z"
title: "digiKam and Kipi-plugins 1.7 windows installer is available"
author: "julien"
description: "digiKam team is proud to announce that a new windows installer for digiKam software collection is available. You can download it from here This installer"
category: "news"
aliases: "/node/559"

---

<a href="http://www.flickr.com/photos/digikam/5162506984/" title="digiKam-wininstaller01 by digiKam team, on Flickr"><img src="http://farm2.static.flickr.com/1085/5162506984_705dd61ef6_z.jpg" width="640" height="360" alt="digiKam-wininstaller01"></a>

<p>digiKam team is proud to announce that a new windows installer for digiKam software collection is available. You can download it from <a href="http://sourceforge.net/projects/digikam/files/digikam/1.7.0/digiKam-installer-1.7-win32.exe/download">here</a></p>

<p>This installer includes a copy of all the dependencies of digiKam and kipi-plugins including Qt 4.6.2, KDE 4.4.4, exiv2 0.21, libkdcraw 1.3.0, libraw 0.12.1, etc...</p> 

<a href="http://www.flickr.com/photos/digikam/5162534446/" title="digiKam-wininstaller07 by digiKam team, on Flickr"><img src="http://farm5.static.flickr.com/4011/5162534446_9101be01a4.jpg" width="500" height="281" alt="digiKam-wininstaller07"></a>

<p>Unfortunately, this port of digiKam under windows is not yet as stable as the linux version.
digiKam works mostly fine under windows, but some features are still broken due to some bugs in the underlying KDE libraries.</p>

<p>For example:</p>

<ul>
    <li>Sending items to the trash is currently broken: <a href="https://bugs.kde.org/show_bug.cgi?id=229465">https://bugs.kde.org/show_bug.cgi?id=229465</a></li>
    <li>Camera auto detection doesn't work under windows</li>
    <li>Drag and drop from outside digiKam does not work:
<a href="https://bugs.kde.org/show_bug.cgi?id=240139">https://bugs.kde.org/show_bug.cgi?id=240139 </a>
</li>
</ul>

You can find the list of open bugs about the windows version 
<a href="https://bugs.kde.org/buglist.cgi?query_format=advanced&amp;short_desc_type=allwordssubstr&amp;short_desc=&amp;product=digikam&amp;long_desc_type=substring&amp;long_desc=&amp;bug_file_loc_type=allwordssubstr&amp;bug_file_loc=&amp;keywords_type=allwords&amp;keywords=&amp;bug_status=UNCONFIRMED&amp;bug_status=NEW&amp;bug_status=ASSIGNED&amp;bug_status=REOPENED&amp;op_sys=MS+Windows&amp;emailassigned_to1=1&amp;emailtype1=substring&amp;email1=&amp;emailassigned_to2=1&amp;emailreporter2=1&amp;emailcc2=1&amp;emailtype2=substring&amp;email2=&amp;bugidtype=include&amp;bug_id=&amp;votes=&amp;chfieldfrom=&amp;chfieldto=Now&amp;chfieldvalue=&amp;cmdtype=doit&amp;order=Reuse+same+sort+as+last+time&amp;field0-0-0=noop&amp;type0-0-0=noop&amp;value0-0-0=">
here
</a>.

<p>To report new bugs, please use this url:</p>

<p><a href="http://www.digikam.org/support">http://www.digikam.org/support </a></p>



<p>Thanks in advance for your feedback.</p>

<div class="legacy-comments">

  <a id="comment-19696"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/559#comment-19696" class="active">I think this downloader for</a></h3>    <div class="submitted">Submitted by annma (not verified) on Fri, 2010-12-24 11:14.</div>
    <div class="content">
     <p>I think this downloader for Windows should be far more prominent from your Download page. Currently it's impossible for a user to find it (it's under "package" a term not known from Win world). It is extra important that this Windows installer is found from the first look on Digikam webpage as Digikam is in the APRIL "Catalogue Libre" which is remarkable.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19705"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/559#comment-19705" class="active">Yes, I am aware of this.
We</a></h3>    <div class="submitted">Submitted by julien on Sun, 2010-12-26 09:46.</div>
    <div class="content">
     <p>Yes, I am aware of this.<br>
We will make it easier to find when digiKam will be very stable on windows.</p>
<p>Julien,</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-19697"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/559#comment-19697" class="active">J'en ai rêvé ...</a></h3>    <div class="submitted">Submitted by <a href="http://www.croucrou.com" rel="nofollow">croucrou</a> (not verified) on Fri, 2010-12-24 18:20.</div>
    <div class="content">
     <p>J'en ai rêvé, le pére noel me l'apporte sous le sapin.</p>
<p>Merci papa noel</p>
<p> I dreamed it, Santa Claus brings me under the tree. Thank you papa noel</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-19698"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/559#comment-19698" class="active">Hurray!</a></h3>    <div class="submitted">Submitted by DRB (not verified) on Fri, 2010-12-24 19:18.</div>
    <div class="content">
     <p>I know of a few people who will now be using digiKam!</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-19700"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/559#comment-19700" class="active">Regional</a></h3>    <div class="submitted">Submitted by <a href="http://www.croucrou.com" rel="nofollow">croucrou</a> (not verified) on Sat, 2010-12-25 01:59.</div>
    <div class="content">
     <p>This version is in english</p>
<p>is there a solution to have a french version ?</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19702"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/559#comment-19702" class="active">Go To Help menu...</a></h3>    <div class="submitted">Submitted by digiKam on Sat, 2010-12-25 08:36.</div>
    <div class="content">
     <p>There is an option to switch language.</p>
<p>digiKam</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19709"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/559#comment-19709" class="active">help Menu</a></h3>    <div class="submitted">Submitted by <a href="http://www.croucrou.com" rel="nofollow">croucrou</a> (not verified) on Sun, 2010-12-26 19:12.</div>
    <div class="content">
     <p>OK thank you</p>
<p>I spend many time to search this option in the configuration menu. But i don't think to search in help menu.</p>
<p>Thanks</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div><a id="comment-19703"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/559#comment-19703" class="active">Bloat?</a></h3>    <div class="submitted">Submitted by Jacques Cuse (not verified) on Sun, 2010-12-26 01:57.</div>
    <div class="content">
     <p>C'mon, 251 MB for a relatively simple photo editing program? And I only need the part that imports and renames... =:-)</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19704"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/559#comment-19704" class="active">sorry</a></h3>    <div class="submitted">Submitted by julien on Sun, 2010-12-26 09:44.</div>
    <div class="content">
     <p>but this is the price to pay for an easy installation !</p>
<p>Julien</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-19706"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/559#comment-19706" class="active">It's due to debug Symbol...</a></h3>    <div class="submitted">Submitted by digiKam on Sun, 2010-12-26 10:55.</div>
    <div class="content">
     <p>We have used current Qt4 and KDE4 windows package compiled with MinGW. I think it due to debug symbol which bloat binary and library file sizes.</p>
<p>digiKam</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19714"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/559#comment-19714" class="active">Little Mac off-topic</a></h3>    <div class="submitted">Submitted by Mikel (not verified) on Thu, 2010-12-30 00:52.</div>
    <div class="content">
     <p>It may be a little bit off-topic, but I'd like to ask about the Mac version.</p>
<p>I have been a digikam user since 0.9 or earlier, on Mandriva Gnu/Linux. Nowadays due to several reasons I swap system to Mac OSX and I really miss Digikam. I know that there is the Macport version, but since the por file was upgraded to 1.6 I haven't been able to use digikam, (not any other kde app).<br>
I'm just asking, whether it's is planed a stand alone Mac version of digikam, or I should go on using the macports thing (whenever it's fixed, task I cant help in due to my idiocy). </p>
<p>In the other hand, I don't want to leave the ocasion going without giving thanks to all the people behind  the very best program of the kde world and the best pic manager. </p>
<p>Thanks. </p>
<p>Good night and good luck.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19715"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/559#comment-19715" class="active">look this bugzilla entry...</a></h3>    <div class="submitted">Submitted by digiKam on Thu, 2010-12-30 08:19.</div>
    <div class="content">
     <p>https://bugs.kde.org/show_bug.cgi?id=257679</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19716"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/559#comment-19716" class="active">Thanks Gilles for taking the</a></h3>    <div class="submitted">Submitted by Mikel (not verified) on Thu, 2010-12-30 23:18.</div>
    <div class="content">
     <p>Thanks Gilles for taking the time to point to that  entry and, of course, for the rest of work you do with this great piece of software. </p>
<p>Mikel</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div><a id="comment-19760"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/559#comment-19760" class="active">Strip bynaries?</a></h3>    <div class="submitted">Submitted by Stefano Ferri (not verified) on Fri, 2011-01-07 19:21.</div>
    <div class="content">
     <p>Well, a strip over all the necessary files should reduce filesizes of about 30-40%. These are the commands we use on slacky.eu to provide .txz packages, included Digikam:</p>
<p>find . | xargs file | grep "executable" | grep ELF | cut -f 1 -d : | xargs -r strip --strip-unneeded 2&gt; /dev/null || true<br>
find . | xargs file | grep "shared object" | grep ELF | cut -f 1 -d : | xargs -r strip --strip-unneeded 2&gt; /dev/null || true<br>
find . | xargs file | grep "current ar archive" | cut -f 1 -d : | xargs -r strip --strip-unneeded 2&gt; /dev/null || true</p>
<p>All these command are executed in the directory where is being compiled. This should be done over all the depencencies too.</p>
<p>Stefano</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div><a id="comment-19707"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/559#comment-19707" class="active">Thank you so much! I was</a></h3>    <div class="submitted">Submitted by Marc (not verified) on Sun, 2010-12-26 11:05.</div>
    <div class="content">
     <p>Thank you so much! I was waiting for this last years! This is one of the greatest surprise for christmas.....<br>
Marc</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-19708"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/559#comment-19708" class="active">Thanks a lot!</a></h3>    <div class="submitted">Submitted by Lukas Sommer (not verified) on Sun, 2010-12-26 15:40.</div>
    <div class="content">
     <p>Thanks a lot. That's really cool! A recent Windows version - and with an easy installer!</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-19717"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/559#comment-19717" class="active">About bug reports...</a></h3>    <div class="submitted">Submitted by <a href="http://gerlos.altervista.org" rel="nofollow">gerlos</a> (not verified) on Fri, 2010-12-31 00:05.</div>
    <div class="content">
     <p>Since, as you say, most of the bugs expected on this Windows release are somehow related to the KDE libraries on Windows, should we post bug reports to the digikam team or somewhere else?</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19718"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/559#comment-19718" class="active">post in digiKam in first</a></h3>    <div class="submitted">Submitted by digiKam on Fri, 2010-12-31 08:36.</div>
    <div class="content">
     <p>in first post to digiKam. We will re-route files at the right place in bugzilla if necessary.</p>
<p>digiKam</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-19719"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/559#comment-19719" class="active">More than happy</a></h3>    <div class="submitted">Submitted by fredege (not verified) on Fri, 2010-12-31 11:10.</div>
    <div class="content">
     <p>Thank you for giving us the opportunity to play with the latest digikam revision on windows. I have been waiting for this since the very beginning of the digikam for window announcement.<br>
Having a similar installer for Mac will then lead digikam to the position it deserves as a very serious cataloging tool.</p>
<p>Now I am giving feed to the digikam for windows : mu database and pictures from Linux. Life is unfortunately not so fine as it seems that I cannot give the digikam4.db generated on Linux to windows as it includes hard coded path. Is there any way to get under windows all the cataloging work made on Linux ?</p>
<p>Regards</p>
<p>Frederic</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19724"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/559#comment-19724" class="active">You can configure digiKam</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Tue, 2011-01-04 16:33.</div>
    <div class="content">
     <p>You can configure digiKam under linux to write the metadata into files and then move your file into windows. DigiKam under Windows will rebuild the metadata from the files.<br>
Unfortunately this will not work for all file types as digiKam does not support writing metadata to some raw files for example. You can check which file type are supported by looking into Help/Components Information. If exiv2 supports writing to you files then it will work.</p>
<p>Julien</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-19723"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/559#comment-19723" class="active">Finally!
I've been waiting</a></h3>    <div class="submitted">Submitted by Finally! (not verified) on Tue, 2011-01-04 14:26.</div>
    <div class="content">
     <p>Finally!</p>
<p>I've been waiting for about two years for this! Thanks!</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19755"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/559#comment-19755" class="active">Me too! Thanks and it's</a></h3>    <div class="submitted">Submitted by Edisso (not verified) on Fri, 2011-01-07 11:14.</div>
    <div class="content">
     <p>Me too! Thanks and it's working great (besides the minor bugs that are already listed)!</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19771"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/559#comment-19771" class="active">excellent news!!</a></h3>    <div class="submitted">Submitted by anw (not verified) on Sun, 2011-01-16 21:36.</div>
    <div class="content">
     <p>Great news! Many thanks for your work, I was wishing a windows version so much, now I can share and synchronize tagged photo albums with the family :-)</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div><a id="comment-19757"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/559#comment-19757" class="active">Amarok and digiKam on Window</a></h3>    <div class="submitted">Submitted by <a href="http://temporaryland.wordpress.com" rel="nofollow">rm42</a> (not verified) on Fri, 2011-01-07 16:29.</div>
    <div class="content">
     <p>I hope you don't mind that I spread the news about this item a bit. ;)</p>
<p>http://temporaryland.wordpress.com/2011/01/06/amarok-and-digikam-on-windows/</p>
         </div>
    <div class="links">» </div>
  </div>

</div>
