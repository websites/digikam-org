---
date: "2012-05-21T08:28:00Z"
title: "Soft Proofing in digiKam"
author: "Dmitri Popov"
description: "Soft proofing is a technique which allows you to see what the photo will look like when printed using a specific printer and photo media"
category: "news"
aliases: "/node/654"

---

<p>Soft proofing is a technique which allows you to see&nbsp;what the photo will look like when printed using a specific printer and photo media (paper, canvas, etc.) without actually printing the photo. Many professional photo processing applications support soft proofing, and digiKam is no exception.</p>
<p>To make this feature work in digiKam, you need to specify color profiles for your display and the output device (e.g., printer). But before you do that, you need to obtain the ICC color profile for your specific printer and print media.</p>
<p><a href="https://scribblesandsnaps.files.wordpress.com/2012/03/digikam_softproofing.png"><img class="alignnone size-medium wp-image-2352" title="digikam_softproofing" src="https://scribblesandsnaps.files.wordpress.com/2012/03/digikam_softproofing.png?w=500" alt="" width="500" height="263"></a></p>
<p><a href="http://scribblesandsnaps.wordpress.com/2012/05/21/soft-proofing-in-digikam/">Continue to read</a></p>

<div class="legacy-comments">

</div>