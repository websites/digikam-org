---
date: "2010-10-15T10:12:00Z"
title: "Using digiKam with MySQL"
author: "Dmitri Popov"
description: "By default, digiKam uses SQLite as its back-end for storing important metadata and thumbnails. But the photo management application also provides support for the popular"
category: "news"
aliases: "/node/541"

---

<p>By default, digiKam uses SQLite as its back-end for storing important metadata and thumbnails. But the photo management application also provides support for the popular MySQL database engine, and it comes with a database migration tool that can help you to move your data from SQLite to MySQL. <a href="http://scribblesandsnaps.wordpress.com/2010/10/15/using-digikam-with-mysql/">Continue to read</a></p>

<div class="legacy-comments">

</div>