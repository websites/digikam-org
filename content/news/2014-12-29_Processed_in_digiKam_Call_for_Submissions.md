---
date: "2014-12-29T13:18:00Z"
title: "Processed in digiKam Call for Submissions"
author: "Dmitri Popov"
description: "Sharing is caring, right? So if you use digiKam for processing photos, why not share your photo editing techniques and tips with other users and"
category: "news"
aliases: "/node/726"

---

<p>Sharing is caring, right? So if you use digiKam for processing photos, why not share your photo editing techniques and tips with other users and showcase your best photos? I invite you to participate in the new Processed in digiKam feature on the Scribbles and Snaps blog. <a href="http://scribblesandsnaps.com/2014/12/29/processed-in-digikam-call-for-submissions/">Continue to read</a></p>

<div class="legacy-comments">

</div>