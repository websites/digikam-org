---
date: "2014-12-18T16:20:00Z"
title: "digiKam Software Collection 4.6.0 released..."
author: "digiKam"
description: "Dear digiKam fans and users, The digiKam Team is proud to announce the release of digiKam Software Collection 4.6.0. This release includes many bugs fixes"
category: "news"
aliases: "/node/725"

---

<a href="https://www.flickr.com/photos/digikam/16002729706"><img src="https://farm9.staticflickr.com/8652/16002729706_938fd41235_c.jpg" width="800" height="238"></a>

<p>Dear digiKam fans and users,</p>

<p>
The digiKam Team is proud to announce the release of digiKam Software Collection 4.6.0. This release includes many bugs fixes in Image Editor and Batch Queue Manager. Thanks to <b>Maik Qualmann</b> and <b>Jan Wolter</b> to propose patches in KDE bugzilla.
</p>

<p>
See the new list of the <a href="https://bugs.kde.org/buglist.cgi?f1=cf_versionfixedin&amp;o1=equals&amp;query_format=advanced&amp;bug_status=RESOLVED&amp;bug_status=NEEDSINFO&amp;bug_status=VERIFIED&amp;bug_status=CLOSED&amp;v1=4.6.0&amp;product=digikam&amp;product=digikamimageplugins&amp;product=kipiplugins&amp;product=showfoto">issues closed</a> in digiKam 4.6.0 available through the KDE Bugs-tracking System.
</p>

<p>The digiKam software collection tarball can be downloaded from the <a href="http://download.kde.org/stable/digikam/digikam-4.6.0.tar.bz2.mirrorlist">KDE repository</a>.
</p>

<p>The next digiKam bug fix release is planed next year. The main work progress in parallel to port all implementations under Qt5/KF5 as you can see <a href="https://techbase.kde.org/Projects/Digikam/CodingSprint2014#KDE_Framework_Port">in this wiki page</a>.</p>

<p>Have a Merry Christmas and an happy new year, playing with your photos using this new release.</p>

<p>digiKam Team...</p>
<div class="legacy-comments">

  <a id="comment-20950"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/725#comment-20950" class="active">Good Job</a></h3>    <div class="submitted">Submitted by JoshG (not verified) on Fri, 2014-12-19 10:03.</div>
    <div class="content">
     <p>Thumbnails of offline collections can now be viewed: Thankyou!</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-20951"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/725#comment-20951" class="active">Digikam: a very nice program</a></h3>    <div class="submitted">Submitted by Jens Verwaerde (not verified) on Sat, 2014-12-20 12:37.</div>
    <div class="content">
     <p>Hi, just to let you know I'm a great fan of digikam. I've been using it for about 4 years now.</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-20952"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/725#comment-20952" class="active">fail to extract and compile</a></h3>    <div class="submitted">Submitted by <a href="http://jurnal.snydez.com" rel="nofollow">snydez</a> (not verified) on Mon, 2014-12-22 05:04.</div>
    <div class="content">
     <p>hi,<br>
how to compile it?<br>
normal process such tar -xvjf is seem just extracting it.<br>
when i run ./configure , it said not found<br>
please advise.<br>
thanks</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20954"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/725#comment-20954" class="active">...look here...</a></h3>    <div class="submitted">Submitted by digiKam on Mon, 2014-12-22 11:32.</div>
    <div class="content">
     <p>https://www.digikam.org/download?q=download/tarball</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-20953"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/725#comment-20953" class="active">thx to Philip Johnsson and the Digikam Team</a></h3>    <div class="submitted">Submitted by <a href="http://www.freunde-schloss-beuggen.de" rel="nofollow">Schlossfreund</a> (not verified) on Mon, 2014-12-22 11:30.</div>
    <div class="content">
     <p>I'm using Kubuntu 14.10 and Philip provided a ppa that works perfectly. So since yesterday i'm using 4.6 and it's a pleasure. Lense correction and RAW processing works fine. I wonder if no additional tools will be needed for me. Thanks a lot!</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20971"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/725#comment-20971" class="active">Good to hear that you find my</a></h3>    <div class="submitted">Submitted by philip5 (not verified) on Sat, 2015-01-10 16:49.</div>
    <div class="content">
     <p>Good to hear that you find my PPA is useful. From the statistics I have about 1000 users of my Digikam packages from my PPA if anyone is curious.</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-20956"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/725#comment-20956" class="active">There is no support for</a></h3>    <div class="submitted">Submitted by Vladimir (not verified) on Mon, 2014-12-22 14:19.</div>
    <div class="content">
     <p>There is no support for negatives Nikon D3300.<br>
Thanks in advance for implementation.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20957"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/725#comment-20957" class="active">Not yet...</a></h3>    <div class="submitted">Submitted by digiKam on Mon, 2014-12-22 14:22.</div>
    <div class="content">
     <p>Nikon D3300 is not yet supported by libraw library used by digiKam to decode RAW image.</p>
<p>Please report this problem to libraw team (http://www.libraw.org)</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-20972"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/725#comment-20972" class="active">Nikon D3300 (also Nikon D810)</a></h3>    <div class="submitted">Submitted by philip5 (not verified) on Sat, 2015-01-10 17:20.</div>
    <div class="content">
     <p>Nikon D3300 (also Nikon D810) i supported in libraw git. Either patch libraw 0.16 for support or use git code to build updated libraw. Otherwise wait for the next full upstream release of libraw. libkdcraw will also need to be rebuilt against it for Digikam (maybe digikam needs a rebuild aginst them both too) to take use of the update.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20973"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/725#comment-20973" class="active">in theory no...</a></h3>    <div class="submitted">Submitted by digiKam on Sat, 2015-01-10 18:59.</div>
    <div class="content">
     <p>digiKam do not know libraw. Only libkdcraw is linked with libraw.</p>
<p>If libkdcraw API/ABI are unchanged, digiKam must work as well without a recompilation</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-20980"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/725#comment-20980" class="active">Reply to comment | digiKam - Photo Management Program</a></h3>    <div class="submitted">Submitted by <a href="https://www.facebook.com/NikonD810Price" rel="nofollow">Nikon D810 price</a> (not verified) on Mon, 2015-02-23 22:39.</div>
    <div class="content">
     <p>ACR has added support for three new camera raw file formats: the Nikon D810, Panasonic LUMIX<br>
AG-GH4 and Panasonic LUMIX DMC-FZ1000. On the video<br>
end of things, there are some refinements that a<br>
serious video shooter should appreciate. And it can effectively convert MXF and<br>
AVCHD files to other usable formats for editing in FCP, FCE, i<br>
- Movie, Avid MC, Adobe Premiere, Sony Vegas and other editing software.</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div><a id="comment-20958"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/725#comment-20958" class="active">Digikam crasches on OSX 10.10.1</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Tue, 2014-12-23 17:37.</div>
    <div class="content">
     <p>Hi,</p>
<p>Digikam 4.6.0 is unusable on OSX 10.10.1: after a fresh install of macports and digikam that apparently ran well, drkonqi bounces for about 30' to finally generate a crash report. It's reproducible at each start.<br>
HW config: MBP 17" mid 2010, 2.66GHz core i7, 8GB RAM, NVIDIA GeForce GT 330M 512 MB, 1TB fusiondrive.<br>
I would have like to try it.<br>
Regards,<br>
Daniel</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20959"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/725#comment-20959" class="active">I'd be glad, if I were able</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Wed, 2014-12-24 16:22.</div>
    <div class="content">
     <p>I'd be glad, if I were able to compile it. The 4.0.0 at MacPorts is broken and the current won't compile either.</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-20963"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/725#comment-20963" class="active">digikam 4.6.0 will not build</a></h3>    <div class="submitted">Submitted by Holger Wagemann (not verified) on Sat, 2015-01-03 10:26.</div>
    <div class="content">
     <p>Hi,</p>
<p>I try the debian way to compile digikam 4.6.0. Before this procedere I've done an apt-get build-dep digikam and then I've used the .bootstrap.linux script, apart from  MySQL Database Support, which is optional, all dependencies seem to be installed.</p>
<p>After a <a>dh_make -f ../digikam-4.6.0.tar.bz2</a> I start building procedere with <a>dpkg-buildpackage -uc -us -j8</a></p>
<p>The compiling procedere is okay but then some tests start, 10 of 12 tests pass, but 2 tests with dimagehistory fail and the building procedere stops.</p>
<p>Which further packages I need to avoid this issue?</p>
<p>I've try it on a Debian Testing and on a Debian Unstable (siduction), on both systems I get this failing tests with dimagehistory.</p>
<p>Thanks for any help, kind regards,<br>
  Holger</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20964"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/725#comment-20964" class="active">drop tests...</a></h3>    <div class="submitted">Submitted by digiKam on Sat, 2015-01-03 10:38.</div>
    <div class="content">
     <p>You can drop tests at compilation time turn off right cmake flag at configuration time. Look README file for details</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-20965"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/725#comment-20965" class="active">Windows version, PLEASE.</a></h3>    <div class="submitted">Submitted by cc (not verified) on Sun, 2015-01-04 00:19.</div>
    <div class="content">
     <p>I'd like to request an updated windows build for 4.6, please.</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-20966"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/725#comment-20966" class="active">Crash on startup</a></h3>    <div class="submitted">Submitted by <a href="http://www.bazakolejowa.pl" rel="nofollow">Qrczakoff</a> (not verified) on Sun, 2015-01-04 17:50.</div>
    <div class="content">
     <p>Opensuse 13.2 x64 and repo KDE Extra<br>
At start always ends with:<br>
digikam: symbol lookup error: digikam: undefined symbol: _ZN7KGeoMap14GeoCoordinatesC1Ev</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20969"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/725#comment-20969" class="active">Update libkgeomap</a></h3>    <div class="submitted">Submitted by António Pedro Neves (not verified) on Wed, 2015-01-07 22:06.</div>
    <div class="content">
     <p>I had the same issue, but managed to solve it, updating package <code>libkgeomap</code> in Yast.</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-20974"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/725#comment-20974" class="active">OpenSUSE Tumbleweed has the same problems</a></h3>    <div class="submitted">Submitted by Kenneth Ingham (not verified) on Sun, 2015-01-25 01:24.</div>
    <div class="content">
     <p>It appears to be crashing on a Canon Raw file.  I have reported it as OpenSUSE bug 904446.</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-20967"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/725#comment-20967" class="active">Works very well</a></h3>    <div class="submitted">Submitted by Casper Blom (not verified) on Mon, 2015-01-05 16:50.</div>
    <div class="content">
     <p>I used Phillips ppa to update, this version 4.6.0 seems much more stable than previous ones.<br>
I especially run a KDE environment (Linux Mint 17) for this tool.<br>
Love Digikam</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20968"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/725#comment-20968" class="active">stable for me too</a></h3>    <div class="submitted">Submitted by <a href="http://alicious.com" rel="nofollow">pbhj</a> (not verified) on Wed, 2015-01-07 03:19.</div>
    <div class="content">
     <p>For the first time in probably a year digikam has let me import photos without crashing, from 2 devices; and the import dialog/window came up really quickly too. Seems very promising. Thanks to all devs.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20970"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/725#comment-20970" class="active">Same for me. The 4.x series</a></h3>    <div class="submitted">Submitted by hamster (not verified) on Thu, 2015-01-08 16:58.</div>
    <div class="content">
     <p>Same for me. The 4.x series was a tough ride, but it was worth suffering :) 4.6 seems to be almost flawless! Good job by the digiKam team, thank you!</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div>
</div>