---
date: "2014-03-30T14:45:00Z"
title: "digiKam Software Collection 4.0.0-beta4 is out.."
author: "digiKam"
description: "Dear all digiKam fans and users! digiKam team is proud to announce the fourth beta release of digiKam Software Collection 4.0.0. This version currently under"
category: "news"
aliases: "/node/712"

---

<a href="https://www.flickr.com/photos/digikam/13510597753"><img src="https://farm4.staticflickr.com/3706/13510597753_4cbbd7b97c_c.jpg" width="800" height="225"></a>


<p>Dear all digiKam fans and users!</p>

<p>
digiKam team is proud to announce the fourth beta release of digiKam Software Collection 4.0.0.
This version currently under development, include many new features as:
</p>

<ul>

<li>
A new tool dedicated to organize whole tag hierarchies. This new feature is relevant of <a href="http://community.kde.org/Digikam/GSoC2013#digiKam_Tag_Manager">GoSC-2013 project</a> from <b>Veaceslav Munteanu</b>. This project include also a back of Nepomuk support in digiKam broken since a while due to important KDE API changes. Veaceslav has also implemented multiple selection and multiple drag-n-drop capabilities on Tags Manager and Tags View from sidebars, and the support for writing face rectangles metadata in Windows Live Photo format.
</li>

<li>
A new maintenance tool dedicated to parse image quality and auto-tags items automatically using Pick Labels. This tool is relevant to another <a href="http://community.kde.org/Digikam/GSoC2013#Image_Quality_Sorter_for_digiKam">GoSC-2013 project</a> from <b>Gowtham Ashok</b>. This tool require feedback and hack to be finalized for this release.
</li>

<li>
<p>Showfoto thumbbar is now ported to Qt model/view in order to switch later full digiKam code to Qt5. This project is relevant to another <a href="http://community.kde.org/Digikam/GSoC2013#Port_Showfoto_Thumb_bar_to_Qt4_Model.2FView">GoSC-2013 project</a> from <b>Mohamed Anwer</b>.</p>
</li>

<li>
<p>A lots of work have been done into Import tool to fix several dysfunctions reported since a very long time. For example, The status to indicate which item have been already downloaded from the device is back. Thanks to <b>Teemu Rytilahti</b> and <b>Islam Wazery</b> to contribute. All fixes are not yet completed and the game continue until the next beta release.</p>
</li>

<li>
<p>This release is now fully ported to Qt4 model-view and drop last Q3-Support classes. The last pending part was Image Editor Canvas ported and completed by <b>Yiou Wang</b> through this <a href="http://community.kde.org/Digikam/GSoC2013#Port_Image_digiKam_Editor_Canvas_Classes_to_Qt4_Model.2FView">GoSC-2013 project</a>. 
In the future, port to Qt5 will be easy and quickly done, when KDE 5 API will stabilized and released.</p>
</li>

</ul>

<p>This is the last beta release before ending line. Another release candidate will be published following the <a href="http://community.kde.org/Digikam/GSoC2013#Roadmap_and_Releases_Plan_including_all_GSoC-2013_works">release plan</a> to conclude final 4.0.0.</p>

<p>As usual with a major release, we have a long list of <a href="https://bugs.kde.org/buglist.cgi?f1=cf_versionfixedin&amp;o1=equals&amp;query_format=advanced&amp;bug_status=RESOLVED&amp;bug_status=NEEDSINFO&amp;bug_status=VERIFIED&amp;bug_status=CLOSED&amp;v1=4.0.0&amp;product=digikam&amp;product=digikamimageplugins&amp;product=kipiplugins&amp;product=showfoto">files closed</a> with this release into KDE bugzilla.</p>

<p>digiKam software collection tarball can be downloaded from <a href="http://download.kde.org/unstable/digikam/digikam-4.0.0-beta4.tar.bz2.mirrorlist">KDE repository</a></p>

<p>This version is for testing purposes. <b>Please do not use it yet in production!</b></p>

<p>Thanks in advance for your reports.

</p><p>Happy digiKam...</p>
<div class="legacy-comments">

  <a id="comment-20677"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/712#comment-20677" class="active">Bug in face recognition is still there.</a></h3>    <div class="submitted">Submitted by Rob Dean (not verified) on Mon, 2014-03-31 11:29.</div>
    <div class="content">
     <p>Great product - apart from the bug in face recognition (Bug 323888, https://bugs.kde.org/show_bug.cgi?id=323888) that chews up memory and makes face recognition unusable. Will this be fixed before the final release?</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20678"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/712#comment-20678" class="active">It must be... </a></h3>    <div class="submitted">Submitted by digiKam on Mon, 2014-03-31 12:39.</div>
    <div class="content">
     <p>we need to found time to investiguate</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20696"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/712#comment-20696" class="active">This bug has been a big</a></h3>    <div class="submitted">Submitted by <a href="http://www.researchut.com" rel="nofollow">Ritesh Raj Sarraf</a> (not verified) on Wed, 2014-04-30 22:07.</div>
    <div class="content">
     <p>This bug has been a big annoyance. I tried to follow the advise in most bug report, i.e. to build libpgf wihtout the multithreading capabilities, but that does not help. Digikam still falls apart. I'm afraid the bug might be somewhere else, and that implies that this annoying bug is going to bite us for some more time. :-(</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div><a id="comment-20679"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/712#comment-20679" class="active">You really ought to provide</a></h3>    <div class="submitted">Submitted by photoken (not verified) on Tue, 2014-04-01 05:36.</div>
    <div class="content">
     <p>You really ought to provide at least one 4.0 beta for Windows so that bugs on that platform can be identified before the 4.0 release.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20680"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/712#comment-20680" class="active">That would be really cool. Is</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Tue, 2014-04-01 10:55.</div>
    <div class="content">
     <p>That would be really cool. Is there a chance for this? Thanks.</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-20681"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/712#comment-20681" class="active">is there any release plan for</a></h3>    <div class="submitted">Submitted by wolfgang (not verified) on Thu, 2014-04-10 20:25.</div>
    <div class="content">
     <p>is there any release plan for the windows Version?</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-20693"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/712#comment-20693" class="active">I can understand the</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Tue, 2014-04-22 15:10.</div>
    <div class="content">
     <p>I can understand the simplicity in developing Linux-only applications but Digikam has broad appeal among photographers. And only a tiny minority of photographers are using Linux. Linux has about a 1.5% market share on desktops and laptops ( http://en.wikipedia.org/wiki/Usage_share_of_operating_systems ) The developers would have a much larger (&gt;50X!) pool of users to draw on for donations, sponsorship, and feedback, if they would provide better Windows and OS X support. </p>
<p>I would gladly donate to the project if I could verify the 4.0 release worked reasonably well on Windows. With better Windows support Digikam 4 would be a viable alternative to paid software like Lightroom, Adobe Bridge, IMatch, etc. The developers have not even offered an opinion on Windows support despite the fact it's been raised many times with the betas. The impression is they simply don't care about the other 98.5% of users. That's their choice but it severely limits Digikam's use, acceptance, growth, donations, etc.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20694"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/712#comment-20694" class="active">Multi-Platform Users</a></h3>    <div class="submitted">Submitted by Multi-Platform User (not verified) on Tue, 2014-04-22 16:52.</div>
    <div class="content">
     <p>Or as I've noted in the past - users like me who have a home network wired and wireless with multiple users on a variety of platforms (linux, Windows XP, Windows 7 and Windows 8).  It's a big investment in time to setup setup a database only to find  that it won't work for all machines on the network.  </p>
<p>Other software like GIMP have made the plunge by including the required software components in the package.  This also allows them to be used as portable applications, which is great when you can't (or don't want to) install a package on someone else's machine.  Imagine being able to show/search your photos remotely without having to install the software!</p>
<p>digiKam's constant (for years) Windows bugginess (Yes - we know its KDE) makes it productively unusable for most of us.  I'm left torn between the Canon software I have (Windows only) and digiKam (linux) - not a great choice.  </p>
<p>Again, kudos to the digiKam developers - it's an amazing piece of software.</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-20697"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/712#comment-20697" class="active">Digikam 4 windows</a></h3>    <div class="submitted">Submitted by MBB (not verified) on Tue, 2014-05-06 18:23.</div>
    <div class="content">
     <p>I think it's a chicken-egg problem, without regular windows versions it won't get any name recongition, and without a use base, there is no interest in developping for windows. </p>
<p>It was why I was so happy to see regular updates from 3.0 upto 3.4 for windows. But I guess those did not get enough downloads or did not bring in new develloppers.<br>
I tried to get it added to ny usual software review site, but it did not get accepted. </p>
<p>I myself, besides beeing a freetard and liking the filosophy, am interested in it's harmless storing of metadata of picture databases, so I'll bee keeping an eye on it. </p>
<p>It didn't actually work for me as my (test) pictures got changed, but I'm not sure if i did something wrong (probably) or if it's a windows version bug or a feature by design, but I'll try again someday.</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-20698"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/712#comment-20698" class="active">There is a rock solid digikam</a></h3>    <div class="submitted">Submitted by Eru (not verified) on Mon, 2014-05-12 21:41.</div>
    <div class="content">
     <p>There is a rock solid digikam version for windows: VMPlayer running Fedora 20.<br>
It's realy robust and Fedora provides updates stable version quite fast (within a week)!</p>
<p>I love it so much. I totally turned away from ACDSee (for years beloved) on Windows to Digikam on VMWare on Windows.</p>
<p>I have all my images on the network (Synology NAS), so interaction between the Fedora VM and my Windows host is not an issue. Just give it a try!</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-20699"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/712#comment-20699" class="active">Then why don't you run Linux on a computer?</a></h3>    <div class="submitted">Submitted by HKR (not verified) on Wed, 2014-05-14 07:26.</div>
    <div class="content">
     <p>Questions like this makes me wonder why you don't use a Linux distro on one computer and have windows on another. Digikam runs best in the system it was made for and that is a KDE Linux system. The other option is to make a Digikam Windows support team and start doing it yourself. This is after all free software, and how you run it is up to you.</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div><a id="comment-20689"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/712#comment-20689" class="active">Proposals</a></h3>    <div class="submitted">Submitted by Tobias (not verified) on Wed, 2014-04-16 12:41.</div>
    <div class="content">
     <p>Are the proposals from GoSC archived somewhere? I'd like to read more about the tagging feature, but unfortunately the GoSC page is no longer there.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20691"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/712#comment-20691" class="active">link to proposals</a></h3>    <div class="submitted">Submitted by Andreas Hirczy (not verified) on Wed, 2014-04-16 20:27.</div>
    <div class="content">
     <p>The link is included several times in the text.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20692"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/712#comment-20692" class="active">Yes, the link to the overview</a></h3>    <div class="submitted">Submitted by Tobias (not verified) on Wed, 2014-04-16 21:38.</div>
    <div class="content">
     <p>Yes, the link to the overview is there, but the proposals on the GoSC page are missing. I'm asking for the proposals that could be found there. After reading this: <a href="http://community.kde.org/Digikam/GSoC2013#digiKam_Tag_Manager">digiKam Tag Manager </a> I have no idea what the new tag manager is about, so the proposal would be a nice read.</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div>
</div>