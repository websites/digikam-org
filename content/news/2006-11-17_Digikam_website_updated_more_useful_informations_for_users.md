---
date: "2006-11-17T17:34:00Z"
title: "Digikam website updated : more useful informations for users !"
author: "fabien"
description: "In the past days (also weeks...), we added more informations to the Digikam website. Browse it ! What's new New support page New contrib page"
category: "news"
aliases: "/node/180"

---

<p>In the past days (also weeks...), we added more informations to the Digikam website. Browse it !</p>
<p><strong>What's new</strong></p>
<ul>
<li>New support page</li>
<li>New contrib page</li>
<li>Updated contact page</li>
<li>Updated Download/SVN page (see below)</li>
<li>More screenshots</li>
<li>New about/features for the 0.9 series</li>
<li>More FAQ entries</li>
</ul>
<p><strong>Test the latest SVN release</strong><br>
There are new scripts in the <a href="/?q=download/svn">Download/SVN</a> page to download, compile and install digikam in your home directory without conflicting with your standard digikam version...</p>

<div class="legacy-comments">

</div>