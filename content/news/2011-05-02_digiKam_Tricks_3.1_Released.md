---
date: "2011-05-02T08:30:00Z"
title: "digiKam Tricks 3.1 Released"
author: "Dmitri Popov"
description: "A new version of the digiKam Tricks book has been released. This release includes two new topics: Process RAW Files in digiKam Generate HTML Photo"
category: "news"
aliases: "/node/595"

---

<p>A new version of the digiKam Tricks book has been released. This release includes two new topics:</p>
<ul>
<li>Process RAW Files in digiKam</li>
<li>Generate HTML Photo Galleries</li>
</ul>
<p><a href="http://scribblesandsnaps.wordpress.com/2011/05/02/digikam-tricks-3-1-released/">Continue to read</a></p>

<div class="legacy-comments">

</div>