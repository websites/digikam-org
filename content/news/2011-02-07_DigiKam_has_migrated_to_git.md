---
date: "2011-02-07T20:03:00Z"
title: "DigiKam has migrated to git"
author: "Martin Klapetek"
description: "Thanks to the marvelous job of our KDE sysadmins, digiKam is another project to join the git repository. All the code (digiKam, kipi-plugins) that lived"
category: "news"
aliases: "/node/574"

---

<p>Thanks to the marvelous job of our KDE sysadmins, digiKam is another project to join the git repository. All the code (digiKam, kipi-plugins) that lived in the SVN is no more accessible through SVN and all is part of git now. DigiKam and its dependencies live in several repos, but thanks to Marcel Wiesweg, you can use a nice small script, that will get you going:</p>
<p>1) Edit your ~/.gitconfig as given in<br>
<a href="http://community.kde.org/Sysadmin/GitFAQ#Let_Git_rewrite_URL_prefixes">http://community.kde.org/Sysadmin/GitFAQ#Let_Git_rewrite_URL_prefixes</a><br>
2) git clone http://anongit.kde.org/digikam-software-compilation digikam-sc<br>
Enter the directory<br>
3) Execute ./download-repos</p>
<p>We're planning to release one more version from 1.x branch in about two weeks, the last one, version 1.9.0. After that, all focus will go to the new 2.0 branch, which features new non-destructive editing with versioning support, face detection &amp; recognition, extended geo-location features and scripting support. Second beta of this branch is already out and you're all more than welcome to join the testing!</p>
<p>DigiKam team</p>

<div class="legacy-comments">

  <a id="comment-19824"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/574#comment-19824" class="active">What about kdesrc-build?</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Mon, 2011-02-07 20:46.</div>
    <div class="content">
     <p>Why not post a module for use with kdesrc-build? kdesrc-build is supposed to allow easy build of all kde components and I'd consider digikam an important one at that...</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-19825"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/574#comment-19825" class="active">Digikam 2.0.0 git</a></h3>    <div class="submitted">Submitted by Alexey (not verified) on Wed, 2011-02-09 17:41.</div>
    <div class="content">
     <p>How to get digikam 2.0.0 from git?</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-19826"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/574#comment-19826" class="active">Changelog</a></h3>    <div class="submitted">Submitted by Schusch (not verified) on Fri, 2011-02-11 19:04.</div>
    <div class="content">
     <p>will there be a chance to get the changelog files again? The links don't work, maybe because of the change to git?</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19827"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/574#comment-19827" class="active">Right...</a></h3>    <div class="submitted">Submitted by digiKam on Fri, 2011-02-11 20:45.</div>
    <div class="content">
     <p>I will fix it</p>
<p>digiKam</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-19828"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/574#comment-19828" class="active">migration killed splashscreen-page</a></h3>    <div class="submitted">Submitted by Jannis (not verified) on Sat, 2011-02-12 22:15.</div>
    <div class="content">
     <p>Since the migration to git, the splashscreen-page (http://www.digikam.org/drupal/splashcreens) stopped working (since it directly used the websvn-URLs)</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19830"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/574#comment-19830" class="active">right...</a></h3>    <div class="submitted">Submitted by digiKam on Sun, 2011-02-13 00:37.</div>
    <div class="content">
     <p>I will fix it too...</p>
<p>digiKam</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-19829"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/574#comment-19829" class="active">Now, after migration, how can</a></h3>    <div class="submitted">Submitted by Wojtek Migda (not verified) on Sat, 2011-02-12 22:27.</div>
    <div class="content">
     <p>Now, after migration, how can I browse the repository with an internet browser ? What's the new url ?</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19831"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/574#comment-19831" class="active">project pages are here...</a></h3>    <div class="submitted">Submitted by digiKam on Sun, 2011-02-13 00:38.</div>
    <div class="content">
     <p>https://projects.kde.org/projects/extragear/graphics/digikam<br>
https://projects.kde.org/projects/extragear/graphics/kipi-plugins</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-19835"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/574#comment-19835" class="active">GIT clone not working</a></h3>    <div class="submitted">Submitted by Tobias (not verified) on Tue, 2011-02-22 08:25.</div>
    <div class="content">
     <p>[08:26:58]|[tobi@tobi-desktop]|/tmp<br>
$cat ~/.gitconfig<br>
[url "git://anongit.kde.org/"]<br>
     insteadOf = kde:<br>
      [url "git@git.kde.org:"]<br>
           pushInsteadOf = kde:</p>
<p>[08:27:00]|[tobi@tobi-desktop]|/tmp<br>
$git clone kde:scratch/mwiesweg/digikam-sc<br>
Cloning into digikam-sc...<br>
warning: You appear to have cloned an empty repository.<br>
[08:27:03]|[tobi@tobi-desktop]|/tmp<br>
$ls digikam-sc/<br>
total 0<br>
drwxr-xr-x  3 tobi tobi   60 Feb 22 08:27 .<br>
drwxrwxrwt 34 root root 5620 Feb 22 08:27 ..<br>
drwxr-xr-x  6 tobi tobi  180 Feb 22 08:27 .git</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19836"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/574#comment-19836" class="active">All have been moved here...</a></h3>    <div class="submitted">Submitted by digiKam on Tue, 2011-02-22 08:41.</div>
    <div class="content">
     <p>https://projects.kde.org/projects/extragear/graphics/digikam/digikam-software-compilation/repository</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19837"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/574#comment-19837" class="active">$ls
total 108
drwxr-xr-x  4</a></h3>    <div class="submitted">Submitted by Tobias (not verified) on Tue, 2011-02-22 10:02.</div>
    <div class="content">
     <p>$ls<br>
total 108<br>
drwxr-xr-x  4 tobi tobi   220 Feb 22 10:03 .<br>
drwxrwxrwt 33 root root  5600 Feb 22 10:03 ..<br>
drwxr-xr-x  7 tobi tobi   240 Feb 22 10:03 .git<br>
-rw-r--r--  1 tobi tobi    99 Feb 22 10:03 .gitignore<br>
-rw-r--r--  1 tobi tobi   200 Feb 22 10:03 .gitslave<br>
-rw-r--r--  1 tobi tobi  1764 Feb 22 10:03 CMakeLists.txt<br>
-rw-r--r--  1 tobi tobi    77 Feb 22 10:03 NEWS<br>
-rw-r--r--  1 tobi tobi   879 Feb 22 10:03 README<br>
-rwxr-xr-x  1 tobi tobi   434 Feb 22 10:03 download-repos<br>
drwxr-xr-x  2 tobi tobi    60 Feb 22 10:03 extra<br>
-rwxr-xr-x  1 tobi tobi 73867 Feb 22 10:03 gits<br>
[10:03:57]|[tobi@tobi-desktop]|/tmp/digikam-software-compilation<br>
$./download-repos<br>
Cloning into core...<br>
fatal: The remote end hung up unexpectedly<br>
Could not clone git://anongit.kde.org/digikam-software-compilation^digikam onto core<br>
./download-repos: line 8: cd: core: No such file or directory<br>
./download-repos: line 9: cd: extra/kipi-plugins: No such file or directory<br>
./download-repos: line 10: cd: extra/libkexiv2: No such file or directory<br>
./download-repos: line 11: cd: extra/libkdcraw: No such file or directory<br>
[10:03:58]|[tobi@tobi-desktop]|/tmp/digikam-software-compilation<br>
$./gits populate<br>
Cloning into core...<br>
fatal: The remote end hung up unexpectedly<br>
Could not clone git://anongit.kde.org/digikam-software-compilation^digikam onto core</p>
<p>If I create "core" I get:<br>
$./gits populate<br>
sh: line 0: cd: core/.git/hooks: No such file or directory</p>
<p>Thank you</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19838"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/574#comment-19838" class="active">It seems as it is working</a></h3>    <div class="submitted">Submitted by Tobias (not verified) on Tue, 2011-02-22 10:04.</div>
    <div class="content">
     <p>It seems as it is working with:<br>
"kde://digikam" "core"<br>
instead of<br>
"^digikam" "core"</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div></div>
</div>
