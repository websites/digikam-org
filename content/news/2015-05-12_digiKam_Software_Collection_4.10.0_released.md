---
date: "2015-05-12T19:23:00Z"
title: "digiKam Software Collection 4.10.0 released..."
author: "digiKam"
description: "Dear digiKam fans and users, The digiKam Team is proud to announce the release of digiKam Software Collection 4.10.0. This release includes a new sets"
category: "news"
aliases: "/node/739"

---

<a href="https://www.flickr.com/photos/digikam/17379488119"><img src="https://farm9.staticflickr.com/8820/17379488119_905f381070_c.jpg" width="800" height="225"></a>

<p>Dear digiKam fans and users,</p>

<p>
The digiKam Team is proud to announce the release of digiKam Software Collection 4.10.0. This release includes a new sets of bugs fixes from <a href="https://plus.google.com/u/0/107171232114475191915/about">Maik Qualmann</a> who maintain KDE4 version while <a href="https://techbase.kde.org/Projects/Digikam/CodingSprint2014#KDE_Framework_Port">KF5 port is under progress</a>.
</p>

<p>This version introduce also new interroperability patches made by <a href="https://plus.google.com/u/0/+AlanPater/about">Alan Pater</a> to support <b>ACDSee</b> and <b>MediaPro</b> metadata.
</p>

<p>
See the new list of the <a href="https://bugs.kde.org/buglist.cgi?f1=cf_versionfixedin&amp;o1=equals&amp;query_format=advanced&amp;bug_status=RESOLVED&amp;bug_status=NEEDSINFO&amp;bug_status=VERIFIED&amp;bug_status=CLOSED&amp;v1=4.10.0&amp;product=digikam&amp;product=digikamimageplugins&amp;product=kipiplugins&amp;product=showfoto">issues closed</a> in digiKam 4.10.0 available through the KDE Bugs-tracking System.
</p>

<p>The digiKam software collection tarball and Windows installer can be downloaded from the <a href="http://download.kde.org/stable/digikam/">KDE repository</a>.
</p>

<p>Have fun to use this new digiKam release.</p>

<p>digiKam Team...</p>
<div class="legacy-comments">

  <a id="comment-21027"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/739#comment-21027" class="active">Thank you Maik and Gilles</a></h3>    <div class="submitted">Submitted by Alan Pater (not verified) on Tue, 2015-05-12 21:33.</div>
    <div class="content">
     <p>To be clear, Maik did most of the heavy lifting to support ACDSee Hierarchical keywords in digikam. Thanks!</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-21028"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/739#comment-21028" class="active">Philip Johnsson's extra PPA</a></h3>    <div class="submitted">Submitted by Alan Pater (not verified) on Tue, 2015-05-12 21:43.</div>
    <div class="content">
     <p>4.10.0 is already available from Philip Johnsson's extra PPA</p>
<p>https://launchpad.net/~philip5/+archive/ubuntu/extra</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-21029"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/739#comment-21029" class="active">I'm working on support for</a></h3>    <div class="submitted">Submitted by philip5 (not verified) on Tue, 2015-05-12 21:52.</div>
    <div class="content">
     <p>I'm working on support for ubuntu 15.04 (vivid) too this time with digikam 4.10. Porting a few other updated packages that Digikam use at the same time. Hopefully those packages will be available in a few hours.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-21030"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/739#comment-21030" class="active">Cool!</a></h3>    <div class="submitted">Submitted by Alan Pater (not verified) on Tue, 2015-05-12 21:56.</div>
    <div class="content">
     <p>I'll check back in a few hours then.</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div><a id="comment-21031"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/739#comment-21031" class="active">digiKam</a></h3>    <div class="submitted">Submitted by mike (not verified) on Tue, 2015-05-12 23:40.</div>
    <div class="content">
     <p>Just today discovered this software.  Would love to try it.  Need instructions on how to install it on my Windows7 system.  I assume a ready-to-run (already compiled) version is available.<br>
Thanks.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-21033"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/739#comment-21033" class="active"> a couple of days</a></h3>    <div class="submitted">Submitted by Alan Pater (not verified) on Wed, 2015-05-13 01:22.</div>
    <div class="content">
     <p>Check the download site in a couple of days for the newest Windows version. 4.9 is already there if you don't need the new features of 4.10.</p>
<p>Here: http://download.kde.org/stable/digikam/</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-21034"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/739#comment-21034" class="active">install digiKam</a></h3>    <div class="submitted">Submitted by mike (not verified) on Wed, 2015-05-13 21:49.</div>
    <div class="content">
     <p>Alan<br>
Thanks for your reply.  I now find windows version 4.10 "installer" on the cited page.  I also see there a version 4.10 .tar file.  Do I need to download both.  If so, where do they go?</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-21035"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/739#comment-21035" class="active">Windows installer exe...</a></h3>    <div class="submitted">Submitted by digiKam on Wed, 2015-05-13 22:11.</div>
    <div class="content">
     <p>...is a stand alone application...</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-21036"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/739#comment-21036" class="active">digiKam installation</a></h3>    <div class="submitted">Submitted by mike (not verified) on Thu, 2015-05-14 00:42.</div>
    <div class="content">
     <p>Just download and run digiKam-installer-4.10.0-win32.exe and I'm done??<br>
If so, that's great!</p>
<p>Thanks.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-21037"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/739#comment-21037" class="active">yes</a></h3>    <div class="submitted">Submitted by digiKam on Thu, 2015-05-14 06:59.</div>
    <div class="content">
     <p>...</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-21038"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/739#comment-21038" class="active">Broken on Windows 7</a></h3>    <div class="submitted">Submitted by goat (not verified) on Sat, 2015-05-16 23:40.</div>
    <div class="content">
     <p>Looks like that may not be the case.  I can't get it to function as designed on Windows 7 x64.  It seems to be related to thumbnails.  It works while it initially starts to build the thumbnails.digikam file, but never responds when you switch over to a screen in the program that may potentially have a thumbnail from one of your pictures.  This makes it pretty much useless as a photo manager since displaying photos makes it stop working.  The same issue existed in 4.9.0.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-21039"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/739#comment-21039" class="active">Doesnt work in win 8.1</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Sun, 2015-05-24 20:17.</div>
    <div class="content">
     <p>Doesnt work in win 8.1 either.</p>
<p>After you added a collection, then browse to album, it loads outlines of the images and then doesnt respond.<br>
After killing the program and process and restarting and try to import and it instantly crashes as well.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-21041"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/739#comment-21041" class="active">The crashing on import bug</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Mon, 2015-06-01 11:05.</div>
    <div class="content">
     <p>The crashing on import bug has been around for all of 4.x. Have submitted bug and logs, nothing is done. Quite a shame as import from memory card is a critical function of a photo management app!</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div></div></div></div></div></div><a id="comment-21040"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/739#comment-21040" class="active">I am new to digiKam.  I</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Sun, 2015-05-24 22:36.</div>
    <div class="content">
     <p>I am new to digiKam.  I downloaded the windows 4.10 version and installed it.  However, it will not load.  I have a toshiba, intel dual 2.0ghz, 4 gb ram and 1 TB hd.  64bit windows OS.  </p>
<p>Anybody have any suggestions on how to get this program to work?</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-21042"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/739#comment-21042" class="active">More info?</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Thu, 2015-06-04 23:34.</div>
    <div class="content">
     <p>Please give us more information: does the splash screen appear? Have you tried re-installing?</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div><a id="comment-21032"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/739#comment-21032" class="active">Thank you</a></h3>    <div class="submitted">Submitted by Pedro Lucas (not verified) on Wed, 2015-05-13 00:33.</div>
    <div class="content">
     <p>I'm just fan and I like to thank you for great job. You're amazing!</p>
         </div>
    <div class="links">» </div>
  </div>

</div>