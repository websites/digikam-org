---
date: "2006-07-21T17:34:00Z"
title: "digiKam 0.9.0-beta1 and digiKamImagePlugins 0.9.0-beta1 released"
author: "Marcel"
description: "The digiKam team is proud to announce the first beta release of digiKam 0.9.0 and digiKamimageplugins 0.9.0. The 0.9 series has been under heavy development"
category: "news"
aliases: "/node/130"

---

<p>The digiKam team is proud to announce the first beta release of<br>
digiKam 0.9.0 and digiKamimageplugins 0.9.0.</p>
<p>The 0.9 series has been under heavy development since November and this beta<br>
release offers you the opportunity to test and enjoy a variety of new<br>
features, additions and improvements.</p>
<p>The source for digikam and digikamimageplugins 0.9.0-beta1 is available from:<br>
<a href="http://sourceforge.net/project/showfiles.php?group_id=42641"><br>
    http://sourceforge.net/project/showfiles.php?group_id=42641<br>
</a></p>
<p><strong>A choice from the list of new features</strong></p>
<ul>
<li>the sidebar is now used to display image file properties, metadata, comments, tags and color information</li>
<li>full support for 16bit images in the image editor, all image plugins have been ported to make use of the new 16 bit infrastructure</li>
<li>full support for color management, reading and storing color profiles</li>
<li>greatly improved support for image metadata such as EXIF and IPTC (based on libexiv2)</li>
<li>the main view provides a fast preview mode most useful for RAW images</li>
<li>more than 100 bug fixes</li>
</ul>
<p>A comprehensive list of new features and fixed bugs can be found in the <a href="http://websvn.kde.org/trunk/extragear/graphics/digikam/NEWS?rev=564673&amp;view=markup"><br>
NEWS</a> file.</p>
<p>If you are interested in some of our plans for the future and in what is still missing for the final 0.9 release, have a look at the <a href="http://websvn.kde.org/trunk/extragear/graphics/digikam/TODO?rev=564483&amp;view=markup"> TODO</a> file.</p>
<p><strong>Digikam Movie Tour</strong></p>
<p>There are four demonstration videos (Flash format) available highlighting some of the new features.</p>
<ul>
<li>
How to import a RAW file in 16 bits color depth into digiKam image editor using Color Management:<a href="http://www.digikam.org/?q=node/124"><br>
http://www.digikam.org/?q=node/124<br>
</a>
</li>
<li>
On the usage of the camera interface and how to add IPTC information to an image file:<a href="http://www.digikam.org/?q=node/125"><br>
http://www.digikam.org/?q=node/125<br>
</a>
</li>
<li>
On the new fast RAW file preview mode, the use of color profiles, image plugins supporting 16 bit and user interface improvements for all image plugins:<a href="http://www.digikam.org/?q=node/126"><br>
http://www.digikam.org/?q=node/126<br>
</a>
</li>
<li>
How to use the black and white converter and the add border image plugin, again demonstrating user interface improvements and full 16 bit support:<a href="http://www.digikam.org/?q=node/127"><br>
http://www.digikam.org/?q=node/127<br>
</a>
</li>
</ul>
<p><strong>How to help us</strong></p>
<p>This release is a beta release, which means that we ask users to test this version of digiKam and report bugs.<br>
If you have found a bug, please report it to the bug tracking system at bugs.kde.org. In a first step, confirm that there is not already a bug report for your problem. A list of open bug reports for digikam can be found at <a href="http://bugs.kde.org/buglist.cgi?product=digikam&amp;bug_status=UNCONFIRMED&amp;bug_status=NEW&amp;bug_status=ASSIGNED&amp;bug_status=REOPENED"><br>
here</a>, and for digikamimageplugins <a href="http://bugs.kde.org/buglist.cgi?product=digikamimageplugins&amp;bug_status=UNCONFIRMED&amp;bug_status=NEW&amp;bug_status=ASSIGNED&amp;bug_status=REOPENED"> at this URL</a>.</p>

<div class="legacy-comments">

</div>