---
date: "2023-04-16T00:00:00"
title: "digiKam 8.0.0 is released"
author: "digiKam Team"
description: "Dear digiKam fans and users, we are proud to announce the stable release of digiKam 8.0.0."
category: "news"
---

[![](https://i.imgur.com/SlzCTWf.png "digiKam 8.0.0 First Run Assistant")](https://imgur.com/SlzCTWf)

Dear digiKam fans and users,

After two years of development and bugs triage, and tests, the digiKam team is proud to present the new major version 8.0.0 of its open source digital photo manager.

See below the list of most important features coming with this release.

### New Online Documentation

A huge Application as digiKam needs good documentation for end users, and is well written with plenty of screen-shots and screen-casts. We have been working many months to migrate and proof-read the old digiKam documentation based on [DocBook](https://en.wikipedia.org/wiki/DocBook) format to a new architecture, more simple, easy to maintain, and translatable.
After 20 years, we left the DocBook manual for the modern [Sphinx/ReStructuredText](https://www.sphinx-doc.org/) framework. This one is really a pleasure to use by documentation writers.

Application includes buttons and links everywhere as possible to guide end-users with the digiKam usages. This will open the online contents at the right section depending on the context.

The content is published in a [dedicated web site](https://docs.digikam.org/en/index.html), internationalized by the KDE translation teams. An [EPUB](https://docs.digikam.org/en/epub/DigikamManual.epub) version is also available for the off-line use cases.

The new documentation is open for contributions, to fix contents, translate, and add new sections/chapters. Please look at the [README](https://invent.kde.org/documentation/digikam-doc/-/blob/master/README.md) file where you will find all technical details to help us with this manual.

[![](https://i.imgur.com/6WLHPqp.png "The G'MIC-Qt tool section of the documentation")](https://imgur.com/6WLHPqp)

### File Format Support

Image support was greatly improved in this release with the important changes listed below.

The Libjasper codec version > 4.0 is supported to handle more exotic formats based on JPEG-2000 and to improve stability.

We add the TIFF 16 bits float (half-float) images encoding to support images generated with HDR feature by other photo management program.

The RAW file decoder Libraw have been updated to the last snapshot 20230403 with new camera and features:

* Phase One/Leaf IIQ-S v2.
* Canon CR3 filmrolls/RawBurst.
* Canon CRM (movie) files.
* Tiled bit-packed (and 16-bit unpacked) DNGs support.
* Non-standard Deflate-compressed integer DNG files support.
* Canon EOS R3, R7 and R10.
* Fujifilm X-H2S, X-T30 II.
* Olympus System OM-1.
* Leica M11.
* Sony A7-IV (ILCE-7M4).
* DJI Mavic 3.

The image formats JPEG-XL, WEBP, and AVIF can be used everywhere in digiKam to export your data using lossy compression or not. For example, when you import new items from a camera, a post-process conversion from JPEG to a safety lossless container can be done in the background. The Batch Queue Manager now supports these format as new codecs to convert your files. Finally, you can also store the versioned images in the Image Editor using these lossless formats, as with the Import Tool.

[![](https://i.imgur.com/hnZ1CEr.png "The Image Editor Versioning format from configuration dialog")](https://imgur.com/6WLHPqp)

### New OCR tool, Spell-checking, and Localize Settings

Furthermore, we added a new tool to [perform OCR over scanned text](https://community.kde.org/GSoC/2022/StatusReports/QuocHungTran). It's based on the very powerful multi-platforms and open-source Tesserac engine. The online documentation section of this new post processing tool for digiKam and Showfoto [can be read here](https://docs.digikam.org/en/post_processing/ocrtext_converter.html).

[![](https://i.imgur.com/d846qCp.png "The new OCR post processing tool")](https://imgur.com/d846qCp)

To improve the textual information experience everywhere in digiKam, spell-checking support has been added in the captions/information/properties edit-text widgets, along with a new Spell Check configuration panel in the Setup/Misc menu for digiKam and Showfoto. A new setting has been integrated to list preferred alternative or translated languages in the text edit widget.

[![](https://i.imgur.com/gQmM56e.png "The new Spell Check configuration page")](https://imgur.com/gQmM56e)

With these new options, this will allow users to check sentences automatically where textual contents can be entered, to translate
text automatically using an online translation engine and store the result in alternative-languages. All strings stored in metadata and editable in digiKam support these features.

[![](https://i.imgur.com/UjgKGtK.png "The new Localize configuration page")](https://imgur.com/UjgKGtK)

### Metadata and ExifTool Improvements

All bundles integrate ExifTool to 12.59 with fixes to detect this metadata engine properly at run-time. Performances to use ExifTool have been also improved, the goal in the long term is to replace Exiv2 library to play with metadata in critical use-cases.

[![](https://i.imgur.com/0YZbdHm.png "Metadata Setup Page with ExifTool Engine write options")](https://imgur.com/0YZbdHm)

A new option to write metadata to files with ExifTool back-end has been included in digiKam Metadata configuration page. Optional write metadata operations to DNG and RAW files are now always delegated to ExifTool, as Exiv2 library is not able since a very long time to support these formats safely.

The Metadata advanced configuration page adds new options to save and load configuration profiles. This allows to configure digiKam to be interoperable with other photo management programs by tuning the place where information must be read and
saved in metadata chunks from images. digiKam proposes a profile to work with DarkTable software. See the Metadata section from the [digiKam online documentation](https://docs.digikam.org/en/setup_application/metadata_settings.html) for details.

The Metadata Editor dialog receives layout optimizations for a better usability, especially with small screens. As this tool interface includes a lot of information, we take care about the ergonomy to improve the user workflow while editing image properties. This post processing tool is now fully described in the [digiKam online documentation](https://docs.digikam.org/en/post_processing/metadata_editor.html)

The Batch Queue Manager gains 2 new tools to assign properties to items while your workflow. The first allows to set the rating, pick, and color labels values, and the other one to handle titles and captions string with alternative languages, translations, and spell-checking support. The Batch Queue Manager is fully described in the [digiKam online documentation](https://docs.digikam.org/en/batch_queue.html).

[![](https://i.imgur.com/RCtjZyZ.png "The new Batch Queue Manager tool to Translate Metadata")](https://imgur.com/RCtjZyZ)

### Item Renaming Engine and Usability Improvements.

The Advanced Rename tool available in Icon-View, Batch-Queue-Manager, and Import tool, support for increment file counter to sequence numbering with option [c] or [c,step], support for random text sequence numbering with option [r], as well as the ability to configure the first character for the unique modifier with {unique:n,c}. The Advanced Rename tool is fully described in the [digiKam online documentation](https://docs.digikam.org/en/main_window/image_view.html#renaming-a-photograph).

[![](https://i.imgur.com/Et6K1BZ.png "Advanced Rename Tool with the context helper dialog")](https://imgur.com/Et6K1BZ)

The usability of the application has been also improved by adding a new hamburger menu to the toolbar that will appear only when the main menu is hidden or when it switches to the full-screen mode. We also introduce a new Icon-View settings to customize Labels visibility over thumbnails.

[![](https://i.imgur.com/slrKtUu.png "The new hamburger menu available on the right side if main menu is disabled")](https://imgur.com/slrKtUu)

### Search Engine, Database, and Collections Options

The database search engine adds 2 new features to let you search for items by the number of face regions or without face regions. This information is populated in the database by the face workflow deep learning based engine. The Advanced Search tool is fully described in the [digiKam online documentation](https://docs.digikam.org/en/main_window/search_view.html).

[![](https://i.imgur.com/hPVbqV5.png "Advanced Search Dialog with face query options")](https://imgur.com/hPVbqV5)

We also added support for cross-platform network paths in Collections to register remote storage hosted by different operating systems. We also add support for the new SQLite WAL (Write-Ahead-Logging) mode to the main database. This will improve the robustness of the Sqlite container in time. Both are explained in the digiKam online documentation [here](https://docs.digikam.org/en/setup_application/collections_settings.html#the-network-shares-specificity) and [here](https://docs.digikam.org/en/setup_application/database_settings.html#the-sqlite-database).

[![](https://i.imgur.com/d6s2uzB.png "SQlite database settings including WAL option")](https://imgur.com/d6s2uzB)

### Image Quality Classifier Gain With a Neural Network Engine

digiKam adds the ability to use global or customized Image Quality Sorting settings everywhere. The settings now a Aesthetic Detection to classify images using deep learning models instead to tune plenty of parameters. In other words, you can [delegate to an IA](https://community.kde.org/GSoc/2022/StatusReports/PhuocKhanhLe) the way to categorize the quality of your images automatically, based on contents, design, position of subjects, etc.

The Image Quality Sorting is now integrated into the Batch Queue Manager to be used in your workflow.

[![](https://i.imgur.com/lYLcJAq.png "Batch Queue Manager workflow including the Image Quality Sort tool")](https://imgur.com/lYLcJAq)

See this [digiKam online documentation](https://docs.digikam.org/en/maintenance_tools/maintenance_quality.html) section for the details to configure the new Image Quality Sort tool to maintain your collections.

### Better Integration with G'MIC-Qt 3.2.2

The famous G'MIC-Qt tool is now updated to last version 3.2.2 and is better integrated in digiKam. We resolved the stability problem of the tool under Windows and MacOS, and it's now safely usable in Image Editor to apply filters on your photos. As this tool is very powerful but a little bit complex to use, it's now well documented in [digiKam online manual](https://docs.digikam.org/en/image_editor/enhancement_tools.html#g-mic-qt-tool).

[![](https://i.imgur.com/UAdythF.png "The Image Editor running G'MIC-Qt tool")](https://imgur.com/UAdythF)

### Qt6 Support in Source Code

All the 1.5M lines of C++ code are now ported to the Qt6 framework and still compatible with Qt5 LTS. For the moment, Qt5 is still primarily in use to publish official bundles, as few external and important dependencies are not yet ported to Qt6 as Marble for the geo-location support.

### Maintenance and Bugs Triage

This new version arrives with more than
[420 triaged and closed files in bugzilla](https://bugs.kde.org/buglist.cgi?f1=cf_versionfixedin&limit=0&o1=equals&order=bug_id&product=digikam&v1=8.0.0).

The application internationalization has also been updated. digiKam and Showfoto are proposed with 57 different languages for the graphical interface. Go to Settings/Configure Languages dialog and change the localization as you want. Applications need to be restarted to apply changes. If you want to contribute to the internationalization of digiKam, please contact the [translator teams](https://l10n.kde.org/), following the translation [how-to](https://l10n.kde.org/docs/translation-howto/). The statistics about translation states are available
[here](https://l10n.kde.org/stats/gui/trunk-kf5/po/#digikam).

[![](https://i.imgur.com/XfMEF5v.jpg "The Languages selection dialog")](https://imgur.com/XfMEF5v)

### Future Plans and Final Words

digiKam 8.0.0 entered a new live cycle with this major release. We will focus on the switch from Qt5 to Qt6 and integrate huge pending work on Similarity Search Engine proposed by new contributors. We plan also to integrate G'MIC-Qt in the Batch Queue Manager.

Thanks to all users for [your support and donations](https://www.digikam.org/donate/),
and to all contributors, students, testers who allowed us to improve this release.

digiKam 8.0.0 can be downloaded from [this repository](https://download.kde.org/stable/digikam/8.0.0/) as:

- Source code tarball.
- Linux 64 bits AppImage bundles compatible with systems based on glibc >= 2.27.
- Windows 64 bits installers or bundle archives.
- macOS Intel packages compatible with Apple M1 and M2 CPU computers using [Rosetta 2](https://support.apple.com/en-us/HT211861),

[![](https://i.imgur.com/oHLv6Sc.jpg "The Welcome page with backgroung photo credits")](https://imgur.com/oHLv6Sc)

Rendez-vous in a few weeks for the next digiKam 8.1.0 release.

Happy digiKaming.
