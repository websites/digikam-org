---
date: "2008-08-01T12:30:00Z"
title: "digiKam 0.10.0-beta2 release for KDE4"
author: "digiKam"
description: "Dear all digiKam fans and users! The digiKam development team is happy to release the 2nd beta release dedicated to KDE4. The digiKam tarball can"
category: "news"
aliases: "/node/363"

---

Dear all digiKam fans and users!<br><br>

The digiKam development team is happy to release the 2nd beta release dedicated to KDE4. The digiKam tarball can be downloaded from <a href="http://sourceforge.net/project/showfiles.php?group_id=42641">SourceForge</a>.

<br>
<a href="http://www.flickr.com/photos/digikam/2722226408/" title="digiKam under KDE 4.1"><img src="http://farm4.static.flickr.com/3050/2722226408_82efa216dd.jpg" width="500" height="125" alt="digiKam under KDE4.1"></a>
<br>

<p>Take care, it's always a BETA code with many of new bugs not yet fixed... To resume:</p>
 
<p></p><h1><b>"DO NOT USE YET IN PRODUCTION"</b></h1><p></p>
 
<p>... for production, use KDE3 with 0.9.x, which is still mantained and <a href="http://www.digikam.org/drupal/node/359">0.9.4 stable release</a> have been just published.</p>

<p>To compile this KDE4 version, you need libkexiv2, libkdcraw, and libkipi for KDE4. These libraries are now included in KDE 4.1 by default. There is no more separate tarball planed for these libraries.</p>
 
<p>Note for KDE 4.0.x users : you can extract libraries source code from svn, and compile all as well. It does work fine. You don't need especially KDE 4.1.x code for the moment. Look at bottom of <a href="http://www.digikam.org/drupal/download?q=download/svn">this page</a> for details</p>

<p>For others depencies, consult the README file. There are also optional depencies to enable some new features as lensfun for lens auto-correction, and marble for geolocation.</p>
 
<p>The digikam.org web site has been redesigned for the new KDE4 series. All <a href=" http://www.digikam.org/drupal/node/323">screenshots</a> have been updated and a lots of <a href="  http://www.digikam.org/drupal/tour">video demos</a> have been added to present new features included in this version.</p>
 
<h5>NEW FEATURES (since 0.9.x series):</h5><br>

<b>General</b> : Ported to CMake/Qt4/KDE4.<br>
<b>General</b> : Thumbs KIO-Slave removed. digiKam now use multi-threading to generate thumnails.<br>
<b>General</b> : Removed all X11 library dependencies. Code is now portable under MACOS-X and Win32.<br>
<b>General</b> : Support of XMP metadata (require <a href="http://www.exiv2.org">Exiv2 library</a> &gt;= 0.16).<br>
<b>General</b> : Hardware handling using KDE4 Solid interface.<br>
<b>General</b> : Preview of Video and Audio files using KDE4 Phonon interface.<br>
<b>General</b> : Database file can be stored on a customized place to support remote album library path.<br>
<b>General</b> : New database schema to host more photo and collection informations.<br>
<b>General</b> : Database interface fully re-written using Qt4 SQL plugin.<br>
<b>General</b> : Support of multiple roots album paths.<br>
<b>General</b> : Physical root albums are managed as real album.<br>
<b>General</b> : New option in Help menu to list all RAW file formats supported.<br>
<b>General</b> : Geolocation of pictures from sidebars is now delegate to KDE4 Marble widget.<br>
<b>General</b> : New option in Help menu to list all main components/libraries used by digiKam.<br><br>
 
<b>CameraGUI</b> : New design for camera interface.<br>
<b>CameraGUI</b> : New Capture tool.<br>
<b>CameraGUI</b> : New bargraph to display camera media free-space.<br><br>
 
<b>AlbumGUI</b> : Added Thumbbar with Preview mode to easy navigate between pictures.<br>
<b>AlbumGUI</b> : Integration of Simple Text Search tool to left sidebar as Amarok.<br>
<b>AlbumGUI</b> : New advanced Search tools. Re-design of Search backend, based on XML. Re-design of search dialog for a better usability. Searches based on metadata and image properties are now possible.<br>
<b>AlbumGUI</b> : New fuzzy Search tools based on sketch drawing template. Fuzzy searches backend use an Haar wevelet interface. You simply draw a rough sketch of what you want to find and digiKam displays for you a thumbnail view of the best matches.<br>
<b>AlbumGUI</b> : New Search tools based on marble widget to find pictures over a map.<br>
<b>AlbumGUI</b> : New Search tools to find similar images against a reference image.<br>
<b>AlbumGUI</b> : New Search tools to find duplicates images around whole collections.<br><br>
 
<b>ImageEditor</b> : Added Thumbbar to easy navigate between pictures.<br>
<b>ImageEditor</b> : New plugin based on <a href="http://lensfun.berlios.de">LensFun library</a> to correct automaticaly lens aberrations.<br>
<b>ImageEditor</b> : LensDistortion and AntiVignetting are now merged with LensFun plugin.<br>

<h5>BUGS FIXES:</h5><br>

001 ==&gt; 146864 : Lesser XMP support in digiKam.<br>
002 ==&gt; 145096 : Request: acquire mass storage from printer as from camera. Change menu "Camera" to "Acquire".<br>
003 ==&gt; 134206 : Rethink about: Iptc.Application2.Urgency digiKam Rating.<br>
004 ==&gt; 149966 : Alternative IPTC Keyword Separator (dot notation).<br>
005 ==&gt; 129437 : Album could point to network path. Now it's impossible to view photos from shared network drive.<br>
006 ==&gt; 137694 : Allow album pictures to be stored on network devices.<br>
007 ==&gt; 114682 : About library path.<br>
008 ==&gt; 122516 : Album Path cannot be on Network device (Unmounted).<br>
009 ==&gt; 107871 : Allow multiple album library path.<br>
010 ==&gt; 105645 : Impossible to not copy images in ~/Pictures.<br>
011 ==&gt; 132697 : Metadata list has no scrollbar.<br>
012 ==&gt; 148502 : Show rating in embedded preview / slideshow.<br>
013 ==&gt; 155408 : Thumbbar in the album view.<br>
014 ==&gt; 138290 : GPSSync plugin integration in the side bar.<br>
015 ==&gt; 098651 : Image Plugin filter based on clens.<br>
016 ==&gt; 147426 : Search for non-voted pics.<br>
017 ==&gt; 149555 : Always present search box instead of search by right-clicking and selecting simple or advanced search.<br>
018 ==&gt; 139283 : IPTC Caption comment in search function.<br>
019 ==&gt; 150265 : Avanced search filter is missing search in comment / description.<br>
020 ==&gt; 155735 : Make it possible to search on IPTC-text.<br>
021 ==&gt; 147636 : GUI error in advanced searches: lots of free space.<br>
022 ==&gt; 158866 : Advanced Search on Tags a mess.<br>
023 ==&gt; 149026 : Search including sub-albums.<br>
024 ==&gt; 153070 : Search for image by geo-location.<br>
025 ==&gt; 154764 : Pictures saved into root album folder are not shown.<br>
026 ==&gt; 162678 : digiKam crashed while loading.<br>
027 ==&gt; 104067 : Duplicate image finder should offer more actions on duplicate images found.<br>
028 ==&gt; 107095 : Double image removal: Use trashcan.<br>
029 ==&gt; 112473 : findimages shows only small thumbnails.<br>
030 ==&gt; 150077 : Find Duplicate Images tool quite unusable on many images (a couple of issues).<br>
031 ==&gt; 161858 : Find Duplicate Image fails with Canon Raw Files.<br>
032 ==&gt; 162152 : Batch Duplicate Image Management.<br>
033 ==&gt; 164418 : GPS window zoom possibility.<br>
034 ==&gt; 117287 : Search albums on read only album path.<br>
035 ==&gt; 164600 : No picture in view pane.<br>
036 ==&gt; 164973 : Showfoto crashed at startup.<br>
037 ==&gt; 165275 : build-failure - imageresize.cpp - 'KToolInvocation' has not been declared.<br>
038 ==&gt; 165292 : albumwidgetstack.cpp can't find Phonon/MediaObject.<br>
039 ==&gt; 165341 : Crash when changing histogram channel when welcome page is shown.<br>
040 ==&gt; 165318 : digiKam doesn't start because of SQL.<br>
041 ==&gt; 165342 : Crash when changing album sort modus.<br>
042 ==&gt; 165338 : Right sidebar initally too big.<br>
043 ==&gt; 165280 : Sqlite2 component build failure.<br>
044 ==&gt; 165769 : adjustcurves.cpp - can't find version.h.<br>
045 ==&gt; 166472 : Thumbnail bar gone in image editor when switching back from fullscreen.<br>
046 ==&gt; 166663 : Tags not showing pictures without "Include Tag Subtree".<br>
047 ==&gt; 166616 : Filmstrip mode in View.<br>
048 ==&gt; 165885 : Thumbnails and images are NOT displayed in the main view center pane.<br>
049 ==&gt; 167139 : Crash if Exif.GPSInfo.GPSAltitude is empty.<br>
050 ==&gt; 167168 : Timeline view shows redundant extra sections.<br>
051 ==&gt; 166440 : Removing images from Light Table is not working properly.<br>
052 ==&gt; 166484 : digiKam crashes when changing some settings and using "find similar" after the changes made.<br>
053 ==&gt; 167124 : Timeline view not updated when changing selected time range.<br>

054 ==&gt; 166564 : Display of *already* *created* thumbnails is slow.<br>
055 ==&gt; 166483 : Error in "not enough disk space" error message.<br>
056 ==&gt; 167379 : Image selection for export plugin.<br>
057 ==&gt; 166622 : Confusing Add Images use.<br>
058 ==&gt; 166576 : digiKam quits when running "Rebuild all Thumbnail Images" twice.<br>
059 ==&gt; 167562 : Image Editor Shortcut Keys under Edit redo/undo are missing in GUI.<br>
060 ==&gt; 167561 : Crash when moving albums.<br>
061 ==&gt; 167529 : Image Editor not working correctly after setting "use horizontal thumbbar" option.<br>
062 ==&gt; 167621 : digiKam crashes when trying to remove a tag with the context menu.<br>
063 ==&gt; 165348 : Problems when trying to import KDE3 data.<br>
064 ==&gt; 166424 : Crash when editing Caption with Digikam4 SVN.<br>
<div class="legacy-comments">

  <a id="comment-17683"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/363#comment-17683" class="active">digiKam 0.10.0-beta2 release for KDE4</a></h3>    <div class="submitted">Submitted by Stefan Waidheim (not verified) on Thu, 2008-08-07 03:24.</div>
    <div class="content">
     <p>It looks and runs very nice, indeed. I especially like the dark theme - helps you focus on the work.<br>
In my opinion it's almost done - if it wasn't for two tiny little things(tm)... ;-)<br>
First is a long-standing bug since the KDE3 times: digiKam is unable to "see" image files without a known filename extension (like "2008-03-01.1201001", "2008-03-01.1201002", "image 0001", "pic.000", "pic.001" ... you get the idea). Even when you enter "*" in the config dialog field it just ignores them. This is really bad, since I get most of my images from two big readonly nfs mounts so renaming is out of the question. Atm i use a bunch of symlinks but this is an ugly hack and neither scales nor updates itself.<br>
Second is a little wish: Would it be possible to enable the single-click-selection like in Dolphin for digiKam too? Is this a Dolphin-only feature or in kdelibs?</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-17685"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/363#comment-17685" class="active">It looks really good, it's a</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Fri, 2008-08-08 21:30.</div>
    <div class="content">
     <p>It looks really good, it's a pity the debian package is not working at all, and I didn't manage to compile it myself :(</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-17727"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/363#comment-17727" class="active">after adding an deb-src line</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Wed, 2008-09-03 18:08.</div>
    <div class="content">
     <p>after adding an deb-src line for experimental doing an apt-get -t experimental build-deps digikam followed by typing the commands in the tarballs's README did the trick here - even worked with checkinstall</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div><a id="comment-17911"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/363#comment-17911" class="active">It looks really good, it's a</a></h3>    <div class="submitted">Submitted by red (not verified) on Sat, 2008-11-01 11:00.</div>
    <div class="content">
     <p>It looks really good, it's a pity the debian package is not working at all, and I didn't manage to compile it myself thank you </p>
<p> <a href="http://www.trsohbet.name">sohbet</a></p>
         </div>
    <div class="links">» </div>
  </div>

</div>