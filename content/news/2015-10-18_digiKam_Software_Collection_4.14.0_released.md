---
date: "2015-10-18T10:20:00Z"
title: "digiKam Software Collection 4.14.0 released..."
author: "digiKam"
description: "Dear digiKam fans and users, The digiKam Team is proud to announce the release of digiKam Software Collection 4.14.0. This version is the result of"
category: "news"
aliases: "/node/745"

---

<a href="https://www.flickr.com/photos/digikam/22279631841/in/dateposted-public/" title="digikam4.14.0-2"><img src="https://farm1.staticflickr.com/761/22279631841_d998b29747_c.jpg" width="800" height="225" alt="digikam4.14.0-2"></a>

<p>Dear digiKam fans and users,</p>

<p>
The digiKam Team is proud to announce the release of digiKam Software Collection 4.14.0. 
This version is the result of another bugs triage on KDE bugzilla where more than 20 files have been closed.
Thanks to <a href="https://plus.google.com/u/0/107171232114475191915/about">Maik Qualmann</a> who maintain KDE4 version.
</p>

<p>
This release will be the last stable release dedicated from 4.x series. We are now focused on next 5.0.0 release planed on spring 2016. This next main version will support Qt5 and KF5. The first beta1 will be released soon.
</p>

<p>In this release, OpenCV 3.x is now supported for face management and others tools based on OpenCV algorithm. SOme work have been done about overlay icon to improve usability. Icons are now re-sized accordingly with thumbnail sizes, and a new icon have been add to quickly run slideshow in full-screen mode.</p>
<p>
For more details, you can consult the huge list of the <a href="https://bugs.kde.org/buglist.cgi?f1=cf_versionfixedin&amp;o1=equals&amp;query_format=advanced&amp;bug_status=RESOLVED&amp;bug_status=NEEDSINFO&amp;bug_status=VERIFIED&amp;bug_status=CLOSED&amp;v1=4.14.0&amp;product=digikam&amp;product=digikamimageplugins&amp;product=kipiplugins&amp;product=showfoto">issues closed</a> in digiKam 4.14.0 available through the KDE Bugs-tracking System.
</p>

<p>The digiKam software collection can be downloaded from the <a href="http://download.kde.org/stable/digikam/">KDE mirrors</a>.
</p>

<p>Have fun to use this new important digiKam stable release.</p>

<p>digiKam Team...</p>
<div class="legacy-comments">

  <a id="comment-21108"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/745#comment-21108" class="active">PKG Version?</a></h3>    <div class="submitted">Submitted by <a href="http://www.etiennebretteville.com" rel="nofollow">Etienne</a> (not verified) on Mon, 2015-10-19 12:27.</div>
    <div class="content">
     <p>Hye,<br>
This time you're not providing a .pkg mac file?<br>
I wasn't able to make the last pkg runs on macosx el capitan but I was wondering if this one should work.</p>
<p>Genrally, is the 5 version focused on making digikam work on debian and macosx?!</p>
<p>Thanks for your reply</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-21110"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/745#comment-21110" class="active">macports broken...</a></h3>    <div class="submitted">Submitted by digiKam on Mon, 2015-10-19 22:13.</div>
    <div class="content">
     <p>Currently it's impossible to build KDE4 under Macports because both Qt4 and Qt5 are installed while the process, which is not compatible...</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-21111"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/745#comment-21111" class="active">Thanks for your reply, I gues</a></h3>    <div class="submitted">Submitted by <a href="http://www.etiennebretteville.com" rel="nofollow">Etienne</a> (not verified) on Wed, 2015-10-21 14:41.</div>
    <div class="content">
     <p>Thanks for your reply, I gues the latest comment is the same as mine, windows users feels a bit left backyard.</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-21128"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/745#comment-21128" class="active">I was able to get it build</a></h3>    <div class="submitted">Submitted by David (not verified) on Thu, 2015-10-29 16:13.</div>
    <div class="content">
     <p>I was able to get it build using this patch to bootstrap.macports:</p>
<p><code><br>
37,38c37,38<br>
&lt; export QTDIR=${INSTALL_PREFIX}/lib<br>
&lt; export QT_INCLUDE_DIR=${INSTALL_PREFIX}/include<br>
---<br>
&gt; export QTDIR=/opt/local/libexec/qt4<br>
&gt; export QT_INCLUDE_DIR=/opt/local/libexec/qt4/include<br>
76d75<br>
&lt;       -Wno-dev \<br>
83,85c82,84<br>
&lt;       -DQT_QT_INCLUDE_DIR=${INSTALL_PREFIX}/include \<br>
&lt;       -DQT_LIBRARY_DIR=${INSTALL_PREFIX}/lib \<br>
&lt;       -DQT_QMAKE_EXECUTABLE=${INSTALL_PREFIX}/bin/qmake \<br>
---<br>
&gt;       -DQT_QT_INCLUDE_DIR=${QT_INCLUDE_DIR} \<br>
&gt;       -DQT_LIBRARY_DIR=${QTDIR}/lib \<br>
&gt;       -DQT_QMAKE_EXECUTABLE=${QTDIR}/bin/qmake \<br>
</code></p>
         </div>
    <div class="links">» </div>
  </div>
</div></div><a id="comment-21109"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/745#comment-21109" class="active">Fine about 4.14.0. but what</a></h3>    <div class="submitted">Submitted by H.Rekki (not verified) on Mon, 2015-10-19 16:31.</div>
    <div class="content">
     <p>Fine about 4.14.0. but what about windows version?</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-21117"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/745#comment-21117" class="active">Windows users seems to be</a></h3>    <div class="submitted">Submitted by Manu (not verified) on Sat, 2015-10-24 15:09.</div>
    <div class="content">
     <p>Windows users seems to be forgotten, no 4.13, no 4.14.  Will we have to wait for version 5?<br>
Thanks in advance to the windows builder.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-21125"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/745#comment-21125" class="active">It is pity. I has intrested</a></h3>    <div class="submitted">Submitted by Heikki (not verified) on Wed, 2015-10-28 12:22.</div>
    <div class="content">
     <p>It is pity. I has intrested about this software, but now is goes, like we finns says: it goes down like the tail of the cow.</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-21136"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/745#comment-21136" class="active">Don't complain. Are you</a></h3>    <div class="submitted">Submitted by KamilS (not verified) on Tue, 2015-11-03 12:00.</div>
    <div class="content">
     <p>Don't complain. Are you missing any features or are you bitten by any bugs?<br>
I'm on Kubuntu 15.10 myself which was just released and digiKam comes in version 4.12</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-21138"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/745#comment-21138" class="active">I'm working on packages of</a></h3>    <div class="submitted">Submitted by philip5 (not verified) on Fri, 2015-11-06 00:20.</div>
    <div class="content">
     <p>I'm working on packages of Digikam 4.14 for Ubuntu 15.10 (wily) right now and also bringing back the geo-location and map features and some other dependent packages updates and fixes for Wily users. Will upload it to my PPA as soon as possible, maybe in the next few days. Have been busy with life and haven't had time for this last two weeks.</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div></div><a id="comment-21112"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/745#comment-21112" class="active">Thank you</a></h3>    <div class="submitted">Submitted by <a href="http://www.researchut.com" rel="nofollow">Ritesh Raj Sarraf</a> (not verified) on Wed, 2015-10-21 14:51.</div>
    <div class="content">
     <p>You guys are the best. There's nothing that I've used before that is as good as Digikam. Thank you.</p>
<p>Just a question: Will the format of the metadata stored/written be same and compatible across Digikam4 and Digkam5 ?</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-21113"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/745#comment-21113" class="active">yes</a></h3>    <div class="submitted">Submitted by digiKam on Wed, 2015-10-21 14:55.</div>
    <div class="content">
     <p>nothing has changed...</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-21115"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/745#comment-21115" class="active">bundled plugins</a></h3>    <div class="submitted">Submitted by <a href="http://www.researchut.com" rel="nofollow">Ritesh Raj Sarraf</a> (not verified) on Thu, 2015-10-22 23:56.</div>
    <div class="content">
     <p>Hello,</p>
<p>Up till 4.13.0, the plugins were bundled as part of digikam in extra/ folder.<br>
IN 4.14.0, that is no more the case. Is it intentional ? Is digikam, henceforth, not going to ship those libraries in its archive ?</p>
<p>Nothing about this change seems to be mentioned in the release announcement.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-21116"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/745#comment-21116" class="active">All libs are packaged separatly now...</a></h3>    <div class="submitted">Submitted by digiKam on Fri, 2015-10-23 05:18.</div>
    <div class="content">
     <p>All libs are packaged separately now, at each KDE release. This is why libraries are not included into the digiKam tarball.</p>
<p>kipi-plugins still released into digiKam tarball anyway.</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div></div><a id="comment-21118"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/745#comment-21118" class="active">Digikam for Kubuntu 15.10</a></h3>    <div class="submitted">Submitted by Schlossfreund (not verified) on Sat, 2015-10-24 22:52.</div>
    <div class="content">
     <p>Digikam (it's only 14.12) crashes in the brandnew 15.10 release from Kubuntu. The error is: symbol lookup error: /usr/lib/libkexiv2.so.11: undefined symbol: _ZN5Exiv213XmpProperties10registerNsERKNSt7__cxx1112basic_stringIcSt11char_traitsIcESaIcEEES8_<br>
I know such an error came up in earlier releases of Kubuntu and Philip created a ppa, and then it was solved. Will Philip provide a ppa for wily?<br>
Thanks so much for the great work you are doing with digikam!</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-21119"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/745#comment-21119" class="active">when you upgraded to ubuntu</a></h3>    <div class="submitted">Submitted by philip5 (not verified) on Sat, 2015-10-24 23:19.</div>
    <div class="content">
     <p>when you upgraded to ubuntu 15.10 digikam must have changed so what you have installed doesn't match when it comes to digikam and libkexiv2. check so the installed packages come from the same source, ie not my PPA and then the official ubuntu source for digikam wily packages.</p>
<p>Yes I will provide digikam packages for wily too and hopefully give back geolocation support in digikam as it's disabled in official digikam packages for wily due to changes in how the marble packages are built that digikam use for geolocation and map features. My priority right now is to get working digikam 5 beta packages ready to be released when official digikam 5 beta1 is released then I will make digikam 4.14 for wily but that will be in a week or so.</p>
<p>/Philip</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-21120"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/745#comment-21120" class="active">libkgeomap</a></h3>    <div class="submitted">Submitted by <a href="http://www.researchut.com" rel="nofollow">Ritesh Raj Sarraf</a> (not verified) on Sun, 2015-10-25 10:12.</div>
    <div class="content">
     <p>Where can I download libkgeomap from ?</p>
<p>Almost all the link are broken or outdated.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-21121"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/745#comment-21121" class="active">All libs are in KDE download repository...</a></h3>    <div class="submitted">Submitted by digiKam on Sun, 2015-10-25 10:15.</div>
    <div class="content">
     <p>http://download.kde.org/stable/applications/15.08.2/src/</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div><a id="comment-21122"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/745#comment-21122" class="active">I had a similar problem after</a></h3>    <div class="submitted">Submitted by Rob (not verified) on Mon, 2015-10-26 14:56.</div>
    <div class="content">
     <p>I had a similar problem after installing the vivid version from the ppa then uninstalling to go back to the Kubuntu universe packages.  I found that there were several packages from the ppa that I had to manually downgrade.  (used "apt list --installed | grep vivid" to find them)</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-21123"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/745#comment-21123" class="active">change geolocalisation of a picture</a></h3>    <div class="submitted">Submitted by Thibaut (not verified) on Mon, 2015-10-26 22:40.</div>
    <div class="content">
     <p>Hi, thank you for digikam, it's a great software I'm using all the time.</p>
<p>I'm using Digikam 4.13.0 with fedora 22. I can't find the way to change the geolocalisation (lat/lng) for a picture anymore. Did it disappear from digikam?</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-21124"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/745#comment-21124" class="active">no...</a></h3>    <div class="submitted">Submitted by digiKam on Mon, 2015-10-26 22:42.</div>
    <div class="content">
     <p>No... geolocation is optional at compilation time. Sound like a problem with Fedora packages</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-21126"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/745#comment-21126" class="active">The problem with the latest</a></h3>    <div class="submitted">Submitted by philip5 (not verified) on Wed, 2015-10-28 20:30.</div>
    <div class="content">
     <p>The problem with the latest releases of linux distributions like Fedora 22 and ubuntu 15.10 is that they package and build Marble (which is a KDE framework that Digikam uses for the geolocation and map feature) based on QT5 and Digikam 4.x doesn't work with QT5 but QT4 which previous versions of Linux distribution have been using. This will instead work better with coming Digikam 5. That's why geolocation have been disabled in those new versions of Linux distributions and Digkam 4.x.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-21133"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/745#comment-21133" class="active">I agree with the comment</a></h3>    <div class="submitted">Submitted by <a href="http://www.researchut.com" rel="nofollow">Ritesh Raj Sarraf</a> (not verified) on Sun, 2015-11-01 05:01.</div>
    <div class="content">
     <p>I agree with the comment here. For 4.x, you should have bundled all the libraries as you had been doing in the last releases. I fail to see what benefit you got by dropping those libraries, in the so called last 4.x stable release.</p>
<p>For Debian, I'm still waiting for libkface to enter unstable, and my guess is that it is being held off for similar reasons.</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div><a id="comment-21127"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/745#comment-21127" class="active">please where is the version</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Thu, 2015-10-29 12:20.</div>
    <div class="content">
     <p>please where is the version for x64 windows?</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-21129"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/745#comment-21129" class="active">nowhere... Digikam is mostly</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Fri, 2015-10-30 15:23.</div>
    <div class="content">
     <p>nowhere... Digikam is mostly for users of Ubuntu.</p>
<p>"Be careful as digiKam is not as stable under Windows as under Linux because of some bugs in the underlying KDE SC libraries that digiKam depends on."</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-21130"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/745#comment-21130" class="active">*Ubuntu-&gt;Linux</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Fri, 2015-10-30 15:24.</div>
    <div class="content">
     <p>*Ubuntu-&gt;Linux</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-21132"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/745#comment-21132" class="active">Dear Digikam team,
It appears</a></h3>    <div class="submitted">Submitted by Manu (not verified) on Sat, 2015-10-31 17:46.</div>
    <div class="content">
     <p>Dear Digikam team,</p>
<p>It appears that this anonymous user is right, Digikam is not for Windows users!<br>
Should we (all the windows users) consider this as the new strategy of the Development team: only have linux versions.  In that case, just let us know and we will look for another software!</p>
<p>Regards.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-21135"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/745#comment-21135" class="active">Hi All,
I'm using Digikam</a></h3>    <div class="submitted">Submitted by Sören (not verified) on Tue, 2015-11-03 09:08.</div>
    <div class="content">
     <p>Hi All,<br>
I'm using Digikam 4.12.0 on Windows 7 and it works excellent. I guess we just have to be a bit patient for further Windows updates which is not really a serious problem, isn't it?<br>
All the best.</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-21137"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/745#comment-21137" class="active">Sorry, but Windows users are screwed ....</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Thu, 2015-11-05 10:56.</div>
    <div class="content">
     <p>The one thing that I liked about Open Source projects, like this; gave Windows users access to freely published software. Which breaks the Oh So Old, Micro-Monopoly. Sorry to say, but it looks like your going to have to go back to the paid software like Photoshop again.</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div><a id="comment-21134"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/745#comment-21134" class="active">Mac Version Update</a></h3>    <div class="submitted">Submitted by Geoff (not verified) on Sun, 2015-11-01 14:32.</div>
    <div class="content">
     <p>I've been trying the Mac version 4.13.0 for Yosemite quite a bit recently and its working great.  I have 21,000 photos and have not had a crash after many imports the last day.  There are a couple things not perfect, but those seem to be minor and overall it is really looking good.  I do have good backups just incase.  There is a big difference in stability from just a few versions ago (probably due to the upgraded exiv2 library).  I've reported one bug and will do a few more.  I'm hoping to switch out of Aperture (will need to eventually) soon and come back go Digikam. </p>
<p>It appears that the dev intend to work towards better Mac and Windows versions.  Going forward It would be appreciated if the dev could better communicate to us users what the status of these builds are for Mac and Windows, and what the timeframe is for upgrades.  Will this the support for Mac still be coming in the 5.0 version?   Will there be a specific El Cap version - or does anyone know if the Yosemite version works fine on El Cap?</p>
<p>Thanks, Geoff</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-21141"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/745#comment-21141" class="active">It take a while to make MAC version...</a></h3>    <div class="submitted">Submitted by digiKam on Sat, 2015-11-07 11:38.</div>
    <div class="content">
     <p>Compiling MAC/OSX PKG take a while, due to huge changes in Macports to support Qt4 and Qt5 at the same time.</p>
<p>PKG will be available when all will be tested and verified. Not before. It's the rules.</p>
<p>For Windows, packaging is delegate to a developer who is busy currently. You must be patient....</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-21139"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/745#comment-21139" class="active">Support  for NIKON D3300 (raw files)</a></h3>    <div class="submitted">Submitted by Guido Borgna (not verified) on Sat, 2015-11-07 11:31.</div>
    <div class="content">
     <p>Hi all,<br>
first of all, thank you very much for the excellent software you are developing.<br>
I have a question about my new Nikon D3300; I have currently Digikam 4.14.0 on Ubuntu 14.04, and it looks like my camera raw files are still not supported. I use to install updates from Philip Johnsson's PPA.<br>
Do I have to wait for new release 5.0?<br>
Thanks a lot for the precious help, and I wish you all the best.</p>
<p>Guido</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-21140"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/745#comment-21140" class="active">Check libkdcraw/libraw...</a></h3>    <div class="submitted">Submitted by digiKam on Sat, 2015-11-07 11:35.</div>
    <div class="content">
     <p>Libraw is digiKam Raw decoder. You must update this library to have last camera support. 0.17 support this Nikon camera.</p>
<p>http://www.libraw.org/news/libraw-0-17</p>
<p>Note : digiKam 4.14.0 use libkdcraw interface to play with libraw. digiKam do not use libraw directly. You must update this library too...</p>
         </div>
    <div class="links">» </div>
  </div>
</div>
</div>