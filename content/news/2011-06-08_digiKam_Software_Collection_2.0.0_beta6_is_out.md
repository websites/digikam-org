---
date: "2011-06-08T14:15:00Z"
title: "digiKam Software Collection 2.0.0 beta6 is out..."
author: "digiKam"
description: "Dear all digiKam fans and users! digiKam team is proud to announce the 6th digiKam Software Collection 2.0.0 beta release! With this release, digiKam include"
category: "news"
aliases: "/node/607"

---

<p>Dear all digiKam fans and users!</p>

<p>digiKam team is proud to announce the 6th digiKam Software Collection 2.0.0 beta release!</p>

<p>With this release, digiKam include a lots of bugs fixes to progress in stability for future production use.</p>

<p>digiKam include since 2.0.0-beta5 a new tool to export on RajCe web service.</p>

<a href="http://www.flickr.com/photos/digikam/5702563277/" title="rajcetool by digiKam team, on Flickr"><img src="http://farm3.static.flickr.com/2678/5702563277_bfbcaaf5cf_z.jpg" width="640" height="256" alt="rajcetool"></a>

<p>digiKam include since 2.0.0-beta4 <b>Group of items feature</b> and <b>MediaWiki export tool</b>.</p>

<a href="http://www.flickr.com/photos/digikam/5564467832/" title="wikimediatool-windows by digiKam team, on Flickr"><img src="http://farm6.static.flickr.com/5062/5564467832_98ec55be99_z.jpg" width="640" height="211" alt="wikimediatool-windows"></a>

<a href="http://www.flickr.com/photos/digikam/5488098135/" title="digiKam-groupitems by digiKam team, on Flickr"><img src="http://farm6.static.flickr.com/5179/5488098135_17e6307452_z.jpg" width="640" height="360" alt="digiKam-groupitems"></a>

<p>digiKam include since 2.0.0-beta3 <b>Color Labels</b> and <b>Pick Labels</b> feature to simplify image cataloging operations in your photograph workflow.</p>

<a href="http://www.flickr.com/photos/digikam/5453984332/" title="digikampicklabel2 by digiKam team, on Flickr"><img src="http://farm6.static.flickr.com/5013/5453984332_bba7f267da_z.jpg" width="640" height="256" alt="digikampicklabel2"></a>

<p>Also, digiKam include since 2.0.0-beta2 the <b>Tags Keyboard Shorcuts</b> feature to simplify tagging operations in your photograph workflow.</p>

<a href="http://www.flickr.com/photos/digikam/5389657319/" title="tagshortcuts2 by digiKam team, on Flickr"><img src="http://farm6.static.flickr.com/5218/5389657319_edb75c8ccb_z.jpg" width="640" height="256" alt="tagshortcuts2"></a>

<p>digiKam software collection 2.0.0 include all Google Summer of Code 2010 projects, as <b>XMP sidecar support</b>, <b>Face Recognition</b>, <b>Image Versioning</b>, and <b>Reverse Geocoding</b>. All this works have been processed during <a href="http://techbase.kde.org/Projects/Digikam/CodingSprint2010">Coding Sprint 2010</a>. You can find a resume of this event <a href="http://www.digikam.org/drupal/node/538">at this page</a>.</p>

<a href="http://www.flickr.com/photos/digikam/5323487542/" title="digikam2.0.0-19 by digiKam team, on Flickr"><img src="http://farm6.static.flickr.com/5008/5323487542_fdea5ef240.jpg" width="500" height="400" alt="digikam2.0.0-19"></a>

<p>This beta release is not yet stable. Do not use yet in production. Please report all bugs to KDE bugzilla following indications <a href="http://www.digikam.org/drupal/support">from this page</a>. The release plan can be seen <a href="http://www.digikam.org/drupal/about/releaseplan">at this url</a>.</p>

<a href="http://www.flickr.com/photos/digikam/5319746295/" title="digikam2.0.0-05 by digiKam team, on Flickr"><img src="http://farm6.static.flickr.com/5130/5319746295_df7bba3672_z.jpg" width="640" height="256" alt="digikam2.0.0-05"></a>

<p>See <a href="https://projects.kde.org/projects/extragear/graphics/digikam/repository/revisions/eab49c59fc5f6844df96da8221cf25cf604b2f6c/raw/NEWS">the list of digiKam file closed</a> with this release into KDE bugzilla.</p>

<p>See <a href="https://projects.kde.org/projects/extragear/graphics/kipi-plugins/repository/revisions/6b872ccf3623e079464868a63259b51caf7b1a90/raw/NEWS">the list of Kipi-plugins file closed</a> with this release into KDE bugzilla.</p>

<p>digiKam software collection tarball can be downloaded from <a href="http://sourceforge.net/projects/digikam/files">SourceForge Repository</a></p>

<a href="http://www.flickr.com/photos/digikam/5319746291/" title="digikam2.0.0-04 by digiKam team, on Flickr"><img src="http://farm6.static.flickr.com/5249/5319746291_60bcdc0b28_z.jpg" width="640" height="256" alt="digikam2.0.0-04"></a>

<p>Happy digiKam testing...</p>
<div class="legacy-comments">

  <a id="comment-19905"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/607#comment-19905" class="active">PPA for digiKam</a></h3>    <div class="submitted">Submitted by <a href="http://www.oceanwatcher.com/" rel="nofollow">Svein</a> (not verified) on Wed, 2011-06-08 16:11.</div>
    <div class="content">
     <p>It would be a great advantage, both for users, testers and you as developers to keep two ppa's for digiKam:</p>
<p>Stable - only stable releases<br>
Beta - only beta releases</p>
<p>And no other software than what is needed to get it working so that it do not influence anything else in an installation.</p>
<p>This would allow anyone with a *buntu system to keep their preferred ppa in the list and automatically get new versions when they are available. I think this would increase both the user base and tester base and eventually help making digiKam an even better program (if possible!!).</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19910"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/607#comment-19910" class="active">Upstream -&gt; Downstream | Developers -&gt; Distributors</a></h3>    <div class="submitted">Submitted by Fri13 on Thu, 2011-06-09 14:56.</div>
    <div class="content">
     <p>It isn't responsible, duty or demand for the upstream to maintain repository for users. Developers have own development style than distributors who are in downstream.</p>
<p>The development goes: Upstream -&gt; Downstream<br>
Distributors responsible, duty and demand is to maintain precompiled binary packages for their users. They pull the code from upstream, package it and then place it to repositories for users to easily install and use. When upstream does something, downstreams pulls code changes and compiled new packages and so on.</p>
<p>The ideal ecosystem works that every code development happens in upstream. If upstream does not accept something, then downstream can do it by themselfs but they just cuts off their users (clients etc) from the development in upstream and slows down users.</p>
<p>Ubuntu users are minority in Linux world, not the biggest. They are just users among other distribution users and it is Canonicals job to compile precompiled packages for their users as stable. And then Ubuntu fans who have PPA to compile patched or development version of the software. But every user who use PPA are at their own.</p>
<p>Upstream does not take responsibility what so ever how users wants their packages.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19922"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/607#comment-19922" class="active">A limited view of the world...</a></h3>    <div class="submitted">Submitted by <a href="http://www.oceanwatcher.com/" rel="nofollow">Svein</a> (not verified) on Wed, 2011-06-29 00:03.</div>
    <div class="content">
     <p>I think you have a very limited view of the world...</p>
<p>PPA's does not need to be maintained by packagers alone (or fans). As the Ubuntu repositories updates slowly, the PPA's give users the opportunity to stay closer to the new releases.</p>
<p>A good example is Mozilla. There are PPA's for stable, beta and alpha packages.</p>
<p>How big marketshare Ubuntu has can probably be talked a lot about. But I think nobody would ever count it as irrelevant. It is probably by far, the biggest distribution out there. And with the various derivatives, it is even bigger.</p>
<p>A PPA made by the developers, or someone approved by them so that you announce that you know that this is a safe PPA is something very valuable and it makes sure users can relax and trust what they get.</p>
<p>It is also valuable to the digiKam project to know that users gets the new releases as soon as possible and can report back bugs immediately.</p>
<p>Fans come and go. A project like this needs stability. And the only way to get stability is for this project to offer packages for the major distributions directly.</p>
<p>Why bother with a website? Why not just keep an ftp server and a mailing list? Why bother with Facebook pages? A website like this with comments, a Facebook group, Twitter - all of this is direct contact with end users. If you really think that devs should not interact with end users, then an ftp server and a mailing list is all you need. But this is not the world we live in now. We now live in a world where direct contact is crucial to any project. You might think that you do not have time or resources to keep a PPA up to date. But just ask around, and I am sure there are people that would like to help. Just keep the control of the PPA inside the project. Don't let it be under the control of someone else that lets it die if he do not have time anymore. When it is under the control of the digiKam project, you just replace the one that do the work - not the PPA.</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-19911"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/607#comment-19911" class="active">Full ACK</a></h3>    <div class="submitted">Submitted by Michael (not verified) on Fri, 2011-06-10 10:50.</div>
    <div class="content">
     <p>Full ACK</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-19912"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/607#comment-19912" class="active">Felt like giving a response</a></h3>    <div class="submitted">Submitted by <a href="https://launchpad.net/~philip5" rel="nofollow">Philip Johnsson</a> (not verified) on Fri, 2011-06-10 13:28.</div>
    <div class="content">
     <p>Felt like giving a response to this post as I feel like my PPA have become more or less the unofficial PPA for Digikam 2.0 when it comes to Ubuntu. </p>
<p>I have had this request once in a while to break out Digikam to a PPA of its own but the purpose of my PPA is to provide packages for Ubuntu that I anyway make for myself, friends or by request. I get this request for almost every software you find in my PPA from time to time. If you like to use just Digikam from it then just grab that and inactivate the PPA until you want to update it again. I just want all my packages to work as any other user so that's of course my main goal.</p>
<p>When it comes to Digikam 2.0 beta6 I have had about 800 downloads of it for Ubuntu 11.04 and would say that there is almost as many unique Digikam users of the PPA (most people tend to download the packages one time and have it in apt cache on their computer even if they reinstall the same package). I have had little complain about the packages them self but that is of course no guarantee for the future.</p>
<p>Still, using PPAs can make unwanted effects to a system so it's better to feel confident enough to know what's in them and maybe also track records of how it is to use it. Combinations of different PPAs can also make stuff conflict but that usually gives problems with upgrading, updating or installing and apt, aptitude, kpackagekit, synaptic or any other package manager will let you know when that happens before any system breaks (fingers crossed).</p>
<p>If someone want to setup a special purpose digikam PPA they are as alway in the Linux world free to fork/copy Digikam packages in my PPA or make their own and just upload that somewhere. I'm even happy to help out but I will keep doing what I'm doing with mine as long as I feel it's needed.</p>
<p>Happy Digikaming!</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19923"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/607#comment-19923" class="active">Thank you!</a></h3>    <div class="submitted">Submitted by <a href="http://www.oceanwatcher.com/" rel="nofollow">Svein</a> (not verified) on Wed, 2011-06-29 00:21.</div>
    <div class="content">
     <p>Maybe you should add the link to your ppa :-)</p>
<p>It is great that you keep a PPA for your own stuff, but it would be better to have a dedicated ppa for digiKam for several reasons.</p>
<p>You might for some reason, some day, decide that you do not want to have digiKam there anymore. All the users would then have to go hunting for a new fan that have a PPA. An official PPA would be a lot better. If whoever that maintain it decide to stop doing so, someone else can step in as the official one will not be tied to a fan somewhere in the world.</p>
<p>A dedicated ppa for digiKam alone makes it less likely that people get new version of software they did not plan on upgrading for various reasons. It allows you to turn on a PPA and leave it on. Most users are able to do an add-apt-repository, but sooner or later you forget to turn the ppa off after an update and suddenly you get a lot of updates you did not want.</p>
<p>Understand me right - I love the job you are doing. If I was able to maintain a PPA, I would set up the digiKam PPA myself, but I have no clue how to do it. I am not a programmer.</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div><a id="comment-19906"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/607#comment-19906" class="active">Compiling beta on OSX 10.6</a></h3>    <div class="submitted">Submitted by lks (not verified) on Wed, 2011-06-08 17:15.</div>
    <div class="content">
     <p>I haven't been able to compile the digikam beta since beta-2 under OSX 10.6 with macports, XCode 4, and a Mac Mini 64 bit system.  Has anybody elese succeeded in doing this?</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19907"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/607#comment-19907" class="active">myself...</a></h3>    <div class="submitted">Submitted by digiKam on Wed, 2011-06-08 18:29.</div>
    <div class="content">
     <p>I use also a macbook, and i compile digiKam and co like you through macport (at least to resolve dependencies).</p>
<p>I checkout code from digikam Software Compilation :</p>
<p>https://projects.kde.org/projects/extragear/graphics/digikam/digikam-software-compilation </p>
<p>... And all work fine :</p>
<p>http://www.flickr.com/groups/digikam-macosx/</p>
<p>digiKam</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19913"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/607#comment-19913" class="active">still having problems</a></h3>    <div class="submitted">Submitted by lks (not verified) on Sun, 2011-06-12 02:00.</div>
    <div class="content">
     <p>As I am still having problems, could you explicitly list the steps you performed for both checking out the code from kde.org and to compile using cmake?  I want to make sure I didn't miss anything here.</p>
<p>Thanks.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19914"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/607#comment-19914" class="active">I need more info...</a></h3>    <div class="submitted">Submitted by digiKam on Sun, 2011-06-12 09:00.</div>
    <div class="content">
     <p>when you checkout digiKam SC, run bootstrap.macports and give a trace of cmake results...</p>
<p>digiKam</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19915"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/607#comment-19915" class="active">Success</a></h3>    <div class="submitted">Submitted by lks (not verified) on Sun, 2011-06-12 19:21.</div>
    <div class="content">
     <p>I deleted my old repositories and rebuilt from scratch, and everything appears to be working now.</p>
<p>For the record, here is what I did:</p>
<p>git clone kde:digikam-software-compilation<br>
cd digikam-software-compilation<br>
./download-repos<br>
./bootstrap.macports<br>
make<br>
sudo make install</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19919"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/607#comment-19919" class="active">The same happened to me!</a></h3>    <div class="submitted">Submitted by <a href="http://www.zygorguidesp.com/" rel="nofollow">zygor guides</a> (not verified) on Mon, 2011-06-20 14:15.</div>
    <div class="content">
     <p>And I did exactly that, and it worked! thank you!</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div></div></div></div><a id="comment-19908"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/607#comment-19908" class="active">What version of libkipi</a></h3>    <div class="submitted">Submitted by StefanT. (not verified) on Wed, 2011-06-08 21:26.</div>
    <div class="content">
     <p>What version of libkipi should I use for compiling, from Kubuntu 11.04 repositories or from digikam-2.0.0-beta6.tar.bz2? If I remove libikipi8, it also removes Ksnapshot and Gwenview (and digikam)?</p>
<p>Thank you.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19909"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/607#comment-19909" class="active">none especially..</a></h3>    <div class="submitted">Submitted by digiKam on Wed, 2011-06-08 22:08.</div>
    <div class="content">
     <p>There no specific version to use, excepted a version &gt; 1.0.</p>
<p>To get whole dependencies of libraries managed by digiKam team, use Software Compilation repository :</p>
<p>https://projects.kde.org/projects/extragear/graphics/digikam/digikam-software-compilation</p>
<p>digiKam</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-19918"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/607#comment-19918" class="active">NEW</a></h3>    <div class="submitted">Submitted by <a href="http://agapeantiaging.com/lifecell-review" rel="nofollow">lifecell</a> (not verified) on Sun, 2011-06-19 00:02.</div>
    <div class="content">
     <p>You can edit the way google map thing has. It can achieve such things most people want to get personalized. I want to grab this. How does one avail this type free of fee?</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-19920"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/607#comment-19920" class="active">Congratulations for getting</a></h3>    <div class="submitted">Submitted by Sorin (not verified) on Mon, 2011-06-20 23:19.</div>
    <div class="content">
     <p>Congratulations for getting closer to 2.0</p>
<p>Are there any news on the Windows builds front? I am unfortunately stuck with no functional Linux install on my laptop, and no time to put it together soon :(</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19921"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/607#comment-19921" class="active">try this one...</a></h3>    <div class="submitted">Submitted by digiKam on Tue, 2011-06-21 06:16.</div>
    <div class="content">
     <p>It's not yet official :</p>
<p>http://digikam3rdparty.free.fr/misc.tarballs/temp/digiKam-installer-2.0.0-beta6-win32.exe</p>
<p>digiKam</p>
         </div>
    <div class="links">» </div>
  </div>
</div>
</div>
