---
date: "2016-01-30T13:06:00Z"
title: "digiKam Explained: Chroma Subsampling"
author: "Dmitri Popov"
description: "When configuring saving settings for the JPEG format in the Saving Images section of the Configure digiKam window, you can choose between medium and high"
category: "news"
aliases: "/node/751"

---

<p>When configuring saving settings for the JPEG format in the <strong>Saving Images</strong> section of the <strong>Configure digiKam</strong> window, you can choose between medium and high chroma subsampling, or disable it altogether.</p>
<p><img src="https://scribblesandsnaps.files.wordpress.com/2016/01/chroma_subsampling.png" alt="chroma_subsampling" width="600"></p>
<p>But what is chroma subsampling and what does it do? As you may know, JPEG is a lossy format, which means that it trades quality for a smaller size. One of the methods for achieving a smaller size is to store color information (chroma) at a lower resolution than brightness (a.k.a intensity or luma) data. This is possible due to the fact that the human eye is less sensitive to color than brightness, so discarding color information doesn't produce a perceptible loss of quality.</p>
<p><a href="http://scribblesandsnaps.com/2016/01/30/digikam-explained-chroma-subsampling/">Continue reading</a></p>

<div class="legacy-comments">

</div>