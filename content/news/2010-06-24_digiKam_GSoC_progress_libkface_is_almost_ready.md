---
date: "2010-06-24T06:08:00Z"
title: "digiKam GSoC progress: libkface is almost ready"
author: "digiKam"
description: "Aditya Bhatt working on implementing automatic tagging of faces for digiKam, with face detection and recognition. For that, he have been working on a library"
category: "news"
aliases: "/node/529"

---

<p>Aditya Bhatt working on implementing automatic tagging of faces for digiKam, with face detection and recognition. For that, he have been working on a library named libface that does the detection and training/recognition. <a href="http://adityabhatt.wordpress.com/2010/06/23/digikam-gsoc-progress-libkface-is-almost-ready">Continue to read</a></p>

<div class="legacy-comments">

</div>