---
date: "2016-12-09T09:13:00Z"
title: "Geotagging in digiKam with a Lazy Bash Script"
author: "Dmitri Popov"
description: "Sometimes, the easiest way to geotag photos in digiKam is to copy and paste geographical coordinates from an existing photo. I usually use Google Photos"
category: "news"
aliases: "/node/762"

---

<p>Sometimes, the easiest way to geotag photos in digiKam is to copy and paste geographical coordinates from an existing photo. I usually use Google Photos for that, as it conveniently displays geographical coordinates of the currently viewed photos in the information sidebar.</p>
<p><img src="https://scribblesandsnaps.files.wordpress.com/2016/12/google-photos-location.png" alt="google-photos-location" width="597" height="514"></p>
<p>There is only one problem with this technique: copying and pasting the geographical coordinates directly doesn't work.</p>
<p><a href="https://scribblesandsnaps.com/2016/12/09/geotagging-in-digikam-with-a-lazy-bash-script/">Continue reading</a></p>

<div class="legacy-comments">

</div>