---
date: "2010-08-30T09:04:00Z"
title: "Correct Lens Distortion with digiKam"
author: "Dmitri Popov"
description: "Lens distortion is a fact of life. You can mitigate this problem, but you can’t avoid it completely (unless you are willing to invest in"
category: "news"
aliases: "/node/537"

---

<a href="http://www.flickr.com/photos/digikam/4998290696/" title="lenfuntool by digiKam team, on Flickr"><img src="http://farm5.static.flickr.com/4126/4998290696_d55b585b26_z.jpg" width="640" height="256" alt="lenfuntool"></a>

<p>
Lens distortion is a fact of life. You can mitigate this problem, but you can’t avoid it completely (unless you are willing to invest in seriously expensive professional-grade lenses, that is). Fortunately, digiKam provides a set of tools that can help you to fix lens distortion with relative ease. In fact, the application sports the Auto-Correction feature that attempts to fix lens distortion with a minimum of tweaking. <a href="http://scribblesandsnaps.wordpress.com/2010/08/30/correct-lens-distortion-with-digikam/">Continue to read</a>
</p>
<div class="legacy-comments">

  <a id="comment-19489"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/537#comment-19489" class="active">I really like the idea of the</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Tue, 2010-08-31 18:15.</div>
    <div class="content">
     <p>I really like the idea of the lens distortion correction plugin, but it seems that the included lensfun database is too small - most of the lenses I use (in particular some popular Canon EF-S lenses and Sigmas) and also my camera (Canon 7d) are missing. It seems that this database is not updated any more. Is there an easy way of adding the relevant data about new lenses/cameras?</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19493"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/537#comment-19493" class="active">LensFun Database update tutorial...</a></h3>    <div class="submitted">Submitted by digiKam on Thu, 2010-09-02 09:45.</div>
    <div class="content">
     <p>Please take a look to LensFun Project tutorial to know how to add your lens data to database:</p>

<a href="http://lensfun.berlios.de/lens-calibration/lens-calibration.html">Lens Fun Database Update Tutorial</a>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19513"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/537#comment-19513" class="active">The lens distortion tool is</a></h3>    <div class="submitted">Submitted by Buchan Milne (not verified) on Thu, 2010-09-16 14:54.</div>
    <div class="content">
     <p>The lens distortion tool is fantastic, but it would be even better if:<br>
-There was a plugin to assist with the lens calibration<br>
-If the lens auto-correction was available for batch processing</p>
<p>I will be trying to add profiles for EF 70-300mm F4-5.6 IS, and EF-S 17-85mm F4-F5.6 IS, but once I do so, how can other digikam users benefit? Wait for a new lensfun release? Shouldn't it be easier to update the lens profiles (e.g. like GHNS)?</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19514"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/537#comment-19514" class="active">About batch processing...</a></h3>    <div class="submitted">Submitted by digiKam on Thu, 2010-09-16 15:01.</div>
    <div class="content">
     <p>&gt;-If the lens auto-correction was available for batch processing</p>
<p>I'm working on currently. It will be available for next digiKam 1.5.0 release.</p>
<p>digiKam</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19518"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/537#comment-19518" class="active">I'm working on currently. It</a></h3>    <div class="submitted">Submitted by Buchan Milne (not verified) on Fri, 2010-09-17 13:41.</div>
    <div class="content">
     <p><cite>I'm working on currently. It will be available for next digiKam 1.5.0 release.</cite></p>
<p>Fantastic! I have over 1000 photos from a trip to Kenya, most of which need correction (EF 70-300 F4-5.6 IS isn't bad, but has noticeable distortion near the 300mm end).</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19548"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/537#comment-19548" class="active">And it's done in trunk (1.5.0)</a></h3>    <div class="submitted">Submitted by digiKam on Tue, 2010-09-21 10:45.</div>
    <div class="content">
     <a href="http://www.flickr.com/photos/digikam/5011325653/" title="digiKam-lensfuntools by digiKam team, on Flickr"><img src="http://farm5.static.flickr.com/4149/5011325653_a896c07e63.jpg" width="500" height="200" alt="digiKam-lensfuntools"></a>         </div>
    <div class="links">» </div>
  </div>
</div></div><a id="comment-19515"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/537#comment-19515" class="active">It cannot be a plugin...</a></h3>    <div class="submitted">Submitted by digiKam on Thu, 2010-09-16 15:02.</div>
    <div class="content">
     <p>&gt;-There was a plugin to assist with the lens calibration</p>
<p>It's a tiedous subject. A dedicated application need to be written for that. It's not only around digiKam but all photo world.</p>
<p>digiKam</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-19517"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/537#comment-19517" class="active">I will be trying to add</a></h3>    <div class="submitted">Submitted by Buchan Milne (not verified) on Fri, 2010-09-17 13:38.</div>
    <div class="content">
     <p><cite>I will be trying to add profiles for EF 70-300mm F4-5.6 IS, and EF-S 17-85mm F4-F5.6 IS</cite></p>
<p>When starting to do this (starting with EF-S 17-85, as I already had some panorama sets), I noticed that there was already an entry for the EF-S 17-85mm F4-F5.6, but the Digikam (1.3.0) Auto-Correction doesn't list it. My version (0.2.4) of lensfun has a number of EF-S lenses listed in /usr/share/lensfun/slr-canon.xml, but the only one listed in the Digikam Lens drop-down is the EF-S 55-250 f4-5.6 IS, however, in slr-canon.xml, this lens is tagged as having a EF mount. So, none of the lenses that have &lt;mount&gt;EF-S&lt;/mount&gt; are listed. Editing the lensfun definition for the EF-S 17-85mm to make the mount 'EF' results in it being listed in Digikam.</p>
<p>Is this a digikam bug, or a lensfun bug (so I can file a bug to the appropriate software)?</p>
<p>Thanks for your work on this fantastic software.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19519"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/537#comment-19519" class="active">sound like a LensFun bug...</a></h3>    <div class="submitted">Submitted by digiKam on Fri, 2010-09-17 13:49.</div>
    <div class="content">
     <p>...because, we take lens list through LensFun api as well...</p>
<p>digiKam</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19520"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/537#comment-19520" class="active">Fixed in lensfun 0.2.5</a></h3>    <div class="submitted">Submitted by Buchan Milne (not verified) on Sat, 2010-09-18 12:12.</div>
    <div class="content">
     <p>It turns out the problem was that up to lensfun 0.2.4, many of the entry-level Canon DSLR's were identified as having EF mounts, not EF-S mounts. Although not listed in the changelog (which I looked at previously to see if it was worthwhile upgrading), this is fixed in 0.2.5.</p>
<p>Of interest to Mandriva users, I have sent 0.2.5 to <a href="http://kenobi.mandriva.com/bs//package.php?key=20100917135950.buchan.kenobi.28414">main/backports for 2010.1</a>, so you can upgrade lensfun by running <code>urpmi.update 'Main Backports' &amp;&amp; urpmi --searchmedia Backports lensfun</code>. I will consider backporting digikam 1.4.0 as well ...</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div></div></div><a id="comment-19547"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/537#comment-19547" class="active">Upgrade lensfun to 0.2.5</a></h3>    <div class="submitted">Submitted by Buchan Milne (not verified) on Tue, 2010-09-21 09:51.</div>
    <div class="content">
     <p>Lensfun 0.2.4 had incorrect listings for some Canon DSLRs, listing their mounts as EF instead of EF-S. Cameras listed as EF will only show EF lenses, while cameras tagged as having EF-S mounts will list EF and EF-S lenses, so on 0.2.4, you would probably only see one incorrectly tagged EF-S lens (55-200mm).</p>
<p>Upgrade to 0.2.5, and this is fixed, and the 7D is also present in 0.2.5.</p>
<p>If your lens still isn't listed in digikam, check in the lens profile files first (on my installation this is /usr/share/lensfun/slr-canon.xml), and ensure that it has the correct mount type listed etc. If it is missing, you'll need to do the calibration.</p>
<p>/me proceeds with EF 70-300 f/4-5.6 IS calibration</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-19490"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/537#comment-19490" class="active">I wonder why digital camera</a></h3>    <div class="submitted">Submitted by Andreas (not verified) on Wed, 2010-09-01 05:19.</div>
    <div class="content">
     <p>I wonder why digital camera lenses aren't optimized for things like contrast and brightness instead of absence of aberrations. There is plenty of CPU power available to correct images nowadays. Heck, there could be algorithms (or coefficients for a standard algorithm) stored in the lenses that are fed to the camera CPU/DSP to correct any trivial aberrations and other errors right there.<br>
Can anybody guess why such a system hasn't happened yet?</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19494"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/537#comment-19494" class="active">Comparing image quality</a></h3>    <div class="submitted">Submitted by Fri13 on Thu, 2010-09-02 22:24.</div>
    <div class="content">
     <p>Comparing image quality (pixel sharpness etc) it is more important than the possibilities to correct brightness and contrast later.</p>
<p>What we really would need is very dynamic ranged imaging sensors for cameras. So that they could give now more like +/- f:2 dynamic. Just too bad that D/A conversion is still so bad.</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-19496"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/537#comment-19496" class="active">Autodetection of Canon</a></h3>    <div class="submitted">Submitted by escho (not verified) on Fri, 2010-09-03 18:25.</div>
    <div class="content">
     <p>That´s a really great plugin, thank you for it!</p>
<p>I played a little bit with this filter. The resulting picture looks much better than without the filter.</p>
<p>But I´ve found a little problem for me, using a Canon EOS 450D. The autodetection of the lens does not run.</p>
<p>I searched a little in the source files. And I found, that for Canon the "Lens"-Tag is used for the lens-search. I think, it would be better to use the "LensType"-Tag. I changed this in "dmetadata.cpp" and compiled it for me and now all works great for my lenses and me.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19497"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/537#comment-19497" class="active">send me a patch</a></h3>    <div class="submitted">Submitted by digiKam on Fri, 2010-09-03 19:02.</div>
    <div class="content">
     <p>Great. Please send me a patch of your changes. I will include it later.</p>
<p>Thanks in advance</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19498"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/537#comment-19498" class="active">sending adress</a></h3>    <div class="submitted">Submitted by escho (not verified) on Fri, 2010-09-03 20:22.</div>
    <div class="content">
     <p>Where can I send the patch to?</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19499"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/537#comment-19499" class="active">bugzilla</a></h3>    <div class="submitted">Submitted by digiKam on Fri, 2010-09-03 20:27.</div>
    <div class="content">
     <p>open a new file in bugzilla, into digiKam section. take a look <a href="http://www.digikam.org/drupal/contrib">here</a> </p>
<p>digiKam</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19549"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/537#comment-19549" class="active">Patch added in bug 251920</a></h3>    <div class="submitted">Submitted by Buchan Milne (not verified) on Tue, 2010-09-21 12:19.</div>
    <div class="content">
     <p>Since I didn't find a bug about this when searching on bko, I patched and tested for myself on 1.4.0. After verifying that it works correctly (with Canon EOS 500D, I may be able to test with images from a 7D a bit later), I filed <a href="https://bugs.kde.org/show_bug.cgi?id=251920">bug 251920</a> with the trivial patch.</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div></div></div>
</div>
