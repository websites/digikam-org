---
date: "2015-08-07T14:17:00Z"
title: "digiKam Software Collection 4.12.0 released..."
author: "digiKam"
description: "Dear digiKam fans and users, The digiKam Team is proud to announce the release of digiKam Software Collection 4.12.0. This release is the result of"
category: "news"
aliases: "/node/741"

---

<a href="https://www.flickr.com/photos/digikam/20367240135/in/dateposted-public/" title="digikam4.12.0"><img src="https://farm1.staticflickr.com/360/20367240135_83c65518ee_c.jpg" width="800" height="225" alt="digikam4.12.0"></a>

<p>Dear digiKam fans and users,</p>

<p>
The digiKam Team is proud to announce the release of digiKam Software Collection 4.12.0. 
This release is the result of another huge bugs triage on KDE bugzilla where more than 145 files have been closed.
Thanks to <a href="https://plus.google.com/u/0/107171232114475191915/about">Maik Qualmann</a> who maintain KDE4 version while 
<a href="https://techbase.kde.org/Projects/Digikam/CodingSprint2014#KDE_Framework_Port">KF5 port</a> and <a href="https://community.kde.org/GSoC/2015/Ideas#digiKam">GSoC 2015 projects</a> are in progress. Both are planed to be completed before end of this year.
</p>

<p>
As with 4.11.0, this new release comes with OSX PKG installers for Mountain Lion, Maverick, and Yosemite. Note that you must considerate digiKam under OSX as beta stage, not stable as under Linux. Some dysfunctions have been identified and fixed with this release.
PKG installer includes last 
<a href="http://www.exiv2.org/changelog.html">Exiv2 0.25</a>, 
<a href="http://www.libraw.org/download#stable">LibRaw 0.16.2</a>, 
and <a href="http://lensfun.sourceforge.net/changelog/2015/05/10/Release-0.3.1-Changelog/">LensFun 0.3.1</a> 
which do not exist yet in Macports repository. Exiv2 includes huge fixes about video support which can crash digiKam, and Libraw/Lensfun add new recent camera supports. In the future, we plan to include Hugin CLI tools too required at run-time by Panorama and ExpoBlending tools.
</p>

<p>
The OSX digiKam PKG is not an Apple signed bundle installer. To do it we must pay 99$ by year which is very expensive for an Open Source project. If your computer is protected by Apple GateKeeper, you can bypass safety this protection at install time without to touch your computer settings. Just follow simple instructions given <a href="http://www.bu.edu/infosec/howtos/bypass-gatekeeper-safely/">in this tutorial</a>.
</p>

<p>
For more details, you can consult the huge list of the <a href="https://bugs.kde.org/buglist.cgi?f1=cf_versionfixedin&amp;o1=equals&amp;query_format=advanced&amp;bug_status=RESOLVED&amp;bug_status=NEEDSINFO&amp;bug_status=VERIFIED&amp;bug_status=CLOSED&amp;v1=4.12.0&amp;product=digikam&amp;product=digikamimageplugins&amp;product=kipiplugins&amp;product=showfoto">issues closed</a> in digiKam 4.12.0 available through the KDE Bugs-tracking System.
</p>

<p>The digiKam software collection tarball, OSX, and Windows installer can be downloaded from the <a href="http://download.kde.org/stable/digikam/">KDE mirrors</a>.
</p>

<p>Have fun to use this new important digiKam stable release.</p>

<p>digiKam Team...</p>
<div class="legacy-comments">

  <a id="comment-21056"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/741#comment-21056" class="active">animated gifs</a></h3>    <div class="submitted">Submitted by goat (not verified) on Sat, 2015-08-08 07:39.</div>
    <div class="content">
     <p>Any word on when animated gifs will be supported?  It seems very strange that a picture manager doesn't support one of the more common types of pictures out there.</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-21057"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/741#comment-21057" class="active">100'000 thanks</a></h3>    <div class="submitted">Submitted by <a href="http://www.freunde-schloss-beuggen.de" rel="nofollow">Schlossfreund</a> (not verified) on Tue, 2015-08-11 12:30.</div>
    <div class="content">
     <p>I really love digikam because it helps so much with my hobby: taking fotos and using them for publications in my volunteer engagements. I'm absolute no professional but I have to search pictures out of some 80'000 files. How to that without digikam?</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-21058"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/741#comment-21058" class="active">This Release with KDE 4.13.3</a></h3>    <div class="submitted">Submitted by Das Auge (not verified) on Tue, 2015-08-11 15:33.</div>
    <div class="content">
     <p>Is it possible to install this new Release of Digikam on a KDE 4.13.3 System?<br>
I would really like to, since I am using Kubuntu 14.04 LTS and won't get a KDE upgrade until 16.04.1</p>
<p>Any tipps or experiences?</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-21059"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/741#comment-21059" class="active">ppa:philip5/extra</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Tue, 2015-08-11 20:57.</div>
    <div class="content">
     <p>ppa:philip5/extra (<a href="https://launchpad.net/~philip5/+archive/ubuntu/extra">https://launchpad.net/~philip5/+archive/ubuntu/extra</a>)</p>
<p>Cheers.</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-21060"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/741#comment-21060" class="active">crash-fest?</a></h3>    <div class="submitted">Submitted by Sascha (not verified) on Wed, 2015-08-12 14:22.</div>
    <div class="content">
     <p>Hi there - I installed digiKam 4.10 on my 64Bit Windows10 Notebook. unfortunately it crashed every time, I tried to edit metadata or even the description of a picture. after rebooting the notebook digiKam doesn't even start any more and freezes as it's showing the beginners-tips.<br>
which is the latest STABLE version that will work on my machine?<br>
thanks for the work, anyway - I really appreciate OpenSource Software that can be supported by donations</p>
<p>-Sascha-</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-21061"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/741#comment-21061" class="active">http://download.kde.org/stabl</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Thu, 2015-08-13 11:28.</div>
    <div class="content">
     <p>http://download.kde.org/stable/digikam/digiKam-installer-4.12.0-win32.exe</p>
<p>Remember:<br>
Be careful as digiKam is not as stable under Windows as under Linux because of some bugs in the underlying KDE SC libraries that digiKam depends on.</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-21062"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/741#comment-21062" class="active">Video Thumbnails on Windows</a></h3>    <div class="submitted">Submitted by Constanca (not verified) on Thu, 2015-08-13 12:14.</div>
    <div class="content">
     <p>Can anybody please tell me how to get video thumbnails on digiKam on Windows? I'm using digiKam 4.12.0 on Windows 8.1 and everything works great except I can't get thumbnails for video files, only photos...</p>
<p>I've seen the related question in the FAQ, but I don't know how to make that work under Windows, since I don't know how to install packages... I've also been googling for the last 3 days and every info I find is related to various Linux distros and the instructions are not 'translatable' to Windows.</p>
<p>I don't know if this helps, but I also don't get thumbs neither in Konqueror nor Dolphin for anything.</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-21063"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/741#comment-21063" class="active">Keeps hanging at startup</a></h3>    <div class="submitted">Submitted by Bizy (not verified) on Sun, 2015-08-16 01:04.</div>
    <div class="content">
     <p>So glad that I discovered Digikam (only a month ago).<br>
Such a pleasure to finally face and organize my huge photo collection.</p>
<p>Until yesterday:  after the startup screen, the program keeps hanging - I do not even obtain a crash report.<br>
And I don't know why.</p>
<p>(It started with being unable to tag images (by typing + enter), but I still could click on the right tags, and apply them.  Then I couldn't use the information pane anymore.  All of a sudden, everything worked fine again.  And then (after system resume?), as described above...)</p>
<p>Possible related factors (???):  updates from Windows 7?  Clean up from registry with CCleaner?  Size of the database?  (rather large by now - but the program doesn't show a progress bar anymore, so the 'hanging' is not the time it takes to scan the database?)  I don't know...</p>
<p>I spent day &amp; night trying to figure it out.  Removing, reinstalling different versions (4.5, 4.11, 4.12).  In or outside of Program Files folder.  Unistalled those latest Windows 7 updates.  Had my databases checked/reindexed/compressed/defragmented with Valentina Studio 5.  (I tried to 'dump' and recreate the databases with Sqlite3, but the tutorials I could find didn't work for me - not my cup of tea.)  Used older backups of my databases...<br>
All for nothing...  Much, much time that I would have loved to spend actually using the program...</p>
<p>Anybody an idea?</p>
<p>(I have reports from debugview.exe, if necessary)</p>
<p>Thanks in advance, Bizy</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-21066"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/741#comment-21066" class="active">I'm not shure if CCleaner and</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Wed, 2015-08-19 11:59.</div>
    <div class="content">
     <p>I'm not shure if CCleaner and such Programs are doing great stuff...Having actual Backups and playing less with system would be the best ;-)</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-21067"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/741#comment-21067" class="active">Well, too late for that</a></h3>    <div class="submitted">Submitted by Bizy (not verified) on Thu, 2015-08-20 04:00.</div>
    <div class="content">
     <p>Well, too late for that now... :-(<br>
Don't think that's the reason...<br>
There seem to be locked database files.  Curious thing is that I use a copy of the same database files in Ubuntu, and that gives no problem...  (there are other problems though... ;-) )</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-21068"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/741#comment-21068" class="active">Interesting. Nice that you</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Thu, 2015-08-20 19:43.</div>
    <div class="content">
     <p>Interesting. Nice that you are trying Ubuntu!</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-21069"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/741#comment-21069" class="active">I LOVE working with Ubuntu. </a></h3>    <div class="submitted">Submitted by Bizy (not verified) on Fri, 2015-08-21 16:25.</div>
    <div class="content">
     <p>I LOVE working with Ubuntu.  But I started to miss some (Windows) programs.  And installed a dual boot.  And - up to a certain moment in time - I found Digikam to be more stable in Windows ... sorry to say so.  The roles are reversed now... :-(  I can't use the maintenance function in Ubuntu, but there are work arounds.  Periodical crashes, but all I need to do is to start the program again.  Unfortunately I can't make any decent crash reports (without going into much further inquiry as indicated on the support page).  The debug symbols are installed, but the crash handler refuses to create decent bug reports (or... crashes...).</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-21070"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/741#comment-21070" class="active">interesting. Do you use 14.04</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Sat, 2015-08-22 23:25.</div>
    <div class="content">
     <p>interesting. Do you use 14.04 LTS or something later?</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-21071"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/741#comment-21071" class="active">Yes, 14.04</a></h3>    <div class="submitted">Submitted by Bizy (not verified) on Sun, 2015-08-23 00:07.</div>
    <div class="content">
     <p>Yes, 14.04</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-21075"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/741#comment-21075" class="active">funny... That shouldn't be...</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Mon, 2015-08-24 17:34.</div>
    <div class="content">
     <p>funny... That shouldn't be...</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-21078"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/741#comment-21078" class="active">It is NOT funny...!   (;-) )</a></h3>    <div class="submitted">Submitted by Bizy (not verified) on Wed, 2015-08-26 12:40.</div>
    <div class="content">
     <p>It is NOT funny...!   (;-) )</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-21084"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/741#comment-21084" class="active">;-)
I have the same with one</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Thu, 2015-08-27 14:21.</div>
    <div class="content">
     <p>;-)<br>
I have the same with one Game which should work, but it won't start...</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-21086"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/741#comment-21086" class="active">Funny! ;-)</a></h3>    <div class="submitted">Submitted by Bizy (not verified) on Thu, 2015-08-27 21:35.</div>
    <div class="content">
     <p>Funny! ;-)</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div></div><a id="comment-21076"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/741#comment-21076" class="active">The biggest problem with</a></h3>    <div class="submitted">Submitted by Philip5 (not verified) on Wed, 2015-08-26 02:24.</div>
    <div class="content">
     <p>The biggest problem with standard packages in Ubuntu 14.04 is that digikam use exiv2 and the version in there is buggy and bring down the whole digikam with crashes. There is also a problem with the version of sqlite3 that also give problems for digikam. With both of them updated and digikam rebuilt to use them will make it much more stable. Also using the latest version of digital itself instead of the rather old version in standard Ubuntu 14.04 will make the whole digikam experience better.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-21077"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/741#comment-21077" class="active">Hello 'Philip5',
Thanks for</a></h3>    <div class="submitted">Submitted by Bizy (not verified) on Wed, 2015-08-26 12:39.</div>
    <div class="content">
     <p>Hello 'Philip5',<br>
Thanks for your comment!<br>
'Unfortunately':  I do have the latest version of Digikam installed - that's when the problem started... (!).<br>
I did install the latest update of exiv2, and when I did, the installer said it was not even installed on my system... (?! so Digikam has been working before without...).<br>
I don't know about a 'latest' version of sqlite (as there doesn't seem to have been an update for years, or am I mistaken).  In any case:  if you know how to do that, please give me the right commands...<br>
Greetings, B.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-21079"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/741#comment-21079" class="active">Have you built your digikam</a></h3>    <div class="submitted">Submitted by Philip5 (not verified) on Wed, 2015-08-26 14:41.</div>
    <div class="content">
     <p>Have you built your digikam yourself? You can't just update exiv2 without rebuilding digikam to use the update otherwise it will think you still have the old one as binary. You should use sqlite3 3.8.4 or newer with Digikam to get rid of the database bugs. Backport it from Ubuntu Vivid is one way.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-21080"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/741#comment-21080" class="active">I'm sorry:  two problems</a></h3>    <div class="submitted">Submitted by Bizy (not verified) on Wed, 2015-08-26 17:17.</div>
    <div class="content">
     <p>I'm sorry:  two problems might be getting mixed up here (and I have contributed to that by responding about Ubuntu).</p>
<p>The original post is about Windows.  The program keeps hanging from the start - no crash, and if so, no crash report.  It just keeps hanging.  The problem might have to do with the integration of phonon (KDE) in Windows.  That problem has been handed over to the Windows manager of DigiKam.<br>
Then there is Linux (but this page isn't supposed to tackle those problems...?).</p>
<p>In Linux, I can't do maintenance (whatever function, whatever album (big or small), with our without all processor heads involved).  That's a different problem (for which there are workarounds, but still, not so handy...)</p>
<p>As for an answer:  no, I didn't build 'my' DigiKam.  I wouldn't know how to do that...  I followed these instructions:  <a>http://ubuntuhandbook.org/index.php/2015/08/install-latest-digikam-4-12-ubuntu/</a>.  And I used Ubuntu Softwarecentrum to install (not even update, as it said it was not installed) exiv2.<br>
I don't know how to verify which version of sqlite I have installed (a search in the dash has no results... but I don't think I have a problem with the database in Ubuntu:  it's a 'memory leak', the program starts using all available memory, hijacks one processor head and after (lots of) time, simply disappears).</p>
<p>Instructions are (very) welcome (thanks!), but please, be (more) specific.  I you say:  'backport it from Ubuntu Vivid', I don't know what to do with that...  I'm just, well, a 'user'...</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-21081"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/741#comment-21081" class="active">That guide for installing</a></h3>    <div class="submitted">Submitted by Philip5 (not verified) on Wed, 2015-08-26 19:02.</div>
    <div class="content">
     <p>That guide for installing digikam 4.12 isn't correct. If your ubuntu 14.04 is using kde 4.13 as base then you should ONLY use the "extra" PPA BUT if you have somehow using kde 4.14 from some backport or other updated source then you should ALSO use the "kubunt-backports" PPA and NOT either of them. So in short with KDE 4.14 as base then BOTH PPAs are needed otherwise just the "extra".</p>
<p>I never uploaded any updated version of sqlite3 to my PPA as it would need a closer follow up for maintenance and patching for any security problem in the future as sqlite3 is quite an essential part of your system.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-21082"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/741#comment-21082" class="active">How do I check which version</a></h3>    <div class="submitted">Submitted by Bizy (not verified) on Wed, 2015-08-26 20:07.</div>
    <div class="content">
     <p>How do I check which version of KDE I have?  (if at all;  in other words:  does Ubuntu 14.04 come with a KDE version, and if so, which version?)<br>
(I would have been a good idea to ask this question before trying to install Digikam 4.12 ;-) )</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-21085"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/741#comment-21085" class="active">To check which version of KDE</a></h3>    <div class="submitted">Submitted by philip5 (not verified) on Thu, 2015-08-27 16:33.</div>
    <div class="content">
     <p>To check which version of KDE you have as base on your system you can either open any KDE application you happen to have installed on your system and check in it's menu under Help &gt; About KDE. Otherwise make a search for KDE base packages like the kdebase-runtime or just kde-runtime with the tools you use to install packages and see that the version of the package is. If version name is 4.13.x or 4.14.x then you know.</p>
<p>(*)Ubuntu 14.04 comes with KDE 4.13 as standard but depending on the sources you have made available it's not uncommon that you have it updated or backported to KDE 4.14.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-21087"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/741#comment-21087" class="active">That's clear! ;-)  Thanks.
So</a></h3>    <div class="submitted">Submitted by Bizy (not verified) on Thu, 2015-08-27 21:36.</div>
    <div class="content">
     <p>That's clear! ;-)  Thanks.<br>
So 4.14 it is.  Of course, becoz of my settings.  And becoz I installed both packages anyway.</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div></div></div></div></div></div></div></div></div></div></div></div></div><a id="comment-21064"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/741#comment-21064" class="active">I've made my own distributive</a></h3>    <div class="submitted">Submitted by crossblade (not verified) on Tue, 2015-08-18 14:00.</div>
    <div class="content">
     <p>I've made my own distributive in susestudio (based on opensuse 13.2). latest version (4.12) is on, but digiKam does not even start... How to find out the problem? maybe some libs are too old? Starting digikam in command line does not show any output.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-21065"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/741#comment-21065" class="active">Looks like problem with</a></h3>    <div class="submitted">Submitted by crossblade (not verified) on Tue, 2015-08-18 16:05.</div>
    <div class="content">
     <p>Looks like problem with 64-bit version - same package selection but in 32 bit works well...</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-21072"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/741#comment-21072" class="active">digikam and gmic based inpainting stuff</a></h3>    <div class="submitted">Submitted by Holger (not verified) on Sun, 2015-08-23 13:59.</div>
    <div class="content">
     <p>Hello,</p>
<p>reading about a fix in in-painting option of digikam editor I hopefully start digikam 4.12 after a dist-upgrade on my arch based distribution antergos.</p>
<p>But it is still same behaviour since digikam 2.6 (!). When trying to modify things with in-painting options, digikam freezes and only a killall digikam closes this application. On computer of my father this behaviour is reproducable, with other distributions it is reproducable.</p>
<p>Some years ago I've got explaination, that this error has to do with switching from cimg library to gmic library. So again my question to core developer: When will you fix gmic based stuff? To refer to a comment from Gilles Caullier: When will you find a student to fix this?</p>
<p>When there is no interest to fix this error, then it would be nice not to offer in-painting in editor of digikam.</p>
<p>Kind regards,<br>
  Holger</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-21073"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/741#comment-21073" class="active">Actually we tried to fix</a></h3>    <div class="submitted">Submitted by Veaceslav Munteanu (not verified) on Sun, 2015-08-23 15:12.</div>
    <div class="content">
     <p>Actually we tried to fix cImg/gmic stuff, which is responsible for in-paint option. But the current version of gmic does not meet digiKam's requirements. The library itself is not suited to be used as it is. We contacted developers of cImg but they didn't agree to cooperate. What gmic does not have: option to stop the algorithm if it is taking too long. Option to show progress if a large image is processed. </p>
<p>This is why digiKam only has an internal version, modified by us, which is very old. We really can't do anything about it.</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-21074"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/741#comment-21074" class="active">Splash Screen Peeve</a></h3>    <div class="submitted">Submitted by <a href="http://www.mbarrick.net" rel="nofollow">Michael</a> (not verified) on Sun, 2015-08-23 20:56.</div>
    <div class="content">
     <p>Every time I start digiKam I'm irked at the text on the splash screen, "Manage your photographs like a professional with the power of open source"</p>
<p>*like* implies that it is *not* professional and that professionals use something else, that while similar, is not digiKam. I keep hoping this will go away with each release, but it doesn't. </p>
<p>How about, "Professional-quality photo management for everyone with the power of open source" or some such other wording that does not imply that digiKam is not used by real professionals.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-21090"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/741#comment-21090" class="active">Agreed</a></h3>    <div class="submitted">Submitted by Calle (not verified) on Sun, 2015-08-30 23:01.</div>
    <div class="content">
     <p>Here's a little +1 or "me too" post...<br>
I agree, something like what you wrote would be an improvement.</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-21089"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/741#comment-21089" class="active">How to not install mysql ???</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Sat, 2015-08-29 15:49.</div>
    <div class="content">
     <p>How can I install digikam without mysql? </p>
<p>The Philip Johnsson ppa unfortunately bloats my system by pulling in lots of unneeded stuff, like mysql-server. I do not want this, and from what i understand Digikam can run without mysql. How to avoid installing it with that ppa? Or is there any other official package for Ubuntu 14.04? Actually, why not? </p>
<p>root@ubuntu:~# apt-get install --no-install-recommends digikam<br>
Reading package lists... Done<br>
Building dependency tree<br>
Reading state information... Done<br>
The following extra packages will be installed:<br>
  akonadi-backend-mysql akonadi-server digikam-data kdepim-runtime<br>
  kdepimlibs-kio-plugins libaccounts-qt1 libakonadi-calendar4<br>
  libakonadi-contact4 libakonadi-kabc4 libakonadi-kcal4 libakonadi-kde4<br>
  libakonadi-kmime4 libakonadi-notes4 libakonadi-socialutils4<br>
  libakonadiprotocolinternals1 libastro1 libbaloocore4 libbaloofiles4<br>
  libbalooxapian4 libdmtx0a libexiv2-14 libkabc4 libkalarmcal2 libkcal4<br>
  libkcalcore4 libkcalutils4 libkdcraw-data libkdcraw23 libkexiv2-11<br>
  libkexiv2-data libkface-data libkface3 libkfbapi1 libkgapi2-2<br>
  libkgeomap-data libkgeomap2 libkholidays4 libkimap4 libkipi-data libkipi11<br>
  libkldap4 libkmbox4 libkmime4 libkolab0 libkolabxml1 libkpimidentities4<br>
  libkpimtextedit4 libkpimutils4 libkresources4 libmailtransport4<br>
  libmarblewidget18 libmicroblog4 libopencv-photo2.4 libpgf6 libprison0<br>
  libqt4-sql-mysql libsignon-qt1 libsqlite0 marble-data mysql-client-core-5.5<br>
  mysql-server-core-5.5</p>
         </div>
    <div class="links">» </div>
  </div>

</div>