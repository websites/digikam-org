---
date: "2016-03-13T10:17:00Z"
title: "digiKam Recipes 4.11.1 Released"
author: "Dmitri Popov"
description: "A new release of digiKam Recipes is ready for your reading pleasure. This version introduces the Basic Concepts Explained appendix that covers key terms and"
category: "news"
aliases: "/node/754"

---

<p>A new release of <a href="http://scribblesandsnaps.com/digikam-recipes/">digiKam Recipes</a> is ready for your reading pleasure. This version introduces the <em>Basic Concepts Explained</em> appendix that covers key terms and concepts used in digiKam. Currently, the appendix contains information about chroma subsampling, cor (bit) depth, hue, saturation, brightness, and vibrance. I plan to gradually expand the appendix with time.</p>
<p><a href="http://scribblesandsnaps.com/digikam-recipes/"><img src="https://scribblesandsnaps.files.wordpress.com/2015/11/digikamprecipes-4-9-7.png" alt="digikamprecipes-4.9.7" width="375" height="500"></a></p>
<p><a href="http://scribblesandsnaps.com/2016/03/10/digikam-recipes-4-11-1-released/">Continue reading</a></p>

<div class="legacy-comments">

</div>