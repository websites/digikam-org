---
date: "2010-02-02T14:12:00Z"
title: "kipi-plugins 1.1.0 released..."
author: "digiKam"
description: "Dear all digiKam fans and users! digiKam team is proud to announce Kipi-plugins 1.1.0 ! kipi-plugins tarball can be downloaded from SourceForge at this url"
category: "news"
aliases: "/node/499"

---

<p>Dear all digiKam fans and users!</p>

<p>digiKam team is proud to announce Kipi-plugins 1.1.0 !</p>

<a href="http://www.flickr.com/photos/languitar/4276861015/" title="Exposure Blending Plugin at Work by languitar, on Flickr"><img src="http://farm3.static.flickr.com/2704/4276861015_1102332798.jpg" width="500" height="379" alt="Exposure Blending Plugin at Work"></a>

<p>kipi-plugins tarball can be downloaded from SourceForge <a href="http://sourceforge.net/projects/kipi/files">at this url</a></p>

<p>Kipi-plugins will be also available for Windows. Precompiled packages can be donwloaded with KDE-Windows installer. See <a href="http://windows.kde.org">KDE-Windows project</a> for details.</p>

<p>See below the list of new features and bugs-fix coming with this release:</p>

<p><b>NEW FEATURES:</b></p>

ExpoBlending              : New tool to make pseudo HDR image with a stack of bracketed images.<br>
PrintAssistant            : New custom grid layout.<br>
PrintAssistant            : Filling page with custom size images.<br>

<p><b>BUGFIXES FROM KDE BUGZILLA:</b></p>

001 ==&gt; ExpoBlending      : 144593 : New High Dynamic Range (HDR) plugin.<br>
002 ==&gt; GalleyExport      : 219998 : Export to Gallery plugin crashes when adding files.<br>
003 ==&gt; AdvancedSlideShow : 215798 : Slideshow crashes when starting.<br>
004 ==&gt; JPEGLossLess      : 200031 : Photo is rotated but not thumbnail.<br>
005 ==&gt; PicasaWebExport   : 199145 : Tags not exported to picasaweb [patch].<br>
006 ==&gt; PicasaWebExport   : 215846 : PicasaWeb plugin crashes digiKam after start sending the pic [delete, KIPIPicasawebExportPlugin::PicasawebWindow::slotUploadImages] (double deletion).<br>
007 ==&gt; ExpoBlending      : 221056 : Expoblending.app causes digiKam compile to fail.<br>
008 ==&gt; PicasaWebExport   : 205903 : accentuated chars are converted into a mess during picasa web export [patch].<br>
009 ==&gt; PicasaWebExport   : 220433 : digiKam crash when has long time working on it.<br>
010 ==&gt; PicasaWebExport   : 208426 : Exporting images fails with : "Could not connect to host xxxx.xxxx.xxxx.xxxx: Unknown error.".<br>
011 ==&gt; ExpoBlending      : 222297 : Overwriting on Windows and multiple saves fixes [patch].<br>
012 ==&gt; PicasaWebExport   : 198045 : New albums don't appear in list.<br>
013 ==&gt; PicasaWebExport   : 200469 : Ampersand character in caption causes Picasa export to freeze.<br>
014 ==&gt; JPEGLossLess      : 222328 : In-place rotation converts 16 bit TIFF to 8 bit.<br>
015 ==&gt; PrintAssistant    : 219381 : fill page with images of certain size.<br>
016 ==&gt; HTMLExport        : 194946 : Simple Fix - extra " ' " in cleanframes/themes/template.xsl.<br>
017 ==&gt; GPSSync           : 182326 : Remember last position when geotagging images.<br>
018 ==&gt; GPSSync           : 165613 : Crash when GPS altitude is negative.<br>
019 ==&gt; GPSSync           : 167286 : GPS tagging fails with message titled "digikam3rdparty.free.fr" and text "unauthorized call of the API" when starting the gps tagger window.<br>
020 ==&gt; GPSSync           : 224205 : Geolocation correlator: GPX date/time format.<br>
021 ==&gt; GPSSync           : 222067 : GPSSync no more search field to fill with a town.<br>
022 ==&gt; AdvancedSlideShow : 224379 : digiKam doesn't compile (problem with advanced slideshow).<br>

<div class="legacy-comments">

  <a id="comment-19014"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/499#comment-19014" class="active">Thanks one more time!!
Are</a></h3>    <div class="submitted">Submitted by <a href="http://www.yiannakos.gr" rel="nofollow">Bill Yiannakos</a> (not verified) on Tue, 2010-02-02 15:39.</div>
    <div class="content">
     <p>Thanks one more time!!<br>
Are they passed on the repository? Update manager did not find any updates on Kipi yet.........</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19019"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/499#comment-19019" class="active">This is not how it works, it</a></h3>    <div class="submitted">Submitted by Michal T. (not verified) on Tue, 2010-02-02 21:16.</div>
    <div class="content">
     <p>This is not how it works, it can take a weeks or even month for your distro maintainers to put this release on repos.</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-19024"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/499#comment-19024" class="active">You need to ask from</a></h3>    <div class="submitted">Submitted by Fri13 on Wed, 2010-02-03 17:15.</div>
    <div class="content">
     <p>You need to ask from distributors who packages your distribution. Most distributors pick ups these on their next release.</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-19021"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/499#comment-19021" class="active">pseudo?</a></h3>    <div class="submitted">Submitted by bkn (not verified) on Wed, 2010-02-03 06:21.</div>
    <div class="content">
     <p>what do you mean by 'pseudo HDR image'?</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19027"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/499#comment-19027" class="active">This is about the algorithm</a></h3>    <div class="submitted">Submitted by languitar on Wed, 2010-02-03 23:35.</div>
    <div class="content">
     <p>This is about the algorithm that is used to create the image. The normal "HDR" workflow means to 1. create an image (from several differently exposed source images) with a much higher color depth then a usual image and what can be displayed by a normal monitor etc. 2. this image is mapped onto an image with the normal 8 or 16 bit color depth with an algorithm that produces images that are nice to view. So the main idea is that you first create an image with much more color depth for reducing this later in a way that makes the result nice.</p>
<p>The way enfuse goes, which is wrapped by the new plugin, is different in the way that it doesn't create such an intermediate high-depth image but instead blends the source images directly all done in the original color depth limits.</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-19022"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/499#comment-19022" class="active">Thanks</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Wed, 2010-02-03 10:11.</div>
    <div class="content">
     <p>Another big thank you for the hard work and time of the team!</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-19028"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/499#comment-19028" class="active">www.kipi-plugins.org</a></h3>    <div class="submitted">Submitted by Simon Schmeißer (not verified) on Sat, 2010-02-06 20:32.</div>
    <div class="content">
     <p>you might want to update that site?<br>
thanks!</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-19045"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/499#comment-19045" class="active">Great work on the new</a></h3>    <div class="submitted">Submitted by tanemahuta (not verified) on Wed, 2010-02-10 16:34.</div>
    <div class="content">
     <p>Great work on the new plugins!</p>
<p>I was really looking forward to try the new expoblending tool, couldn't get it to compile on 64bit kubuntu karmic though. Hugin 0.8, Enblend 3.2 and libgomp are installed - any ideas? </p>
<p>cheers Michael</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19046"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/499#comment-19046" class="active">look libkdcraw</a></h3>    <div class="submitted">Submitted by digiKam on Wed, 2010-02-10 20:03.</div>
    <div class="content">
     <p>It require libkdcraw from KDE 4.4. take a look in README file for details</p>
<p>digiKam</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-19053"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/499#comment-19053" class="active">Compile help: digkam with kipi plugins 1.1.0 on debian/lenny</a></h3>    <div class="submitted">Submitted by omaenner (not verified) on Thu, 2010-02-18 23:26.</div>
    <div class="content">
     <p>The debian/lenny  digikam is quite out-of-date, so I tried to compile and install the latest digikam under /usr/local</p>
<p>1. ) uninstalled debian digikam and kipi-plugins</p>
<p>2.) compiled &amp; installed kipi-plugins 1.1.0 with<br>
  cmake  -DCMAKE_BUILD_TYPE=debugfull DCMAKE_INSTALL_PREFIX=/usr/local .<br>
  make ; sudo make install</p>
<p>3.) compiled &amp; installed digikam 1.1.0 with<br>
  export CMAKE_MODULE_PATH=/usr/local/lib/kde4/<br>
  export CMAKE_PREFIX_PATH=/usr/local<br>
  cmake  -DCMAKE_BUILD_TYPE=debugfull DCMAKE_INSTALL_PREFIX=/usr/local .<br>
  make ; sudo make install<br>
4.) digikam starts OK, but there are no plugins</p>
<p>The freshly compiled kipiplugin_advancedslideshow.so, etc. and the digikamimageplugin_freerotation.so, etc. are in /usr/local/lib/kde4/<br>
There is a fresh /usr/local/lib/libkipiplugins.so.1.0.0</p>
<p>However there is also an old /usr/lib/libkipi.so.6 that belongs to a debian package libkipi6</p>
<p>Any help is very much appreciated</p>
<p>Oswald</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19074"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/499#comment-19074" class="active">Any luck on this..</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Mon, 2010-03-15 03:33.</div>
    <div class="content">
     <p>Any luck on this..</p>
<p>I commented out line 349 in cmake list as advanced  advanced slideshow keeps throwing a syntax error.</p>
<p>Digikam compiles okay and runs on kde 4.4.x or whatever it is...</p>
<p>Kipi complies okay..</p>
<p>All appears well but when dk starts there are no kipi plugins...</p>
<p>I am lost..</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19075"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/499#comment-19075" class="active">Any luck on this..</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Mon, 2010-03-15 20:51.</div>
    <div class="content">
     <p>I had similar problems which I solved by installing Kipi in /usr instead of /usr/local.</p>
<p>I believe the problem to be that all KDE 4 components need to be in the same place, so unless you have compiled the whole of KDE in a non-standard location, you will be using the repository KDE 4 which will be installed in /usr. You therefore need to put the Kipi plugins there as well.</p>
<p>Good luck!</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div><a id="comment-19066"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/499#comment-19066" class="active">can install kipi-plugins</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Tue, 2010-03-02 21:07.</div>
    <div class="content">
     <p>hello,<br>
I would like to install this new release (because error in digikam when trying to export photo to PicasaWeb)<br>
I don't know how to do it. I followed the instructions from http://www.kipi-plugins.org/drupal/node/3 but got stuck at the line: make -f Makefile.cvs (make: Makefile.cvs: Aucun fichier ou dossier de ce type)</p>
<p>or tried to follow the instruction in the README, but again got stuck at the cmake . line. Don't know how to use it. Never compiled anything in my life...<br>
any help?<br>
I am using kubuntu 9.10<br>
thanks<br>
regards</p>
<p>samuel</p>
         </div>
    <div class="links">» </div>
  </div>

</div>
