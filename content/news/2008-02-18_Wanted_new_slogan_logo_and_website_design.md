---
date: "2008-02-18T21:10:00Z"
title: "Wanted: new slogan, logo and website design"
author: "gerhard"
description: "DigiKam has come a long way since the early beginnings in 2002. It has come of age and deserves new cloths or a new 'corporate"
category: "news"
aliases: "/node/300"

---

<p>DigiKam has come a long way since the early beginnings in 2002. It has come of age and deserves new cloths or a new 'corporate design' in today's parlance. </p>
<p>To find a new slogan, a new logo, iconized slash screen and web design has been itching me and others for quite a while now. But those are sensitive and difficult subjects, in particular for us amateurs in that field. </p>
<p><strong>A contest is herewith opened</strong> to create such new cloths, closing end of march 2008. We shall then make a pre-selection and vote on the best candidates. I will act as coordinator and point of contact. Please supply your proposals to gerhard at kulzer dot net.</p>
<p>Guide lines:</p>
<ul>
<li>Photography and FOSS should be the general context.</li>
<li>the webdesign should include a color scheme, fonts and header, possibly include the logo from another contributor.
</li><li>the slogan shall reflect the general context
</li><li>the logo must not necessarily include the penguin, and should convey the more advanced state of digiKam's development and aspiration of a professional tool.
</li><li>The KDE4 version could have an iconized splash screen derived from the logo (or vis-versa).
</li></ul>
<p>To kick it off I add some ideas of mine:<br>
<em>Slogan</em>: The truly open &amp; free photo management tool<br>
<em>Web design</em>: <a href="http://www.digikam.org/?q=system/files/digikam-web-design.png">here</a><br>
<em>Logo</em>: <a href="http://www.digikam.org/?q=system/files/digi-logo.png">here</a><br>
<em>Splash screen</em>: <a href="http://www.digikam.org/?q=system/files/digikam-splash.png">here</a></p>

<div class="legacy-comments">

  <a id="comment-18053"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/300#comment-18053" class="active">Three months into the project</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Wed, 2008-12-17 13:19.</div>
    <div class="content">
     <p>Three months into the project (Sept. 2005), BioCorder is at the proof of concept stage. We are deploying Life Science Identifiers (LSID’s) within our current web databases (more info here) and converting these databases to output Resource Description Framework (RDF) statements. These are prerequisites of the Semantic Web. For the most part Semantic Web technologies are not usually something you can point your web browser at like websites.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18235"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/300#comment-18235" class="active">logo</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Tue, 2009-02-10 19:39.</div>
    <div class="content">
     <p>The logo looks good right now. I don't think it needs to be changed. I don't really think logos are that big of a deal anyway. Sites should be about content. The logo could look terrible, but the person who visits it won't care if there is good information on the site.</p>
         </div>
    <div class="links">» </div>
  </div>
</div>
</div>