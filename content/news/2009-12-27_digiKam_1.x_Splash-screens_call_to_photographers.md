---
date: "2009-12-27T19:23:00Z"
title: "digiKam 1.x Splash-screens: call to photographers !"
author: "digiKam"
description: "Just in time for Christmas 2009, digiKam 1.0 have been released. But the future is already there. Next digiKam 1.1 is planed for end of"
category: "news"
aliases: "/node/493"

---

<a href="http://websvn.kde.org/*checkout*/trunk/extragear/graphics/digikam/data/pics/splash-digikam.png?revision=953345" title="digiKam 1.0.0 splash"><img src="http://websvn.kde.org/*checkout*/trunk/extragear/graphics/digikam/data/pics/splash-digikam.png?revision=953345" width="500" height="307" alt="digiKam 1.0.0 splash"></a>

<p>Just in time for Christmas 2009, digiKam 1.0 have been released. But the future is already there. Next digiKam 1.1 is planed for end of January 2010, as a bugfixes release, to consolidate code with users feedback after production using...</p>

<p>So, time has come to find the ideal splash-screens to go with it. Now is your chance to join the ranks of the precious few who have had their artwork associated with a release of digiKam!</p> 

<p>Splash-screen is a simple way for users to contribute to digiKam project. The pictures must be correctly exposed and composed, and the subject must be chosen using a real photographer inspiration.</p> 

<a href="http://websvn.kde.org/*checkout*/trunk/extragear/graphics/digikam/data/pics/splash-showfoto.png?revision=953345" title="Showfoto 1.1.0 splash"><img src="http://websvn.kde.org/*checkout*/trunk/extragear/graphics/digikam/data/pics/splash-showfoto.png?revision=953345" width="500" height="307" alt="Showfoto 1.1.0 splash"></a>

<p>As usual, we need two new splash-screens dedicated to digiKam and Showfoto startup. You can send me <a href="http://www.digikam.org/?q=contact">by e-mail</a> your Colors or Black and White pictures, taken horizontally. We need only photos, no need to put few marks about program name and version. We have two SVG templates for <a href="http://websvn.kde.org/*checkout*/trunk/extragear/graphics/digikam/data/pics/splash-digikam.svgz">digiKam</a> and for <a href="http://websvn.kde.org/*checkout*/trunk/extragear/graphics/digikam/data/pics/splash-showfoto.svgz">showfoto</a> to do it later. I will review the best items with the team later...</p> 

<p>As reference, see in this post the official digiKam and Showfoto 1.0 splash-screens.</p>

<p>Have fun to contribute to digiKam project... and thanks in advance...</p>

<p>Happy new year 2010</p>
<div class="legacy-comments">

  <a id="comment-18943"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/493#comment-18943" class="active">Does digikam really take that</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Sun, 2009-12-27 20:47.</div>
    <div class="content">
     <p>Does digikam really take that long to load that a splash screen is necessary?</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18944"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/493#comment-18944" class="active">it can be disabled if you want...</a></h3>    <div class="submitted">Submitted by digiKam on Sun, 2009-12-27 20:54.</div>
    <div class="content">
     <p>there is an option in setup if you don't like it...</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18945"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/493#comment-18945" class="active">Yes. :)  Digikam 1.0 looks in</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Mon, 2009-12-28 03:25.</div>
    <div class="content">
     <p>Yes. :)  Digikam 1.0 looks in the following directory locations at start up for the splash screen image.</p>
<p>~/.kde/share/apps/digikam/data/splash-digikam.png<br>
/usr/share/kde-settings/kde-profile/default/share/apps/digikam/data/splash-digikam.png<br>
/usr/share/kde4/apps/digikam/data/splash-digikam.png (default location)</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18950"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/493#comment-18950" class="active">You do not need to go there.</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Mon, 2009-12-28 17:29.</div>
    <div class="content">
     <p>You do not need to go there. The digiKam has a setting in options to turn splash screen off/on. </p>
<p>I hope the digiKam would get boost to starting proccess. To be under 5 seconds at least. Now it is over 3 minutes here =(</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18951"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/493#comment-18951" class="active">yes, i see this problem there.</a></h3>    <div class="submitted">Submitted by digiKam on Mon, 2009-12-28 18:02.</div>
    <div class="content">
     <p>I think it's relevant of SQLite. close and restart digiKam a second time, it's faster.<br>
MySQL port is in progress. I hope that it can fix it soon.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18953"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/493#comment-18953" class="active">MySQL</a></h3>    <div class="submitted">Submitted by xeros (not verified) on Tue, 2009-12-29 08:35.</div>
    <div class="content">
     <p>Great! Since more services in KDE already use MySQL it might speed up digiKam if MySQL server is already running before starting KDE apps :-)</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18956"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/493#comment-18956" class="active">and more...</a></h3>    <div class="submitted">Submitted by digiKam on Tue, 2009-12-29 11:34.</div>
    <div class="content">
     <p>...especially to have a remote Mysql server to host digiKam database, used by the network by more than one production computers running digiKam...</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div></div></div></div></div><a id="comment-18946"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/493#comment-18946" class="active">Do you need the photos in a</a></h3>    <div class="submitted">Submitted by <a href="http://kalyanvarma.net" rel="nofollow">Kalyan Varma</a> (not verified) on Mon, 2009-12-28 03:39.</div>
    <div class="content">
     <p>Do you need the photos in a specific dimensions/proportions ? Looking forward to sending in some photographs</p>
<p>Kalyan</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18947"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/493#comment-18947" class="active">We don't need the full version image</a></h3>    <div class="submitted">Submitted by digiKam on Mon, 2009-12-28 10:05.</div>
    <div class="content">
     <p>At end, image will be resized to 300x200 (around) and converted to PNG. But to have a large version can be suitable to select a dedicated area.</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-18959"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/493#comment-18959" class="active">Contributions arrive...</a></h3>    <div class="submitted">Submitted by digiKam on Wed, 2009-12-30 13:03.</div>
    <div class="content">
     <a href="http://www.flickr.com/photos/digikam/4228263890/" title="0014 by digiKam team, on Flickr"><img src="http://farm3.static.flickr.com/2792/4228263890_827ed7a570_s.jpg" width="75" height="75" alt="0014"></a>
<a href="http://www.flickr.com/photos/digikam/4227493373/" title="0007 by digiKam team, on Flickr"><img src="http://farm3.static.flickr.com/2718/4227493373_e048fb36f1_s.jpg" width="75" height="75" alt="0007"></a>
<a href="http://www.flickr.com/photos/digikam/4228262978/" title="0001 by digiKam team, on Flickr"><img src="http://farm5.static.flickr.com/4064/4228262978_41d1afd525_s.jpg" width="75" height="75" alt="0001"></a>
<a href="http://www.flickr.com/photos/digikam/4227485399/" title="1 by digiKam team, on Flickr"><img src="http://farm3.static.flickr.com/2798/4227485399_e5117f921e_s.jpg" width="75" height="75" alt="1"></a>
<a href="http://www.flickr.com/photos/digikam/4227504383/" title="piet rechts by digiKam team, on Flickr"><img src="http://farm3.static.flickr.com/2687/4227504383_b4259c92ee_s.jpg" width="75" height="75" alt="piet rechts"></a>
<a href="http://www.flickr.com/photos/digikam/4228274858/" title="moritzs links by digiKam team, on Flickr"><img src="http://farm5.static.flickr.com/4042/4228274858_cf31466f0b_s.jpg" width="75" height="75" alt="moritzs links"></a>
<a href="http://www.flickr.com/photos/digikam/4227510201/" title="p1000373 by digiKam team, on Flickr"><img src="http://farm3.static.flickr.com/2509/4227510201_e9fa1c64ee_s.jpg" width="75" height="75" alt="p1000373"></a>
<a href="http://www.flickr.com/photos/digikam/4228283974/" title="spider by digiKam team, on Flickr"><img src="http://farm5.static.flickr.com/4033/4228283974_c8c8f8b745_s.jpg" width="75" height="75" alt="spider"></a>
<a href="http://www.flickr.com/photos/digikam/4227542641/" title="Black_Hole_by_Hjoranna by digiKam team, on Flickr"><img src="http://farm3.static.flickr.com/2699/4227542641_e1e9cda715_s.jpg" width="75" height="75" alt="Black_Hole_by_Hjoranna"></a>
<a href="http://www.flickr.com/photos/digikam/4228323030/" title="Long-billed Vultures by digiKam team, on Flickr"><img src="http://farm3.static.flickr.com/2516/4228323030_f980755df8_s.jpg" width="75" height="75" alt="Long-billed Vultures"></a>
<a href="http://www.flickr.com/photos/digikam/4228322918/" title="Olive Ridley turtle by digiKam team, on Flickr"><img src="http://farm3.static.flickr.com/2623/4228322918_53104140a6_s.jpg" width="75" height="75" alt="Olive Ridley turtle"></a>
<a href="http://www.flickr.com/photos/digikam/4228322660/" title="_DSC8171 by digiKam team, on Flickr"><img src="http://farm3.static.flickr.com/2498/4228322660_2d5b4508e3_s.jpg" width="75" height="75" alt="_DSC8171"></a>
<a href="http://www.flickr.com/photos/digikam/4227552155/" title="Leopard (Panthera pardus) by digiKam team, on Flickr"><img src="http://farm5.static.flickr.com/4064/4227552155_d74bc53664_s.jpg" width="75" height="75" alt="Leopard (Panthera pardus)"></a>
<a href="http://www.flickr.com/photos/digikam/4228322398/" title="Monitor lizard by digiKam team, on Flickr"><img src="http://farm5.static.flickr.com/4052/4228322398_5891403013_s.jpg" width="75" height="75" alt="Monitor lizard"></a>
<a href="http://www.flickr.com/photos/digikam/4228333242/" title="_mg_5323 by digiKam team, on Flickr"><img src="http://farm5.static.flickr.com/4067/4228333242_35fe1c2ebf_s.jpg" width="75" height="75" alt="_mg_5323"></a>
<a href="http://www.flickr.com/photos/digikam/4228342500/" title="Leaves by digiKam team, on Flickr"><img src="http://farm3.static.flickr.com/2586/4228342500_b6853418f4_s.jpg" width="75" height="75" alt="Leaves"></a>
<a href="http://www.flickr.com/photos/digikam/4227609465/" title="DSC07557 by digiKam team, on Flickr"><img src="http://farm3.static.flickr.com/2671/4227609465_bb84368946_s.jpg" width="75" height="75" alt="DSC07557"></a>
<a href="http://www.flickr.com/photos/digikam/4230098225/" title="OmarVieira_1 by digiKam team, on Flickr"><img src="http://farm3.static.flickr.com/2722/4230098225_7a96957b73_s.jpg" width="75" height="75" alt="OmarVieira_1"></a>
<a href="http://www.flickr.com/photos/digikam/4230865944/" title="OmarVieira_3 by digiKam team, on Flickr"><img src="http://farm3.static.flickr.com/2540/4230865944_feb91f5410_s.jpg" width="75" height="75" alt="OmarVieira_3"></a>
<a href="http://www.flickr.com/photos/digikam/4234318222/" title="DSC01489 by digiKam team, on Flickr"><img src="http://farm5.static.flickr.com/4071/4234318222_897ca2b9db_s.jpg" width="75" height="75" alt="DSC01489"></a>
<a href="http://www.flickr.com/photos/digikam/4230098301/" title="OmarVieira_2 by digiKam team, on Flickr"><img src="http://farm5.static.flickr.com/4028/4230098301_4c61d90ee9_s.jpg" width="75" height="75" alt="OmarVieira_2"></a>
<a href="http://www.flickr.com/photos/digikam/4232352116/" title="p1010033 by digiKam team, on Flickr"><img src="http://farm3.static.flickr.com/2650/4232352116_1485bf6c3e_s.jpg" width="75" height="75" alt="p1010033"></a>
<a href="http://www.flickr.com/photos/digikam/4232352356/" title="DSC_0749 by digiKam team, on Flickr"><img src="http://farm5.static.flickr.com/4071/4232352356_30cfa9a6f6_s.jpg" width="75" height="75" alt="DSC_0749"></a>
<a href="http://www.flickr.com/photos/digikam/4234319650/" title="dsc02521_optimized by digiKam team, on Flickr"><img src="http://farm3.static.flickr.com/2800/4234319650_808199ebc7_s.jpg" width="75" height="75" alt="dsc02521_optimized"></a>
<a href="http://www.flickr.com/photos/dmpop/4154634751/" title="Blue Fireworks by dmpop, on Flickr"><img src="http://farm3.static.flickr.com/2635/4154634751_6c58f3000d_s.jpg" width="75" height="75" alt="Blue Fireworks"></a>
<a href="http://www.flickr.com/photos/dmpop/4071655950/" title="Amaryllis by dmpop, on Flickr"><img src="http://farm4.static.flickr.com/3172/4071655950_cb6952a941_s.jpg" width="75" height="75" alt="Amaryllis"></a>
<a href="http://www.flickr.com/photos/dmpop/4089219200/" title="Pincushion Protea by dmpop, on Flickr"><img src="http://farm3.static.flickr.com/2690/4089219200_8a3a64e25f_s.jpg" width="75" height="75" alt="Pincushion Protea"></a>
<a href="http://www.flickr.com/photos/dmpop/4127127215/" title="Green Stars with Teeth by dmpop, on Flickr"><img src="http://farm3.static.flickr.com/2670/4127127215_722003557c_s.jpg" width="75" height="75" alt="Green Stars with Teeth"></a>
<a href="http://www.flickr.com/photos/dmpop/4176300159/" title="Grewia Occidentalis (Tiliaceae) by dmpop, on Flickr"><img src="http://farm3.static.flickr.com/2684/4176300159_2779565054_s.jpg" width="75" height="75" alt="Grewia Occidentalis (Tiliaceae)"></a>
<a href="http://www.flickr.com/photos/dmpop/4165348241/" title="Droplets on Petal by dmpop, on Flickr"><img src="http://farm3.static.flickr.com/2732/4165348241_e65af99c3d_s.jpg" width="75" height="75" alt="Droplets on Petal"></a>
<a href="http://www.flickr.com/photos/dmpop/4149940756/" title="Sleeping Ladybug by dmpop, on Flickr"><img src="http://farm3.static.flickr.com/2686/4149940756_d10bf25265_s.jpg" width="75" height="75" alt="Sleeping Ladybug"></a>
<a href="http://www.flickr.com/photos/digikam/4237823902/" title="lake-amsoldingen by digiKam team, on Flickr"><img src="http://farm5.static.flickr.com/4010/4237823902_896613c96d_s.jpg" width="75" height="75" alt="lake-amsoldingen"></a>
<a href="http://www.flickr.com/photos/digikam/4247699064/" title="obr948 by digiKam team, on Flickr"><img src="http://farm5.static.flickr.com/4031/4247699064_0dea51afc3_s.jpg" width="75" height="75" alt="obr948"></a>
<a href="http://www.flickr.com/photos/digikam/4247699118/" title="obr1009 by digiKam team, on Flickr"><img src="http://farm3.static.flickr.com/2727/4247699118_8dbc229f1f_s.jpg" width="75" height="75" alt="obr1009"></a>
<a href="http://www.flickr.com/photos/digikam/4247699162/" title="obrazyvody555 by digiKam team, on Flickr"><img src="http://farm3.static.flickr.com/2696/4247699162_5983aa2787_s.jpg" width="75" height="75" alt="obrazyvody555"></a>
<a href="http://www.flickr.com/photos/digikam/4247699224/" title="oimagga by digiKam team, on Flickr"><img src="http://farm3.static.flickr.com/2749/4247699224_a8901f218d_s.jpg" width="75" height="75" alt="oimagga"></a>
<a href="http://www.flickr.com/photos/digikam/4247699272/" title="zubr45 by digiKam team, on Flickr"><img src="http://farm5.static.flickr.com/4021/4247699272_fed8dda73f_s.jpg" width="75" height="75" alt="zubr45"></a>
<a href="http://www.flickr.com/photos/digikam/4248444653/" title="showfoto by digiKam team, on Flickr"><img src="http://farm3.static.flickr.com/2737/4248444653_c55b13fd60_s.jpg" width="75" height="75" alt="showfoto"></a>
<a href="http://www.flickr.com/photos/digikam/4248444235/" title="digikam by digiKam team, on Flickr"><img src="http://farm5.static.flickr.com/4026/4248444235_f0664077b8_s.jpg" width="75" height="75" alt="digikam"></a>
<a href="http://www.flickr.com/photos/digikam/4259488823/" title="splash-digikam by digiKam team, on Flickr"><img src="http://farm3.static.flickr.com/2748/4259488823_764d6aa9e3_s.jpg" width="75" height="75" alt="splash-digikam"></a>
<a href="http://www.flickr.com/photos/digikam/4269739856/" title="00011 by digiKam team, on Flickr"><img src="http://farm3.static.flickr.com/2702/4269739856_dd35eefe56_s.jpg" width="75" height="75" alt="00011"></a>
<a href="http://www.flickr.com/photos/digikam/4290348660/" title="04-16-2009_014 by digiKam team, on Flickr"><img src="http://farm3.static.flickr.com/2743/4290348660_dc4a58c7a6_s.jpg" width="75" height="75" alt="04-16-2009_014"></a>
<a href="http://www.flickr.com/photos/digikam/4289605021/" title="004 by digiKam team, on Flickr"><img src="http://farm3.static.flickr.com/2744/4289605021_d7c3a19ff4_s.jpg" width="75" height="75" alt="004"></a>
<a href="http://www.flickr.com/photos/digikam/4289605059/" title="003 by digiKam team, on Flickr"><img src="http://farm3.static.flickr.com/2715/4289605059_2843eb183d_s.jpg" width="75" height="75" alt="003"></a>
<a href="http://www.flickr.com/photos/digikam/4290348830/" title="002 by digiKam team, on Flickr"><img src="http://farm5.static.flickr.com/4002/4290348830_63be75cc1d_s.jpg" width="75" height="75" alt="002"></a>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18964"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/493#comment-18964" class="active">...and some draft done with template</a></h3>    <div class="submitted">Submitted by digiKam on Wed, 2009-12-30 23:30.</div>
    <div class="content">
     <a href="http://www.flickr.com/photos/languitar/4228854161/" title="splash-0001 by languitar, on Flickr"><img src="http://farm5.static.flickr.com/4027/4228854161_a6bb38d066_t.jpg" width="100" height="61" alt="splash-0001"></a>
<a href="http://www.flickr.com/photos/languitar/4229621644/" title="splash-0014 by languitar, on Flickr"><img src="http://farm3.static.flickr.com/2643/4229621644_8e990a0ac8_t.jpg" width="100" height="61" alt="splash-0014"></a>
<a href="http://www.flickr.com/photos/languitar/4229621832/" title="splash- DSC8171 by languitar, on Flickr"><img src="http://farm3.static.flickr.com/2561/4229621832_e4eba7487a_t.jpg" width="100" height="61" alt="splash- DSC8171"></a>
<a href="http://www.flickr.com/photos/languitar/4228854941/" title="splash-Leopard (Panthera pardus)-2 by languitar, on Flickr"><img src="http://farm5.static.flickr.com/4024/4228854941_f5dbcb94cb_t.jpg" width="100" height="61" alt="splash-Leopard (Panthera pardus)-2"></a>
<a href="http://www.flickr.com/photos/languitar/4229677566/" title="splash-languitar1 by languitar, on Flickr"><img src="http://farm3.static.flickr.com/2677/4229677566_7dc1eb7a56_t.jpg" width="100" height="61" alt="splash-languitar1"></a>
<a href="http://www.flickr.com/photos/languitar/4229678222/" title="splash-languitar2 by languitar, on Flickr"><img src="http://farm3.static.flickr.com/2720/4229678222_f97a13c6f6_t.jpg" width="100" height="61" alt="splash-languitar2"></a>
<a href="http://www.flickr.com/photos/languitar/4229678474/" title="splash-languitar3 by languitar, on Flickr"><img src="http://farm3.static.flickr.com/2738/4229678474_9cd7bdb5a2_t.jpg" width="100" height="61" alt="splash-languitar3"></a>
<a href="http://www.flickr.com/photos/languitar/4229679064/" title="splash-languitar5 by languitar, on Flickr"><img src="http://farm3.static.flickr.com/2540/4229679064_f3b8a2aebd_t.jpg" width="100" height="61" alt="splash-languitar5"></a>
<a href="http://www.flickr.com/photos/languitar/4231195416/" title="splash-OmarVieira 2 by languitar, on Flickr"><img src="http://farm5.static.flickr.com/4032/4231195416_d9925e8706_t.jpg" width="100" height="61" alt="splash-OmarVieira 2"></a>
<a href="http://www.flickr.com/photos/languitar/4231178482/" title="splash-OmarVieira 1 by languitar, on Flickr"><img src="http://farm5.static.flickr.com/4021/4231178482_30f50f24a7_t.jpg" width="100" height="61" alt="splash-OmarVieira 1"></a>         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-18992"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/493#comment-18992" class="active">Contest is closed. And winners are...</a></h3>    <div class="submitted">Submitted by digiKam on Mon, 2010-01-25 13:10.</div>
    <div class="content">
     <p>digiKam 1.1.0 splashscreen by <b>Johannes Wienke</b>:</p>

<a href="http://websvn.kde.org/*checkout*/trunk/extragear/graphics/digikam/data/pics/splash-digikam.png?revision=1079654" title="digiKam 1.1.0 splash"><img src="http://websvn.kde.org/*checkout*/trunk/extragear/graphics/digikam/data/pics/splash-digikam.png?revision=1079654" width="500" height="307" alt="digiKam 1.1.0 splash"></a>

<p>Showfoto 1.1.0 splashscreen by <b>Petr Sigut</b></p>

<a href="http://websvn.kde.org/*checkout*/trunk/extragear/graphics/digikam/data/pics/splash-showfoto.png?revision=1079624" title="Showfoto 1.1.0 splash"><img src="http://websvn.kde.org/*checkout*/trunk/extragear/graphics/digikam/data/pics/splash-showfoto.png?revision=1079624" width="500" height="307" alt="Showfoto 1.1.0 splash"></a>

<p>digiKam 1.2.0 splashscreen by <b>Santoro Domenico</b>:</p>

<a href="http://websvn.kde.org/*checkout*/trunk/extragear/graphics/digikam/data/pics/splash-digikam.png?revision=1085128" title="digiKam 1.2.0 splash"><img src="http://websvn.kde.org/*checkout*/trunk/extragear/graphics/digikam/data/pics/splash-digikam.png?revision=1085128" width="500" height="307" alt="digiKam 1.2.0 splash"></a>

<p>Showfoto 1.2.0 splashscreen by <b>Kalyan Varma</b></p>

<a href="http://websvn.kde.org/*checkout*/trunk/extragear/graphics/digikam/data/pics/splash-showfoto.png?revision=1085129" title="Showfoto 1.2.0 splash"><img src="http://websvn.kde.org/*checkout*/trunk/extragear/graphics/digikam/data/pics/splash-showfoto.png?revision=1085129" width="500" height="307" alt="Showfoto 1.2.0 splash"></a>

<p>digiKam 1.3.0 splashscreen by <b>Kalyan Varma</b>:</p>

<a href="http://websvn.kde.org/*checkout*/trunk/extragear/graphics/digikam/data/pics/splash-digikam.png?revision=1110198" title="digiKam 1.3.0 splash"><img src="http://websvn.kde.org/*checkout*/trunk/extragear/graphics/digikam/data/pics/splash-digikam.png?revision=1110198" width="500" height="307" alt="digiKam 1.3.0 splash"></a>

<p>Showfoto 1.3.0 splashscreen by <b>Dmitri Popov</b></p>

<a href="http://websvn.kde.org/*checkout*/trunk/extragear/graphics/digikam/data/pics/splash-showfoto.png?revision=1110265" title="Showfoto 1.3.0 splash"><img src="http://websvn.kde.org/*checkout*/trunk/extragear/graphics/digikam/data/pics/splash-showfoto.png?revision=1110265" width="500" height="307" alt="Showfoto 1.3.0 splash"></a>

<p>digiKam 1.4.0 splashscreen by <b>Johannes Wienke</b>:</p>

<a href="http://websvn.kde.org/*checkout*/trunk/extragear/graphics/digikam/data/pics/splash-digikam.png?revision=1144599" title="digiKam 1.4.0 splash"><img src="http://websvn.kde.org/*checkout*/trunk/extragear/graphics/digikam/data/pics/splash-digikam.png?revision=1144599" width="500" height="307" alt="digiKam 1.4.0 splash"></a>

<p>Showfoto 1.4.0 splashscreen by <b>Johannes Wienke</b></p>

<a href="http://websvn.kde.org/*checkout*/trunk/extragear/graphics/digikam/data/pics/splash-showfoto.png?revision=1144599" title="Showfoto 1.4.0 splash"><img src="http://websvn.kde.org/*checkout*/trunk/extragear/graphics/digikam/data/pics/splash-showfoto.png?revision=1144599" width="500" height="307" alt="Showfoto 1.4.0 splash"></a>

<p>digiKam 1.5.0 splashscreen by <b>Petr Sigut</b>:</p>

<a href="http://websvn.kde.org/*checkout*/branches/extragear/graphics/digikam/data/pics/splash-digikam.png?revision=1144059" title="digiKam 1.5.0 splash"><img src="http://websvn.kde.org/*checkout*/branches/extragear/graphics/digikam/data/pics/splash-digikam.png?revision=1144059" width="500" height="307" alt="digiKam 1.5.0 splash"></a>

<p>Showfoto 1.5.0 splashscreen by <b>Jan Wachsmann</b></p>

<a href="http://websvn.kde.org/*checkout*/branches/extragear/graphics/digikam/data/pics/splash-showfoto.png?revision=1144064" title="Showfoto 1.5.0 splash"><img src="http://websvn.kde.org/*checkout*/branches/extragear/graphics/digikam/data/pics/splash-showfoto.png?revision=1144064" width="500" height="307" alt="Showfoto 1.5.0 splash"></a>         </div>
    <div class="links">» </div>
  </div>

</div>
