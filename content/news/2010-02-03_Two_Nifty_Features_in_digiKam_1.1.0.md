---
date: "2010-02-03T20:12:00Z"
title: "Two Nifty Features in digiKam 1.1.0"
author: "Dmitri Popov"
description: "Hot on the heels of digiKam 1.0, the team announced the 1.1.0 release of the popular open source photo management application. While the main focus"
category: "news"
aliases: "/node/500"

---

<a href="http://scribblesandsnaps.wordpress.com/2010/02/03/two-nifty-features-in-digikam-1-1-0" title="Photography and Open Source Two Nifty Features in digiKam 1.1.0"><img src="http://farm3.static.flickr.com/2746/4310695463_95811d3650.jpg" width="500" height="400" alt="Photography and Open Source Two Nifty Features in digiKam 1.1.0"></a>

<p>Hot on the heels of digiKam 1.0, the team <a href="http://www.digikam.org/drupal/node/497">announced</a> the 1.1.0 release of the popular open source photo management application. While the main focus in version 1.0 was on squashing bugs, the new release of digiKam does sport a couple of new nifty features and improvements.</p>

<a href="http://scribblesandsnaps.wordpress.com/2010/02/03/two-nifty-features-in-digikam-1-1-0">Continue to read...</a>
<div class="legacy-comments">

  <a id="comment-19054"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/500#comment-19054" class="active">Nepomuk</a></h3>    <div class="submitted">Submitted by xeros (not verified) on Fri, 2010-02-19 23:11.</div>
    <div class="content">
     <p>I've just set up properly Nepomuk on KDE 4.4 on Kubuntu 9.10 and I wanted to know if digiKam 1.0.0 supports Nepomuk already or it's integrated or planned in newer digiKam versions? It looks like Gwenview does share rating and tags on my desktop with Nepomuk but digiKam does not.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19055"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/500#comment-19055" class="active">Nepomuk support already done...</a></h3>    <div class="submitted">Submitted by digiKam on Fri, 2010-02-19 23:18.</div>
    <div class="content">
     <p>Since 1.0.0, with KDE 4.3.x, you can use Nepomuk in digiKam:</p>

<a href="http://www.flickr.com/photos/digikam/4042212776/" title="digikam-nepomuk-winxp by digiKam team, on Flickr"><img src="http://farm3.static.flickr.com/2542/4042212776_7b889ca661.jpg" width="500" height="313" alt="digikam-nepomuk-winxp"></a>         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19056"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/500#comment-19056" class="active">Thanks for a quick reply.</a></h3>    <div class="submitted">Submitted by xeros (not verified) on Fri, 2010-02-19 23:28.</div>
    <div class="content">
     <p>Thanks for a quick reply. I've just saw that digiKam package I've installed does not have libsoprano4 dependency (1.0.0-beta5 from official repository is dependent on libsoprano4) so maybe that could be a problem.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19057"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/500#comment-19057" class="active">install digiKam 1.1.0 not 1.0.0-beta5</a></h3>    <div class="submitted">Submitted by digiKam on Sat, 2010-02-20 09:59.</div>
    <div class="content">
     <p>Do not install an old beta. Use last stable release instead...</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-19069"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/500#comment-19069" class="active">Documentation for the other tabs</a></h3>    <div class="submitted">Submitted by <a href="http://russellharrison.com/" rel="nofollow">Russell</a> (not verified) on Fri, 2010-03-05 21:48.</div>
    <div class="content">
     <p>Could you point us to some documentation on what the other config tabs are used for in your screen shot?  I can't seem to find anything on them.</p>
<p>Thanks!<br>
Russell</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div>
</div>
