---
date: "2022-01-19T00:00:00"
title: "digiKam Recipes 22.01.21 released"
author: "Dmitri Popov"
description: "A new revision of the digiKam Recipes book is available"
category: "news"
---

New year, new revision of the [digiKam Recipes](https://dmpop.gumroad.com/l/digikamrecipes) book. It is a relatively modest update that features two new additions: how to upload photos to a remove machine via SSH directly from digiKam and how to access digiKam remotely via RDP. Oh, and there is a new colorful book cover.

As always, all _digiKam Recipes_ readers will receive the updated version of the book automatically and free of charge. The _digiKam Recipes_ book is available from [Google Play Store](https://play.google.com/store/books/details/Dmitri_Popov_digiKam_Recipes?id=T83DBAAAQBAJ) and [Gumroad](https://gumroad.com/l/digikamrecipes/).