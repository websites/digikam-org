---
date: "2023-12-03T00:00:00"
title: "digiKam 8.2.0 is released"
author: "digiKam Team"
description: "Dear digiKam fans and users, we are proud to announce the stable release of digiKam 8.2.0."
category: "news"
---

[![](https://i.imgur.com/bBj6VQb.png "digiKam 8.2.0 running under Microsoft Windows 10 with Qt6 framework")](https://imgur.com/bBj6VQb)
Dear digiKam fans and users,

After five months of active maintenance and long bugs triage, the digiKam team is proud to present version 8.2.0 of its open source digital photo manager.

See below the list of most important features coming with this release.

- Libraw       : Updated to snapshot 2023-11-21
- Bundles      : Updated Exiv2 to last [0.28.1 release](https://github.com/Exiv2/exiv2/releases/tag/v0.28.1)
- Bundles      : Updated ExifTool to last [12.70 release](https://exiftool.org/history.html).
- Bundles      : Linux and macOS uses KF5 framework to last 5.110

This version arrives with a [long review of bugzilla entries](https://bugs.kde.org/buglist.cgi?f1=cf_versionfixedin&limit=0&list_id=2536450&o1=equals&order=bug_id&product=digikam&v1=8.2.0).
Long time bugs present in older versions have been fixed and we spare a lots of time to contact users to validate changes in pre-release to confirm
fixes before to deploy the program in production.

The application internationalization has also been updated. digiKam and Showfoto are proposed with 61
different languages for the graphical interface. Go to Settings/Configure Languages dialog and change
the localization as you want. Applications need to be restarted to apply changes. If you want to
contribute to the internationalization of digiKam, please contact the
[translator teams](https://l10n.kde.org/), following the translation
[how-to](https://l10n.kde.org/docs/translation-howto/). The statistics about
translation states are available [here](https://l10n.kde.org/stats/gui/trunk-kf5/po/#digikam).

Thanks to the translators who have worked on the online documentation
[internationalisations](https://l10n.kde.org/stats/gui/trunk-kf5/package/digikam-doc/).
You can read and search over the document [here](https://docs.digikam.org/en/).
You are welcome to contribute to application handbook translations following the coordination team instructions.

### Windows Version Revival

[![](https://i.imgur.com/mn8lJ1H.png "Showfoto 8.2.0 running under Microsoft Windows 10 with Qt6 framework")](https://imgur.com/mn8lJ1H)

For a very long time, digiKam for Windows was provided to end users by a version fully cross compiled under Linux using [MXE](https://mxe.cc/).
No Windows operating system was used in the process to build the application and to package the installer. The advantage of this
solution was the simplicity of the production workflow, using the same environnement to build the Linux and the Windows versions.
Also, it's known that GNU compilers are faster than Microsoft compilers.

The disadvantages of the cross-compilation was the incompatibilities with some core Windows functionalities and client code
needs specific implementations to turn-around. Also, important limitations in the frameworks used by the application breaks
some features in time, as the web-services usage for example, where for security reasons the web access components need to be up-to-date.
The usage of most recent frameworks was also impossible which limited the way to include new functionalities and bugfixes.

[![](https://i.imgur.com/hYvBdqw.png "digiKam 8.2.0 running the Flickr Export Web Service Tool")](https://imgur.com/hYvBdqw)

This is why the cross-compiled version was left in favor of a native Windows 10 build using the open source [Microsoft VCPKG](https://vcpkg.io/en/)
tool-chain. Native Microsoft compiler is now used to build the application which guarantees a better compatibility at run time.

We also switched the Windows version from Qt and KDE frameworks version 5 to the version 6. It's a big step for the project as we left
the long time version of core framework used internally to the new major version where plenty of bugs have been fixed with more modern
and optimized implementations. Note that macOS and Linux bundles are still running with Qt version 5, but it's just a question of time.

### Future Plans

Next maintenance version is planned to be published in february 2024 with more bug fixes and improvements.
Particularly, one important new feature will be included: the Auto-Tags support.

[![](https://i.imgur.com/rwSFQiE.png "digiKam 8.3.0 Auto-Tags Maintenance tool")](https://imgur.com/rwSFQiE)

This summer, a student works on a project based on deep-learning to assign tags automatically,
based on content analysis, to detect forms, objects, places, animals, plants, monuments, etc.
This project adds two new tools in digiKam: one in the Maintenance engine, and one other in Batch Queue Manager.

[![](https://i.imgur.com/0lAgaNf.png "digiKam 8.3.0 Auto-Tags Batch Queue Manager tool")](https://imgur.com/0lAgaNf)

You can read more details about the Auto-Tags project in [the student blog](https://community.kde.org/GSoc/2023/StatusReports/QuocHungTran#Add_Automatic_Tags_Assignment_Tools_and_Improve_Face_Recognition_Engine_for_digiKam).
Even if this project will be only available in next 8.3.0 stable release, you can already test it with
the rolling [pre-release bundles available here](https://files.kde.org/digikam/). Note that pre-release are not dedicated to use yet in production.

We also plan to port the Linux AppImage based on the last Qt and KDE frameworks version 6 as the Windows version.

### Final Words

Thanks to all users for [your support and donations](https://www.digikam.org/donate/),
and to all contributors, students, testers who allowed us to improve this release.

digiKam 8.2.0 can be downloaded from [this repository](https://download.kde.org/stable/digikam/8.2.0/) as:

- Source code tarball.
- Linux 64 bits AppImage bundles compatible with systems based on glibc >= 2.27.
- Windows 10 (or later) 64 bits installers or bundle archives.
- macOS Intel packages compatible with Apple Silicon CPU computers using [Rosetta 2](https://support.apple.com/en-us/HT211861),

Rendez-vous in a few months for the next digiKam 8.3.0 maintenance release.

Happy digiKaming and all the best for this end of year...
