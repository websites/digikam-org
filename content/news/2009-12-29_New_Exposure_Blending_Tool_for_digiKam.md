---
date: "2009-12-29T20:06:00Z"
title: "New Exposure Blending Tool for digiKam"
author: "digiKam"
description: "...or how to make pseudo HDR images with digiKam using a stack of bracketed images... ExpoBlending is the name of a new tool that i"
category: "news"
aliases: "/node/494"

---

<p>...or how to make pseudo HDR images with digiKam using a stack of bracketed images...</p>

<a href="http://www.flickr.com/photos/digikam/4225472620/" title="expoblending-28 by digiKam team, on Flickr"><img src="http://farm3.static.flickr.com/2647/4225472620_a21310982d.jpg" width="500" height="313" alt="expoblending-28"></a>

<p><b>ExpoBlending</b> is the name of a new tool that i implement currently for next kipi-plugins 1.1 release planed to end of January. This plugin use 2 command line tools available under Linux, Windows, and Mac : <b>align_image_stack</b> from <a href="http://hugin.sourceforge.net">Huggin project</a> and <b>enfuse</b> from <a href="http://enblend.sourceforge.net">Enblend project</a></p>

<p>Plugin is composed by two parts : an Import Wizard to pre-process bracketed images, and an second one which is a Front-End to <b>enfuse</b>. See below some plugin screen-shots with details</p>

<a href="http://www.flickr.com/photos/digikam/4224701715/" title="expoblending-20 by digiKam team, on Flickr"><img src="http://farm5.static.flickr.com/4058/4224701715_691634986e_m.jpg" width="240" height="150" alt="expoblending-20"></a>
<a href="http://www.flickr.com/photos/digikam/4224701935/" title="expoblending-21 by digiKam team, on Flickr"><img src="http://farm3.static.flickr.com/2589/4224701935_82b6c7480e_m.jpg" width="240" height="150" alt="expoblending-21"></a>
<a href="http://www.flickr.com/photos/digikam/4225470776/" title="expoblending-22 by digiKam team, on Flickr"><img src="http://farm3.static.flickr.com/2526/4225470776_59d3ba062f_m.jpg" width="240" height="150" alt="expoblending-22"></a>

<p>ExpoBlending plugin is available in digiKam Tools menu entry. The first wizard page welcome you and explain what you can do with it. If you have set before a selection of bracketed images from your digiKam collection, you will see all items in the stack. You can add more of course using drag and drop or usual images selection dialog.</p>

<a href="http://www.flickr.com/photos/digikam/4225471026/" title="expoblending-23 by digiKam team, on Flickr"><img src="http://farm3.static.flickr.com/2737/4225471026_f2c14aab30_m.jpg" width="240" height="150" alt="expoblending-23"></a>
<a href="http://www.flickr.com/photos/digikam/4225471248/" title="expoblending-24 by digiKam team, on Flickr"><img src="http://farm3.static.flickr.com/2629/4225471248_0e5e979975_m.jpg" width="240" height="150" alt="expoblending-24"></a>
<a href="http://www.flickr.com/photos/digikam/4225471504/" title="expoblending-25 by digiKam team, on Flickr"><img src="http://farm5.static.flickr.com/4071/4225471504_8e760f2696_m.jpg" width="240" height="150" alt="expoblending-25"></a>

<p>The goal of this wizard is to prepare images. If Raw images are selected, there are converted to tiff 16 bits with auto-gamma. If you turn on auto-alignment option, align_image_stack program will be called to adjust bracketed images position. New tiff files will be generated for that. Auto-alignment is very important there, especially if you haven't used a tripod to take pictures. Raw conversion and alignment can take a while of course... Start job and take a Champagne cup to enjoy this new year...</p>

<a href="http://www.flickr.com/photos/digikam/4225471686/" title="expoblending-26 by digiKam team, on Flickr"><img src="http://farm5.static.flickr.com/4052/4225471686_10e4f09340_m.jpg" width="240" height="150" alt="expoblending-26"></a>
<a href="http://www.flickr.com/photos/digikam/4225472076/" title="expoblending-27 by digiKam team, on Flickr"><img src="http://farm3.static.flickr.com/2626/4225472076_87a7c784ec_m.jpg" width="240" height="150" alt="expoblending-27"></a>
<a href="http://www.flickr.com/photos/digikam/4225472620/" title="expoblending-28 by digiKam team, on Flickr"><img src="http://farm3.static.flickr.com/2647/4225472620_a21310982d_m.jpg" width="240" height="150" alt="expoblending-28"></a>

<p>Great. Pre-processing is done. It's time to use the second part of this plugin and to fuse bracketed images to a pseudo HDR image. On this new window, you can see a preview area on the left, and on the right, the bracketed images stack on the top, all Enfuse settings on the center, and finally, all processed images generated on the bottom. For each enfused image, you can choose which input bracketed images you want to use. Selecting a processed image on the bottom will load it to preview, to easy compare results. You can zoom in/out and pan preview if you want. To save processed images, just press Save button, and all selected items from processed stack will be saved to the current digiKam album.</p>

<p>As all is multi-threaded in this plugin, you can go back to digiKam and lets computations running in background. Like this, you can process enfused items and at the same time compare the best one on the Light Table.</p>

<p>Note: this plugin exist also as a stand alone application named... <b>expoblending</b></p>

<p>
</p>
<div class="legacy-comments">

  <a id="comment-18957"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/494#comment-18957" class="active">Wow!</a></h3>    <div class="submitted">Submitted by mutlu (not verified) on Tue, 2009-12-29 21:30.</div>
    <div class="content">
     <p>Gilles, this is great news! I often take architecture pictures which can have incredibly high contrast, leading to underexposed parts. I was considering getting a Nikon DSLR since some of them have "active d-lighting". However, they are quite expensive and I was very glad to discover that enfuse can basically do the same.</p>
<p>Reading your blog and seeing that my favorite photo management program will offer this functionality soon, makes me very happy!</p>
<p>Thank you so much for your amazing work!</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-18960"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/494#comment-18960" class="active">Great job!</a></h3>    <div class="submitted">Submitted by <a href="http://gerlos.altervista.org" rel="nofollow">gerlos</a> (not verified) on Wed, 2009-12-30 13:51.</div>
    <div class="content">
     <p>Great! I've used enfuse some time from the command line and liked it a lot, I can't wait to see it in action in DigiKam!!!</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-18962"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/494#comment-18962" class="active">Absolutely great news!</a></h3>    <div class="submitted">Submitted by Mikkel (not verified) on Wed, 2009-12-30 18:54.</div>
    <div class="content">
     <p>I have been working with this in the Hugin GUI and QtpfsGUI, but I think it is absolutely wonderful that it will be integrated into digiKam!</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-18965"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/494#comment-18965" class="active">Fantastic news!</a></h3>    <div class="submitted">Submitted by Timothee Groleau (not verified) on Thu, 2009-12-31 01:28.</div>
    <div class="content">
     <p>I've been using these 2 tools at the command line and I love them both! It's great to see them in a kipi GUI.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18966"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/494#comment-18966" class="active">Agreed. Enfuse usually</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Thu, 2009-12-31 20:24.</div>
    <div class="content">
     <p>Agreed. Enfuse usually produces wonderful results, much better than what I usually get by doing stuff manually. This is will be a fantastic enhancement to digikam!</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-18971"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/494#comment-18971" class="active">digikam filters</a></h3>    <div class="submitted">Submitted by brutus (not verified) on Tue, 2010-01-05 20:05.</div>
    <div class="content">
     <p>I tried digikam a while ago, It seems evolving pretty well.</p>
<p>So happy for these new and beautiful linux software :) Linux freedom is a must.</p>
<p>Digikam is really good, usable and competing with photoshop. Gimp is good, but a bit ugly and unusable for the new web 2.0 generation. Lightroom is the best tool in years. And digikam as all of the goodies, but it will be better. </p>
<p>Please integrate it in Ubuntu! </p>
<p>Will I be able to import lightroom filters ? Or even a similar filter system would be great :)<br>
That would allow to exchange out of the box effects for our photographs, share ideas and also being able to save and load custom filters.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18972"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/494#comment-18972" class="active">LR metadata filter already there...</a></h3>    <div class="submitted">Submitted by digiKam on Wed, 2010-01-06 09:37.</div>
    <div class="content">
     <p>LightRoom XMP namespace metadata import and export is already available in 1.0.0. But it don't manage xmp sidecar yet, only XMP metadata embedded in images.</p>
<p>digiKam</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-18979"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/494#comment-18979" class="active">Tourist remover</a></h3>    <div class="submitted">Submitted by pilki (not verified) on Fri, 2010-01-08 14:26.</div>
    <div class="content">
     <p>Hi!</p>
<p>That's, once again, a great news. I was actually looking for a tool to do that, and BOOM, you put it in digikam. Awesome :)</p>
<p>Do you think it would be hard to modify this so it could become a "tourist remover tool" (something like http://www.snapmania.com/info/en/trm/)</p>
<p>Thanks again for your work</p>
         </div>
    <div class="links">» </div>
  </div>

</div>
