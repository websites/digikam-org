---
date: "2010-06-23T23:01:00Z"
title: "Install the Latest Version of digiKam on Ubuntu 10.04"
author: "Dmitri Popov"
description: "Can’t wait till the latest version of digiKam appears in the official Ubuntu software repositories? You don’t have to: using the personal package archives (PPA)"
category: "news"
aliases: "/node/528"

---

<p>Can’t wait till the latest version of digiKam appears in the official Ubuntu software repositories? You don’t have to: using the personal package archives (PPA) provided by the Launchpad service, you can install the latest release of digiKam with a few simple commands. <a href="http://scribblesandsnaps.wordpress.com/2010/06/24/install-the-latest-version-of-digikam-on-ubuntu-10-04/">Continue to read</a></p>

<div class="legacy-comments">

  <a id="comment-19325"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/528#comment-19325" class="active">DigiKam for Ubuntu 10.04</a></h3>    <div class="submitted">Submitted by Greg Heath (not verified) on Fri, 2010-06-25 19:57.</div>
    <div class="content">
     <p>I just got started with Ubuntu 10.04 and DigiKam and It's just AWESOME !!!!!</p>
         </div>
    <div class="links">» </div>
  </div>

</div>