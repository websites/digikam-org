---
date: "2008-01-20T23:10:00Z"
title: "digiKam has a new Time-Line view!"
author: "digiKam"
description: "Since digiKam 0.9.3 is released, i working hard to a new major feature for digiKam 0.9.4 : a Time-Line to perform complex date search around"
category: "news"
aliases: "/node/293"

---

Since digiKam 0.9.3 is released, i working hard to a new major feature for digiKam 0.9.4 : a Time-Line to perform complex date search around whole image collection.

<br><br>

My first approach of Time-Line have been describe in this <a href="http://www.digikam.org/?q=node/283">blog entry</a>, including this tool in calendar view. This way been limited and unadapted because calendar view is not a Search view as well. To solve this problem, i have created a new left sidebar tab named "timeline"... 

<br><br>
<a href="/files/images/timeline1.png"><img src="/files/images/timeline1.preview.png" width="480" height="320">
</a>
<br><br>

See below an advanced description of Time-Line tool in action:

<br><br>
<a href="/files/images/timeline2.png"><img src="/files/images/timeline2.png" width="640" height="402">
</a>
<br><br>

On the top, 2 options can be used to change Time Decade and Histogram Scale of diagram. Diagram is composed of 3 parts: 

<ul>
<li>On the bottom, a grid with time decade description. At this place user can select select date range (in blue).</li>
<li>On the middle, histogram bar (in green) witch counts number of items for time-stamp.</li>
<li>A cursor bar (black rectangle) to explore count of items.</li>
</ul>

<br><br>

Cursor is used to get descriptions of histogram bar: date and item counts. These informations are print just over the diagram. To move cursor, just press left mouse button somewhere on histogram bar. You can drag and drop cursor over the widget, time will scroll automaticaly. You can also use Mouse wheel over Histogram to naviguate around whole Time-Line.

<br><br>

Selection are processed to press left mouse button behind histogram bar. Selection can be contiguous or discontiguous. Use CTRL key to make more than one selection at the same time. When a new selection is done, digiKam search KIO-slave will query database in background and result will be displayed on icon-view. Selection are preserved if you change Time-Unit. Internally, all selection are based on days (smaller time decade).

<br><br>
<a href="/files/images/timelineselectionmode.png"><img src="/files/images/timelineselectionmode.png" width="453" height="378">
</a>
<br><br>

There are two possible selections:

<ul>
<li>A blue rectange when selection is complete. With Days decade, selection are always complete.</li>
<li>A greyed blue rectangle when selection are uncomplete. This case become when you toogle to an higher decade than Days and if all days correponding to a time unit are not fully selected. For example, if you select 3 days of week in Days decade, and if you toogle to Week time-unit, you will see the corresponding Week as an uncomplete selection.</li>
</ul>

<br><br>
<a href="/files/images/timelineselection.png"><img src="/files/images/timelineselection.preview.png" width="255" height="600">
</a>
<br><br>

Selection still active if you change Time-Unit Decade. Nothing is lost. To reset current selection, press on clear selection button behind histogram. When your selection is fine, you can save it into a new Virtual folder. Set virtual name on text field and press on save selection button. That all. Later if you select a virtual album saved in "My Search Date" virtual folder, selection will be resored on Time-Line and images corresponding to your search will appear on icon-view.

<br><br>

I hope than Time-Line view will give happiness to all digiKam-users about Date Search query around whole images collection...
<div class="legacy-comments">

</div>