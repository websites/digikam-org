---
date: "2007-06-20T21:36:00Z"
title: "Final DigiKam 0.9.2 Release available"
author: "Anonymous"
description: "Dear digiKam users, the digiKam team is happy to announce a new release of the advanced digital photo management application for linux. In addition to"
category: "news"
aliases: "/node/234"

---

<p>Dear digiKam users,</p>
<p>the digiKam team is happy to announce a new release of the<br>
advanced digital photo management application for linux.</p>
<p>In addition to various bug-fixes and improvements,<br>
several new features have been added. </p>
<p>Some important improvements/changes are:</p>
<ul>
<li>a <a href="http://www.digikam.org/?q=node/222">light-table</a> to quickly compare similar images side by side
  </li>
<li>Thumbnail size in the album view can be adjusted continuously, <a href="http://www.digikam.org/?q=node/218">details</a>.
  </li>
<li>Image Editor: reorganized menu structure
  </li>
<li>Image Editor: new <a href="http://www.digikam.org/?q=system/files&amp;file=images/neweditorpantool.png">pan tool</a>
  </li>
<li>Image Editor: new Sharpness Editor (replacing Unsharp Mask, Refocus, Sharpen)
  </li>
<li>DigikamImagePlugins have been merged into digiKam, <a href="http://www.digikam.org/?q=node/217">details</a>.
    </li>
<li>Image Plugins: completely rewritten <a href="http://www.digikam.org/?q=node/212">Red Eyes Correction</a>
  </li>
<li>Image Plugins: Color Effects pack: solarize, velvia (new!), neon, edge
   </li>
<li>Image Plugins: improved <a href="http://www.digikam.org/?q=node/216">Black &amp; White converter</a>
  </li>
<li>Image Plugins : Speed improvements for Restoration, Inpainting and<br>
  Blowup (update of the CImg library to 1.1.9), <a href="http://www.digikam.org/?q=node/215">details</a>.
  </li>
<li>several keyboard shortcuts have changed to comply with KDE guidelines
  </li>
<li>New dependency to libkdcraw to decode RAW file (uses dcraw 8.60 inside)
  </li>
</ul>
<p>For more information and resolved issues<br>
please consult NEWS.0.9.2 and the Changelog.</p>
<p>The digiKam developer team would like to thank<br>
everyone who has helped by testing, bug reports, bug fixes,<br>
and suggestions for improvements.</p>
<p>Download and installation</p>
<p>The tarball can be downloaded from <a href="http://sourceforge.net/project/showfiles.php?group_id=42641&amp;package_id=34800">SourceForge</a>.</p>
<p>Shared libraries required by digiKam have been updated:<br>
<a href="http://sourceforge.net/project/showfiles.php?group_id=42641&amp;package_id=34800">libkexiv2</a> and <a href="https://sourceforge.net/project/showfiles.php?group_id=149779&amp;package_id=230894">libkdcraw</a></p>
<p>Note, that the formerly separate DigikamImagePlugins<br>
have been merged into digiKam.</p>
<p>Kubuntu packages are already available <a href="http://www.mpe.mpg.de/~ach/kubuntu/feisty/Pkgs.php">here</a> (thanks to Achim).</p>

<div class="legacy-comments">

</div>