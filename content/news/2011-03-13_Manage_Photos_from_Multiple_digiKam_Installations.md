---
date: "2011-03-13T18:53:00Z"
title: "Manage Photos from Multiple digiKam Installations"
author: "Dmitri Popov"
description: "Storing your photos on a server or network disk? Want to manage them from several Linux-based machines using digiKam? Here is how to do that."
category: "news"
aliases: "/node/585"

---

<p>Storing your photos on a server or network disk? Want to manage them from several Linux-based machines using digiKam? Here is how to do that. <a href="http://scribblesandsnaps.wordpress.com/2011/03/13/manage-photos-from-multiple-digikam-installations/">Continue to read</a></p>

<div class="legacy-comments">

</div>