---
date: "2010-11-22T19:15:00Z"
title: "Install digiKam on Windows"
author: "Dmitri Popov"
description: "If you want to install digiKam on Windows, you have two options: you can either compile the application from the source code or you can"
category: "news"
aliases: "/node/549"

---

<p>If you want to install digiKam on Windows, you have two options: you can either compile the application from the source code or you can use the KDE Windows installer. The latter approach is by far the easiest one, but there is a drawback: the installer usually includes an older version of digiKam. If you can live with that, and you don’t feel like getting your hands dirty with compiling digiKam from the source, then the KDE installer is the way to go. <a href="http://scribblesandsnaps.wordpress.com/2010/11/21/install-digikam-on-windows/">Continue to read</a></p>

<div class="legacy-comments">

</div>