---
date: "2009-07-20T15:05:00Z"
title: "digiKam digest - 2009-07-19"
author: "Anonymous"
description: "Great week for metadata maniacs! Support for different authors of comments in various languages Fully customizable \"Short view\" (aka \"Human readable\") in Metadata tab; unfortunately"
category: "news"
aliases: "/node/467"

---

<p>Great week for metadata maniacs!</p>
<ul>
	<li>Support for different authors of comments in various languages
	<ul>
		<li><a href="http://www.flickr.com/photos/digikam/3724445487/" title="captionedit by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3499/3724445487_d126c5c111.jpg" width="213" height="250" alt="captionedit"></a></li>
	</ul></li>
	<li>Fully customizable "Short view" (aka "Human readable") in Metadata tab; unfortunately due to feature freeze for 4.3 duration this will not be available in digiKam 1.0 with this KDE release, but only with KDE 4.4.
	<ul>
		<li><a href="http://www.flickr.com/photos/digikam/3736004772/" title="metadatafilter by digiKam team, on Flickr"><img src="http://farm3.static.flickr.com/2515/3736004772_af1ee3cb0b.jpg" width="250" height="156" alt="metadatafilter"></a>
		</li><li><a href="http://www.flickr.com/photos/digikam/3731291883/" title="metadatafilter01 by digiKam team, on Flickr"><img src="http://farm3.static.flickr.com/2566/3731291883_d9de8b8d2e.jpg" width="250" height="156" alt="metadatafilter01"></a>
		</li><li><a href="http://www.flickr.com/photos/digikam/3732089800/" title="metadatafilter02 by digiKam team, on Flickr"><img src="http://farm3.static.flickr.com/2574/3732089800_eac7be0685.jpg" width="250" height="156" alt="metadatafilter02"></a>
		</li><li><a href="http://www.flickr.com/photos/digikam/3732089964/" title="metadatafilter03 by digiKam team, on Flickr"><img src="http://farm3.static.flickr.com/2593/3732089964_8c8b3cddf2.jpg" width="250" height="156" alt="metadatafilter03"></a>
		</li><li><a href="http://www.flickr.com/photos/digikam/3732090158/" title="metadatafilter04 by digiKam team, on Flickr"><img src="http://farm3.static.flickr.com/2646/3732090158_e3d24d9f65.jpg" width="250" height="156" alt="metadatafilter04"></a>
	</li></ul></li>
	<li>Further optimizations in image plugins</li>
	<li>Fixed bug about creation date for non-EXIF-able files</li>
	<li>Fixes and improvements in templates system</li>
	<li>Compatibility with libjpeg7</li>
	<li>Multiple fixes and improvements in advanced slideshow kipi-plugin</li>
</ul>

<style type="text/css">
div.dd { font-family: sans-serif }
div.dd tr.bugs { border: 1px solid white }
div.dd tr.bugs td, div.dd tr.bugs th { text-align: center; padding: 3px }
div.dd code { white-space: pre-wrap; overflow: auto ; font-size: 10pt; font-family: DejaVu Sans Mono, Lucida Console, monospace}
div.dd h5 { font-weight: bold; margin-bottom: 0; padding-bottom: 0 }
div.dd a { text-decoration: none }
</style>

<div class="dd">
<hr>

<h3>Bug/wish table</h3>

<table summary="Bugs and wishes dealt with in last week">
<thead>
<tr class="bugs">
<th></th>
<th>Opened bugs</th>
<th>Opened last week</th>
<th>Closed last week</th>
<th>Change</th>
<th>Opened wishes</th>
<th>Opened last week</th>
<th>Closed last week</th>
<th>Change</th>
</tr>
</thead>


<tbody><tr class="bugs"><td>digikam</td><td>241</td><td>+22</td><td>-16</td><td>6</td><td>274</td><td>+2</td><td>-4</td><td>-2</td></tr>
<tr class="bugs"><td>kipiplugins</td><td>102</td><td>+12</td><td>-11</td><td>1</td><td>150</td><td>+2</td><td>-2</td><td>0</td></tr>
</tbody></table>

<p>
Full tables:
</p>
<ul>
<li><a href="https://bugs.kde.org/component-report.cgi?product=digikam">digiKam</a></li>
<li><a href="https://bugs.kde.org/component-report.cgi?product=kipiplugins">KIPI-plugins</a></li>
</ul>

<hr>

<h3>NEWS reports</h3>

<p>Issues closed last week (note: due to different methodologies numbers may not add up with table above)</p>

<h4>digiKam</h4>

<code> 
+General        : Exif, Makernotes, Iptc, and Xmp metadata tags filtering can be customized in setup config dialog.
+General        : Add support of Author properties to multi-language Captions. User can set 
+                 an author name for each caption value.
+ImageEditor    : Close tools by pressing ESC.
+016 ==&gt; <a href="https://bugs.kde.org/show_bug.cgi?id=199251">199251</a> : digiKam crashes when setting exif orientation tag.
+017 ==&gt; <a href="https://bugs.kde.org/show_bug.cgi?id=199482">199482</a> : Caption can't be canged.
+018 ==&gt; <a href="https://bugs.kde.org/show_bug.cgi?id=174586">174586</a> : Loading video from camera consumes ll memory.
+019 ==&gt; <a href="https://bugs.kde.org/show_bug.cgi?id=199667">199667</a> : Make writing of metadata to picture enabled by default.
+020 ==&gt; <a href="https://bugs.kde.org/show_bug.cgi?id=199981">199981</a> : XMP synchronization of digiKam namespace occurs for second tag.
+021 ==&gt; <a href="https://bugs.kde.org/show_bug.cgi?id=199967">199967</a> : Creation date newer than last modified date.
+022 ==&gt; <a href="https://bugs.kde.org/show_bug.cgi?id=200190">200190</a> : tag filters: unlike older versions AND-condition doesn't match anymore.
+023 ==&gt; <a href="https://bugs.kde.org/show_bug.cgi?id=193616">193616</a> : Location not shown digiKam right sidebar after geolocating in external program.
+024 ==&gt; <a href="https://bugs.kde.org/show_bug.cgi?id=189156">189156</a> : digikam crashes on importing directories with some (cyrillic?) file names.
+025 ==&gt; <a href="https://bugs.kde.org/show_bug.cgi?id=191963">191963</a> : calender sort by wrong dates on images without properties.
+026 ==&gt; <a href="https://bugs.kde.org/show_bug.cgi?id=189072">189072</a> : Right click popup menu.
+027 ==&gt; <a href="https://bugs.kde.org/show_bug.cgi?id=200223">200223</a> : Default language of spellchecker is ignored.
+028 ==&gt; <a href="https://bugs.kde.org/show_bug.cgi?id=126086">126086</a> : Besides the basic and full exif info I would like a page with selectable fields.
+029 ==&gt; <a href="https://bugs.kde.org/show_bug.cgi?id=200637">200637</a> : Too small fonts in collections setup page.
+030 ==&gt; <a href="https://bugs.kde.org/show_bug.cgi?id=135476">135476</a> : Support for multiple comment authors.
+031 ==&gt; <a href="https://bugs.kde.org/show_bug.cgi?id=200246">200246</a> : digiKam does not recognize GPS EXIF information.
</code>

<h4>Kipi-plugins</h4>

<code>
+012 ==&gt; SendImages   : <a href="https://bugs.kde.org/show_bug.cgi?id=146487">146487</a> : kipi-plugins export to email fails when email too large.
+013 ==&gt; SendImages   : <a href="https://bugs.kde.org/show_bug.cgi?id=189984">189984</a> : Impossible to email images if the path contains characters such as ?Š?¨? .
+014 ==&gt; RawConverter : <a href="https://bugs.kde.org/show_bug.cgi?id=161337">161337</a> : Image rotation of converted RAW photos (portrait format) broken.
+015 ==&gt; Calendar     : <a href="https://bugs.kde.org/show_bug.cgi?id=196888">196888</a> : Create Calender: Year formatted as 2,009 on last page of wizard.
+016 ==&gt; Calendar     : <a href="https://bugs.kde.org/show_bug.cgi?id=111697">111697</a> : I wish for the option to change the first day of week (sun / mon).
+017 ==&gt; GPSSync      : <a href="https://bugs.kde.org/show_bug.cgi?id=187517">187517</a> : gpsssync when entering no map.
+018 ==&gt; BatchProcess : <a href="https://bugs.kde.org/show_bug.cgi?id=200483">200483</a> : digiKam crashes after batch image-color-improvement on single picture.
+019 ==&gt; AcquireImages: <a href="https://bugs.kde.org/show_bug.cgi?id=200558">200558</a> : Crash when saving scanned image to .tiff.
+020 ==&gt; BatchProcess : <a href="https://bugs.kde.org/show_bug.cgi?id=200217">200217</a> : Crash after batch recompress.
+021 ==&gt; BatchProcess : <a href="https://bugs.kde.org/show_bug.cgi?id=200722">200722</a> : Crashes after successfull batch and closing resize dialog.
+022 ==&gt; AdvSlideShow : <a href="https://bugs.kde.org/show_bug.cgi?id=200647">200647</a> : Not all main settings are not restored correctly.
+023 ==&gt; BatchProcess : <a href="https://bugs.kde.org/show_bug.cgi?id=168243">168243</a> : Delete photo after not found imagemagick
+024 ==&gt; BatchProcess : <a href="https://bugs.kde.org/show_bug.cgi?id=149488">149488</a> : after batch renaming pictures, the pictures appears as unkown images, and can't be removed from album nor can be viewed or edit
+025 ==&gt; SendImages   : <a href="https://bugs.kde.org/show_bug.cgi?id=146828">146828</a> : Sendimages opera command line.
</code>

<hr>

<h3>Selected commits</h3>
<h5>KDE/kdegraphics/libs/libkdcraw/libkdcraw (2009-07-13 01:03)</h5>
<code>
SVN commit <a href="http://websvn.kde.org/?view=rev&amp;revision=995545">995545</a> by cfeck:

Make it possible to create a DcrawSettingsWidget with only a QWidget as parameter (again :) 

<em>1 file changed</em>
</code>
<hr>
<h5>extragear/graphics/digikam (2009-07-13 17:29)</h5>
<code>
SVN commit <a href="http://websvn.kde.org/?view=rev&amp;revision=995957">995957</a> by mwiesweg:

Use the file descriptor based method to download a file with gphoto2.
This should solve problems with very large files because they can directly be written to disk.

BUG: <a href="https://bugs.kde.org/show_bug.cgi?id=174586">174586</a> 

<em>2 files changed</em>
</code>
<hr>
<h5>extragear/graphics/digikam/libs/widgets/imageplugins (2009-07-14 13:16)</h5>
<code>
SVN commit <a href="http://websvn.kde.org/?view=rev&amp;revision=996427">996427</a> by aclemens:

Do not create / draw the cursor on every mouse move.
The drawing of the mask is a little bit faster now, but still it
flickers with cursor-sizes above 64px. 

<em>2 files changed</em>
</code>
<hr>
<h5>KDE/kdegraphics/libs/libkexiv2/libkexiv2 (2009-07-14 16:26)</h5>
<code>
SVN commit <a href="http://websvn.kde.org/?view=rev&amp;revision=996566">996566</a> by sengels:

people might have older exiv2 versions installed - do not use newer API before that 

<em>1 file changed</em>
</code>
<hr>
<h5>extragear/graphics/digikam/libs/widgets/imageplugins (2009-07-14 16:59)</h5>
<code>
SVN commit <a href="http://websvn.kde.org/?view=rev&amp;revision=996602">996602</a> by aclemens:

More performance fixes: Do not call repaint() directly, ever... in most
of the cases we don't need this.
This greatly improves performance on my machine. 

<em>1 file changed</em>
</code>
<hr>
<h5>extragear/graphics/digikam/libs/database (2009-07-14 12:27)</h5>
<code>
SVN commit <a href="http://websvn.kde.org/?view=rev&amp;revision=996418">996418</a> by mwiesweg:

Use the oldest of ctime, mtime when falling back to the file system to determine the creation date

CCBUG: <a href="https://bugs.kde.org/show_bug.cgi?id=199967">199967</a> 

<em>2 files changed</em>
</code>
<hr>
<h5>extragear/graphics/digikam/libs/database (2009-07-14 12:27)</h5>
<code>
SVN commit <a href="http://websvn.kde.org/?view=rev&amp;revision=996417">996417</a> by mwiesweg:

Prepare to use CollectionScanner/Imagescanner to rescan files ("Read metadata to database")
- so that a manual rescan uses 100% the same code as an initial scan when the
  file is imported
Add a flag to CollectionScanner::scanFile to signal if a normal scan, a scan as if modified
or a full rescan shall be done.
Some internal restructuring.

CCBUG: <a href="https://bugs.kde.org/show_bug.cgi?id=193616">193616</a> 

<em>4 files changed</em>
</code>
<hr>
<h5>extragear/graphics/digikam (2009-07-14 10:07)</h5>
<code>
SVN commit <a href="http://websvn.kde.org/?view=rev&amp;revision=996339">996339</a> by cgilles:

add support of Captions author properties.
For each language alternative caption values, user can fill caption author name
A caption date properties is also set accordingly, using current date when caption is enter.
These new Captions properties are recordrd to datase only, for the moment.
TODO: record all captions properties to digiKam XMP namespace
CCBUGS: <a href="https://bugs.kde.org/show_bug.cgi?id=135476">135476</a>




http://websvn.kde.org/?view=rev&amp;revision=996339 

<em>15 files changed</em>
</code>
<hr>
<h5>extragear/graphics/digikam/libs/widgets/imageplugins (2009-07-14 00:52)</h5>
<code>
SVN commit <a href="http://websvn.kde.org/?view=rev&amp;revision=996160">996160</a> by aclemens:

speed up drawing of the mask a little bit, do not convert the DImg to a
Pixmap all the time. 

<em>1 file changed</em>
</code>
<hr>
<h5>extragear/graphics/digikam/libs/widgets/imageplugins (2009-07-13 23:46)</h5>
<code>
SVN commit <a href="http://websvn.kde.org/?view=rev&amp;revision=996141">996141</a> by aclemens:

I would suggest to display the draw cursor all the time, so it is
easier to set the correct size in the panel.
Right now we show the cursor only when pressing the mouse.

One question: Why do we allow to set a size of 100 in the panel for the
brush size, but disallow the maximum size in the mouseMoveEvent?
In there we only allow a max size of 64, which looks weird, because the
mask "blob" we draw is bigger as the cursor.

Is this some old, "wrong" code?

Andi

CCMAIL:digikam-devel@kde.org 

<em>1 file changed</em>
</code>
<hr>
<h5>extragear/graphics/digikam/imageplugins/perspective (2009-07-14 19:33)</h5>
<code>
SVN commit <a href="http://websvn.kde.org/?view=rev&amp;revision=996686">996686</a> by aclemens:

call update() instead of repaint() whenever possible 

<em>1 file changed</em>
</code>
<hr>
<h5>extragear/graphics/digikam/libs/dmetadata (2009-07-14 21:09)</h5>
<code>
SVN commit <a href="http://websvn.kde.org/?view=rev&amp;revision=996721">996721</a> by cgilles:

Save/restore Captions properties to/from digiKam XMP namespace.
Authors and Dates roperties are saved in alternative language bags. Captions Map is build using all these properties,
plus the comments get from standard XMP tags.

Marcel,
When i import a new image with these XMP metadata into collection, authors and dates map are not imported in database...
CCBUGS: <a href="https://bugs.kde.org/show_bug.cgi?id=135476">135476</a>
CCMAIL: marcel.wiesweg@gmx.de 

<em>3 files changed</em>
</code>
<hr>
<h5>extragear/graphics/digikam/utilities (2009-07-15 14:28)</h5>
<code>
SVN commit <a href="http://websvn.kde.org/?view=rev&amp;revision=997148">997148</a> by aclemens:

Fullscreen is a toggle action and should be used as such.
Either press the button in the toolbar, or CTRL+SHIFT+F to exit
fullscreen.
This mechanism is used throughout KDE and should be used in digiKam as
well of course.
I would strongly recommend NOT to use ESC to quit the fullscreen mode.

I disable these actions for now, but again I would rather remove it
completely. What do you think?

Andi

CCMAIL:digikam-devel@kde.org 

<em>3 files changed</em>
</code>
<hr>
<h5>extragear/graphics/digikam/utilities/imageeditor/editor (2009-07-15 15:47)</h5>
<code>
SVN commit <a href="http://websvn.kde.org/?view=rev&amp;revision=997192">997192</a> by aclemens:

Use global action for closing the tools in the image editor.
Sometimes the assigned shortcut is not working, which is weird.
It doesn't seem to work so well to assign a shortcut manually to a
button.
With this global action all the tools will close fine.
Also with this method the shortcut becomes configurable. 

<em>6 files changed</em>
</code>
<hr>
<h5>extragear/graphics/digikam (2009-07-15 15:10)</h5>
<code>
SVN commit <a href="http://websvn.kde.org/?view=rev&amp;revision=997167">997167</a> by cgilles:

Add new widet to edit text (based on KTextEdit) which display a click message as with KLineEdit.
This improve usability and make interface more easily to understand.
This widget will be moved in libkexiv2 later, with others widgets hosted by Template code. 

<em>7 files changed</em>
</code>
<hr>
<h5>extragear/graphics/digikam/utilities/imageeditor/editor (2009-07-15 14:23)</h5>
<code>
SVN commit <a href="http://websvn.kde.org/?view=rev&amp;revision=997143">997143</a> by aclemens:

Close tools by pressing ESC.
We used ESC in the editor window to get out of fullscreen, but this is
inconsistent within digiKam itself as well as throughout the whole KDE
desktop.
Fullscreen is a toggle action and should be used as such.
By re-assigning ESC to the cancel button in the EditorToolSettings, we
can now easily abort a filter, which greatly improves usability and is
also a often asked feature. 

<em>2 files changed</em>
</code>
<hr>
<h5>extragear/graphics/digikam/utilities/imageeditor/editor (2009-07-15 15:58)</h5>
<code>
SVN commit <a href="http://websvn.kde.org/?view=rev&amp;revision=997196">997196</a> by aclemens:

Oops, committed the wrong branch. The commit before was some
experimental code.
This patch will fix the closing of the tool when a filter is aborted. 

<em>3 files changed</em>
</code>
<hr>
<h5>extragear/graphics/digikam/libs/template (2009-07-16 05:15)</h5>
<code>
SVN commit <a href="http://websvn.kde.org/?view=rev&amp;revision=997578">997578</a> by cgilles:

set checkspelling language accordingly with settings 

<em>1 file changed</em>
</code>
<hr>
<h5>extragear/graphics/digikam/libs/dmetadata (2009-07-15 23:11)</h5>
<code>
SVN commit <a href="http://websvn.kde.org/?view=rev&amp;revision=997480">997480</a> by mwiesweg:

Use common method to retrieve copyright information
(this now includes the IPTC legacy fields as well)

I think the different scanning code paths are consolidated now, but I need
to review tomorrow.
Hope no regressions introduced. 

<em>1 file changed</em>
</code>
<hr>
<h5>extragear/graphics/digikam/libs (2009-07-15 22:51)</h5>
<code>
SVN commit <a href="http://websvn.kde.org/?view=rev&amp;revision=997471">997471</a> by mwiesweg:

DMetadata:
- add a method to fill the copyright-related fields of a given Template
- add a convenience method to convert an AltLangMap passed as QVariant.
ImageScanner:
- code from scanImageCopyright has now been moved to DMetadata and ImageCopyright 

<em>3 files changed</em>
</code>
<hr>
<h5>extragear/graphics/digikam/libs/database (2009-07-15 21:24)</h5>
<code>
SVN commit <a href="http://websvn.kde.org/?view=rev&amp;revision=997367">997367</a> by mwiesweg:

Put code to read and write from template into ImageCopyright. May be reused. 

<em>3 files changed</em>
</code>
<hr>
<h5>extragear/graphics/digikam/libs/database (2009-07-15 20:57)</h5>
<code>
SVN commit <a href="http://websvn.kde.org/?view=rev&amp;revision=997336">997336</a> by mwiesweg:

In ImageScanner, rely on DMetadata::getImageComments
to handle all interpretation of comment values. No custom code here.

CCBUG: <a href="https://bugs.kde.org/show_bug.cgi?id=135476">135476</a> 

<em>1 file changed</em>
</code>
<hr>
<h5>extragear/graphics/digikam/libs/widgets/metadata (2009-07-16 16:13)</h5>
<code>
SVN commit <a href="http://websvn.kde.org/?view=rev&amp;revision=997792">997792</a> by cgilles:

new widget to config metadata tags to display on EXIF, IPTC, and XMP view
It will be used in setupMetadata panel.
CCBUGS: <a href="https://bugs.kde.org/show_bug.cgi?id=126086">126086</a> 

<em>2 files changed</em>
</code>
<hr>
<h5>extragear/graphics/digikam/libs/dmetadata (2009-07-16 19:19)</h5>
<code>
SVN commit <a href="http://websvn.kde.org/?view=rev&amp;revision=997946">997946</a> by mwiesweg:

Get IPTC Core subject codes from XMP or Iptc.
Add a method getIptcCoreSubjects which returns subject from XMP or IPTC.
Some internal restructuring. 

<em>2 files changed</em>
</code>
<hr>
<h5>extragear/graphics/digikam/digikam (2009-07-16 21:00)</h5>
<code>
SVN commit <a href="http://websvn.kde.org/?view=rev&amp;revision=997976">997976</a> by mwiesweg:

Add a method to abort all current activity and suspend scanning.
When the database is changed, all scanning tasks should better be aborted. 

<em>2 files changed</em>
</code>
<hr>
<h5>extragear/graphics/digikam/digikam (2009-07-16 21:00)</h5>
<code>
SVN commit <a href="http://websvn.kde.org/?view=rev&amp;revision=997977">997977</a> by mwiesweg:

Suspend all running scanning tasks before changing the database.
Throw in a few wait cursors for the code of opening a new database. May take 1-2secs. 

<em>1 file changed</em>
</code>
<hr>
<h5>extragear/graphics/digikam (2009-07-16 21:00)</h5>
<code>
SVN commit <a href="http://websvn.kde.org/?view=rev&amp;revision=997978">997978</a> by mwiesweg:

Use CollectionScanner/ImageScanner to read metadata from file to database.
This means that this method now uses 100% the same code as if the image
was completely unknown so far and newly added to the collection.

So this method can now be used if
- any metadata was changed by external tools
- a missing piece or bug in our scanning code filled wrong or incomplete info in the db

BUG: <a href="https://bugs.kde.org/show_bug.cgi?id=135476">135476</a> 

<em>2 files changed</em>
</code>
<hr>
<h5>extragear/graphics/digikam/libs/widgets/metadata (2009-07-16 23:36)</h5>
<code>
SVN commit <a href="http://websvn.kde.org/?view=rev&amp;revision=998041">998041</a> by cgilles:

optimize 

<em>1 file changed</em>
</code>
<hr>
<h5>extragear/graphics/digikam/libs/widgets/metadata (2009-07-17 10:05)</h5>
<code>
SVN commit <a href="http://websvn.kde.org/?view=rev&amp;revision=998204">998204</a> by cgilles:

added tooltip 

<em>1 file changed</em>
</code>
<hr>
<h5>extragear/graphics/kipi-plugins/calendar (2009-07-17 18:29)</h5>
<code>
SVN commit <a href="http://websvn.kde.org/?view=rev&amp;revision=998433">998433</a> by nlecureuil:

make year locale aware
CCBUG:<a href="https://bugs.kde.org/show_bug.cgi?id=196888">196888</a> 

<em>1 file changed</em>
</code>
<hr>
<h5>extragear/graphics/kipi-plugins/jpeglossless (2009-07-17 14:02)</h5>
<code>
SVN commit <a href="http://websvn.kde.org/?view=rev&amp;revision=998312">998312</a> by aclemens:

When messing with pointers, always check if they are valid.
This gives another backtrace now, which seems to confirm my guess that
the libjpeg update is responsible for the crash.

CCBUG:<a href="https://bugs.kde.org/show_bug.cgi?id=200159">200159</a> 

<em>1 file changed</em>
</code>
<hr>
<h5>extragear/graphics/digikam (2009-07-18 07:54)</h5>
<code>
SVN commit <a href="http://websvn.kde.org/?view=rev&amp;revision=998593">998593</a> by cgilles:

Implement metadata tags filter settings and rules for metadata viewer from right side bar.
In setup metadata dialog page, a new panel is available to set which metadata tags must be 
displayed in EXIF, Makernotes, Iptc and XMP viewer. This panel list all tags supported by Exiv2.
User can search on whole tags collection supported with a search bar to found easily metadata title to select.
Buttons can clear selection, select all, or use Human Readable default list of tags. 

<em>0 files changed</em>
</code>
<hr>
<h5>extragear/graphics/digikam (2009-07-18 10:59)</h5>
<code>
SVN commit <a href="http://websvn.kde.org/?view=rev&amp;revision=998637">998637</a> by cgilles:

Added Metadata Tags filter support to Showfoto
BUGS: <a href="https://bugs.kde.org/show_bug.cgi?id=126086">126086</a> 

<em>6 files changed</em>
</code>
<hr>
<h5>extragear/graphics/kipi-plugins (2009-07-18 09:21)</h5>
<code>
SVN commit <a href="http://websvn.kde.org/?view=rev&amp;revision=998596">998596</a> by nlecureuil:

Fix crash when saving images in .tiff format 

<em>2 files changed</em>
</code>
<hr>
<h5>KDE/kdegraphics/libs/libkexiv2/libkexiv2 (2009-07-18 16:07)</h5>
<code>
SVN commit <a href="http://websvn.kde.org/?view=rev&amp;revision=998759">998759</a> by cgilles:

handle new XMP namespace with right exiv2 version 

<em>1 file changed</em>
</code>
<hr>
<h5>extragear/graphics/digikam (2009-07-18 14:24)</h5>
<code>
SVN commit <a href="http://websvn.kde.org/?view=rev&amp;revision=998727">998727</a> by aclemens:

Remove bold tags from descriptive text in setup pages 

<em>3 files changed</em>
</code>
<hr>
<h5>extragear/graphics/digikam/libs/widgets/metadata (2009-07-18 16:21)</h5>
<code>
SVN commit <a href="http://websvn.kde.org/?view=rev&amp;revision=998768">998768</a> by cgilles:

support new XMP namespace 

<em>3 files changed</em>
</code>
<hr>
<h5>extragear/graphics/digikam/utilities/firstrun (2009-07-18 18:08)</h5>
<code>
SVN commit <a href="http://websvn.kde.org/?view=rev&amp;revision=998799">998799</a> by osterfeld:

Actually save the selected album path in the config.

<em>1 file changed</em>
</code>
<hr>
<h5>extragear/graphics/digikam/libs (2009-07-18 14:54)</h5>
<code>
SVN commit <a href="http://websvn.kde.org/?view=rev&amp;revision=998736">998736</a> by mwiesweg:

Following hint from other KDE commit to make this work with libjpeg7.
Untested with libjpeg 7. only with libjpeg 6.2, where numerator and denominator
are always 1 so the effect is the same. 

<em>2 files changed</em>
</code>
<hr>
<h5>extragear/graphics/kipi-plugins/advancedslideshow (2009-07-19 00:31)</h5>
<code>
SVN commit <a href="http://websvn.kde.org/?view=rev&amp;revision=998997">998997</a> by aclemens:

fix memory leaks 

<em>4 files changed</em>
</code>
<hr>
<h5>extragear/graphics/kipi-plugins/advancedslideshow (2009-07-19 00:32)</h5>
<code>
SVN commit <a href="http://websvn.kde.org/?view=rev&amp;revision=999002">999002</a> by aclemens:

little speed improvement: don't connect the slots at the beginning of
readSettings() 

<em>1 file changed</em>
</code>
<hr>
<h5>extragear/graphics/kipi-plugins/advancedslideshow (2009-07-19 00:32)</h5>
<code>
SVN commit <a href="http://websvn.kde.org/?view=rev&amp;revision=999000">999000</a> by aclemens:

remove some Q3support classes. We still have Qt3 classes in this plugin,
which is really bad.
Other things to fix: drag &amp; drop 

<em>11 files changed</em>
</code>
<hr>
<h5>extragear/graphics/kipi-plugins/advancedslideshow (2009-07-19 00:32)</h5>
<code>
SVN commit <a href="http://websvn.kde.org/?view=rev&amp;revision=999001">999001</a> by aclemens:

This can't be right: the urlList of the sharedData is always appended.
Every click on "Start Slideshow" will duplicate the list content. 

<em>1 file changed</em>
</code>
<hr>
<h5>extragear/graphics/kipi-plugins/advancedslideshow (2009-07-19 12:07)</h5>
<code>
SVN commit <a href="http://websvn.kde.org/?view=rev&amp;revision=999122">999122</a> by aclemens:

Use QPointers when showing modal dialogs via exec(), as discussed in http://www.kdedevelopers.org/node/3919 

<em>1 file changed</em>
</code>
<hr>
<h5>extragear/graphics/kipi-plugins/advancedslideshow (2009-07-19 14:25)</h5>
<code>
SVN commit <a href="http://websvn.kde.org/?view=rev&amp;revision=999207">999207</a> by aclemens:

use sharedData-&gt;urlList in slotSlideShow(), now the selected images are
displayed correcly. 

<em>1 file changed</em>
</code>
<hr>
<h5>extragear/graphics/kipi-plugins/advancedslideshow (2009-07-19 13:38)</h5>
<code>
SVN commit <a href="http://websvn.kde.org/?view=rev&amp;revision=999169">999169</a> by aclemens:

fix showSelectedFilesOnly: don't save this value in the config file,
chances are that the radiobutton is always disabled, even if a valid
selection is made.
Add a different logic: If only one image is selected, assume to display
the whole album content, otherwise enable showSelectedFilesOnly and
display the selected images. 

<em>2 files changed</em>
</code>
<hr>
<h5>extragear/graphics/kipi-plugins/advancedslideshow (2009-07-19 16:17)</h5>
<code>
SVN commit <a href="http://websvn.kde.org/?view=rev&amp;revision=999247">999247</a> by aclemens:

Increase the maximum slideshow delay to 20s, some users might want to
have more... 

<em>1 file changed</em>
</code>
<hr>
<h5>extragear/graphics/kipi-plugins/advancedslideshow (2009-07-19 16:17)</h5>
<code>
SVN commit <a href="http://websvn.kde.org/?view=rev&amp;revision=999245">999245</a> by aclemens:

Fix loading of the saved delay value from the config file.
The value was actually saved correctly, but not used properly.
This commit should have fixed this.

BUG:<a href="https://bugs.kde.org/show_bug.cgi?id=200647">200647</a> 

<em>3 files changed</em>
</code>
<hr>
<h5>extragear/graphics/kipi-plugins/batchprocessimages (2009-07-19 17:25)</h5>
<code>
SVN commit <a href="http://websvn.kde.org/?view=rev&amp;revision=999296">999296</a> by nlecureuil:

Do not crash if imagemagick is not installed
BUG:<a href="https://bugs.kde.org/show_bug.cgi?id=168243">168243</a> 

<em>1 file changed</em>
</code>
<hr>
<h5>extragear/graphics/digikam/libs/widgets/metadata (2009-07-19 19:46)</h5>
<code>
SVN commit <a href="http://websvn.kde.org/?view=rev&amp;revision=999368">999368</a> by cgilles:

Metadata Filter : if metadata tags from filter are not available in image, show tags as "unavaialble" as well instead is hide it. 
This will improve usability to compare image metadata. 

<em>7 files changed</em>
</code>
<hr>
</div>

<div class="legacy-comments">

  <a id="comment-18676"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/467#comment-18676" class="active">Looks great!
Talking about</a></h3>    <div class="submitted">Submitted by mutlu (not verified) on Mon, 2009-07-20 15:34.</div>
    <div class="content">
     <p>Looks great!</p>
<p>Talking about meta-data: what about nepomuk integration? I know it did not have high priority so far, but I believe it would be a great feature, most importantly since it would enable us to reuse their tags in other applications.</p>
<p>Keep up the great work!</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18677"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/467#comment-18677" class="active">During the Nepomuk coding</a></h3>    <div class="submitted">Submitted by Marcel Wiesweg (not verified) on Mon, 2009-07-20 16:04.</div>
    <div class="content">
     <p>During the Nepomuk coding sprint in June I have implemented a basic syncing for comments, rating and tags (tags is missing the KDE-&gt;digikam direction still).<br>
I need to add options to setup to enable this.<br>
Will require KDE4.3.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18679"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/467#comment-18679" class="active">Awesome! You guys are an</a></h3>    <div class="submitted">Submitted by mutlu (not verified) on Tue, 2009-07-21 00:08.</div>
    <div class="content">
     <p>Awesome! You guys are an amazing team. :)</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-18678"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/467#comment-18678" class="active">Nepomuk question, Again.</a></h3>    <div class="submitted">Submitted by Fri13 on Mon, 2009-07-20 23:11.</div>
    <div class="content">
     <p>Well, I would not keep the breath for nepomuk integration. It should be possible but currently the KDE does not support (lack of manpower) it either well. There is lots of stuff what should be fixed and done to get nepomuk on such state that it can be used as wanted on KDE. Even that nepomuk is ready, the usage is still too minimal.</p>
<p>I do not even know should we wait KDE about this technology and implent all the nepomuk features what are already there or not.</p>
<p>For personally opinion about this, I do not wait nepomuk integration (yet!) because if I want to find photos fast, I use digiKam searches. I can not find any good ways to find wanted photos trought krunner, menu or dolphin as I can do with digiKam or even with the other photo managers like Lightroom and Aperture.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18680"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/467#comment-18680" class="active">I am not big fan of Nepomuk</a></h3>    <div class="submitted">Submitted by mikolajmachowski (not verified) on Tue, 2009-07-21 09:31.</div>
    <div class="content">
     <p>I am not big fan of Nepomuk and would prefer if in future digiKam could keep its data separate - much more important is cooperation and interoperability with other photo software and services which could be easier by keeping its own storage.</p>
<p>But Nepomuk has its uses in "semantic desktop". Example: e-mail sending from inside of digiKam is nice but in reality much often I am writing e-mail and halfway I think that would be good to attach some photos. It would be much more comfortable to use standard file dialog for quick browsing of files with metadata enabled. In the past it could be done with digikamtags:// protocol, now there is no such possibility.</p>
<p>For other uses think about: graphics for presentations in KPresenter, looking for images from Krita, inserting illustrations for article in KWord, etc., etc. I was using all of this with digikamtags.</p>
<p>Nepomuk theoretically has advantage that it could be extended to non-KDE applications (digikamtags protocol is based on KIO-slaves).</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18681"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/467#comment-18681" class="active">digiKam kioslaves...</a></h3>    <div class="submitted">Submitted by digiKam on Tue, 2009-07-21 09:44.</div>
    <div class="content">
     <p>&gt;digikamtags protocol is based on KIO-slaves</p>
<p>True, but if we use another database backend as MySql in the future, BYE BYE kioslave, and welcome to multi-threading.</p>
<p>Kio-slave is used here because SQlite is not fully safe with multi-threading. With Mysql, no problem.</p>
<p>Also, kio-slave is a mess to hack. I hate it...</p>
<p>Removing Kioslave will reduce a lots of dependencies problem which break digiKam, for ex. currently when you update KDE 4.3 under Debian, you lost all thumbs in icons view due to a crash of kio-slave (libmarblewidget symbol unresolved)</p>
<p>digiKam</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div></div>
</div>
