---
date: "2010-07-11T10:32:00Z"
title: "GSoC update: Reverse Geotagging widget is ready!"
author: "Gabriel Voicu"
description: "Hello everyone, I'm very happy to announce you that the reverse geotagging widget is finished! This was the first part and maybe the most important"
category: "news"
aliases: "/node/532"

---

<p>Hello everyone,</p>

<p>I'm very happy to announce you that the reverse geotagging widget is finished! This was the first part and maybe the most important part of my project in this year Google Summer of Code, so I'm glad to see it working. The reverse geotagging widget will be included in kipi-plugins 2.0 release which should be around december 2010. Also, I would like to mention that the world map, the image list and the other widgets(except Reverse Geotagging widget of course) from Geolocation widget are the work of Michael G. Hansen(my GSoC mentor).</p>

<a href="http://www.flickr.com/photos/51929284@N02/4782688018/" title="Photo blog1 by gvoicu, on Flickr"><img src="http://farm5.static.flickr.com/4100/4782688018_262b84e400_b.jpg" width="1024" height="797" alt="Photo blog1"></a>

<p>Now, perhaps some of you will ask: "Ok, but what is reverse geotagging?" 
Well, it's a process that reads the GPS coordinate inside an image and then tags it with address elements(country, city, street...).</p> 

<p>Maybe you will still ask "But why is reverse geotagging widget helping me?"
If we think at the fact that most people tag their images with location tags(Places/Spain, Holidays/France/Paris), then reverse geotagging widget is very usefull because it helps you save many time when you have to tag lots of images. For example, think that you've just came from an Eurotrip and you want to show your friends the pictures taken on Champs-Élysées in Paris or the ones taken in Barcelona. Of course, you can search in all the new photos and tag them manually, but if you have hundrets or thousands of them it would take some time. The solution comes from the reverse geotagging widget, which automatically tags all your photos(that contains GPS coordinates) with location tags, using just a few clicks.</p>

<p>Before showing you the widget I want to tell you that sometimes the results will not be complete. For example, let's say that you've made a photo in Romania, Constanta city, Mamaia boulevard. If you reverse geotag this photo, you will see that was tagged only with "Romania/Constanta" instead of "Romania/Constanta/Mamaia". This is because the backend services haven't covered yet all the globe with all the address elements in their database. The good part in this is that you can help them quicken the globe covering by donating money or involving directly in their work.</p>
  
<p>Now, let's speak about this widget. As I said earlier, you can choose where your data will come from. There are 2 backend services: Open Street Map and Geonames. My oppinion is to try them both, because they provide different types of results. Retrieving the tags in your commomn language is also possible, but take in consideration that will not always work. From my tests, I've seen that the country tag is translated in almost every language. If an address elements doesn't provide translation, it will be displayed in English.</p>

<p>You will also have the tag tree from digiKam, where you will tell the widget where to put the new tags. I've written a tutorial in wiki about how to use it <a href="http://community.kde.org/Digikam/GSoC2010/ReverseGeocoding#Use_Cases">here</a>.</p>

<p>If you want to, you can add your tags image metadata too. I've chosen only XMP and no IPTC because it doesn't supports special characters(just ASCII code).</p> 

<p>If you made an operation in the tag tree and want to undo it, you have the Undo/Redo widget developed by Michael G. Hansen the gives you the possibility to undo and redo each operation you made in Geolocation widget.</p>

<p>Ok, now let's see if this works. I put here two screenshots:one with the widget and the new tags and one with the tags added in digikam.In the first screenshot, for the ones  of you that haven't looked in wiki to see detailed explanations, the red tags are control tags that tell the widget where to put the new tags, the green tags are tags that will be deleted once you close the Geolocation widget and the rest of tags are tags from digikam. In the second screenshot is a proof that the code works and adds properly the tags in digiKam.</p>

<a href="http://www.flickr.com/photos/51929284@N02/4782054139/" title="Photo blog2 by gvoicu, on Flickr"><img src="http://farm5.static.flickr.com/4096/4782054139_646d52892c_b.jpg" width="1024" height="798" alt="Photo blog2"></a>

<p>At the end, I want to special thank Michael G. Hansen for proper mentoring and helping me finish this widget.</p>



<div class="legacy-comments">

  <a id="comment-19343"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/532#comment-19343" class="active">Congratulations, the</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Sun, 2010-07-11 14:31.</div>
    <div class="content">
     <p>Congratulations, the screenshots look wonderful! I would like to give it a try, but apparantly the code is in a separate branch at the moment. Are there instructions available how I can easily switch my digikam (svn version) to this branch?</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19344"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/532#comment-19344" class="active">Look there...</a></h3>    <div class="submitted">Submitted by digiKam on Sun, 2010-07-11 14:43.</div>
    <div class="content">
     <p>Source code is there : http://websvn.kde.org/branches/extragear/graphics/</p>
<p>Considerate it as unstable as well. Take a look in README.GoSC2010 file for details.</p>
<p>digiKam</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19351"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/532#comment-19351" class="active">After reading README.GoSC2010</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Mon, 2010-07-12 10:16.</div>
    <div class="content">
     <p>After reading README.GoSC2010 I have to say: Wow, this sounds as if digikam 2.0 will be really impressive! Looking forward to it ...</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-19345"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/532#comment-19345" class="active">I think you should wait at least some days</a></h3>    <div class="submitted">Submitted by Gabriel Voicu on Sun, 2010-07-11 14:59.</div>
    <div class="content">
     <p>Hi, I'm very glad you like it. As Gilles said, the code can be found there, but I don't recommend to update to this branch for some time if you want stability, because all three GSoC projects are working on it + there might be some unknown bugs and it could mess up your photos. Just to make an idea, these three projects are planned to be included in a digikam/kipi release in winter(around december), so there is still enough work on that branch. Of course, if you want to test it and help us with finding bugs, we will really appreciate it.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19346"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/532#comment-19346" class="active">2.0.0 beta release</a></h3>    <div class="submitted">Submitted by digiKam on Sun, 2010-07-11 15:24.</div>
    <div class="content">
     <p>I plan to release 2.0.0 beta version when it will be possible. Perhaps the first one in september...</p>
<p>digiKam</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div><a id="comment-19352"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/532#comment-19352" class="active">Can I also do this with</a></h3>    <div class="submitted">Submitted by Jerzy Bischoff (not verified) on Mon, 2010-07-12 17:17.</div>
    <div class="content">
     <p>Can I also do this with Marble?</p>
<p>Just curious, I usually just use Gwenview, but I sometimes think about using DigiKam.  I'm not a real hard-core photographer but I prefer using free software.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19353"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/532#comment-19353" class="active">Yes you can</a></h3>    <div class="submitted">Submitted by Gabriel Voicu on Mon, 2010-07-12 17:48.</div>
    <div class="content">
     <p>Hi,</p>
<p>If by "doing this with Marble", you refer to see the photos on the map, yes you can do it. Geolocation widget has a switcher where you can choose which backend you want to use for your map. The choices are Google Maps and Marble.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19354"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/532#comment-19354" class="active">Excellent!
Thanks so much for</a></h3>    <div class="submitted">Submitted by Jerzy Bischoff (not verified) on Mon, 2010-07-12 18:28.</div>
    <div class="content">
     <p>Excellent!</p>
<p>Thanks so much for the work in the project. I knew it was always a highly regarded project, but I still don't know much about what it can do.  I'll give it a shot.</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div><a id="comment-19389"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/532#comment-19389" class="active">Sorry to rain on your parade, but...</a></h3>    <div class="submitted">Submitted by LMB (not verified) on Wed, 2010-07-28 11:39.</div>
    <div class="content">
     <p>...but a similar feature is already out there. You can select an area on the map, and you will see all photos. Not the same, but gives the same effect. </p>
<p>While I personally am going away from manual tagging to geotagging, I may use your feature to keep both. Since I bought a GPS I no longer tag photos geographically, so this can do it for me. But I hope country/city can be achieved, instead of going down to Bezirk, which is beserk.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19392"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/532#comment-19392" class="active">There is no rain ;-)</a></h3>    <div class="submitted">Submitted by Gabriel Voicu on Thu, 2010-07-29 19:53.</div>
    <div class="content">
     <p>"...but a similar feature is already out there. You can select an area on the map, and you will see all photos. Not the same, but gives the same effect."   </p>
<p>    I think that there is a little misunderstanding here. My project is just about taking photos that already have GPS coordinates inside their metadata and tag them automatically with address (reverse-geotagging). I've made an example of it's usage <a href="http://community.kde.org/Digikam/GSoC2010/ReverseGeocoding#Use_Cases">here</a>.<br>
    Another thing is that this map with all it's features is part of Geolocation widget, which is part of Kipi-Plugins. So, this widget will be included in other applications, not only digiKam, (KPhotoAlbum, GwenView, ShowImg) where there may be not a possibility to select an area on the map and select the photos.</p>
<p>"But I hope country/city can be achieved, instead of going down to Bezirk, which is beserk."</p>
<p>    If your photos will have GPS coordinates inside their metadata, at least country/city should be achieved.</p>
         </div>
    <div class="links">» </div>
  </div>
</div>
</div>
