---
date: "2009-03-17T14:08:00Z"
title: "Kipi-plugins 0.2.0 for KDE4 released"
author: "digiKam"
description: "Dear all digiKam fans and users! Official kipi-plugins 0.2.0 release for KDE4 is out. kipi-plugins 0.2.0 tarball can be downloaded from SourceForge at this url"
category: "news"
aliases: "/node/433"

---

<p>Dear all digiKam fans and users!</p>

<p>Official kipi-plugins 0.2.0 release for KDE4 is out.</p>

<a href="http://www.flickr.com/photos/digikam/3362060915/" title="kipiplugins0.2.0 by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3457/3362060915_c01aa64057.jpg" width="500" height="313" alt="kipiplugins0.2.0"></a>

<p>kipi-plugins 0.2.0 tarball can be downloaded from SourceForge <a href="http://sourceforge.net/project/showfiles.php?group_id=149779&amp;package_id=165761">at this url</a></p>

<p>Kipi-plugins is also available for Windows. Precompiled packages can be donwloaded with KDE-Windows installer. See <a href="http://windows.kde.org">KDE-Windows project</a> for details.</p>

<p>See below the list of new features and bugs-fix coming with this release:</p>

<b>General</b>        : Port to CMake/KDE4/QT4.<br>
<b>General</b>        : all plugins can be compiled natively under Microsoft Windows.<br>
<b>General</b>        : Plugins now use metadata settings shared with kipi host application.<br>
<b>General</b>        : All import/export plugins now have keyboard shortcuts.<br>
<b>General</b>        : SimpleViewer plugin has been renamed to FlashExport.<br>
<b>General</b>        : New icons for all Export/import plugins.<br><br>

<b>JPEGLossLess</b>   : XMP metadata support.<br><br>

<b>TimeAdjust</b>     : XMP metadata support.<br><br>

<b>MetadataEdit</b>   : XMP metadata support with EXIF, IPTC, and Comments editor to sync XMP tags.<br>
<b>MetadataEdit</b>   : IPTC Editor dialog is more compliant with IPTC.org recommendations.<br>
<b>MetadataEdit</b>   : IPTC Editor add the capability to set multiple values for a tag.<br><br>

<b>SendImages</b>     : Complete re-write of emailing tool.<br><br>

<b>GPSSync</b>        : New tool to edit GPS track list using googlemaps.<br><br>

<b>RAWConverter</b>   : XMP metadata support.<br>
<b>RAWConverter</b>   : Raw files are decoded in 16 bits color depth using same auto-gamma and auto-white  methods provided by dcraw with 8 bits color depth RAW image decoding.<br><br>

<b>SlideShow</b>      : Now filenames, captions and progress indicators can have transparent backgrounds with OpenGL SlideShow as well.<br>
<b>SlideShow</b>      : Added thumbnails into image list.<br>
<b>SlideShow</b>      : Now you can play your favourite music during slideshow.<br>
<b>SlideShow</b>      : Normal effects are back.<br>
<b>SlideShow</b>      : New effect "Cubism".<br>
<b>SlideShow</b>      : Added support for RAW images.<br>
<b>SlideShow</b>      : New effect "Mosaic".<br><br>

<b>RemoveRedEyes</b>  : New plugin to remove red eyes in batch. Tool is based on OpenCV library.<br><br>

<b>Calendar</b>       : Ported to QT4/KDE4.<br>
<b>Calendar</b>       : Support RAW images.<br><br>

<b>AcquireImages</b>  : Under Windows, TWAIN interface is used to scan image using flat scanner.<br><br>

<b>DNGConverter</b>   : New plugin to convert RAW camera image to Digital NeGative (DNG).<br>
<b>DNGConverter</b>   : plugin is now available as a stand alone application.<br><br>

<b>PrintWizard</b>    : Ported to QT4/KDE4.<br>
<b>PrintWizard</b>    : Added a new option to skip cropping, the image is scaled automatically.<br>
<b>PrintImages</b>    : New plugin that allow to choose old print assistant or to print all the selected photos directly.<br><br>

<b>RemoveRedEyes</b>  : Added an option to preview the correction of the currently selected image.<br><br>

<b>Facebook</b>       : New plugin to export images to a remote Facebook web service (http://www.facebook.com).<br>
<b>Facebook</b>       : Facebook plugin now support also import (download) of photos.<br><br>

<b>FlickrExport</b>   : Add support of Photosets.<br>
<b>FlickrExport</b>   : Add support for 23hq.com service which uses Flickr API.<br><br>

<b>Smug</b>           : New plugin to export images to a remote SmugMug web service.(http://www.smugmug.com).<br>
<b>Smug</b>           : Add support for Album Templates.<br>
<b>Smug</b>           : Import (download) of images from SmugMug web service.<br><br>


001 ==&gt; 135451 : GPSSync            : Improve the gui of the gpssync plugin.<br>
002 ==&gt; 135386 : GPSSync            : Show track on the google map.<br>
003 ==&gt; 149497 : GPSSync            : Geolocalization kipi plugin does not work with non-jpeg file types.<br>
004 ==&gt; 145746 : GPSSync            : Do not recreate thumbs when geolocalizing images.<br>
005 ==&gt; 158792 : GPSsync            : Plugin do not working with Canon RAW CR2.<br>
006 ==&gt; 165078 : GPSSync            : GPS correlator does not show thumbnails.<br>
007 ==&gt; 165278 : GPSSync            : Geotagging dialog doesn't display all thumbnails.<br>
008 ==&gt; 134299 : SimpleViewerExport : Read orientation of image from EXIF.<br>
009 ==&gt; 150076 : SlideShow          : Playing music during a slideshow.<br>
010 ==&gt; 172283 : SlideShow          : SlideShow crashes host application.<br>
011 ==&gt; 172337 : SlideShow          : Slideshow enhancements by ken-burns effects.<br>
012 ==&gt; 173276 : SlideShow          : digiKam crashs after closing.<br>
013 ==&gt; 172910 : GalleryExport      : Gallery crash (letting crash digiKam) with valid data and url.<br>
014 ==&gt; 161855 : GalleryExport      : Remote Gallery Sync loses password.<br>
015 ==&gt; 154752 : GalleryExport      : Export to Gallery2 seems to fail, but in fact works.<br>
016 ==&gt; 157285 : SlideShow          : digiKam advanced slideshow not working with Canon RAWs.<br>
017 ==&gt; 175316 : GPSSync            : digiKam crashes when trying to enter gps data.<br>
018 ==&gt; 169637 : MetadataEdit       : EXIF Data Editing Error.<br>
019 ==&gt; 174954 : HTMLExport         : Support for remote urls.<br>
020 ==&gt; 176640 : MetadataEdit       : Wrong reference to website.<br>
021 ==&gt; 176749 : TimeAdjust         : Incorrect time/date setting with digiKam.<br>
022 ==&gt; 165370 : GPSSync            : crash when saving geocoordinates.<br>
023 ==&gt; 165486 : GPSSync            : Next and Previous photo button for editing geolocalization coordinates.<br>
024 ==&gt; 175296 : GPSSync            : Geolocalization a handy gui to manage all about geotagging.<br>
025 ==&gt; 152520 : MetadataEdit       : Unified entry of metadata.<br>
026 ==&gt; 135320 : Calendar           : Calendar year do not match locale.<br>
027 ==&gt; 116970 : Calendar           : New calendar generator plugin features.<br>
028 ==&gt; 177999 : General            : Kipi-plugins Shortcuts are not listed in the shortcuts editor from Kipi host application.<br>
029 ==&gt; 160236 : JPEGLossLess       : Image Magick convert operations return with errors.<br>
030 ==&gt; 144183 : RawConverter       : Path in RAW-converter broken with the letter "ß"<br>
031 ==&gt; 178495 : MetadataEdit       : Problem with metadata editor.<br>
032 ==&gt; 175219 : PicasaWebExport    : Album creating wrong date in picasaweb.<br>
033 ==&gt; 175222 : PicasaWebExport    : Unable to create non listed album.<br>
034 ==&gt; 166672 : Facebook           : Export to Facebook.<br>
035 ==&gt; 129623 : FlickrExport       : Option to upload to specific photoset.<br>
036 ==&gt; 161775 : FlickrExport       : Photoset support.<br>
037 ==&gt; 179439 : JPEGLossLess       : Rotation or flip do not update internal thumbnail.<br>
038 ==&gt; 180245 : TimeAdjust         : Timeadjust is not building on mac os x leopard.<br>
039 ==&gt; 180656 : PicasaWebExport    : Wrong file selection filter in export picasaweb.<br>
040 ==&gt; 175928 : GalleryExport      : digiKam crashing when trying to export to gallery2 web service.<br>
041 ==&gt; 181694 : MetadataEdit       : Edit caption should present current caption for easier modification.<br>
042 ==&gt; 181973 : RAWConverter       : Raw batch converter loses exif information.<br>
043 ==&gt; 181334 : IpodExort          : The artwork API in libgpod-0.7.0 has changed slightly, IpodExport plugin needs updated.<br>
044 ==&gt; 181853 : MetadataEdit       : Entering new xmp meta data removes old one from the image file.<br>
045 ==&gt; 133649 : Print Wizard       : Scale images instead of cropping.<br> 
046 ==&gt; 182838 : Facebook           : Change API authentication to Desktop application type.<br>
047 ==&gt; 184312 : General            : Use SmoothTransform for scaling images for export, calendar and slideshow.<br>
048 ==&gt; 140438 : General            : 3 different viewers for diferents needs.<br>
049 ==&gt; 184553 : GalleryExport      : Encoding issue with gallery2.<br>
050 ==&gt; 159794 : JPEGLossLess       : Cannot rotate .tif images in digiKam.<br>
051 ==&gt; 184702 : JPEGLossLess       : Cannot rotate TIFFs in Album GUI.<br>
052 ==&gt; 184332 : AdvancedSlideShow  : Compiling error with Qt4.5.<br>
053 ==&gt; 177137 : PrintWizard        : digiKam print assistant problems and ideas.<br>
054 ==&gt; 176367 : PrintWizard        : Crop stage does not allow rotation or selection.<br>
055 ==&gt; 178983 : PrintWizard        : Next/back-buttons in print-assistant should be labeled.<br>
056 ==&gt; 095404 : PrintWizard        : Printwizard print image with custom size, original size, autoscaled size to paper.<br>
057 ==&gt; 183661 : General            : Better action icons for export plugins.<br>
058 ==&gt; 187203 : RAWConverter       : Batch raw converter crash digikam.<br>

<div class="legacy-comments">

  <a id="comment-18338"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/433#comment-18338" class="active">The biggest issue for me with</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Tue, 2009-03-17 18:40.</div>
    <div class="content">
     <p>The biggest issue for me with the previous release is that the "email images" plugin uses a scaling algorithm which produced a lot of aliasing/jagged edges. Was this fixed?<br>
Thanks.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18339"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/433#comment-18339" class="active">yes, i think it is.</a></h3>    <div class="submitted">Submitted by digiKam on Tue, 2009-03-17 21:28.</div>
    <div class="content">
     <p>yes, i remember a thread about this subject with others developers.</p>
<p>digiKam</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18364"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/433#comment-18364" class="active">I have kde4-kipi-plugins</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Thu, 2009-03-19 04:59.</div>
    <div class="content">
     <p>I have kde4-kipi-plugins 0.2-20.2 installed from the openSUSE KDE 4.2 repository (KDE factory) and it is not fixed there. Other than that, the "Components information" command in Digikam shows "LibKipi" version 0.3.0 which however doesn't seem to be correct since the version number is too high.</p>
<p>Maybe that can be fixed for the next version?</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18367"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/433#comment-18367" class="active">Fixed in svn</a></h3>    <div class="submitted">Submitted by digiKam on Thu, 2009-03-19 08:17.</div>
    <div class="content">
     <p>Done with rev. #941222</p>
<p>digiKam</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div></div><a id="comment-18371"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/433#comment-18371" class="active">Print-Wizard</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Thu, 2009-03-19 10:45.</div>
    <div class="content">
     <p>Print Wizard produces too small pictures, when using the preview or small preview pics function. </p>
<p>Regards</p>
<p>Anon</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18377"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/433#comment-18377" class="active">Sorry, i can't get it :(
What</a></h3>    <div class="submitted">Submitted by Angelo Naselli (not verified) on Thu, 2009-03-19 14:33.</div>
    <div class="content">
     <p>Sorry, i can't get it :(<br>
What do you mean by using the preview?<br>
As far as i can remember print preview is not enabled in kde 4.x.</p>
<p>Anyway, is printer output ok?</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18378"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/433#comment-18378" class="active">Hi,
sorry got the german</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Thu, 2009-03-19 15:01.</div>
    <div class="content">
     <p>Hi,</p>
<p>sorry got the german translation. I wanna print much pics on one paper "Contactsheet".<br>
Tried it again and now it works. I made the mistake and dont print them out with digikam. I use Gnome and Eye of Gnome make them smaller.<br>
Printout with Digikam works. </p>
<p>Thx</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18379"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/433#comment-18379" class="active">Great!
for the record, from a</a></h3>    <div class="submitted">Submitted by Angelo Naselli (not verified) on Thu, 2009-03-19 15:06.</div>
    <div class="content">
     <p>Great!<br>
for the record, from a console run the command:<br>
LC_ALL=C digikam<br>
to get english version of it</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div></div>
</div>
