---
date: "2012-03-06T11:17:00Z"
title: "digiKam Software Collection 2.6.0 beta2 is out..."
author: "digiKam"
description: "Dear all digiKam fans and users! digiKam team is proud to announce the 2nd digiKam Software Collection 2.6.0 beta release! With this release, digiKam include"
category: "news"
aliases: "/node/647"

---

<a href="https://lh3.googleusercontent.com/-0_Nc31NkByE/TxHVtpHP8EI/AAAAAAAAAmc/fo7MyE7mUm0/w284-h270-k/DSC00499_v1.JPG" title="DSC00499_v1.JPG by digiKam team"><img src="https://lh3.googleusercontent.com/-0_Nc31NkByE/TxHVtpHP8EI/AAAAAAAAAmc/fo7MyE7mUm0/w284-h270-k/DSC00499_v1.JPG" width="284" height="270" alt="DSC00499_v1.JPG"></a>

<p>Dear all digiKam fans and users!</p>

<p>digiKam team is proud to announce the 2nd digiKam Software Collection 2.6.0 beta release!</p>

<p>With this release, digiKam include a lots of bugs fixes and new features introduced to last <a href="http://techbase.kde.org/Projects/Digikam/CodingSprint2011">Coding Sprint from Genoa</a>. You can read a <a href="http://dot.kde.org/2012/02/22/digikam-team-meets-genoa-italy">resume of this event</a> to dot KDE web page. Thanks again to <a href="http://ev.kde.org">KDE-ev</a> to sponsorship digiKam team...</p>

<p>digiKam include now a progress manager to control all paralelized process done in background. This progress manager is also able to follow processing from Kipi-plugins.</p>

<a href="http://www.flickr.com/photos/digikam/6812381420/" title="progressmanager by digiKam team, on Flickr"><img src="http://farm8.staticflickr.com/7055/6812381420_1463394eba_z.jpg" width="640" height="256" alt="progressmanager"></a>

<p>Also, a new Maintenance Tool have been implemented to simplify all maintenance tasks to process on your whole collections.</p>

<a href="http://www.flickr.com/photos/digikam/6812384920/" title="maintenancetool01 by digiKam team, on Flickr"><img src="http://farm8.staticflickr.com/7062/6812384920_cb3424fd22_z.jpg" width="640" height="256" alt="maintenancetool01"></a>

<p>Kipi-plugins includes a new tool to export your collections to <a href="http://imageshack.us/">ImageShack Web Service</a>. Thanks to <b>Victor Dodon</b> to contribute.</p>

<a href="http://www.flickr.com/photos/digikam/6812381534/" title="imageshackexport by digiKam team, on Flickr"><img src="http://farm8.staticflickr.com/7205/6812381534_b1282f3bcb_z.jpg" width="640" height="256" alt="imageshackexport"></a>

<p>digiKam have been ported to <a href="http://www.littlecms.com/">LCMS version 2</a> color management library by <b>Francesco Riosa</b>. Also a new tool dedicated to manage colors from scanned <a href="http://en.wikipedia.org/wiki/Reversal_film">Reversal Films</a> have been implemented by <b>Matthias Welwarsky</b>.

<a href="http://www.flickr.com/photos/digikam/6955685535/" title="negativefimtool by digiKam team, on Flickr"><img src="http://farm8.staticflickr.com/7059/6955685535_48cf2bcdfc_z.jpg" width="640" height="256" alt="negativefimtool"></a>

</p><p>This beta release is not yet stable. Do not use yet in production. Please report all bugs to KDE bugzilla following indications <a href="http://www.digikam.org/drupal/support">from this page</a>. The release plan can be seen <a href="http://www.digikam.org/drupal/about/releaseplan">at this url</a>.</p>

<p>See <a href="https://projects.kde.org/projects/extragear/graphics/digikam/repository/revisions/ff3e5ef8c8d0e9dd091587b03fb0f011acf6bb19/entry/NEWS">the list of digiKam file closed</a> with this release into KDE bugzilla.</p>

<p>See <a href="https://projects.kde.org/projects/extragear/graphics/kipi-plugins/repository/revisions/2939fe86fa7c5a3d07fd1184ef26a36a8f14cb33/entry/NEWS">the list of Kipi-plugins file closed</a> with this release into KDE bugzilla.</p>

<p>digiKam software collection tarball can be downloaded from <a href="http://sourceforge.net/projects/digikam/files">SourceForge Repository</a></p>

<p>Happy digiKam testing...</p>
<div class="legacy-comments">

  <a id="comment-20201"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/647#comment-20201" class="active">Plasma progress manager ?</a></h3>    <div class="submitted">Submitted by Heller (not verified) on Tue, 2012-03-06 13:52.</div>
    <div class="content">
     <p>Hi,</p>
<p>Many thanks to the digiKam team, I just love it :)</p>
<p>&gt;&gt; digiKam include now a progress manager to control all paralelized process done in background. This progress manager is also able to follow processing from Kipi-plugins.</p>
<p>Any chance to merge this progress manager with the one from plasma (used for file transfert and so on...) ?</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-20202"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/647#comment-20202" class="active">PPA</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Mon, 2012-03-12 11:13.</div>
    <div class="content">
     <p>I'd really love if you had (K)Ubuntu PPA so it's easy to install newer versions of digiKam, this way I'm stuck at 2.1.1</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20203"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/647#comment-20203" class="active">PPA</a></h3>    <div class="submitted">Submitted by <a href="http://about.me/johannrenner" rel="nofollow">Johann Renner</a> (not verified) on Wed, 2012-03-14 13:53.</div>
    <div class="content">
     <p>There is this guy, Philip Johnsson, who for a couple of years has been maintaining a great collection of software in his PPA, among them Digikam: http://launchpad.net/~philip5/+archive/extra </p>
<p>Note that as 14th March 2012 he has the Digikam 2.5.0 version online.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20207"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/647#comment-20207" class="active">ubuntu 12.04 ppa....... ?</a></h3>    <div class="submitted">Submitted by vadim (not verified) on Wed, 2012-03-28 11:00.</div>
    <div class="content">
     <p>ubuntu 12.04 ppa....... ?</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20208"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/647#comment-20208" class="active">I will start supporting</a></h3>    <div class="submitted">Submitted by Philip Johnsson (not verified) on Thu, 2012-03-29 16:25.</div>
    <div class="content">
     <p>I will start supporting Ubuntu 12.04 (Precise) with updates on my PPA as usual when 12.04 is out as final release.</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div></div>
</div>
