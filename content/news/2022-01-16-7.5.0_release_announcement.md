---
date: "2022-01-16T00:00:00"
title: "digiKam 7.5.0 is released"
author: "digiKam Team"
description: "Dear digiKam fans and users, we are proud to announce the stable release of digiKam 7.5.0."
category: "news"
---

[![](https://i.imgur.com/rkQQvk6.png "digiKam 7.5.0 Running Under Microsoft Windows With The Language Selection Dialog")](https://imgur.com/rkQQvk6)

Dear digiKam fans and users,

After one month of active maintenance and a huge bug triage, the digiKam team is proud to present version 7.5.0
of its open source digital photo manager. This new version arrives with more than
[700 files closed in bugzilla](https://bugs.kde.org/buglist.cgi?f1=cf_versionfixedin&limit=0&o1=equals&order=bug_id&product=digikam&v1=7.5.0)
and main improvements about usability.

See below the list of most important features coming with this release.

### Translations, Internationalizations, Localizations, and RTL Support

digiKam and Showfoto have been internationalized by the KDE translators team since the start of the project.
This means 20 years of localization with more than 50 different languages. If you know a proprietary software
which can do better, let's hear...

[![](https://i.imgur.com/1ysgDzZ.png "The list of digiKam languages available in digiKam 7.5.0")](https://imgur.com/1ysgDzZ)

With this release we take care about reviewing all internationalization reports and especially ones about the Right To Left supports.
Usually, in the occidental world, we use Left To Right text orientation, but what's about the rest of the world, as Arabic and Hebrew for example.
With this release at the same time as a huge translation update we have tested and fixed all main problems about [RTL support](https://en.wikipedia.org/wiki/Right-to-left_script).
This has not been an easy task, as this kind of language is not natural for the team...

[![](https://i.imgur.com/SMkk4Ev.png "digiKam 7.5.0 Main Interface in Arabic")](https://imgur.com/SMkk4Ev)

The screenshot behind has been taken after switching the language from English to Arabic. Remark that all the interface is inverted: menus
text, widgets, layout, etc. To change the language, go to the Settings menu and select the Change Language option. A dialog will appear where you can select the main language to use in graphic application. An
alternative language can be also tuned to fill the non translated strings with a replacement localization. See Below the result of
Showfoto translated in Chinese.

[![](https://i.imgur.com/hnbk9iG.png "Showfoto 7.5.0 Main Interface in Chinesse")](https://imgur.com/hnbk9iG)

Translating a huge application like digiKam is a long and complex task. The strings in English are extracted by scripts everyday and
all are packaged for each language by one text formatted file (PO file for Programm Object). The translators work on the file with a dedicated
graphical application. The contextual indications are given, including the place in source code and the contextual helper comments given by programmers
to describe the uses in application. Plural forms are also supported, as this can differ between languages. Finally, when translations is done,
the PO file is updated in a special repository where the developers can checkout data and transform the translation in a binary form
(MO - Machine Object) which will be used by the application at run-time to render translated strings in GUI. These MO files are packaged
by Linux distro teams or in the bundles that we provide for you.

If you want to contribute with the internationalization tasks, don't hesitate [to contact the coordination team](/contribute#translations).

Fcitx aka [Platform Input Context](https://fcitx-im.org/wiki/Fcitx_5) is an input method framework with extension support for Linux systems.
Fcitx supports typing with many languages all over the world with a large number of engines. Fcitx is now included in digiKam Linux AppImage bundle.

### Open-With Option in Context Menu Under macOS.

As the Linux and the Windows version, macOS users reclaims an open-with alternative method for digiKam and Showfoto context menu to open items
in prefered applications. With digiKam 7.5.0, this is now a reality.

[![](https://i.imgur.com/AJNpAsy.png "digiKam 7.5.0 and Open-With contextaul menu under macOS")](https://imgur.com/AJNpAsy)

### Statistics About Bugzilla Reports

[Bugzilla is master piece of software](https://bugs.kde.org/enter_bug.cgi?format=guided&product=digikam) referencing the story of the project, including all the contructive exchanges
between the developers and users. As we process a large bug-triage during this month, it's time to give a general feedback
about the reports statistics in the database classed by supported plateforms.

Since 20 years, digiKam team has closed:

* 9432 files created under Linux.
* 1031 files created under Windows.
* 248 files created under macOS.
* 410 files created under other operating systems.

1097 files are still open in the pending list.

This gives a total of 12218 files registered in the database.

For the files which still in pending list:

* 775 files have been created under Linux.
* 203 files have been created under Windows.
* 43 files have been created under macOS.
* 29 files have been created under other operating systems.

About the type of pending reports:

* 393 files are dysfunctions.
* 704 files are wishes.

Of course, the primary platform is Linux as application is mostly developed under this operating system, but since the 7.0.0 release,the Windows and the macOS reports have become more predominant in bugzilla due to the better integration of the application and a better popularity.

### Final Words

Thanks to all users for [your support and donations](https://www.digikam.org/donate/),
and to all contributors, students, testers who allowed us to improve this release.

digiKam 7.5.0 source code tarball, Linux 64 bits AppImage bundles, macOS Intel package, and Windows 64 bits installers
can be downloaded from [this repository](https://download.kde.org/stable/digikam/7.5.0/).

Rendez-vous in spring 2022 for the next digiKam 7.6.0 release.

Happy digiKaming.
