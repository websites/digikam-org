---
date: "2007-02-19T16:10:00Z"
title: "digiKam 0.9.1-rc1 Release"
author: "gerhard"
description: "Another step in 0.9.1 release cycle is accomplished: RC1 is available for download and testing from here. The sources are on sourceforge.net as well. A"
category: "news"
aliases: "/node/209"

---

<p>Another step in 0.9.1 release cycle is accomplished: RC1 is available for download and testing from <a href="http://digikam3rdparty.free.fr/0.9.1 release/"> here.</a> The sources are on <a href="http://sourceforge.net/projects/digikam/">sourceforge.net</a> as well. A compiled Kubuntu package is <a href="http://www.mpe.mpg.de/~ach/kubuntu/edgy/Pkgs.php">here</a> (thanks to Achim).<br>
0.9.1-final is expected in a weeks time. Download, compile and enjoy!</p>
<p>It is a bugfix and usability release.</p>
<p>For new features of 0.9.1 please refer to the 0.9.1-beta1 release note. With RC1 many things changed under the hood (Gilles prepares the future), but some stuff, in partcular on the core slide show has been added, improved and fixed as well:</p>
<p>This release fixes or adds the following:<br>
- several more EXIF properties can be shown in core slide show and comments<br>
- speed-up of slide show and viewer (F3)<br>
- faster loading of JPEGs<br>
- core slide show (F9) can be cancelled<br>
- mouse button pauses/restarts slide show<br>
- mouse wheel skips though slide show<br>
- Shift + F9 starts a slide show that walkes recursively through the current view tree<br>
- in IconView Ctrl+Space behaves like Ctrl+mouse button<br>
- metadata writing speed to a series of images has improved ~ 10x (e.g. adding tags to many photos)<br>
- add 4 buttons to revert to default settings of mime types<br>
- Bug 141624: Digikam crash when assigning a new tag to a large number of photos: solved<br>
- Bug 141635: Unclear text in digikam (tool tips messages)</p>

<div class="legacy-comments">

</div>