---
date: "2012-01-16T09:47:00Z"
title: "Add a Vintage Effect in digiKam"
author: "Dmitri Popov"
description: "Want to add a vintage effect to your photos? digiKam has all the tools you need to turn your digital snapshots into vintage masterpieces. Continue"
category: "news"
aliases: "/node/640"

---

<p>Want to add a vintage effect to your photos? digiKam has all the tools you need to turn your digital snapshots into vintage masterpieces.</p>
<p><a href="http://scribblesandsnaps.files.wordpress.com/2012/01/digikam_colorbalance.png"><img class="alignnone size-medium wp-image-2181" title="digikam_colorbalance" src="http://scribblesandsnaps.files.wordpress.com/2012/01/digikam_colorbalance.png?w=500" alt="" width="500" height="393"></a></p>
<p><a href="http://scribblesandsnaps.wordpress.com/2012/01/16/add-a-vintage-effect-in-digikam/">Continue to read</a></p>

<div class="legacy-comments">

</div>