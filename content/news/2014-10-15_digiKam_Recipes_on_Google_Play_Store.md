---
date: "2014-10-15T14:44:00Z"
title: "digiKam Recipes on Google Play Store"
author: "Dmitri Popov"
description: "digiKam Recipes is now available on Google Play Store. So you can buy and read the ebook using your favorite Android device."
category: "news"
aliases: "/node/721"

---

<p><a href="http://dmpop.dhcp.io/digikamrecipes/">digiKam Recipes</a> is now available on <a href="https://play.google.com/store/books/details/Dmitri_Popov_digiKam_Recipes?id=T83DBAAAQBAJ">Google Play Store</a>. So you can buy and read&nbsp;the ebook using your favorite&nbsp;Android device.</p>
<p><a href="https://play.google.com/store/books/details/Dmitri_Popov_digiKam_Recipes?id=T83DBAAAQBAJ"><img class="alignnone wp-image-4894 size-medium" src="https://scribblesandsnaps.files.wordpress.com/2014/10/digikamrecipes_googleplaystore.png?w=281" alt="digikamrecipes_googleplaystore" width="281" height="500"></a></p>

<div class="legacy-comments">

</div>