---
date: "2011-01-20T10:31:00Z"
title: "Use the Levels Adjustment Tool in digiKam"
author: "Dmitri Popov"
description: "When it comes to tweaking photos, Levels is the most important weapon in digiKam’s arsenal. This tool lets you adjust brightness and contrast by specifying"
category: "news"
aliases: "/node/566"

---

<p>When it comes to tweaking photos, Levels is the most important weapon in digiKam’s arsenal. This tool lets you adjust brightness and contrast by specifying the location of complete black, complete white, and midtones in a histogram, which makes it a perfect tool for fixing under- and overexposed photos as well as improving the overall tonal range of a photo. <a href="http://scribblesandsnaps.wordpress.com/2011/01/20/use-the-levels-adjustment-tool-in-digikam/">Continue to read</a></p>

<div class="legacy-comments">

</div>