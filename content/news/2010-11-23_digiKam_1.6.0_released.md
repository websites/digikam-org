---
date: "2010-11-23T09:31:00Z"
title: "digiKam 1.6.0 released..."
author: "digiKam"
description: "Dear all digiKam fans and users! digiKam team is proud to announce digiKam 1.6.0 release! digiKam tarball can be downloaded from SourceForge at this url"
category: "news"
aliases: "/node/550"

---

<p>Dear all digiKam fans and users!</p>

<p>digiKam team is proud to announce digiKam 1.6.0 release!</p>

<a href="http://www.flickr.com/photos/digikam/5199790392/" title="101122-0001 by digiKam team, on Flickr"><img src="http://farm6.static.flickr.com/5204/5199790392_459f3a8fc8.jpg" width="491" height="500" alt="101122-0001"></a>

<p>digiKam tarball can be downloaded from SourceForge <a href="http://sourceforge.net/projects/digikam/files">at this url</a></p>

See below the list of new features and bugs-fix coming with this release

<p>Enjoy digiKam.</p>

<h5>NEW FEATURES:</h5><br>

<b>General</b>        : Fix compilation under Mac OS-X through <a href="http://www.macports.org">MacPorts project</a>.<br>
<b>General</b>        : External dependencies to libPGF and Clapack can use used now instead internal 
                        implementation if available in host system dedicated to compile.<br>
<b>Album GUI</b>      : User can exclude tags in the tag-filter view.<br>
<b>Slideshow</b>      : User can change item rating during a slide.<br>
<b>Image Editor</b>   : New option to display over/under exposure indicators if pure colors is detected
                        or if only one color component match the condition.<br>
<b>Image Editor</b>   : New options to adjust the levels of over/under exposed pixels from histogram,
                        displayed by over/under exposure indicators.<br>
<b>Image Editor</b>   : Aspect ratio crop tool is now able to deal with the aspect ratio of the current 
                        loaded image in editor. You can keep the original proportions of the image after 
                        cropping.<br>

<h5>BUGFIXES FROM KDE BUGZILLA:</h5><br>

001 ==&gt; 148434 : Cannot change rating in slideshow.<br>
002 ==&gt; 253911 : libkdcraw access beyond array bounds (cameraXYZMatrix).<br>
003 ==&gt; 254135 : Pipette-tool des not work in image editor/showfoto.<br>
004 ==&gt; 254182 : Can't create mysql-entries for new tags.<br>
005 ==&gt; 253702 : Under/overexposure indicator doesn't work on big-endian processors.<br>
006 ==&gt; 254397 : Full stop at the end of checkbox label.<br>
007 ==&gt; 254381 : Copy any pic to some place - only _move_ is available.<br>
008 ==&gt; 148382 : Overexposure indication already when fully "saturated".<br>
009 ==&gt; 233438 : digiKam crash when editing raw images on OS X.<br>
010 ==&gt; 248302 : Crash while jumping back and forth in preview.<br>
011 ==&gt; 254315 : Please consider attached patch for build system.<br>
012 ==&gt; 224245 : Each image editor plugin should remember the zoom ratio.<br>
013 ==&gt; 220425 : Esc doesn't work for editor tools when lose focus.<br>
014 ==&gt; 255271 : digiKam crashes on lens auto-correction function.<br>
015 ==&gt; 254679 : Crash when closign digikam while filter active.<br>
016 ==&gt; 254357 : digiKam crashes after selecting a month in calendarview.<br>
017 ==&gt; 254203 : Libpgf relevant : digiKam crashes while displaying thumbnails on Ultra-SPARC platform.<br>
018 ==&gt; 233005 : Sorting in thumb panel differs from tree.<br>
019 ==&gt; 229282 : digiKam shutdown as I was sellecting crop to crop an image.<br>
020 ==&gt; 241575 : digiKam crashes after saving several (&gt;2) pictures.<br>
021 ==&gt; 227901 : Can't search for Ratings in Advanced Search.<br>
022 ==&gt; 141753 : Ability to exclude tags in tag filter.<br>
023 ==&gt; 206866 : Moving / renaming files creates duplicates in the ThumbsDB.<br>
024 ==&gt; 255478 : digiKam database tables only grow in size and get never cleaned.<br>
025 ==&gt; 189454 : Preview images react strange to mouse movement.<br>
026 ==&gt; 257134 : digiKam crashes when entering its settings.<br>
027 ==&gt; 257103 : Can't save images modified with Image editor.<br>
028 ==&gt; 247175 : digiKam doesn't exit after closing main window.<br>
029 ==&gt; 252737 : Direct capture bug 2 of 2.<br>
030 ==&gt; 243497 : digikam does not exit when "quit", stuck in select.<br>
031 ==&gt; 257329 : Build fails - openmp flags.<br>
<div class="legacy-comments">

  <a id="comment-19638"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/550#comment-19638" class="active">Well done!</a></h3>    <div class="submitted">Submitted by Riccardo (not verified) on Tue, 2010-11-23 15:29.</div>
    <div class="content">
     <p>Great, thank you. This release is out sooner than expected. ;)</p>
<p>Any news about Digikam 2.0?</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19639"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/550#comment-19639" class="active">digiKam 2.0...</a></h3>    <div class="submitted">Submitted by digiKam on Tue, 2010-11-23 15:33.</div>
    <div class="content">
     <p>... look on <a href="http://www.digikam.org/drupal/about/releaseplan">Release Plan</a><a> page...</a></p><a>         </a></div><a>
    <div class="links">» </div>
  </a></div><a>
</a></div><a id="comment-19640"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/550#comment-19640" class="active">Lucid</a></h3>    <div class="submitted">Submitted by <a href="http://judjohnsonphoto.com" rel="nofollow">Jud</a> (not verified) on Tue, 2010-11-23 20:38.</div>
    <div class="content">
     <p>Can this be installed on Lucid yet, or am I still stuck with 1.2?</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-19641"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/550#comment-19641" class="active">Build broken on mingw32 Win32</a></h3>    <div class="submitted">Submitted by Andrew Goodbody (not verified) on Wed, 2010-11-24 11:11.</div>
    <div class="content">
     <p>I get<br>
Scanning dependencies of target digikam-svnversion<br>
[  0%] Generating digikam/svnversion.h<br>
[  0%] Built target digikam-svnversion<br>
Scanning dependencies of target ExternalDraw_automoc<br>
Generating externaldraw.moc<br>
[  0%] Built target ExternalDraw_automoc<br>
Scanning dependencies of target ExternalDraw<br>
[  0%] Building CXX object libs/imageproperties/markerclusterholderplugin/CMakeFiles/ExternalDraw.dir/ExternalDraw_automoc.obj<br>
The input line is too long.</p>
<p>The generated command line is well over 8300 characters and the limit I think is about 8192.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19642"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/550#comment-19642" class="active">it compile here with TDM-GCC...</a></h3>    <div class="submitted">Submitted by digiKam on Wed, 2010-11-24 11:21.</div>
    <div class="content">
     <p>Before to release 1.6.0, i checked compilation under Win7 using TDM-GCC. All work fine.</p>
<p>Where is defined this 8192 char. limit ?</p>
<p>digiKam</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19643"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/550#comment-19643" class="active">XP limit is 8191</a></h3>    <div class="submitted">Submitted by Andrew Goodbody (not verified) on Wed, 2010-11-24 13:13.</div>
    <div class="content">
     <p>The limit in XP is 8191 according to this MS article http://support.microsoft.com/kb/830473</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19644"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/550#comment-19644" class="active">wonderfull M$ world...</a></h3>    <div class="submitted">Submitted by digiKam on Wed, 2010-11-24 13:27.</div>
    <div class="content">
     <p>... and of course, there is no way to increase this limit in the system ???</p>
<p>To compare, Linux has a similar limitation somewhere ? This can be adjusted ?</p>
<p>digiKam</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19645"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/550#comment-19645" class="active">No increase possible on XP</a></h3>    <div class="submitted">Submitted by Andrew Goodbody (not verified) on Wed, 2010-11-24 13:55.</div>
    <div class="content">
     <p>No, there seems no way to increase it in XP.</p>
<p>The situation elsewhere is a bit more complicated.<br>
http://www.in-ulm.de/~mascheck/various/argmax/</p>
<p>I think that reducing the command line length would be good for compilation speed anyway.</p>
<p>Andrew</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div></div></div><a id="comment-19646"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/550#comment-19646" class="active">Mac OS X Interface</a></h3>    <div class="submitted">Submitted by thorGT (not verified) on Wed, 2010-11-24 14:15.</div>
    <div class="content">
     <p>...is, to be honest with you, just horrible. There are a lot of Mac users that will refuse to work with your application and even to consider using it if it doesn't have a shiny Apple standard interface. I promise to come and try fix that when (and if it's not fixed before =p) I have the time, well, in the next month probably. I have an absolutely legal and perfectly mine Mac Mini.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19647"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/550#comment-19647" class="active">Qt4/KDE4 style.</a></h3>    <div class="submitted">Submitted by digiKam on Wed, 2010-11-24 14:23.</div>
    <div class="content">
     <p>This is relevant of Qt4 and KDE4 style, not digiKam...</p>
<p>digiKam</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19648"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/550#comment-19648" class="active">Mac OS X User Interface</a></h3>    <div class="submitted">Submitted by thorGT (not verified) on Wed, 2010-11-24 20:45.</div>
    <div class="content">
     <p>Yes, I know, sure thing, I don't blame you personally for that. Qt &amp; Oxygen KDE Style are the main responsible ones here. But have you considered using Cocoa for some parts of the user interface? Writing a (partly) native Mac OS X Cocoa application, as some projects do. If you have a correctly coded project, as they teach us at the university, separating the backend and the interface, then this might be not as hard as it seems (however I completely and absolutely realize that you personally may neither be interested  nor have time to do this, then we'd need a volunteer for this task, if such a port was agreed upon, after all)</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19652"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/550#comment-19652" class="active">Re: Mac OS X User Interface</a></h3>    <div class="submitted">Submitted by <a href="http://vlado-do.de/" rel="nofollow">Vlado Plaga</a> (not verified) on Sun, 2010-11-28 11:06.</div>
    <div class="content">
     <p>Just for the record: I'm using Digikam both on Linux and on Mac OS and I like the way it looks (in version 1.2) on either of them - no need to invest a lot of time in changing it, in my opinion.</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div><a id="comment-19668"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/550#comment-19668" class="active">OS X UI</a></h3>    <div class="submitted">Submitted by Gary (not verified) on Mon, 2010-12-13 21:45.</div>
    <div class="content">
     <p>Unfortunately true about Mac users.  I, a Mac user, say: invest your energy on the guts, not the eye candy.</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-19649"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/550#comment-19649" class="active">shortcut</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Wed, 2010-11-24 21:32.</div>
    <div class="content">
     <p>I have to edit a  huge amount of images in Gimp. A Gimp shortcut would be very useful or, better, the ability to customize shortcuts. Do you plan such possibility ? Otherwise, digikam is a tool that works very fine. Thanks a lot !</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-19653"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/550#comment-19653" class="active">Unable to build</a></h3>    <div class="submitted">Submitted by jostmart (not verified) on Mon, 2010-11-29 23:08.</div>
    <div class="content">
     <p>I don't understand how I could get libkdcraw.<br>
I go to http://www.digikam.org/sharedlibs and can't find it</p>
<p>--  libkdcraw library found.................. NO<br>
--<br>
CMake Error at digikam/CMakeLists.txt:86 (MESSAGE):<br>
   digiKam needs libkdcraw. You need to install the libkdcraw (version &gt;= 1.1.0) library development package.<br>
Call Stack (most recent call first):<br>
  digikam/CMakeLists.txt:287 (PRINT_LIBRARY_STATUS)</p>
<p>I've done this: sudo apt-get build-dep kipi-plugins digikam</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19654"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/550#comment-19654" class="active">kdcraw</a></h3>    <div class="submitted">Submitted by Maderios (not verified) on Tue, 2010-11-30 14:29.</div>
    <div class="content">
     <p>kdcraw here<br>
http://sourceforge.net/projects/kipi/files/libkdcraw/</p>
<p>I tried to compile it:<br>
"checking for KDE... configure: error:<br>
in the prefix, you've chosen, are no KDE libraries installed. This will fail.<br>
So, check this please and use another prefix!"</p>
<p>OK, but all kde libraries are installed ......<br>
(Debian testing)</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-19665"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/550#comment-19665" class="active">I have the same problem,</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Fri, 2010-12-10 22:19.</div>
    <div class="content">
     <p>I have the same problem, help!!!</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-19655"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/550#comment-19655" class="active">maverick meerkat (kubuntu 10.10) repository</a></h3>    <div class="submitted">Submitted by qpal (not verified) on Tue, 2010-11-30 19:08.</div>
    <div class="content">
     <p>I was looking for kubuntu repository with latest digiKam and I found this: https://launchpad.net/~philip5/+archive/extra<br>
Works great, just add repository, update, upgrade and that's it! Necessary for every digiKam fan with kubuntu.</p>
<p>Tribute to digiKam developers, it is really amazing app.</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-19659"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/550#comment-19659" class="active">Again! Great Job!!! I love</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Sun, 2010-12-05 16:31.</div>
    <div class="content">
     <p>Again! Great Job!!! I love you :-)</p>
<p>My only question:</p>
<p>&gt;Slideshow : User can change item rating during a slide.</p>
<p>Which key is this???</p>
<p>Best whishes and many thanks for this great software!</p>
<p>- TK</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19660"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/550#comment-19660" class="active">this one...</a></h3>    <div class="submitted">Submitted by digiKam on Sun, 2010-12-05 16:41.</div>
    <div class="content">
     <p>001 ==&gt; 148434 : Cannot change rating in slideshow.</p>
         </div>
    <div class="links">» </div>
  </div>
</div>
</div>
