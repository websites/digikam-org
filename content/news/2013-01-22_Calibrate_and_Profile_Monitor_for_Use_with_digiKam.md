---
date: "2013-01-22T11:35:00Z"
title: "Calibrate and Profile Monitor for Use with digiKam"
author: "Dmitri Popov"
description: "To calibrate a monitor and generate a color profile for it on Linux, you need two things: a colorimeter and color profiling software. High-quality professional"
category: "news"
aliases: "/node/680"

---

<p>To calibrate a monitor and generate a color profile for it on Linux, you need two things: a colorimeter and color profiling software. High-quality professional colorimeters tend to be rather expensive, but you can use the excellent open source <a href="http://www.hughski.com/">ColorHUG</a> device instead. When it comes to calibration and profiling, the <a href="http://dispcalgui.hoech.net/">displaycalGUI</a> software is the perfect software for the job. It provides a graphical user interface to the display calibration and profiling tools of the <a href="http://www.argyllcms.com/">Argyll CMS</a> open source color management system.</p>
<p><img src="http://scribblesandsnaps.files.wordpress.com/2013/01/digikam-dispcalgui.png?w=500" width="500" height="394"></p>
<p><a href="http://scribblesandsnaps.com/2013/01/22/calibrate-and-profile-monitor-for-use-with-digikam/">Continue to read</a></p>

<div class="legacy-comments">

</div>