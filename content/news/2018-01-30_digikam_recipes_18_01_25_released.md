---
date: "2018-01-30T00:00:00"
title: "digiKam Recipes 18-01-25 released"
author: "Dmitri Popov"
description: "A new revision of the digiKam Recipes book is available"
category: "news"
---

After a somewhat prolonged hiatus  (my move from Denmark to Germany  and full-time job at SUSE Linux GmbH had something to do with this), a new revision of the digiKam Recipes book is ready for your reading pleasure. [Continue reading](https://scribblesandsnaps.com/2018/01/29/digikam-recipes-18-01-25-released/)...
