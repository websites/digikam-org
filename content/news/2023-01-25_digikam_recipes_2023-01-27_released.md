---
date: "2023-01-25T00:00:00"
title: "digiKam Recipes 2023-01-27 released"
author: "Dmitri Popov"
description: "A new revision of the digiKam Recipes book is available"
category: "news"
---

New year, new [digiKam Recipes](https://dmpop.gumroad.com/l/digikamrecipes) book release. The new version features the completely rewritten _Tag faces with the Face Recognition feature_ chapter and an all-new example workflow section in the _Batch process photos and RAW files_ chapter. Several chapters have been revised and improved, including _Edit tags with Tag Manager_, _Color management in digiKam_, and _Move digiKam library and databases_. All screenshots have been refreshed, too. As always, the new revision includes plenty of tweaks and fixes.

As always, all digiKam Recipes readers will receive the updated version of the book automatically and free of charge. The digiKam Recipes book is available from [Google Play Store](https://play.google.com/store/books/details/Dmitri_Popov_digiKam_Recipes?id=T83DBAAAQBAJ) and [Gumroad](https://gumroad.com/l/digikamrecipes/).