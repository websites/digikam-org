---
date: "2011-11-30T09:22:00Z"
title: "digiKam in c’t Digital Photography"
author: "Dmitri Popov"
description: "The latest issue #6 of c't Digital Photography features an in-depth article about digiKam by yours truly. c't Digital Photography is a relatively new quarterly"
category: "news"
aliases: "/node/634"

---

<p>The latest issue #6 of <a href="http://www.ct-digiphoto.com/">c't Digital Photography</a> features an in-depth article about digiKam by yours truly.</p>
<p><a href="http://www.ct-digiphoto.com/magazine/issue-6-2012-1376824.html"><img class="alignnone size-full wp-image-2113" title="ct-digiphoto-6" src="http://scribblesandsnaps.files.wordpress.com/2011/11/ct-digiphoto-6.jpeg" alt="" width="331" height="472"></a></p>
<p>c't Digital Photography is a relatively new&nbsp;quarterly&nbsp;magazine published by&nbsp;<a href="http://www.heise.de/">Heise</a>, one of the most respected magazine publishing houses in Germany. <a href="https://scribblesandsnaps.wordpress.com/2011/11/30/digikam-in-ct-digital-photography/">Continue to read</a></p>

<div class="legacy-comments">

</div>