---
date: "2013-02-08T13:36:00Z"
title: "digiKam Software Collection 3.0.0 is out..."
author: "digiKam"
description: "Dear all digiKam fans and users! After one summer of active development with Google Summer of Code 2012 students, and 4 pre-release, digiKam team is"
category: "news"
aliases: "/node/683"

---

<a href="http://www.flickr.com/photos/digikam/8358547425/" title="splash-showfoto by digiKam team, on Flickr"><img src="http://farm9.staticflickr.com/8194/8358547425_bcd3693714.jpg" width="500" height="307" alt="splash-showfoto"></a>

<p>Dear all digiKam fans and users!</p>

<p>After one summer of active development with Google Summer of Code 2012 students, and 4 pre-release, digiKam team is proud to announce the final release of digiKam Software Collection 3.0.0.
This version include GoSC 2012 projects <a href="http://community.kde.org/Digikam/GSoC2012">listed here</a>.</p>

<p>See <a href="https://bugs.kde.org/buglist.cgi?f1=cf_versionfixedin&amp;o1=equals&amp;query_format=advanced&amp;bug_status=RESOLVED&amp;bug_status=NEEDSINFO&amp;bug_status=VERIFIED&amp;bug_status=CLOSED&amp;v1=3.0.0&amp;product=digikam&amp;product=digikamimageplugins&amp;product=kipiplugins&amp;product=showfoto">the list of files closed</a> with this release into KDE bugzilla.</p>

<p>digiKam software collection tarball can be downloaded from <a href="http://download.kde.org/stable/digikam/digikam-3.0.0.tar.bz2.mirrorlist">KDE repository</a></p>

<p>Happy digiKaming...</p>
<div class="legacy-comments">

  <a id="comment-20487"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/683#comment-20487" class="active">Excellent</a></h3>    <div class="submitted">Submitted by DRB (not verified) on Fri, 2013-02-08 15:24.</div>
    <div class="content">
     <p>I have been eagerly awaiting this release. I am especially interested in using face recognition to assist in tagging my images. The GoSC 2012 projects page lists this feature as "Not completed."--is this accurate?</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20488"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/683#comment-20488" class="active">not completed yet...</a></h3>    <div class="submitted">Submitted by digiKam on Fri, 2013-02-08 15:51.</div>
    <div class="content">
     <p>yes, this feature is not yet completed by student, but he still alive an current is work with him... The plan is to include beta code in next version, perhaps 3.2 or 3.3...</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20490"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/683#comment-20490" class="active">bounty system?</a></h3>    <div class="submitted">Submitted by <a href="http://bridgehosting.net/" rel="nofollow">Bridge Hosting</a> (not verified) on Sat, 2013-02-09 18:32.</div>
    <div class="content">
     <p>Is there a bounty system on open features requests? This is the most long awaited and oft mentioned feature I think.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20491"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/683#comment-20491" class="active">KDE bugzilla...</a></h3>    <div class="submitted">Submitted by digiKam on Sat, 2013-02-09 18:47.</div>
    <div class="content">
     <p>https://bugs.kde.org/show_bug.cgi?id=271679</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div></div><a id="comment-20489"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/683#comment-20489" class="active">Ubuntu ppa</a></h3>    <div class="submitted">Submitted by <a href="http://www.o3foto.cz" rel="nofollow">t.kijas - o3foto.cz</a> (not verified) on Sat, 2013-02-09 15:58.</div>
    <div class="content">
     <p>Thank you for this app, please, is there any official ppa where I can download latest digikam? 3.0?</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20492"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/683#comment-20492" class="active">Thanks a lot for this</a></h3>    <div class="submitted">Submitted by Pepou (not verified) on Sun, 2013-02-10 09:53.</div>
    <div class="content">
     <p>Thanks a lot for this wonderful software !!<br>
I'm also looking for an ubuntu ppa... !</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20493"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/683#comment-20493" class="active">The Digicam team often says,</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Sun, 2013-02-10 11:19.</div>
    <div class="content">
     <p>The Digicam team often says, that they make no ppa for any distribution.<br>
So there are tree waya:<br>
1. Install Windows and run the Windows client. Shame...<br>
2. Compile by yourself. This is a littel bit complicated, because there aree many dependence and you must show by yourself to have the aktuell KDE backports installed in Ubuntu. Remember Digikam is a KDE Programm and Ubuntu use Gnome<br>
3. Unoffical PPA: Philip5 make a great job:<br>
ppa.launchpad.net/philip5/kubuntu-backports/<br>
ppa.launchpad.net/kubuntu-ppa/backports<br>
The first ppa have the compiled digikam 3 for precise, the second have is for the right KDE Backport</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20494"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/683#comment-20494" class="active">Thanks :)</a></h3>    <div class="submitted">Submitted by Pepou (not verified) on Sun, 2013-02-10 20:45.</div>
    <div class="content">
     <p>Thanks :)</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-20496"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/683#comment-20496" class="active">I tried the unofficial</a></h3>    <div class="submitted">Submitted by <a href="http://www.loramel.net" rel="nofollow">Martin Lubich</a> (not verified) on Mon, 2013-02-18 11:34.</div>
    <div class="content">
     <p>I tried the unofficial Philip5 PPA under Kubuntu 12.04.2 with all the KDE backports installed ( -&gt; KDE 4.10).</p>
<p>Unfortunately this does not install. There are dependencies which are not satisfied, and to be honest, I really try to avoid unofficial Repositories if they involve any wider KDE dependencies, this just leads to pain.</p>
<p>Digikam 3.0.0.rc is installed via the official kubuntu backports. Alas I get a crash on startup, which has to do with the color management part. I just wanted to check against the official 3.0.0 release before filing a bugreport.</p>
<p>Is there any change in the final 3.0.0 regarding color management ? If not I can create a report with the rc.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20499"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/683#comment-20499" class="active">If you use my</a></h3>    <div class="submitted">Submitted by Philip5 (not verified) on Wed, 2013-02-20 00:48.</div>
    <div class="content">
     <p>If you use my kubuntu-backports PPA did you also add my "extra" PPA? The kubuntu-backports PPA is just rebuilds against newer versions of KDE (from the Kubuntu team) that doesn't come with official releases of Ubuntu and always need packages in my "extra" PPA.</p>
<p>If something doesn't work I'm always interested in feedback to fix any uncommon problem.</p>
<p>/Philip</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div></div></div><a id="comment-20495"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/683#comment-20495" class="active">When do you update the 3.0.0</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Sat, 2013-02-16 18:22.</div>
    <div class="content">
     <p>When do you update the 3.0.0 Windows package on sourceforge?<br>
Still waiting for the files :(</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20497"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/683#comment-20497" class="active">+1</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Mon, 2013-02-18 16:07.</div>
    <div class="content">
     <p>+1</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-20498"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/683#comment-20498" class="active">Yhea im waiting for the</a></h3>    <div class="submitted">Submitted by Paul (not verified) on Tue, 2013-02-19 17:08.</div>
    <div class="content">
     <p>Yhea im waiting for the updated tars as well!<br>
Where can i get the 3.0.0 Windows files?</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-20500"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/683#comment-20500" class="active">Coming Soon</a></h3>    <div class="submitted">Submitted by Ananta Palani on Thu, 2013-02-21 01:32.</div>
    <div class="content">
     <p>I'm working on releasing this as time allows. I am testing the build a bit to make sure there is no issue upgrading from older versions. I hope to have it released within a week.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20501"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/683#comment-20501" class="active">Windows coming soon</a></h3>    <div class="submitted">Submitted by andreas (not verified) on Thu, 2013-02-21 14:05.</div>
    <div class="content">
     <p>Yeah! How can I transfer some money to you!<br>
And ... will DK need less than 30 minutes to start on Windows 7?</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-20502"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/683#comment-20502" class="active">I have been eagerly awaiting</a></h3>    <div class="submitted">Submitted by <a href="http://datingsites4.tumblr.com" rel="nofollow">date</a> (not verified) on Tue, 2013-02-26 20:57.</div>
    <div class="content">
     <p>I have been eagerly awaiting this release. I am especially interested in using face recognition to assist in tagging my images. The GoSC 2012 projects page lists this feature as <a href="http://datingsites4.tumblr.com">dating sites</a> "Not completed."</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20504"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/683#comment-20504" class="active">no face recognition in 3.0.0</a></h3>    <div class="submitted">Submitted by gr (not verified) on Mon, 2013-03-04 09:52.</div>
    <div class="content">
     <p>I saw in an internet article that face recognition is skipped for the 3.0.0 version. It is assumed that this feature will be added in one of the next minor updates.</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div></div>
</div>
