---
date: "2010-10-11T11:41:00Z"
title: "digiKam 1.5.0 released..."
author: "digiKam"
description: "Dear all digiKam fans and users! digiKam team is proud to announce digiKam 1.5.0 release! digiKam tarball can be downloaded from SourceForge at this url"
category: "news"
aliases: "/node/539"

---

<p>Dear all digiKam fans and users!</p>

<p>digiKam team is proud to announce digiKam 1.5.0 release!</p>

<a href="http://www.flickr.com/photos/digikam/5071377416/" title="digikam1.5.0 by digiKam team, on Flickr"><img src="http://farm5.static.flickr.com/4129/5071377416_c89f48dfba.jpg" width="500" height="200" alt="digikam1.5.0"></a>

<p>digiKam tarball can be downloaded from SourceForge <a href="http://sourceforge.net/projects/digikam/files">at this url</a></p>

<p>See the list of new features below and bug-fixes coming with this release since 1.4.0</p>

<p>Enjoy digiKam.</p>

<h5>NEW FEATURES:</h5><br>

<b>General</b>             : Fix compilation under windows with TDM-GCC and MSVC compilers.<br>
<b>General</b>             : Including LensFun library source code as 3rd-party component, especially for windows target where this library is not available.<br>
<b>Image Editor</b>        : Improvement of algorithm to convert 8 bits image to 16 and limit histogram holes during conversion.<br>

<b>Batch Queue Manager</b> : Add new tool to remove metadata from images.<br>
<b>Batch Queue Manager</b> : Add new tool to apply Lens corrections using LensFun library.<br>

<h5>BUGFIXES FROM KDE BUGZILLA:</h5><br>

001 ==&gt; 247977 : Libpgf relevant : digiKam crash while scrolling the thumbnail view.<br>
002 ==&gt; 249402 : Download window presents file in the opposite order than expected.<br>
003 ==&gt; 249196 : Localization fails after upgrade !<br>
004 ==&gt; 245069 : File name not added for import files.<br>
005 ==&gt; 250003 : Kipi-plugin for xcf is needed.<br>
006 ==&gt; 250081 : Lens-autocorrection does not recognize the lens automaticly for Canon EOS 450D.<br>
007 ==&gt; 207710 : Crash while loading large file (Digikam::DImgScale::dimgScaleAARGB, Digikam::DImg::smoothScaleSection, Digikam::DImgInterface::paintOnDevice).<br>
008 ==&gt; 218256 : digiKam rc1 crashes while generating fingerprints.<br>
009 ==&gt; 237786 : whitebalance doesn't work properly.<br>
010 ==&gt; 250265 : Black background of short summary display.<br>
011 ==&gt; 248889 : Notification name "save file complete" written in lowercase.<br>
012 ==&gt; 249082 : digiKam crashes on startup leaving message of unknown symbol.<br>
013 ==&gt; 249029 : Changing the parameters of a tool in the batch queue manager changes parameters of all instances of such tool.<br>
014 ==&gt; 230515 : Crash of digikam on closing program.<br>
015 ==&gt; 148721 : Remove serial numbers automatically.<br>
016 ==&gt; 229092 : digiKam Scan for New images closes at 90%.<br>
017 ==&gt; 248943 : Annoying and dangerous number input fields in editor.<br>
018 ==&gt; 216353 : Make it possible to apply lens correction in batch.<br>
019 ==&gt; 251920 : digiKam should match lenses on Exif.CanonCs.LensType, not ExifCanonCs.Lens.<br>
020 ==&gt; 251118 : Sorting order is not remembered.<br>
021 ==&gt; 170170 : High quality 8 to 16-bit conversion (histogram repair).<br>
022 ==&gt; 252295 : "Fit to Window" button while using any tool does not permit panning.<br>
023 ==&gt; 243179 : Wrong Taborder in Settings-&gt;Templates-&gt;Location|Contact.<br>
024 ==&gt; 210198 : Copyright and Right Usage information in Settings are not saved in template file.<br>
025 ==&gt; 228547 . Videos cannot return to thumbnail view.<br>
026 ==&gt; 251899 : Can't rename files during import from camera.<br>
027 ==&gt; 169264 : Color Management: Rendering Intents are mixed up.<br>
028 ==&gt; 188335 : Slideshow shows movies as "cannot show picture "xyz.mov".<br>
029 ==&gt; 252488 : digiKam incorrectly renders SD Card size.<br>
030 ==&gt; 252489 : digiKam incorrectly detects SD Card name.<br>
031 ==&gt; 252521 : README incorrectly claims kdegraphics-4.4 is sufficient.<br>
032 ==&gt; 249248 : Updating fingerprints metadata.<br>
033 ==&gt; 125042 : Batch Rename Image from Album do not select all album images.<br>
034 ==&gt; 237516 : Open in Terminal command doesn't take KDE systemsettings into account.<br>
035 ==&gt; 225552 : digiKam can't remember setting.<br>
036 ==&gt; 248087 : Preview not toggling from video mode to image mode.<br>
037 ==&gt; 209382 : Caption not being updated in image file.<br>
038 ==&gt; 253043 : digiKam requires both SQlite and MySql.<br>
039 ==&gt; 252130 : Crash on adding things to batch queue.<br>
040 ==&gt; 237961 : ICC Convert faulty in BQM.<br>
041 ==&gt; 228833 : Rotate image in batch processing doesn't edit EXIF properly.<br>
042 ==&gt; 213896 : After editing color, etc, i saved the jpg and it just crashed.<br>
043 ==&gt; 232854 : Crashed while adding new tag.<br>
044 ==&gt; 241770 : digiKam crashes.<br>
045 ==&gt; 238045 : Random crashes of digiKam.<br>
046 ==&gt; 188263 : Crash after tinkering with star rating widget in thumbbar.<br>
047 ==&gt; 233549 : Database stuck at saving new image.<br>
048 ==&gt; 221191 : Menu not disabled when decoding raw.<br>
049 ==&gt; 237722 : Crash while applying Vivid color effect.<br>
050 ==&gt; 207893 : Crash after undoing change in Image Editor.<br>
051 ==&gt; 199801 : digiKam geolocation tool crash [exiv2].<br>
052 ==&gt; 223774 : digiKam crashed at liquid rescale preview generation.<br>
053 ==&gt; 233997 : Crash while importing from usb device after exivdates are read.<br>
054 ==&gt; 213576 : Thumbbar not updated after saveas in image editor.<br>


<div class="legacy-comments">

  <a id="comment-19566"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/539#comment-19566" class="active">Wrong version at SourceForge</a></h3>    <div class="submitted">Submitted by Morty (not verified) on Mon, 2010-10-11 14:20.</div>
    <div class="content">
     <p>The quick access "Download Now!" button at SourceForge points at the wrong version, it gives you 1.2.0 rater than the latest release.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19567"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/539#comment-19567" class="active">fixed</a></h3>    <div class="submitted">Submitted by digiKam on Mon, 2010-10-11 14:38.</div>
    <div class="content">
     <p>Fixed on sourceforge.net</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-19570"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/539#comment-19570" class="active">Windows Version</a></h3>    <div class="submitted">Submitted by Marc (not verified) on Tue, 2010-10-12 07:26.</div>
    <div class="content">
     <p>Hi, can you provide Windows binerys? The KDE-Windows Port is still dead...</p>
<p>That would be great!<br>
Marc</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19571"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/539#comment-19571" class="active">windows...</a></h3>    <div class="submitted">Submitted by digiKam on Tue, 2010-10-12 07:42.</div>
    <div class="content">
     <p>The problem is : how to package digiKam including all shared libraries" ?</p>
<p>If i make a KDE4 installation backup from my computer, it create a tarball of 700 Mb around !</p>
<p>KDE-Windows installer is an optimized way. KDE-windows team just need to enable digiKam and kipi-plugins in package script lists.</p>
<p>digiKam</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19576"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/539#comment-19576" class="active">Digikam on windows</a></h3>    <div class="submitted">Submitted by JulienN (not verified) on Tue, 2010-10-12 08:26.</div>
    <div class="content">
     <p>Gilles,</p>
<p>I think when Digikam 2.0 will be out the main features that will be missing for DigiKam are:<br>
- an easy to install .dmg for MacOSX<br>
- an easy to install .exe for windows</p>
<p>Look at vlc, firefox, ... they are very popular because they are easy to install under windows.<br>
I think even if the download is very big, many people would be happy to have an option to install DigiKam easily. Is it possible to distribute digiKam with a private copy of qt/kdelibs/ and other dependencies ?</p>
<p>I guess digiKam relies also on some kde process which need to be lunched along with DigiKam ?</p>
<p>Julien</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19588"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/539#comment-19588" class="active">installers...</a></h3>    <div class="submitted">Submitted by digiKam on Tue, 2010-10-12 12:39.</div>
    <div class="content">
     <p>&gt;Look at vlc, firefox, ... they are very popular because they are easy to install under windows.</p>
<p>I know... But building an easy installer for Mac and Windows is not a simple task. It take a while to do, to update, and to test.</p>
<p>&gt;I guess digiKam relies also on some kde process which need to be lunched along with DigiKam ?</p>
<p>yes, this is why currently KDE windows installer is the best way...</p>
<p>I repeat: i'm developer, not packager. All contributors are fine to help us in this way.</p>
<p>digiKam</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19602"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/539#comment-19602" class="active">Windows Version Fee</a></h3>    <div class="submitted">Submitted by DRB (not verified) on Wed, 2010-10-13 21:35.</div>
    <div class="content">
     <p>I have been recommending digiKam to people for years now but most people reply "What is Linux?" I understand that there is currently no one who is willing to be responsible for packaging the Windows version. Unfortunately I am not able to help in this way.</p>
<p>Perhaps a small fee could be charged for the Windows version. I suspect KDE will not want to do this but the 'Official' Windows version could be offered in parallel. Users in the 'Windows world' are used to and comfortable with paying for software -- of course they would be paying instead for the services of the packager not the software, technically.</p>
<p>In the mean time I will have to stick to Picasa as my second recommendation -- unfortunately tagging support is nearly completely missing from Picasa to name just one item.</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div></div><a id="comment-19572"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/539#comment-19572" class="active">at least it compile...</a></h3>    <div class="submitted">Submitted by digiKam on Tue, 2010-10-12 07:44.</div>
    <div class="content">
     <a href="http://www.flickr.com/photos/digikam/5067133639/" title="digikam1.5.0-windows-lensfuntools-2 by digiKam team, on Flickr"><img src="http://farm5.static.flickr.com/4088/5067133639_7ed84f862d.jpg" width="500" height="165" alt="digikam1.5.0-windows-lensfuntools-2"></a>

<p>As you can see, digiKam compile fine under Windows Seven, with GCC and MSVC. I personally manage code for Windows....</p>         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19575"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/539#comment-19575" class="active">Yes, I know. Can you as</a></h3>    <div class="submitted">Submitted by Marc (not verified) on Tue, 2010-10-12 07:54.</div>
    <div class="content">
     <p>Yes, I know. Can you as maintainer enable digikam in the build scripts?</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19577"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/539#comment-19577" class="active">no, this is not my role...</a></h3>    <div class="submitted">Submitted by digiKam on Tue, 2010-10-12 08:40.</div>
    <div class="content">
     <p>This task is delegate to KDE windows team. I'm developer, not packager. I do a lots of work to digiKam. I don't have more free time to manage windows packages.</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div></div><a id="comment-19587"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/539#comment-19587" class="active">THANK YOU SO MUCH!!</a></h3>    <div class="submitted">Submitted by Bender (not verified) on Tue, 2010-10-12 12:28.</div>
    <div class="content">
     <p>I would like to thank You digikam developers SO MUCH for such a great piece of software!! You are doing such a great job with it, this is one if not the best images managing software.</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-19589"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/539#comment-19589" class="active">Thank you very much - and what is next</a></h3>    <div class="submitted">Submitted by Daniel (not verified) on Tue, 2010-10-12 13:39.</div>
    <div class="content">
     <p>First of all thank you very much to all developers and contributors for this software!!!<br>
I really love it and I would also be interested in an easy installation on windows but I can understand Gilles responses. Maybe the KDE Windows guys will help us soon.</p>
<p>But another question: I saw that the release plan has changed and the 2.0.0. is replaced by a 1.6.0. I don't worry about version numbers but also the comment about included GSoC work has been removed. Is the inclusion of this features rescheduled ??</p>
<p>Thanks again and keep on doing this great work.</p>
<p>Daniel</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19590"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/539#comment-19590" class="active">I missed to scroll down</a></h3>    <div class="submitted">Submitted by Daniel (not verified) on Tue, 2010-10-12 13:42.</div>
    <div class="content">
     <p>Sorry, ignore my question about 2.0.0 and GSoC. I just missed to scroll down the page.</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-19604"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/539#comment-19604" class="active">I like the improvement of</a></h3>    <div class="submitted">Submitted by Paul (not verified) on Thu, 2010-10-14 10:17.</div>
    <div class="content">
     <p>I like the improvement of algorithm to convert 8 bits image to 16 and limit histogram holes during conversion.</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-19605"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/539#comment-19605" class="active">SVN revision</a></h3>    <div class="submitted">Submitted by bkn (not verified) on Fri, 2010-10-15 01:01.</div>
    <div class="content">
     <p>Can you publish the svn revision number of your releases for those of us that compile digikam directly from SVN trunk? </p>
<p>I update my local source by 'svn up -r123456789'; when a new version comes out i sort through the logs to figured out the svn revision number. Perhaps there is a better way do update the digikam sources? </p>
<p>thanks and cheers,<br>
 - b</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-19615"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/539#comment-19615" class="active">Debian packages</a></h3>    <div class="submitted">Submitted by Xavier (not verified) on Tue, 2010-10-19 20:20.</div>
    <div class="content">
     <p>Hello,<br>
For those interested, I have packaged digikam 1.5 and the kipi-plugins 1.5 for debian/unstable i386 (these are not official debian packages, so use at your own risks). You can find the packages at http://xraynaudphotos.free.fr/debian.php<br>
Xavier</p>
         </div>
    <div class="links">» </div>
  </div>

</div>
