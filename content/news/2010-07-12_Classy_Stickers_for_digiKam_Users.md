---
date: "2010-07-12T15:33:00Z"
title: "Classy Stickers for digiKam Users"
author: "Dmitri Popov"
description: "Using digiKam? Show this to the world with some classy stickers from World Label. If you use digiKam to process and organize your photos, you"
category: "news"
aliases: "/node/533"

---

<a href="http://blog.worldlabel.com/2010/classy-stickers-for-digikam-lovers-giveaway.html"><img src="http://blog.worldlabel.com/wp-content/myfiles/2010/07/digiKam_sticker.jpg" width="400" height="267"></a>

<p>Using digiKam? Show this to the world with some classy stickers from World Label. If you use digiKam to process and organize your photos, you have a chance to win 5 sheets (18 stickers each) featuring an original design. And you can download the template and print stickers yourself, too. <a href="http://blog.worldlabel.com/2010/classy-stickers-for-digikam-lovers-giveaway.html">Continue to read</a></p>
<div class="legacy-comments">

</div>