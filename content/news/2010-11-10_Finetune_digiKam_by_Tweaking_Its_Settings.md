---
date: "2010-11-10T16:47:00Z"
title: "Finetune digiKam by Tweaking Its Settings"
author: "Dmitri Popov"
description: "While you can start using digiKam without tweaking its settings, you might want to spend a few minutes modifying the application’s default configuration to make"
category: "news"
aliases: "/node/548"

---

<p>While you can start using digiKam without tweaking its settings, you might want to spend a few minutes modifying the application’s default configuration to make it work your way. digiKam features dozens of settings, and which ones you want to adjust is up to you. Here are a few examples to get you started. <a href="http://blog.worldlabel.com/2010/finetune-digikam-by-tweaking-its-settings.html">Continue to read</a></p>

<div class="legacy-comments">

</div>