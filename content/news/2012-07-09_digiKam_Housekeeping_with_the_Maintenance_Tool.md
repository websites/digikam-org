---
date: "2012-07-09T08:14:00Z"
title: "digiKam Housekeeping with the Maintenance Tool"
author: "Dmitri Popov"
description: "Starting with version 2.6, digiKam features the Tools ? Maintenance menu which gives you access to tools designed to perform a variety of housekeeping tasks:"
category: "news"
aliases: "/node/660"

---

<p>Starting with version 2.6, digiKam features the <strong>Tools ? Maintenance&nbsp;</strong>menu which gives you access to tools designed to&nbsp;perform&nbsp;a variety of housekeeping tasks: from scanning for new photos to running a face recognition action. Here is a brief overview of the available tools.</p>
<p><a href="https://scribblesandsnaps.files.wordpress.com/2012/06/digikam_maintenance.png"><img class="size-medium wp-image-2670" title="digikam_maintenance" src="https://scribblesandsnaps.files.wordpress.com/2012/06/digikam_maintenance.png?w=500" alt="" width="500" height="374"></a></p>
<p><a href="http://scribblesandsnaps.wordpress.com/2012/07/09/digikam-housekeeping-with-the-maintenance-tool/">Continue to read</a></p>

<div class="legacy-comments">

</div>