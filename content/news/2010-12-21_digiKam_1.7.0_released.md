---
date: "2010-12-21T09:12:00Z"
title: "digiKam 1.7.0 released..."
author: "digiKam"
description: "Dear all digiKam fans and users! Just at Christmas time, digiKam team is proud to announce digiKam 1.7.0 release! digiKam tarball can be downloaded from"
category: "news"
aliases: "/node/557"

---

<p>Dear all digiKam fans and users!</p>

<p>Just at Christmas time, digiKam team is proud to announce digiKam 1.7.0 release!</p>

<a href="http://www.flickr.com/photos/digikam/5204111502/" title="digiKam-lensfun by digiKam team, on Flickr"><img src="http://farm5.static.flickr.com/4113/5204111502_453a974ede_z.jpg" width="640" height="256" alt="digiKam-lensfun"></a>

<p>digiKam tarball can be downloaded from SourceForge <a href="http://sourceforge.net/projects/digikam/files">at this url</a></p>

<p>See below the list of bugs-fixes coming with this release.</p>

<p>Enjoy digiKam and Happy new year.</p>

<h5>BUGFIXES FROM KDE BUGZILLA:</h5><br>

001 ==&gt; 180224 : Memory leakage in image editor?<br>
002 ==&gt; 257676 : Crash when I renane files.<br>
003 ==&gt; 239862 : digiKam crashes when trying to edit a picture.<br>
004 ==&gt; 224454 : Don't show raw files with metadata when searching or filtering.<br>
005 ==&gt; 214837 : Crash while saving picture in pic editor.<br>
006 ==&gt; 256951 : Showfoto crashes on color correction.<br>
007 ==&gt; 217069 : Undo/Redo are greyed out in image editor under MacOSX.<br>
008 ==&gt; 257769 : Crash on Enhance-&gt;Lens-&gt;Auto-Correction.<br>
009 ==&gt; 257784 : Lens auto correction does not specify unit for "distance to the object".<br>
010 ==&gt; 256050 : digiKam crashed after modifying tag name.<br>
011 ==&gt; 250134 : digiKam crashed during search for duplicates.<br>
012 ==&gt; 243988 : digiKam crashes trying to save plugin changes.<br>
013 ==&gt; 257744 : digiKam crash on importing large collection.<br>
014 ==&gt; 242439 : Crash when going to next picture in picture editor.<br>
015 ==&gt; 235678 : No startup dialog but error dialogs(s).<br>
016 ==&gt; 257898 : Allow displaying folders recursively.<br>
017 ==&gt; 257897 : Amount of duplicate images list is sorted the wrong way round.<br>
018 ==&gt; 234534 : digiKam not synchronizing legacy tags with new nepomuk database.<br>
019 ==&gt; 237642 : digiKam crashes while renaming several files.<br>
020 ==&gt; 258308 : Crash when starting Free Rotation in Image Editor.<br>
021 ==&gt; 248882 : digiKam crashes when renaming using date/time info.<br>
022 ==&gt; 217555 : "Cannot display preview" error when browsing.<br>
023 ==&gt; 258600 : "transupp.cpp" fails to compile on emerged KDE.<br>
024 ==&gt; 233572 : Showfoto crashed on change hue value.<br>
025 ==&gt; 258828 : Crash when I use auto-colors correction tool from image editor.<br>
026 ==&gt; 195561 : Implement a selective saturation filter [patch].<br>
027 ==&gt; 258931 : Linking broken when using -Wl, --no-copy-dt-needed-entries.<br>
028 ==&gt; 258300 : digiKam won't build.<br>
029 ==&gt; 256023 : Cannot work with Sigma-RAW-files (X3F).<br>
030 ==&gt; 257904 : digiKam not reading metadata from foreign images.<br>
031 ==&gt; 256897 : Crash upon unmounting of USB drive.<br>
032 ==&gt; 259257 : Too small font size.<br>
033 ==&gt; 210353 : digiKam duplicates icons for TIFF files.<br>
034 ==&gt; 260299 : X Axis Color Graduation in Curves Window mirrored.<br>
035 ==&gt; 242021 : digiKam crashes during file save.<br>
<div class="legacy-comments">

  <a id="comment-19676"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/557#comment-19676" class="active">What about face recognition</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Tue, 2010-12-21 10:47.</div>
    <div class="content">
     <p>What about face recognition stuff?</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19677"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/557#comment-19677" class="active">In 2.0</a></h3>    <div class="submitted">Submitted by SeaJey (not verified) on Tue, 2010-12-21 10:53.</div>
    <div class="content">
     <p>In 2.0</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-19678"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/557#comment-19678" class="active">Lots of Bug Fixes. Yay!</a></h3>    <div class="submitted">Submitted by Jeff (not verified) on Tue, 2010-12-21 12:46.</div>
    <div class="content">
     <p>It's nice to see that these recent releases are very focussed on fixing<br>
bugs and making DigiKam more stable. New features are nice, but making<br>
existing features work really well is just as nice. Keep up the good<br>
work. I appreciate it.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19683"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/557#comment-19683" class="active">Hear, hear!</a></h3>    <div class="submitted">Submitted by DRB (not verified) on Wed, 2010-12-22 05:35.</div>
    <div class="content">
     <p>I fully agree...although face recognition would help me convert a few Google Picasa users : P</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19687"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/557#comment-19687" class="active">Face Recognition</a></h3>    <div class="submitted">Submitted by Jeff (not verified) on Wed, 2010-12-22 17:33.</div>
    <div class="content">
     <p>Is there anyone out there who knows what we can reasonably<br>
expect from the planned face recognition support? How accurate<br>
is it likely to be? Will it take just as long to go through the<br>
photos and correct the missing or incorrect "Person" tags after<br>
automatic face recognition as it would to do the whole thing<br>
manually? Will automatic recognition take longer to run than<br>
doing it by hand?</p>
<p>I don't really have any issues with doing it manually, but that<br>
might be because I want to tag all the photos anyway. Face<br>
recognition might be of more use for those users who do not<br>
intend to tag all the photos and would just like to find "photos<br>
with faces like this one" from time-to-time.</p>
<p>(Does anyone else think that the CAPTCHA should not ask for<br>
letters like w, c, v, s, p, o, x, u, and others that look the same<br>
in upper-case and lower-case? It's painful!)</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19688"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/557#comment-19688" class="active">look release plan...</a></h3>    <div class="submitted">Submitted by digiKam on Wed, 2010-12-22 19:08.</div>
    <div class="content">
     <p>It's planed to 2.0.0. Look there :</p>
<p>http://www.digikam.org/about/releaseplan</p>
<p>digiKam</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19693"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/557#comment-19693" class="active">Face Recognition</a></h3>    <div class="submitted">Submitted by Jeff (not verified) on Thu, 2010-12-23 12:40.</div>
    <div class="content">
     <p>Hi Gilles,</p>
<p>Thanks, but I've seen that before:</p>
<p><cite>digiKam 2.x Release Plan - including GoSC-2010 works<br>
(as XMP Sidecar support, Face Recognition, and Versioning)<br>
- from git repository</cite></p>
<p>However, it doesn't answer any of my questions. I was<br>
wondering if anyone knew <em>what it would be like to use</em>,<br>
not when it would be available.</p>
<p>J.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19712"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/557#comment-19712" class="active">Anyway the first beta was</a></h3>    <div class="submitted">Submitted by <a href="http://www.goffi.org" rel="nofollow">Goffi</a> (not verified) on Mon, 2010-12-27 15:00.</div>
    <div class="content">
     <p>Anyway the first beta was planed on 26, so it should not be long before you can try by yourself.</p>
<p>BTW, thanks to all the team for this amazing soft, it's so precious for somebody disorganised like me :)</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div></div><a id="comment-19721"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/557#comment-19721" class="active">Picasa</a></h3>    <div class="submitted">Submitted by <a href="http://suratl.tumblr.com" rel="nofollow">Surat</a> (not verified) on Mon, 2011-01-03 11:46.</div>
    <div class="content">
     <p>I would love to switch from Picasa, all I need is a native Mac version.</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div><a id="comment-19679"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/557#comment-19679" class="active">Great pressie :)</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Tue, 2010-12-21 15:13.</div>
    <div class="content">
     <p>A great big thank you to all working on DigiKam.</p>
<p>Merry Xmas and wishing you all a great new year.</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-19680"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/557#comment-19680" class="active">Sexy!</a></h3>    <div class="submitted">Submitted by <a href="http://krita.org" rel="nofollow">Bugsbane</a> (not verified) on Wed, 2010-12-22 03:17.</div>
    <div class="content">
     <p>Thanks for getting down and doing the most important yet unfun side f things, eliminating those bugs and getting Digikam (even more) rock solid, stable.</p>
<p>Just a quick question: Is there any interest in using all the new QML stuff t get a more dynamic, animated interface into Digikam. When I tried Lightroom, I was struck by how nice it just "felt" with images moving around and zooming in when I selected one.</p>
<p>It would also be awesome if settings allowed an image being viewed to take up more of the screen (such as in Gwenview). Having menu's and bars on all four sides makes it a bit cramped.</p>
<p>Enough of the grousing / wishlist items though. Thanks for *already* making Digikam the sexy and massively featured image management app it already is. Your work is appreciated! :D</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-19692"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/557#comment-19692" class="active">Congratulations!</a></h3>    <div class="submitted">Submitted by <a href="http://www.damonlynch.net" rel="nofollow">Damon Lynch</a> (not verified) on Thu, 2010-12-23 04:35.</div>
    <div class="content">
     <p>Many congratulations to your team for all your great work to improve digital photography on Linux!</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-19695"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/557#comment-19695" class="active">digiKam start dialog says to</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Fri, 2010-12-24 03:17.</div>
    <div class="content">
     <p>digiKam start dialog says to not use nfs for the database because of dataloss. my home and all pictures are mounted via nfs so i aborted the setup. nfs is widely used. why is it a problem?</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19701"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/557#comment-19701" class="active">Sqlite database</a></h3>    <div class="submitted">Submitted by digiKam on Sat, 2010-12-25 08:35.</div>
    <div class="content">
     <p>Look on Sqlite database website in FAQ. There is a problem with DB transactions. Use mysql instead...</p>
<p>digiKam</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-19710"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/557#comment-19710" class="active">Entering Image menu cause GDM crash</a></h3>    <div class="submitted">Submitted by Martin (not verified) on Mon, 2010-12-27 12:00.</div>
    <div class="content">
     <p>Sorry for writing it here, but I can't find the solution anywhere else.<br>
Every time I enter the Image menu, gdm crash and I am log out.<br>
Maybe someone can give me a tip when to look for a solution.<br>
I was searching in many forums and bug tracking systems.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19711"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/557#comment-19711" class="active">is possible to install</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Mon, 2010-12-27 14:29.</div>
    <div class="content">
     <p>is possible to install digikam 1.7 on Ubuntu 10.04 LTS ?</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19736"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/557#comment-19736" class="active">Yes, it is possible to</a></h3>    <div class="submitted">Submitted by <a href="http://emcheio.com/11" rel="nofollow">João C.</a> (not verified) on Wed, 2011-01-05 18:45.</div>
    <div class="content">
     <p>Yes, it is possible to install digiKam 1.7.0 under Ubuntu 10.04.</p>
<p>In a few steps:<br>
1) apt-get build-dep digikam kipi-plugins<br>
2) checkout libkexiv2 (apply patch!) and libkdcraw from svn repository (links on this site)<br>
3) download digiKam 1.7.0 sources<br>
4) compile+install libkdcraw, libkexiv2 and digiKam.</p>
<p>Hope I could help you,<br>
cheers.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19769"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/557#comment-19769" class="active">tanks, i try !!!!!</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Fri, 2011-01-14 15:14.</div>
    <div class="content">
     <p>tanks, i try !!!!!</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div></div><a id="comment-19713"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/557#comment-19713" class="active">digikam open one time  wont open again in  mint </a></h3>    <div class="submitted">Submitted by angie (not verified) on Wed, 2010-12-29 03:13.</div>
    <div class="content">
     <p>got  digikam to down   fine   but only opened one time after that nothing,<br>
 uninstalled it then redid it got to run for  2 day  now nothing again</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19737"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/557#comment-19737" class="active">Check if digikam process is</a></h3>    <div class="submitted">Submitted by <a href="http://emcheio.com/11" rel="nofollow">João C.</a> (not verified) on Wed, 2011-01-05 18:47.</div>
    <div class="content">
     <p>Check if digikam process is running in background:<br>
ps -ef | grep digikam</p>
<p>If it's using significant amount of CPU, probably it is refreshing your albums...</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-19773"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/557#comment-19773" class="active">debian</a></h3>    <div class="submitted">Submitted by <a href="http://xraynaudphotos.free.fr" rel="nofollow">xavier</a> (not verified) on Sat, 2011-01-22 13:42.</div>
    <div class="content">
     <p>I know I'm a bit late but you can find digikam 1.7 and kipi-plugins 1.7 for debian i386 at this address: http://xraynaudphotos.free.fr/debian.php (amd64 is already in debian/experimental)</p>
         </div>
    <div class="links">» </div>
  </div>

</div>
