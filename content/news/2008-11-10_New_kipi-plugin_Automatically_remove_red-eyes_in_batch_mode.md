---
date: "2008-11-10T11:31:00Z"
title: "New kipi-plugin: Automatically remove red-eyes (in batch mode)"
author: "Andi Clemens"
description: "Some years ago, we had an addition to our family and I took a lot of photos in that time. During the months I had"
category: "news"
aliases: "/node/384"

---

<p>Some years ago, we had an addition to our family and I took a lot of photos in that time. During the months I had taken nearly 2000 images of the newborn, but most of them had red-eyes on it.<br>
This happens quite a lot with little children since their pupils don't close that fast and the light of the camera flash is reflected by the eye.</p>
<p><a href="http://www.flickr.com/photos/26732399@N05/3004014710/" title="new batch red eyes removal tool von andiclemens bei Flickr"><img src="http://farm4.static.flickr.com/3293/3004014710_0fe2c73b5e.jpg" width="500" height="302" alt="new batch red eyes removal tool"></a></p>
<p>It always annoyed me to remove the red-eyes by hand, although most image software can do this with a single click in the defective image region. One day I had to correct hundreds of images to<br>
sent them to a printing service, and that was the day I decided that I need something that can remove red-eyes all by itself.</p>
<p>I found the tools I need in the <a href="http://sourceforge.net/projects/opencvlibrary">OpenCV image library</a> and trained an eye classifier that works quite well in most situations. I used a predefined classifier before, but it was only trained to<br>
track eyes from a webcamera output, which didn't work that reliable. With the trained classifier, I'm able to find eyes in a photograph and create a mask from those regions that will be used in the final correction process. For the mask creation I use the additional <a href="http://opencvlibrary.sourceforge.net/cvBlobsLib">Blob Extraction Library</a> for OpenCV.</p>
<p>The plugin works quite well with high-res images that are not too blurry, so portraits like the one in the screenshot above are good candidates for it.</p>
<p>To use it select the images you want to be corrected and start the plugin. A good practice is to add only images that contain red-eyes, you can for sure add a whole folder and see what will happen, but then most of the time the false-positive rate is too high. The plugin has two modes, a testrun to test the settings for the imageset and the final correction mode, where the images are corrected and saved.</p>
<p>The plugin just made its way into KIPI SVN and will be released with the next version.</p>

<div class="legacy-comments">

  <a id="comment-17953"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/384#comment-17953" class="active">This is a great feature. In</a></h3>    <div class="submitted">Submitted by scroogie (not verified) on Mon, 2008-11-10 14:37.</div>
    <div class="content">
     <p>This is a great feature. In the example image it looks as if there was an inaccuracy of 1 px though (like a grey afterglow of the red eye). Or am I imagining things?<br>
The other thing is, couldn't you improve the false positives by checking for the color of the retina? (and/or checking the shape?)</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-17957"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/384#comment-17957" class="active">I don't see a "afterglow"...</a></h3>    <div class="submitted">Submitted by Andi Clemens on Mon, 2008-11-10 15:06.</div>
    <div class="content">
     <p>I don't see a "afterglow"... what do you mean by that?</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-17959"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/384#comment-17959" class="active">If you zoom in on the example</a></h3>    <div class="submitted">Submitted by scroogie (not verified) on Mon, 2008-11-10 17:30.</div>
    <div class="content">
     <p>If you zoom in on the example picture there is a grey circle on the iris which certainly isn't a reflection?</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-17961"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/384#comment-17961" class="active">Still don't know what you see</a></h3>    <div class="submitted">Submitted by Andi Clemens on Mon, 2008-11-10 18:24.</div>
    <div class="content">
     <p>Still don't know what you see here. If you mean the gray "border", this can also be found in the original image. I don't overlay any color nor do I paint something here, the plugin just removes the red by reducing the red channel and mixing it with the green channel...</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-17962"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/384#comment-17962" class="active">Sure, that is what I'm</a></h3>    <div class="submitted">Submitted by scroogie (not verified) on Mon, 2008-11-10 19:04.</div>
    <div class="content">
     <p>Sure, that is what I'm talking about. In the original, the border is inside the red eye, so I guess it's either from the lense or because of the curvature of the retina, and is actually part of the red eye effect.</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div></div></div><a id="comment-17955"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/384#comment-17955" class="active">Well the plugin isn't</a></h3>    <div class="submitted">Submitted by Andi Clemens on Mon, 2008-11-10 14:55.</div>
    <div class="content">
     <p>Well the plugin isn't finished, like every piece of software :-)<br>
This is just a first try and by far not perfect, so development goes on. The next steps will be to make the correction faster (if possible) and more reliable. It works quite well though, at least for my image collection :-)<br>
This plugin started out as a fun project and I never thought that it will become serious some day. Now that it is I will check for additional ways to ensure the correct eye detection.<br>
The images are not overwritten with the default settings, but saved in a subfolder. You can also add a custom prefix to the images or, if you are brave, overwrite them.<br>
I tested this plugin with a folder of 120 images, 116 were correctly modified and 4 images where not detected. This is quite good I think.</p>
<p>Andi</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-17965"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/384#comment-17965" class="active">Thank you!</a></h3>    <div class="submitted">Submitted by <a href="http://kwilliam.blogspot.com" rel="nofollow">kwilliam</a> (not verified) on Tue, 2008-11-11 05:22.</div>
    <div class="content">
     <p>Dude, that's brilliant, thinking to use the OpenCV libraries. I can't wait to try it in the next version of KIPI plugins. Even if it is unpolished, I think it's awesome that you're willing to share it with the rest of Digikam users!</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-17984"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/384#comment-17984" class="active">How about a way to quickly do comparisons afterwards?</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Mon, 2008-11-17 16:02.</div>
    <div class="content">
     <p>This is a great idea, and just another example of the fast pace of innovation that can be observed in open source software.</p>
<p>A thought on comparisons:<br>
 I get that originals are preserved and the modified images are saved in a subfolder. But how do you speed up the inevitable comparisons you have to do later?  Ie. how do you enable the user to quickly compare original with modified images and verify that the red eye removal was correct?</p>
<p>I'm thinking that the best way to do that is by being able to browse through the images and see the original and modified side by side.</p>
<p>Alternatively, if the modified images were saved in the same folder, but with a modified filename (ie. "-edited.jpg") it would at least be easy to browse through all images quickly, and flip back and forth between original and modified versions.</p>
<p>With the modified versions saved to a separate subfolder, you'd either have to view all originals or all modifieds...</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-17985"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/384#comment-17985" class="active">You can add a prefix to the</a></h3>    <div class="submitted">Submitted by Andi Clemens on Mon, 2008-11-17 16:21.</div>
    <div class="content">
     <p>You can add a prefix to the modified files if you like, so that you can save the images in the same folder.<br>
There are three types of correction:</p>
<p>1. Save in a subfolder (choose you own name for it)<br>
2. Save with a prefix added (choose your own name for it, filename becomes image01_XXX.jpg for example)<br>
3. Overwrite images (should never be used :-))</p>
<p>You can watch images side by side with digiKam lightTable. If you use another application like gwenview, it will be best to use the second correction mode.</p>
<p>I tested the classifier on another image set and I'm getting disappointed by it :-( I think I need to train another one that works better.<br>
Since the final KIPIplugins release will be in February I guess (KDE 4.2 release), I have some time left.</p>
<p>Andi</p>
         </div>
    <div class="links">» </div>
  </div>
</div>
</div>