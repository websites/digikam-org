---
date: "2012-06-25T08:17:00Z"
title: "Export Photos to Wikimedia Commons with digiKam"
author: "Dmitri Popov"
description: "Sharing is caring, and there is probably no better way to share your photographic masterpieces with the world than adding them to the Wikimedia Commons"
category: "news"
aliases: "/node/658"

---

<p>Sharing is caring, and there is probably no better way to share your photographic masterpieces with the world than adding them to the <a title="Wikimedia Commons" href="http://commons.wikimedia.org/">Wikimedia Commons</a> pool. While the project's website features its own tool for uploading photos, digiKam's Wikimedia Export Kipi plugin can come in rather handy when you need to export multiple photos in one fell swoop without leaving the convenience of your favorite photo management application.</p>
<p><a href="https://scribblesandsnaps.files.wordpress.com/2012/06/digikam_wikimediacommons.png"><img class="size-medium wp-image-2635" title="digikam_wikimediacommons" src="https://scribblesandsnaps.files.wordpress.com/2012/06/digikam_wikimediacommons.png?w=500" alt="Wikimedia Commons Kipi plugin" width="500" height="385"></a></p>
<p><a href="http://scribblesandsnaps.wordpress.com/2012/06/25/export-photos-to-wikimedia-commons-with-digikam/">Continue to read</a></p>

<div class="legacy-comments">

</div>