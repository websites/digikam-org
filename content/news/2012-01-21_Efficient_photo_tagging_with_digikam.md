---
date: "2012-01-21T10:41:00Z"
title: "Efficient photo tagging with digikam"
author: "gerhard"
description: "By Peter Albrecht. In the two years that I'm using digikam by now, I always searched for a way to tag the persons on my"
category: "news"
aliases: "/node/641"

---

<p>By Peter Albrecht.</p>
<p>In the two years that I'm using digikam by now, I always searched for a way to tag the persons on my<br>
photos in a fast and easy way. <strong>Now I discovered a fast way of tagging with the keyboard only.</strong></p>
<p>== A) The batch-click way:</p>
<p>Having read the digikam handbook back in 2009 and using the tools, I saw evolve in the GUI, I ended<br>
up with the following way:</p>
<p> 1 switch to "thumbnails" view (you have to be able to select more than one image)<br>
 2 shift the preview size to the maximum of 256 pixel (you want to be able to recognize the people<br>
   on the photos)<br>
 3 switch the right side-view to "Caption/Tags" and select the tab "Tags"<br>
 4 select picture number one and look at the leftmost person on this picture<br>
 5 hold CTRL down and select all other pictures, showing this person<br>
 6 search for the corresponding tag in the right side-view, select it and apply (you can enable<br>
   "do not confirm when appying changes from right sidebar" in digikam settings "Miscellaneous", so one click saved)<br>
 7 if there are more persons in picture one, then select this picture again go to step 5, while<br>
   looking for the next leftmost person.<br>
 8 if you have all the persons in picture one, select picture two and look again for the<br>
   leftmost person, you have not tagged yet, and go to step 5. (toggling the button "tags already assigned"<br>
   in the lower right corner of the "Tags" tab, is very useful here)</p>
<p>This way you go through all your pictures. In the beginning you have to tag a lot of people, but coming<br>
to the last pictures, most people will already be tagged and you can fast jump to the next picture.</p>
<p>Usability:<br>
This process is good, if you have a high number of photos proportional to the amount of different<br>
persons. If you have lots of different tags in your database, it's worth to think about substituting<br>
step 6 with the "keyboard selection way", I'm going to explain next.</p>
<p>== B) The keyboard way:</p>
<p>A few days ago I found this bug-report at BKO: https://bugs.kde.org/show_bug.cgi?id=114465<br>
According to it's number of 127 comment posts, it is the most impressive bug report, I've ever seen.</p>
<p>    -- epic-tag on --<br>
This bug report holds the secret of a tagging technique which gives you full keyboard-only tagging power.<br>
    -- epic-tag off --</p>
<p>And that's the way you go:<br>
  1 select an album<br>
  2 switch to "view image"-view (the bigger the picture, the better you can recognize the people,<br>
    you want to tag)<br>
  3 switch the right sideview to "Caption/Tags" and select tab "Tags"<br>
  4 click in the uppermost input box, which shows the text: "Enter new tag here..."<br>
  5 look for the leftmost person, you have not tagged yet<br>
  6 start typing a few letters of the desired tagname (You have to start with the first letters in your<br>
    tag name. There is no "mid word matching".)<br>
    -&gt; you will see a list of possible tags appear that shrinks with every letter you add to the input box<br>
  7 if the list is reduced enough, use TAB to select the desired tag from this list<br>
  8 hit ENTER to assign this tag to your picture<br>
  9 if there are more persons, you want to tag, go to step 5 again<br>
 10 if all persons on this image have been tagged, hit PAGE_DOWN to go to the next image and<br>
    resume with step 5</p>
<p>Keyboard focus will always stay in the tag input box. So you can keep your hands on the keyboard all the<br>
time and tag very fast. As mentioned above, you can enable  "do not confirm when appying changes from right sidebar" in digikam settings "Miscellaneous" to get rid of the "Save Changes?"-Dialog.</p>
<p>Usability:<br>
This process is most useful, if you have a high number of different persons proportional to the amount<br>
photos. And especially steps 6 to 8 are very handy, if you have lot of tags, you don't want to scroll<br>
through by hand.</p>
<p>== C) Keyboard shortcuts for specific tags</p>
<p>If you have certain tags, you use very often, you can assign a keyboard shortcut to it.<br>
I did this with "places/myHome", which is assigned to CTRL+SHIFT+H.</p>
<p>But there is already a nice tutorial at:<br>
http://scribblesandsnaps.wordpress.com/2011/05/25/assign-keyboard-shortcuts-to-tags-in-digikam/<br>
So I'll keep this section short. ;)</p>
<p>== Some stats about my digikam usage:</p>
<p>Some numbers:<br>
206 different tags (in multilevel-hierarchical order: places, people, other)<br>
24,580 tag assignments (assigned in the last 2 years)<br>
25,687 pictures in digikam database (collected in 10 years)</p>
<p>I'm currently using digikam 2.5.0, but the features mentioned above have been introduced in<br>
earlier version. If you are curious about, just have a look at the corresponding NEWS-file<br>
in the git repository:<br>
https://projects.kde.org/projects/extragear/graphics/digikam/repository/revisions/master/show/project</p>
<p>E.g.: https://projects.kde.org/projects/extragear/graphics/digikam/repository/revisions/master/entry/project/NEWS.2.0.0</p>
<p>== Final words</p>
<p>Since method B is new to me, I can't tell about long term experience, but I guess the most efficient<br>
way of tagging will be a mixture of the three methods mentioned above, depending on the situation. ;)</p>
<p>Since I was very surprised, reading about this powerful keyboard tagging feature by incident in a bug<br>
report, I decided to write this article about digikam tagging. I hope, I can help spreading the knowledge<br>
this way and help other users to have even more fun with this great product: digikam!</p>
<p>Thanks to all digikam contributors! You are doing a great job!</p>
<p>Peter Albrecht, 17.01.2012</p>

<div class="legacy-comments">

  <a id="comment-20186"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/641#comment-20186" class="active">Digikam tutorials on UserBase</a></h3>    <div class="submitted">Submitted by Anne Wilson (not verified) on Sat, 2012-01-21 14:55.</div>
    <div class="content">
     <p>Hello.  You may know that we have many tutorials for digiKam on UserBase.  I'd like to add today's Planet entry to them, if that is acceptable.  Most of the tutorials linked from http://userbase.kde.org/Digikam/Tutorials are transcribed from blogs by Dmitri Popov and Mohammed Malik, and we always give attribution.  Since I don't have contact for Peter Albrecht would you mind asking him if he agrees to this?</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20187"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/641#comment-20187" class="active">Digikam tutorials on UserBase</a></h3>    <div class="submitted">Submitted by Peter Albrecht (not verified) on Sat, 2012-01-21 21:55.</div>
    <div class="content">
     <p>Hello Anne Wilson,</p>
<p>you met me today at userbase.kde.org. ;)<br>
I'm honored to be picked for the tutorials section! So I happily<br>
agree to add my article there.</p>
<p>Since I'm a great Wiki fan, I would like to add the article myself.<br>
I plan to do this at monday.</p>
<p>Kind regards,<br>
    Peter Albrecht</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20190"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/641#comment-20190" class="active">Article transcribed to tutorial at Wiki: userbase.kde.org</a></h3>    <div class="submitted">Submitted by Peter Albrecht (not verified) on Sat, 2012-01-28 22:17.</div>
    <div class="content">
     <p>This article has been transcribed to a <a href="http://userbase.kde.org/Digikam/TaggingEfficient">tutorial at userbase.kde.org</a>. (Enriched with a screenshot and markup)</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div><a id="comment-20188"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/641#comment-20188" class="active">This will come in handy</a></h3>    <div class="submitted">Submitted by <a href="http://ekot.dk" rel="nofollow">Toke Eskildsen</a> (not verified) on Sun, 2012-01-22 13:52.</div>
    <div class="content">
     <p>With 100K+ pictures, 500+ tags and an obsessive need to tag not only people but also objects and situations, I've struggled a long time with method 1. It fails when the tag collection is non-trivial due to the time it takes to scroll and search the tags for the right one. It has always been a letdown for me that _pure_ keyboard support for tagging was not really possible with DigiKam, but I guess it works okay for a limited number of tags if you can accept switching back and forth between mouse and keyboard.</p>
<p>I am very happy to hear about the bug in method 2 and I'll start tagging again because of it (it became too much work before so I haven't done so for a year or two). Further along, features such as searching for tags with inline matching, multiple image selection with the keyboard and tag search activated by just starting typing would really make tagging shine in DigiKam.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20191"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/641#comment-20191" class="active">Great to see, I could help</a></h3>    <div class="submitted">Submitted by Peter Albrecht (not verified) on Sat, 2012-01-28 22:19.</div>
    <div class="content">
     <p>Great to see, I could help someone!</p>
         </div>
    <div class="links">» </div>
  </div>
</div>
</div>