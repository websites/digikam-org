---
date: "2007-05-05T15:22:00Z"
title: "digiKam 0.9.2 release cycle starts"
author: "gerhard"
description: "Dear all digiKam fans! With the 0.9.2-beta1 release we start into a new release cycle that is planned to be culminating in a 0.9.2 final"
category: "news"
aliases: "/node/220"

---

<p>Dear all digiKam fans!<br>
With the <a href="http://digikam3rdparty.free.fr/">0.9.2-beta1</a> release we start into a new release cycle that is planned to be culminating in a 0.9.2 final release mid June. This beta1 version is already very stable, don't hesitate to use it for production. Beta1 and beta2 are rather used to improve usability than debugging.</p>
<p>In order to make digiKam 0.9.2 compile you need to compile and install libkexiv2 and libkdcraw first. The english documentation can be downloaded as well.</p>
<p>The library tarballs can be downloaded from <a href="https://sourceforge.net/projects/kipi">(1) SourceForge</a>.<br>
The digiKam tarball can be downloaded from <a href="http://sourceforge.net/projects/digikam/">(2) SourceForge</a> as well.</p>
<p><em>So what's new on digiKam planet?</em><br>
--------------------------------<br>
General :<br>
- DigikamImagePlugins have been merged into digiKam. It is easier to release one package for all and nobody will search for promised features that are not installed anymore. Image plugins translations are hosted to digikam.po file instead a .po file for each tool.<br>
- New depency to libkdcraw shared library used to decode RAW file. This library is shared between digiKam and kipi-plugins. The internal dcraw version used is 8.60. digiKam now supports all recent digital camera RAW files released at PMA 2007.<br>
- Make icon size in sidebars configurable to allow more entries to be presented.<br>
- Reduce icon size of album view and make them configurable using a slider in status bar.<br>
- Removing direct Exiv2 library depency. libkexiv2 interface is used everywhere instead.<br>
- A new plugin 'Vivid', similar to Velvia type filters is part of Color Effect plugin.</p>
<p>Album GUI :<br>
- Add Zoom/Scrooling functions with preview mode. Speed incease.</p>
<p>Image Editor :<br>
- Usability improvement : a new pan tool is available on the right bottom corner of the status bar to navigate on large pictures.<br>
- Blowup and Resize tools have been merged.<br>
- Unsharp Mask, Refocus, and Sharpen tools have been merged to a new Sharpness Editor.<br>
- Reorganize menu structure<br>
- persistant selection in all zoom mode.<br>
- Add new option to fit on current selection.<br>
- Red Eyes Correction tool have been completly re-written. There is a preview and the capability to taint the eye pupil with a customized color. The new eye pupil can be blurred to smooth the result.<br>
- Solarize plugin is now a "Color Effects" pack including Solarize, Velvia (new plugin), Neon, and Edge effects.<br>
- Black &amp; White converter now support a lots of B&amp;W analog camera film types (Agfa, Ilford, Kodak). A new 'strength' setting can simulate the amount of Lens filters effect.<br>
- Update internal CImg library to 1.1.9. The Greycstoration algorithm used by Restoration, Inpainting and Blowup plugins is faster and optimized.</p>
<p>Showfoto :<br>
- The thumbbar is now resizable. The thumbnails contents can be redimensionned in live.<br>
- The thumbbar items can show a full configurable tool tip like digiKam album icon tems tool tip.</p>

<div class="legacy-comments">

</div>