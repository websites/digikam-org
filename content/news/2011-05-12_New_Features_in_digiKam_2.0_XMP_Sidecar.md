---
date: "2011-05-12T09:41:00Z"
title: "New Features in digiKam 2.0: XMP Sidecar"
author: "Dmitri Popov"
description: "In addition to writing tags, ratings, labels, descriptions, etc. directly into the photos, version 2.0 of digiKam can save metadata in a separate .xmp file."
category: "news"
aliases: "/node/601"

---

<p>In addition to writing tags, ratings, labels, descriptions, etc. directly into the photos, version 2.0 of digiKam can save metadata in a separate .xmp file. This approach has several advantages. First off, it speeds things up, as writing data to a text file is faster than embedding metadata into photos. This also allows you to store metadata for RAW files, since writing metadata directly to RAW files can sometimes be problematic. <a href="http://scribblesandsnaps.wordpress.com/2011/05/12/new-features-in-digikam-2-0-xmp-sidecar/">Continue to read</a></p>

<div class="legacy-comments">

</div>