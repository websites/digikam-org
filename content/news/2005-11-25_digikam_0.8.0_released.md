---
date: "2005-11-25T22:55:00Z"
title: "digikam 0.8.0 released"
author: "site-admin"
description: "The digiKam team is pleased to bring to you the next generation of the user-friendly digital photo management software and its image plugins. The primary"
category: "news"
aliases: "/node/44"

---

The digiKam team is pleased to bring to you the next generation of the user-friendly digital photo management software and its image plugins. The primary focus for this release is new ways of photo organization and searching. We hope it will be as much fun for you to use it as we had developing it.
<p></p>
The noteworthy features in this release:
<p></p>
<ul>
<li> Albums and Tags have been separated out into different views and these views are collapsible.</li><p></p>
<li>PHOTO DATES AND COMMENTS: Photos are now scanned for their metadata which is then stored in the database. The EXIF timestamp (if available) is used for sorting and searching</li><p></p>
<li>DATE BASED VIRTUAL FOLDERS: Each corresponding to the month in which the pictures were taken. Its also possible to filter this view to show photos from single or multiple day(s) and week(s)</li><p></p>
<li>SEARCHING: Two interfaces have been implemented for searching on photos: </li>
    <ul>
        <li>QUICK SEARCH: a keyword based search which also allows date based lookups</li>
        <li>ADVANCED SEARCH: build rules and groups of rules to do very precise searches</li>
        <li>LIVE FEEDBACK: Both searches show instant results based on the current search criteria</li>
    </ul><p></p>
<li>SEARCH BASED VIRTUAL FOLDERS: The searches can be saved as virtual folders and its possible to edit the search criterion later.</li><p></p>
<li>TAG FILTERING: A new view has been added to the right side which allows quick drag and drop tag assignments and also filtering of photos in the current view. This view is again collapsible</li><p></p>
<li>ALBUM LOCATION INDICATOR: When photos are shown from a virtual folder (tag, date or search), the photos are grouped together based on album locations</li><p></p>
<li>MEMORY OPTIMIZATION: Thumbnails are now loaded on demand and only a fixed cache of thumbnails is kept in memory</li><p></p>
<li>NEW THEMES: Dark, Sandy and Orange Crush</li><p></p>
<li>BACKEND CHANGES: - Database upgraded to sqlite3 - New kioslaves for handling date, search, tag, album listing</li><p></p>
<li>SHOWFOTO : new slide show tool</li><p></p>
<li>IMAGE PLUGIN CHANGES </li>
    <ul>
        <li>Plugin dialogs have been revamped for better usability and easier comparison of filter effects.</li>
        <li>Plugin computations are now threaded</li>
        <li>Plugin preview is now resizable</li>
        <li>BigEndian fixes</li>
    </ul><p></p>
<li>NEW IMAGE PLUGIN FEATURES :</li>
    <ul>
        <li>Aspect Ratio Crop : new photograph composition guide tool based on Fibonacci series (golden mean)</li>
        <li>New Color AutoCorrection tool with preview (Normalize, Autolevels, Equalize, and Stretch Contrast)</li>
        <li>Free Rotation : Auto crop and antialiasing options</li>
        <li>Shear Tool : Antialiasing option</li>
    </ul><p></p>
<li>NEW IMAGE PLUGINS :</li>
    <ul>
        <li>Refocus : An advanced sharpness editor</li>
        <li>HotPixels : An hot pixels removal tool to fix bad pixels from camera CCD</li>
    </ul>
</ul>

<div class="legacy-comments">

</div>