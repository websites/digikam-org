---
date: "2014-09-16T22:43:00Z"
title: "digiKam Software Collection 4.3.0 released..."
author: "digiKam"
description: "Dear digiKam fans and users, The digiKam Team is proud to announce the release of digiKam Software Collection 4.3.0. This release includes some new features:"
category: "news"
aliases: "/node/718"

---

<a href="https://www.flickr.com/photos/digikam/15070641652"><img src="https://farm6.staticflickr.com/5581/15070641652_4ac8876f54_c.jpg" width="800" height="450"></a>

<p>Dear digiKam fans and users,</p>

<p>
The digiKam Team is proud to announce the release of digiKam Software Collection 4.3.0. This release includes some new features:
</p>

<ul>

<li>
New option to display all non geo-located images from collections.
</li>

<li>
New notification event when kio-slave cannot be started. OSX event notifier is now used to dispatch notifications under Mac.
</li>

<li>
New action to exclude items without rating with items filter (see details <a href="http://mohamedanwer.wordpress.com/2014/08/28/new-action-to-exclude-items-without-rating/">on this blog entry</a>).
</li>

</ul>

<p>
After a long bugs triage, we have worked hard also to close your reported issues.. 
A long list of the <a href="https://bugs.kde.org/buglist.cgi?f1=cf_versionfixedin&amp;o1=equals&amp;query_format=advanced&amp;bug_status=RESOLVED&amp;bug_status=NEEDSINFO&amp;bug_status=VERIFIED&amp;bug_status=CLOSED&amp;v1=4.3.0&amp;product=digikam&amp;product=digikamimageplugins&amp;product=kipiplugins&amp;product=showfoto">issues closed</a> in digiKam 4.3.0 is available through the KDE Bugtracking System.
</p>

<p>The digiKam software collection tarball can be downloaded from the <a href="http://download.kde.org/stable/digikam/digikam-4.3.0.tar.bz2.mirrorlist">KDE repository</a>.
</p>

<p>Have fun playing with your photos using this new release,</p>

<p>digiKam Team...</p>
<div class="legacy-comments">

  <a id="comment-20852"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/718#comment-20852" class="active">Detección de fotos</a></h3>    <div class="submitted">Submitted by hector (not verified) on Thu, 2014-09-18 12:47.</div>
    <div class="content">
     <p>Tiene muchos problemas. La aplicación se cierra después de iniciar la detección de rostros y tambien cuando hago clic en algunas otras herramientas (xej. edición de etiquetas).</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-20853"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/718#comment-20853" class="active">Error trying to create a tag</a></h3>    <div class="submitted">Submitted by Piter Dias (not verified) on Fri, 2014-09-19 01:10.</div>
    <div class="content">
     <p>I compiled it under Ubuntu 13.10 x64 and got the error below trying to create a tag inside an existing node in the tag tree.<br>
The database is MySQL, the same one I was using with Digikam 4.2. Should I file a bug report or it is known issue?</p>
<p>digikam(9324)/digikam (core): No DB action defined for "InsertTag" ! Implementation missing for this database type.<br>
digikam(9324)/digikam (core): Attempt to execute null action</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20859"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/718#comment-20859" class="active">I have digiKam 4.2 installed</a></h3>    <div class="submitted">Submitted by Piter Dias (not verified) on Tue, 2014-09-23 02:57.</div>
    <div class="content">
     <p>I have digiKam 4.2 installed under /usr and the 4.3 version under my home.<br>
There is a file called /usr/share/kde4/apps/digikam/database/dbconfig.xml installed due to 4.2 and not compatible with 4.3. Renaming it fixed the issue, although it would be great to really have both versions at the same time.</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-20854"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/718#comment-20854" class="active">Digikam 4.5 crashes almost immediately</a></h3>    <div class="submitted">Submitted by Jens Verwaerde (not verified) on Sun, 2014-09-21 21:34.</div>
    <div class="content">
     <p>Unfortunately, DK recently updated from 4.2 to 4.3 on my Ubuntu 14.04.<br>
And almost every session it crashes, almost immediately after having opened my photo-directories.</p>
<p>I run Ubuntu 14.04 and have about 40.000 pictures in my library.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20855"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/718#comment-20855" class="active">Digikam 4.3 crashes</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Mon, 2014-09-22 08:22.</div>
    <div class="content">
     <p>Updated from 4.2 to 4.3 on OpenSuse 12.3 it crashes. I've build a new database for pictures. It happens unexpected.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20856"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/718#comment-20856" class="active">See this report...</a></h3>    <div class="submitted">Submitted by digiKam on Mon, 2014-09-22 08:34.</div>
    <div class="content">
     <p><a href="https://bugs.kde.org/show_bug.cgi?id=339144">digiKam crashes in libkexiv2 metadata handling</a></p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20857"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/718#comment-20857" class="active">What's up with you guys? You</a></h3>    <div class="submitted">Submitted by Robin (not verified) on Mon, 2014-09-22 17:43.</div>
    <div class="content">
     <p>What's up with you guys? You make such an excellent product, peerless in the open source world. I also can't complain that there are lots of bugs in every release, because you are quick to fix them. What drives me mad though, is that you won't release the fixed version for those of us simple photographers who can't compile from source. So we are to wait till the December release of 4.4 and hope that there is no critical bug? The last usable version for me was 4.1. But I had to go back to 4.0 because that's the latest one available in the *buntu repos.</p>
<p>Sorry for the rant, I really appreciate your work. It's just a pity that you don't make the last step to actually RELEASE a usable bug-free version, which you already have.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20858"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/718#comment-20858" class="active">I have updated my packages</a></h3>    <div class="submitted">Submitted by philip5 (not verified) on Mon, 2014-09-22 22:27.</div>
    <div class="content">
     <p>I have updated my packages with the libkexiv2 fix if you use my PPA (kubuntu-backports) with digikam 4.3.0 for kubuntu 14.04 with the kubuntu teams kde 4.14.x PPA. Hopefully that solves that bug.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20862"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/718#comment-20862" class="active">I have installed Digikam from</a></h3>    <div class="submitted">Submitted by Robin (not verified) on Tue, 2014-09-23 17:47.</div>
    <div class="content">
     <p>I have installed Digikam from your PPA, but libkexiv2-11 cannot be installed because of some dependency problem, which it doesn't state.</p>
<p>Even when I "sudo apt-get -f install libkexiv2-11" it says one package will be installed but it does nothing without any error message.</p>
<p>Thank you for support.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20863"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/718#comment-20863" class="active">To use my kubuntu-backport</a></h3>    <div class="submitted">Submitted by Philipt (not verified) on Tue, 2014-09-23 18:11.</div>
    <div class="content">
     <p>To use my kubuntu-backport PPA you need also my "extra" PPA with updated packages of other software Digikam is dependent of. My kubuntu-backport PPA is also dependent of KDE 4.14.x which is provided by the Kubuntu teams backport PPA. So to sum this up you need total of 3 PPAs.</p>
<p>Philip</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20864"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/718#comment-20864" class="active">Got it, I was missing your</a></h3>    <div class="submitted">Submitted by Robin (not verified) on Tue, 2014-09-23 18:19.</div>
    <div class="content">
     <p>Got it, I was missing your "extra" PPA. Now Digikam doesn't crash anymore!</p>
<p>Thank you all guys for your work!</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div></div><a id="comment-20866"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/718#comment-20866" class="active">May I correct you</a></h3>    <div class="submitted">Submitted by <a href="http://www.londonlight.org/" rel="nofollow">DrSlony</a> (not verified) on Mon, 2014-09-29 06:33.</div>
    <div class="content">
     <p>Hi Robin<br>
I understand how frustrating it is to be stuck with old software, but you need to realize two things:<br>
1- Negative posts do no good to motivate people who code out of passion. Re-word your post in a positive way, so when a coder or package maintainer wakes up in the morning he does not get put off.<br>
2- It is not up to any project to make packages for the countless distributions out there. Find the package maintainer and message him/her, or get in touch with your distribution's package maintenance people to have them get a newer version up.<br>
Maybe worth adding point 3:<br>
3- If your distribution is notorious for lagging months or years behind (looking at you, all *buntu based distros with that forged-in-hell apt system!) then think about changing your distribution.</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div></div><a id="comment-20865"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/718#comment-20865" class="active">digikam crash</a></h3>    <div class="submitted">Submitted by <a href="http://pirekare.blogspot.com" rel="nofollow">Anonymous</a> (not verified) on Wed, 2014-09-24 13:14.</div>
    <div class="content">
     <p>I have been using Digikam for so many years. Every new version meant some forward steps until version 4.  I have two computers running Digikam, one has opensuse 12.3 and the other kubuntu 14.04 and the same happens in both. Besides other troubles, the crash problem is so annoying that I decided to move back to version 3.*. Back to good old days now!</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-20860"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/718#comment-20860" class="active">I'm intrigued by the Mac</a></h3>    <div class="submitted">Submitted by Geoff King (not verified) on Tue, 2014-09-23 16:34.</div>
    <div class="content">
     <p>I'm intrigued by the Mac screenshot and notifier bugfix.<br>
Is Digikam for Mac really ready for use or is it still rough around the edges?<br>
Is the best way to get it still on Macports, which still has version 4.0.0?<br>
Thanks.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20861"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/718#comment-20861" class="active">Mac version still in beta stage but...</a></h3>    <div class="submitted">Submitted by digiKam on Tue, 2014-09-23 16:47.</div>
    <div class="content">
     <p>... it's enough stable to be used in production. You can install mapcort version, but it's not yet updated to last version. You can follow <a href="https://www.digikam.org/download?q=download/binary/">instructions from this page</a> to compile current implementation with Macport.</p>
<p>The future goal is to provide a DMG installer for OSX, but currently, Macports rules to build DMG is broken.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20876"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/718#comment-20876" class="active">Depressing : I tried to</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Tue, 2014-10-07 18:28.</div>
    <div class="content">
     <p>Depressing : I tried to install digikam from macport and from git, no way to go further than the splash-screen... 4 days trying, cleaning, reinstalling, etc. it's enough for me, I would have liked to try the soft, not it's install.</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div><a id="comment-20867"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/718#comment-20867" class="active">Crashes</a></h3>    <div class="submitted">Submitted by IroMe (not verified) on Tue, 2014-09-30 17:25.</div>
    <div class="content">
     <p>My new Ubuntu crashes with 3.5.0 and the philip5/extra ppa. Maybe compiling myself?<br>
Damn, do not want to move to darktable because of the filmrole conzept.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20868"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/718#comment-20868" class="active">Backtrace ?</a></h3>    <div class="submitted">Submitted by digiKam on Tue, 2014-09-30 17:32.</div>
    <div class="content">
     <p>Which kind of GDB backtrace can you get when it crash ? </p>
<p>Look here for details : https://www.digikam.org/contrib</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-20869"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/718#comment-20869" class="active">If you use my PPA you</a></h3>    <div class="submitted">Submitted by philip5 (not verified) on Wed, 2014-10-01 13:37.</div>
    <div class="content">
     <p>If you use my PPA you shouldn't have digikam 3.5 any more but most likely digikam 4.3 if you updated the packages right. Which version of ubuntu and KDE do you use?</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-20870"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/718#comment-20870" class="active">Digikam 4.3 for Windows</a></h3>    <div class="submitted">Submitted by wolfgang (not verified) on Wed, 2014-10-01 19:54.</div>
    <div class="content">
     <p>Hi,</p>
<p>I am one of many Windows users and am looking forward to the new digikam<br>
version 4.3 .<br>
so my question :<br>
when is the 4.3 version downloaded as a Windows version ?</p>
<p>Wolfgang</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20873"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/718#comment-20873" class="active">Wondering the same thing. The</a></h3>    <div class="submitted">Submitted by cc (not verified) on Sat, 2014-10-04 11:15.</div>
    <div class="content">
     <p>Wondering the same thing. The original announcement has been two weeks ago, but the download-location still only contains tarballs and the 4.2.0-2 build.</p>
<p>I'd humbly like to request an installable build, too.</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-20871"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/718#comment-20871" class="active">undock</a></h3>    <div class="submitted">Submitted by Erwin Van Havermaet (not verified) on Fri, 2014-10-03 16:55.</div>
    <div class="content">
     <p>Hi, I Just discovered this fantastic program.<br>
I was trying to undock the 'map' to display that on my second screen, but I can't find any way to do that. Is it posible?<br>
Thanks to all developers who created this!</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20872"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/718#comment-20872" class="active">no</a></h3>    <div class="submitted">Submitted by digiKam on Fri, 2014-10-03 17:22.</div>
    <div class="content">
     <p>This feature is not implemented...</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-20874"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/718#comment-20874" class="active">Raw of Fujifilm X-T1</a></h3>    <div class="submitted">Submitted by <a href="http://www.stephane-brette.fr/" rel="nofollow">Sbphotographies</a> (not verified) on Mon, 2014-10-06 17:27.</div>
    <div class="content">
     <p>hello,<br>
I recently bought a Fujifilm X -T1 .<br>
I am disappointed that digikam does not read the raw of this unit. I have a friend who has an X -E1 / E2 and 2 raw are recognized .<br>
What can I do ?</p>
<p>kind regards ,</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20875"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/718#comment-20875" class="active">libraw</a></h3>    <div class="submitted">Submitted by digiKam on Mon, 2014-10-06 18:09.</div>
    <div class="content">
     <p>1/ Try to update libraw library used by digiKam to decode RAW files.</p>
<p>2/ If it's not enough, report this problem to libraw team (http://www.libraw.org).</p>
         </div>
    <div class="links">» </div>
  </div>
</div>
</div>