---
date: "2014-01-15T18:41:00Z"
title: "digiKam Software Collection 4.0.0-beta2 is out.."
author: "digiKam"
description: "Dear all digiKam fans and users! digiKam team is proud to announce the second beta release of digiKam Software Collection 4.0.0. This version currently under"
category: "news"
aliases: "/node/709"

---

<a href="http://www.flickr.com/photos/digikam/11964874854/" title="digiKam4.0.0-beta2 by digiKam team, on Flickr"><img src="http://farm8.staticflickr.com/7441/11964874854_c3214b0895_c.jpg" width="800" height="225" alt="digiKam4.0.0-beta2"></a>


<p>Dear all digiKam fans and users!</p>

<p>
digiKam team is proud to announce the second beta release of digiKam Software Collection 4.0.0.
This version currently under development, include many new feature as:
</p>

<ul>

<li>
A new tool dedicated to organize whole tag hierarchies. This new feature is relevant of <a href="http://www.google-melange.com/gsoc/proposal/review/google/gsoc2013/slavuttici/35001">GoSC-2013 project</a> from <b>Veaceslav Munteanu</b>. This project include also a back of Nepomuk support in digiKam broken since a while due to important KDE API changes. Veaceslav has also implemented multiple selection and multiple drag-n-drop capabilities on Tags Manager and Tags View from sidebars, and the support for writing face rectangles metadata in Windows Live Photo format.
</li>

<li>
A new maintenance tool dedicated to parse image quality and auto-tags items automatically using Pick Labels. This tool is relevant to another <a href="http://www.google-melange.com/gsoc/proposal/review/google/gsoc2013/gwty/1">GoSC-2013 project</a> from <b>Gowtham Ashok</b>. This tool require feedback and hack to be finalized for this release.
</li>

<li>
<p>Showfoto thumbbar is now ported to Qt model/view in order to switch later full digiKam code to Qt5. This project is relevant to another <a href="http://www.google-melange.com/gsoc/proposal/review/google/gsoc2013/tootis/1">GoSC-2013 project</a> from <b>Mohamed Anwer</b>.</p>
</li>

<li>
<p>A lots of work have been done into Import tool to fix several dysfunctions reported since a very long time. For example, The status to indicate which item have been already downloaded from the device is back. Thanks to <b>Teemu Rytilahti</b> and <b>Islam Wazery</b> to contribute. All fixes are not yet completed and the game continue until the next beta release.</p>
</li>

</ul>

<p>Other pending GoSC-2013 projects will be included in next 4.0.0 beta release, step by step, when code will be considerate as good quality, and ready to test by end users. See the <a href="http://community.kde.org/Digikam/GSoC2013#Roadmap_and_Releases_Plan_including_all_GSoC-2013_works">release plan</a> for details.</p>

<p>As usual with a major release, we have a long list of <a href="https://bugs.kde.org/buglist.cgi?f1=cf_versionfixedin&amp;o1=equals&amp;query_format=advanced&amp;bug_status=RESOLVED&amp;bug_status=NEEDSINFO&amp;bug_status=VERIFIED&amp;bug_status=CLOSED&amp;v1=4.0.0&amp;product=digikam&amp;product=digikamimageplugins&amp;product=kipiplugins&amp;product=showfoto">files closed</a> with this release into KDE bugzilla.</p>

<p>digiKam software collection tarball can be downloaded from <a href="http://download.kde.org/unstable/digikam/digikam-4.0.0-beta2.tar.bz2.mirrorlist">KDE repository</a></p>

<p>This version is for testing purposes. <b>Please do not use it yet in production!</b></p>

<p>Thanks in advance for your reports.

</p><p>Happy digiKam...</p>
<div class="legacy-comments">

</div>
