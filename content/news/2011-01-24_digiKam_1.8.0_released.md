---
date: "2011-01-24T15:40:00Z"
title: "digiKam 1.8.0 released..."
author: "digiKam"
description: "Dear all digiKam fans and users! digiKam team is proud to announce digiKam 1.8.0 release! digiKam tarball can be downloaded from SourceForge at this url"
category: "news"
aliases: "/node/567"

---

<p>Dear all digiKam fans and users!</p>

<p>digiKam team is proud to announce digiKam 1.8.0 release!</p>

<a href="http://www.flickr.com/photos/digikam/5383875699/" title="digiKam1.8.0 by digiKam team, on Flickr"><img src="http://farm6.static.flickr.com/5220/5383875699_120f2c7a10.jpg" width="500" height="200" alt="digiKam1.8.0"></a>

<p>digiKam tarball can be downloaded from SourceForge <a href="http://sourceforge.net/projects/digikam/files">at this url</a></p>

<p>See below the list of bugs-fixes coming with this release.</p>

<p>Enjoy digiKam...</p>

<h5>BUGFIXES FROM KDE BUGZILLA:</h5><br>

001 ==&gt; 256372 : Add a digiKam installer based on NSIS.<br>
002 ==&gt; 261086 : Crash at startup.<br>
003 ==&gt; 212947 : digiKan crashes upon import of large photo collection.<br>
004 ==&gt; 261015 : digiKam Fingerprinter crashes.<br>
005 ==&gt; 237639 : Wish: All thumbs created in a folder automatically.<br>
006 ==&gt; 189585 : Thumbnails do not display.<br>
007 ==&gt; 255852 : digiKam crashed when browsing/rating images.<br>
008 ==&gt; 184635 : Empty menu when right clicking on an image.<br>
009 ==&gt; 261868 : Provide a progress indicator for "rename".<br>
010 ==&gt; 244560 : digiKam crash while viewing photos.<br>
011 ==&gt; 261582 : Default filename for renamed imports: remove the colon character.<br>
012 ==&gt; 262065 : Linking issue with Marble library.<br>
013 ==&gt; 261879 : Ubuntu unity compatibility bugs: export menu disappeared.<br>
014 ==&gt; 262296 : Camera GUI does not open [patch].<br>
015 ==&gt; 201788 : Picture navigation problems and disambiguation.<br>
016 ==&gt; 261415 : digiKam crashes when adding lens data to ~/.local.<br>
017 ==&gt; 253062 : Database of 80,000+ images wiped out.<br>
018 ==&gt; 263083 : digiKam editor crashes on "save" and "save as" (KFileDialog relevant).<br>
019 ==&gt; 252404 : digiKam crashes when modifying caption.<br>
020 ==&gt; 262720 : Rebuild thumbnails or 'scan for new images' should work on just current folder.<br>
021 ==&gt; 242036 : digiKam crashed when pressing 'Album -&gt; New'.<br>
022 ==&gt; 235597 : Disconnected usb passport external hard drive using "safely remove drive" option.<br>
023 ==&gt; 237957 : Crash while looking at folder with 380 images for first time.<br>
024 ==&gt; 253067 : Crash when viewing pictures.<br>
025 ==&gt; 255054 : Crash during picture import [KDirWatch::contains, Digikam::AlbumManager::addAlbumRoot, Digikam::AlbumManager::slotCollectionLocationStatusChanged].<br>
026 ==&gt; 258106 : digiKam crahed on search Pictures on individal Album.<br>
027 ==&gt; 261384 : Crash while scrolling the raw image preview.<br>
028 ==&gt; 258922 : digiKam crashed while scrolling through images in folder.<br>
029 ==&gt; 263726 : Metadata lost when decoding a raw file.<br>
030 ==&gt; 224047 : Thumbnails incorrectly rotated on certain images.<br>
031 ==&gt; 239902 : digiKam seems to find images that are not in the collection directory.<br>
032 ==&gt; 245958 : Some tags lost on batch color space conversion - import metadata does not work.<br>
033 ==&gt; 246735 : Frame rate drops to around 5 fps when dragging &amp; dropping.<br>
<div class="legacy-comments">

  <a id="comment-19774"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/567#comment-19774" class="active">Congrat's!</a></h3>    <div class="submitted">Submitted by <a href="http://proli.net" rel="nofollow">apol</a> (not verified) on Mon, 2011-01-24 16:24.</div>
    <div class="content">
     <p>Congratulations for the new release!!<br>
Are there just bugs fixed? Or there is new features? Looks like a major release...</p>
<p>Why is the code hosted in sourceforge? Isn't it in KDE anymore?</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19777"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/567#comment-19777" class="active">no...</a></h3>    <div class="submitted">Submitted by digiKam on Mon, 2011-01-24 20:58.</div>
    <div class="content">
     <p>It's a bugfix release.</p>
<p>Code is on KDE repository. SF.net is only used to host tarballs.</p>
<p>digiKam</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19804"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/567#comment-19804" class="active">Wise decision as we see now.</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Sat, 2011-01-29 08:23.</div>
    <div class="content">
     <p>Wise decision as we see now. ;) Let's hope your sources' integrity has not been harmed...<br>
Does KDE have any restrictions on hosting that made you decide in this way?</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div><a id="comment-19775"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/567#comment-19775" class="active">windows version</a></h3>    <div class="submitted">Submitted by Ed (not verified) on Mon, 2011-01-24 19:48.</div>
    <div class="content">
     <p>Will there be a windows installer version of 1.8 ?</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19776"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/567#comment-19776" class="active">not yet...</a></h3>    <div class="submitted">Submitted by digiKam on Mon, 2011-01-24 20:55.</div>
    <div class="content">
     <p>I plan to package it soon. Please wait...</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19812"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/567#comment-19812" class="active">Windows installer is</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Sun, 2011-01-30 22:00.</div>
    <div class="content">
     <p>Windows installer is necessary :)</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div><a id="comment-19778"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/567#comment-19778" class="active">Congratulations!</a></h3>    <div class="submitted">Submitted by <a href="http://ungethym.blogspot.com" rel="nofollow">Thomas (ungethym)</a> (not verified) on Tue, 2011-01-25 08:25.</div>
    <div class="content">
     <p>Congratulations to the whole digiKam team. Keep up the great work. It's one of my favorite apps and I really love it! Thank you!<br>
Thomas</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-19779"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/567#comment-19779" class="active">Digikam 1.8</a></h3>    <div class="submitted">Submitted by Normand (not verified) on Tue, 2011-01-25 15:26.</div>
    <div class="content">
     <p>I have been using a lot of DAM softwares including Photoshop Elements, Imatch, IDimager, Picasa and Windows Live Photo Gallery.</p>
<p>Having read that Digikam 2 will support reverse geotagging and face recognition I seriously consider to move to Digikam.</p>
<p>I have three questions that will help me in making my decision :</p>
<p>1 - Does Digikam save the metadata in the file itself or in a sidecar file?<br>
2 - What about tiff? Does Digikam can save metadata into a tif fle?<br>
3 - French and Spanish accents in text and keywords. OK under Linux but all screwed up when pictures are read with softwares under Windows. As more than 95% of the people I know are using Windows this is a big problem.<br>
4 - What about is I use Digikam under Windows? Will the french aand spanish accents be OK?</p>
<p>Thanks</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19780"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/567#comment-19780" class="active">...</a></h3>    <div class="submitted">Submitted by digiKam on Tue, 2011-01-25 15:32.</div>
    <div class="content">
     <p>&gt; 1 - Does Digikam save the metadata in the file itself or in a sidecar file?</p>
<p>Both.</p>
<p>&gt; 2 - What about tiff? Does Digikam can save metadata into a tif file?</p>
<p>yes.</p>
<p>&gt; 3 - French and Spanish accents in text and keywords.</p>
<p>digiKam support UTF-8 char. encoding. It's a standard supported by all operating systems.</p>
<p>&gt; 4 - What about is I use Digikam under Windows? Will the french aand spanish accents be OK?</p>
<p>yes. i'm french too...</p>
<p>digiKam</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19781"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/567#comment-19781" class="active">Thanks Gilles for your quick</a></h3>    <div class="submitted">Submitted by Normand (not verified) on Tue, 2011-01-25 15:53.</div>
    <div class="content">
     <p>Thanks Gilles for your quick response.</p>
<p>I have been testing Digikam for a few weeks now under both Ubuntu 10.10 and Windows 7. I like it very much. Great software.</p>
<p>My previous question should have been: are accents will be supported as keywords in Digikam 2.0?</p>
<p>The other softwares that I use do support them.</p>
<p>I am not only a serious amateur photographer I also collect family pictures and I have a lot. As I am of french origin from the othser side of the Atlantic Ocean ;-)  and my wife is Mexican we do have a lot of family names and Surnames with accents.</p>
<p>With the face recognition coming (as the name linked to the face will become a keyword)this will become important.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19782"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/567#comment-19782" class="active">accents are supported of course...</a></h3>    <div class="submitted">Submitted by digiKam on Tue, 2011-01-25 16:18.</div>
    <div class="content">
     <p>keywords support accents. It's UTF-8 stuff...</p>
<p>digiKam</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19783"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/567#comment-19783" class="active">Digikam 1.8</a></h3>    <div class="submitted">Submitted by Normand (not verified) on Tue, 2011-01-25 16:41.</div>
    <div class="content">
     <p>Gilles,</p>
<p>Maybe there is something I got wrong here but if i try to edit IPTC metadata with Digikam there is a note on the screen advising me that it only supports ASCII characters.</p>
<p>If I try tyo enter a new IPTC keyword with the metadata editor that contains an accent the IPTC editor refuses it.</p>
<p>When I import a picture into Digikam from let say Imatch 3.6 or Adobe Photoshop Elements 9 containing the IPTC keyword "Château de Versailles" the keyword appears OK in Digikam until Digikam resave the matadata into the same file for whatever reason.</p>
<p>Then when I reopen this same picture again in Windows with WLPG, Photo Elements or Imatch  the IPTC keyword is currupted and now appears as follows : ChÃ¢teau de Versailles; </p>
<p>If Digikam support accents what I am doing wrong?</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19784"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/567#comment-19784" class="active">IPTC is not XMP</a></h3>    <div class="submitted">Submitted by digiKam on Tue, 2011-01-25 16:54.</div>
    <div class="content">
     <p>IPTC do not support UTF-8 only ASCII<br>
XMP support UTF-8<br>
digiKam database support UTF-8</p>
<p>digiKam keywords are registered in database, IPTC and XMP.</p>
<p>IPTC is an old standard. XMP replace IPTC. It's a standard from Adobe :</p>
<p>http://en.wikipedia.org/wiki/Extensible_Metadata_Platform</p>
<p>digiKam</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19785"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/567#comment-19785" class="active">OK Gilles understood.
Problem</a></h3>    <div class="submitted">Submitted by Normand (not verified) on Tue, 2011-01-25 17:24.</div>
    <div class="content">
     <p>OK Gilles understood.</p>
<p>Problem is that XMP is a standard only for Adobe and Adobe is not the industry. </p>
<p>The industry just can't get along on new standards.</p>
<p>Not on how they want to save face recognition metadata and not on hierarchical keywords. Adobe Photoshop Elements doesn't even supports hierarchical keywrds.</p>
<p>Adobe, Microsoft, Apple, Canon, Nikon and the others are all pushing for their own set of standards. RAW file formats is a good example.</p>
<p>Convert RAW to DNG? Adobe hasn't created a 64 bit converter for Windows. Can't see or edit these pictures with Windows. Thanks Adobe !!!</p>
<p>Meanwhile we the users are stock with the problem and my problem is that 95% of the people are using softwares that don't use XMP or that just just creates their own xmp sidecar files by importing metadata directly from EXIF and IPTC or/and that creates their keywords from IPTC metadata like Picasa.</p>
<p>The whole thing of metadata standards is a %"/? mess. They can't even agree on a standard fom of saving face recognition metadata.</p>
<p>Not your fault. </p>
<p>I will try to use directly XMP metadata with Digikam and afterwards erase all my IPTC info to see how the other softwares used by the others will react.</p>
<p>I don't live alone. I share my pictures.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19786"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/567#comment-19786" class="active">&gt;Problem is that XMP is a</a></h3>    <div class="submitted">Submitted by digiKam on Tue, 2011-01-25 17:42.</div>
    <div class="content">
     <p>&gt;Problem is that XMP is a standard only for Adobe and Adobe is not the industry. </p>
<p>If Adobe is not the industry... Which company is ?</p>
<p>At least a standard exist and we support it, as Microsoft or Apple :</p>
<p>http://www.metadataworkinggroup.org/specs/</p>
<p>&gt;The industry just can't get along on new standards.</p>
<p>XMP have been created in 2001. It's not fresh...</p>
<p>&gt;Adobe, Microsoft, Apple, Canon, Nikon and the others are all pushing for their own set of standards. RAW &gt;file formats is a good example.</p>
<p>RAW is another complex problem relevant of device, not metadata standard. Standardization exist, is open,  and it's important to support it... digiKam support it...</p>
<p>You talk about Picasa which don't support XMP. It's a shame from Google...</p>
<p>digiKam</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19787"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/567#comment-19787" class="active">Google</a></h3>    <div class="submitted">Submitted by Normand (not verified) on Tue, 2011-01-25 17:58.</div>
    <div class="content">
     <p>Gilles,</p>
<p>I read that Picasa 3.8 do now read from XMP files.</p>
<p>But this all it does reads the info from XMP and copy it into it's own private database. Picasa still doesn't creates or writes metadata into XMP files.</p>
<p>They want to keep you captive. Outside of Picasa all your data is lost !</p>
<p>As for standards it's true that Microsoft complies with the standards but while Adobe uses sidecar files **.xmp) Microsoft doesn't. Microsoft saves the metadata directly into the picture. Seems that in what format you save the metadata is one thing and where you save it is another.</p>
<p>Probably another reason to use Digikam. </p>
<p>But still it would be great if the IPTC editor would accept accents as Imatch and Photoshop Elements do  :-)</p>
<p>Until Adobe standards are fully accepted and followed by the others it would make my life<br>
easier because now when my family members go see my pictures on a diaporama on Skydrive what they see as comments for the picture is the following :</p>
<p>ChÃ¢teau de Versailles; </p>
<p>My mother probably thinks that I drank too much Château De La Grave when I wrote my comments ;-) Or is it ChÃ¢teau De La Grave ?</p>
<p>Merci et salut Gilles.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19791"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/567#comment-19791" class="active">Google</a></h3>    <div class="submitted">Submitted by Normand (not verified) on Wed, 2011-01-26 19:06.</div>
    <div class="content">
     <p>I finally took the decision to switch to Digiam. </p>
<p>And before asking more questions I intend to read all the pertaining documentation.</p>
<p>So I am looking for the digikam.pdf file.</p>
<p>But it seems it's not available on the website anymore.</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-19809"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/567#comment-19809" class="active">Adobe is known for developing</a></h3>    <div class="submitted">Submitted by mbmdan (not verified) on Sun, 2011-01-30 12:59.</div>
    <div class="content">
     <p>Adobe is known for developing PDF. It first became a de facto standard and is now a de jure standard known as ISO 32000-1. Exemplifying innovation through standards, PDF is everywhere. From day one, the PDF specification was publicly published by Adobe without any licensing restrictions, which helped it become the de facto worldwide standard for the reliable distribution and exchange of electronic documents<br>
curt</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div></div></div></div></div></div></div></div><a id="comment-19790"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/567#comment-19790" class="active">is there any .deb file? for</a></h3>    <div class="submitted">Submitted by sunnyp (not verified) on Wed, 2011-01-26 18:16.</div>
    <div class="content">
     <p>is there any .deb file? for kubuntu...</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19798"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/567#comment-19798" class="active">DEB file...?</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Fri, 2011-01-28 11:30.</div>
    <div class="content">
     <p>First, THANK YOU for Digikam ; it's an amazing photographic tool!</p>
<p>PLEASE, where can we find a DEB file for the 1.8? I'm looking forward to it...</p>
<p>Again, bravo for all the work :)</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19806"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/567#comment-19806" class="active">Re: DEB file...?</a></h3>    <div class="submitted">Submitted by <a href="http://www.csamuel.org/" rel="nofollow">Chris Samuel</a> (not verified) on Sun, 2011-01-30 07:53.</div>
    <div class="content">
     <p>Digikam 1.8 is in the Kubuntu backports PPA (along with KDE 4.6.0):</p>
<p><a href="https://launchpad.net/~kubuntu-ppa/+archive/backports">https://launchpad.net/~kubuntu-ppa/+archive/backports</a></p>
         </div>
    <div class="links">» </div>
  </div>
</div></div><a id="comment-19795"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/567#comment-19795" class="active">Layers and Masks!!</a></h3>    <div class="submitted">Submitted by Bob  (not verified) on Thu, 2011-01-27 07:39.</div>
    <div class="content">
     <p>If DigiKam only had layer and layer masks, it could become a premiere pixel editor. But sadly Linux users have to wait for the two Gimp developers to update Gimp. Linux really needs an advanced pixel editor so professionals can start adopting Linux as a viable platform. DigiKam seems to be well maintained, better than Gimp these days, but it still lacks essential features like layers and layer masks. Even PS Elements includes these features. Hint!!.</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-19796"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/567#comment-19796" class="active">First this app rocks!
I love</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Thu, 2011-01-27 23:28.</div>
    <div class="content">
     <p>First this app rocks!<br>
I love it really much :)</p>
<p>Thank you for digikam.</p>
<p>Maybe two small feature request:</p>
<p>1) it would be really nice to have a button in digikam to set a picture as wallpaper in KDE.</p>
<p>2) An option to change the behavior of the mousewheel so it would cycle forward an back in the viewing mode.</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-19797"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/567#comment-19797" class="active">Nepomuk</a></h3>    <div class="submitted">Submitted by Martin Foster (not verified) on Fri, 2011-01-28 08:39.</div>
    <div class="content">
     <p>Has support for writing nepomuk tags been dropped?</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-19799"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/567#comment-19799" class="active">Please, how to build digikam</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Fri, 2011-01-28 12:12.</div>
    <div class="content">
     <p>Please, how to build digikam 1.8 from sources safely?<br>
Thanks a lot!</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19801"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/567#comment-19801" class="active">look here :</a></h3>    <div class="submitted">Submitted by digiKam on Fri, 2011-01-28 12:17.</div>
    <div class="content">
     <p>http://www.digikam.org/drupal/download/KDE4</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19803"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/567#comment-19803" class="active">Thank you!</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Fri, 2011-01-28 15:25.</div>
    <div class="content">
     <p>Merci!</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div><a id="comment-19800"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/567#comment-19800" class="active">Please, how to build digikam</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Fri, 2011-01-28 12:12.</div>
    <div class="content">
     <p>Please, how to build digikam 1.8 from sources safely?<br>
Thanks a lot!</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-19805"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/567#comment-19805" class="active">Debian packages</a></h3>    <div class="submitted">Submitted by <a href="http://xraynaudphotos.free.fr" rel="nofollow">xavier</a> (not verified) on Sat, 2011-01-29 12:57.</div>
    <div class="content">
     <p>Digikam and kipi debian packages for i386 can be found at http://xraynaudphotos.free.fr/debian.php</p>
         </div>
    <div class="links">» </div>
  </div>

</div>
