---
date: "2008-08-24T15:37:00Z"
title: "digiKam Image Editor Usability Improvements... (screencast)"
author: "digiKam"
description: "I am currently working with Andi Clemens to improve some important usability issues in Image Editor and Showfoto. In April, Risto Saukonpää, the designer (and"
category: "news"
aliases: "/node/370"

---

<p>I am currently working with <b>Andi Clemens</b> to improve some important usability issues in Image Editor and Showfoto.</p>

<p>In April, <b>Risto Saukonpää</b>, the designer (and photographer) who re-designed digikam.org website and digiKam logo (updates coming about those) provided <a href="http://saukonpaa.com/digikam/digikam_showfoto.pdf">a great mockup and ideas</a> for Showfoto how to speed-up workflow during image editing.

</p><p>Well, i'm happy to say that work is in  progress in KDE3 branch. I have written a new API for editor named <b>Editor Tool</b> and it will be used for all image plugins. The goal is to remove all independent plugin dialogs and have them as embedded plugins on editor window itself. Preview is stacked over canvas and settings placed as a dedicated tab in right sidebar. The API will plug and unplug all tools automaticlally when changing between them.</p>

<p>Currently only two editor tools have been fully ported to Editor Tool API: Raw Import and Adjust Curves. There is a video of these tools in action below:</p>

<object width="500" height="404"> <param name="movie" value="http://www.youtube.com/v/55__RU3Yh1o">  <embed src="http://www.youtube.com/v/55__RU3Yh1o" type="application/x-shockwave-flash" width="500" height="404">  </object>

<p><a href="http://digikam3rdparty.free.fr/TourMovies/0.9.5-editortoolembedded.ogv">See the full screen video (ogg - 10Mb)</a></p>

<p>Of course, the plan is to port all plugins as well. In future there will be no more dialoges in editor... The new API is already ported to KDE4 and the RawImport tool is working fine too. First we will port all plugins  on KDE3 branch to a new API, before backporting the code to KDE4 branch.</p>

<p>Andi is working with new widgets used in image plugins, so we are able to reset each values independently in tools settings. This includes all combo boxes, double input and integer input widgets.</p>

<div class="legacy-comments">

  <a id="comment-17718"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/370#comment-17718" class="active">wow !</a></h3>    <div class="submitted">Submitted by DanaKil (not verified) on Mon, 2008-08-25 23:44.</div>
    <div class="content">
     <p>"In future there will be no more dialoges in editor"</p>
<p>no comments for this GREAT feature ??! I just love Digikam is it gets better and better<br>
thank you :)</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-17719"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/370#comment-17719" class="active">Right direction it is!</a></h3>    <div class="submitted">Submitted by Samu Mikkonen (not verified) on Tue, 2008-08-26 11:21.</div>
    <div class="content">
     <p>yes indeed this is a major leap to right direction usability wise, hyvä Risto!</p>
<p>But I find it bit odd that you needed someone to make a pdf to show you how your competing software such as lightroom, aperture, lightzone and rawtherapee work. There are loads of usability tricks that one can learn by taking lightroom for a test drive or even if you don't have windows you can watch adobes tutorials for free.</p>
<p>For example If I was about to choose an image editing program for editing say wedding photographs I would choose give some cash to adobe folks just because you don't have to wait for that importing load that is shown in the video but instead you export the images you want in one bulk when you have finished.</p>
<p>Anyway thank you for this change it might even make Digikam my editor of choice on linux.</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-17720"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/370#comment-17720" class="active">Awesome</a></h3>    <div class="submitted">Submitted by <a href="http://prokoudine.info" rel="nofollow">Alexandre</a> (not verified) on Tue, 2008-08-26 11:22.</div>
    <div class="content">
     <p>At last! :-)</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-17881"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/370#comment-17881" class="active">Thnaks for your overview</a></h3>    <div class="submitted">Submitted by <a href="http://www.ffxi-gil.org" rel="nofollow">ffxi gil</a> (not verified) on Tue, 2008-10-21 06:18.</div>
    <div class="content">
     <p>An easy to use interface is provided that enables<br>
you to connect to your camera and preview,<br>
download and/or delete your images.   digiKam<br>
includes too an image editor for image corrections<br>
and manipulations whitch can be extended by<br>
plugins available in DigikamImagePlugins project.</p>
         </div>
    <div class="links">» </div>
  </div>

</div>