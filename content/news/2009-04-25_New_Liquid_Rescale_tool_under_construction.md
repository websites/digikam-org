---
date: "2009-04-25T14:29:00Z"
title: "New Liquid Rescale tool under construction..."
author: "digiKam"
description: "Since few weeks, an active work have been done with Julien Pontabry and Julien Nardoux to solve this bug report. On preview screenshots, you can"
category: "news"
aliases: "/node/439"

---

<a href="http://www.flickr.com/photos/digikam/3471384222/" title="liquid-rescale-with-preservation-mask by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3600/3471384222_3827c905ff.jpg" width="500" height="313" alt="liquid-rescale-with-preservation-mask"></a>

<a href="http://www.flickr.com/photos/digikam/3471384790/" title="liquid-rescale-with-preservation-mask2 by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3620/3471384790_31c1dc64ba.jpg" width="500" height="313" alt="liquid-rescale-with-preservation-mask2"></a>

<p>Since few weeks, an active work have been done with <b>Julien Pontabry</b> and <b>Julien Nardoux</b> to solve <a href="https://bugs.kde.org/show_bug.cgi?id=149485">this bug report</a>.</p>

<p>On preview screenshots, you can seen a picture resized horizontally to 150%. A preservation mask in green is drawn over the image part to not touch elements during transformation. see below, another example with two area to preserve:</p>

<a href="http://www.flickr.com/photos/digikam/3472983544/" title="liquid-rescale-with-preservation-mask3 by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3310/3472983544_fef8816abc.jpg" width="500" height="313" alt="liquid-rescale-with-preservation-mask3"></a>

<a href="http://www.flickr.com/photos/digikam/3472983788/" title="liquid-rescale-with-preservation-mask4 by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3355/3472983788_ddd9a5e753.jpg" width="500" height="313" alt="liquid-rescale-with-preservation-mask4"></a>

<a href="http://www.flickr.com/photos/digikam/3472176295/" title="liquid-rescale-with-preservation-mask5 by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3404/3472176295_e50f787715.jpg" width="500" height="313" alt="liquid-rescale-with-preservation-mask5"></a>

<p>Look like houses are not deformed when image is scaled up horizontally. All the rest of image contents will be transformed. You can created nice banner for web site with this tool for example. Now, another try with a suppresion mask to remove unwanted contents:</p>

<a href="http://www.flickr.com/photos/digikam/3473375236/" title="liquid-rescale-with-suppression-mask3 by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3607/3473375236_2ab0228a01.jpg" width="500" height="313" alt="liquid-rescale-with-suppression-mask3"></a>

<a href="http://www.flickr.com/photos/digikam/3472565629/" title="liquid-rescale-with-suppression-mask4 by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3573/3472565629_3335bbe568.jpg" width="500" height="313" alt="liquid-rescale-with-suppression-mask4"></a>

<a href="http://www.flickr.com/photos/digikam/3473375854/" title="liquid-rescale-with-suppression-mask5 by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3582/3473375854_e71c207213.jpg" width="500" height="313" alt="liquid-rescale-with-suppression-mask5"></a>

<p>With this picture, the kid on the middle will be removed using a suppression mask draw over in red. My son on the bottom must be preserved during transformation. Suppression mask only work for the moment to reduce size of image. With this example, width is scaled down to 80%.</p>

<a href="http://www.flickr.com/photos/digikam/3475199517/" title="digiKam-liquid-rescale-win32-01 by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3300/3475199517_194145df6c.jpg" width="500" height="313" alt="digiKam-liquid-rescale-win32-01"></a>

<a href="http://www.flickr.com/photos/digikam/3475200053/" title="digiKam-liquid-rescale-win32-02 by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3645/3475200053_eda786ac7b.jpg" width="500" height="313" alt="digiKam-liquid-rescale-win32-02"></a>

<p>And the last sample. Yes this one is under Windows Vista ! And it's run fine too, as under Linux...</p>

<p>Liquid Rescale tool will be available in digiKam 0.11.0. Some work need to be done to improve mask creation, and polish settings view to be more ergonomic.</p>

<div class="legacy-comments">

  <a id="comment-18446"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/439#comment-18446" class="active">Thank you guys for your</a></h3>    <div class="submitted">Submitted by xdmx (not verified) on Sat, 2009-04-25 14:54.</div>
    <div class="content">
     <p>Thank you guys for your work.... and to have taken in consideration that wish :)</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-18447"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/439#comment-18447" class="active">impressions</a></h3>    <div class="submitted">Submitted by vivo (not verified) on Sat, 2009-04-25 15:07.</div>
    <div class="content">
     <p>I'm not using your software right now (lack of time). But following your blog anyway and the work done has impessed me more than one time (this one included).</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-18448"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/439#comment-18448" class="active">Resynthesizer</a></h3>    <div class="submitted">Submitted by <a href="http://bmaron.net" rel="nofollow">eMerzh</a> (not verified) on Sat, 2009-04-25 15:15.</div>
    <div class="content">
     <p>Nice Work!</p>
<p>For removing some elements, it will be great to have resynthesizer integraded with digikam.<br>
The results are very impressive.</p>
<p>http://www.logarithmic.net/pfh/resynthesizer</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18453"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/439#comment-18453" class="active">That's cool. Maybe it's a</a></h3>    <div class="submitted">Submitted by maninalift (not verified) on Sat, 2009-04-25 18:54.</div>
    <div class="content">
     <p>That's cool. Maybe it's a little outside the scope of digikam but I'd love to see it in there.</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-18455"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/439#comment-18455" class="active">Just what I was looking for</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Sat, 2009-04-25 23:10.</div>
    <div class="content">
     <p>i was just looking for this yesterday. I think the resynthesizer things looks awesome, but maybe it belongs in an application like Krita ( which would do no harm by  adding liquid scale). Really a nice program</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-18449"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/439#comment-18449" class="active">I know I'm repeating myself</a></h3>    <div class="submitted">Submitted by maninalift (not verified) on Sat, 2009-04-25 15:47.</div>
    <div class="content">
     <p>I know I'm repeating myself but DigiKam not only by a long way the best photo management software that I have experienced on any platform, it is probably the slickest piece of software in KDE.</p>
<p>I thought this thechnique was really cool when I first saw it, even better now it is in DigiKam.</p>
<p>P.S. I the final photo there is some noticeable distortion of the well (is that what it is) at the back near the removed child. It's a result of<br>
(a) an object has been removed very close to it and and the adjacent edge of the removed object is not straight<br>
(b) it is more obvious because the viewer knows that the edge of the well should be straight and vertical.</p>
<p>It's not a simple request but it could be interesting to consider fixing this by being able to insert constraints: For example draw a straight line and enforce that that line be kept straight and optionally enforce that it's orientation be maintained (so a vertical line remains vertical). This would of course lead to more distortion elsewhere in the picture.</p>
<p>... in the above example perhaps just painting green over it would help but there are some cases where it wouldn't work to maintain the size and proportions of something completely but you do want to keep e.g. keep some straight edges straight... maybe</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-18450"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/439#comment-18450" class="active">Wow! This is one of those</a></h3>    <div class="submitted">Submitted by Angel Blue01 (not verified) on Sat, 2009-04-25 16:58.</div>
    <div class="content">
     <p>Wow! This is one of those features I've always wanted in a photo editor but had no idea what to call it. Thanks for including this.</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-18451"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/439#comment-18451" class="active">Simply the best</a></h3>    <div class="submitted">Submitted by Paul Waldo (not verified) on Sat, 2009-04-25 18:18.</div>
    <div class="content">
     <p>This kind of ongoing development is exactly why Digikam is always going to be my choice for an image organizer.  The developers are always responsive to suggestions and wishes for increased functionality; Digikam continues to be cutting edge.</p>
<p>Hats off to everyone involved in Digikam.  It is the example I use when evangelizing Open Source.  Keep up the fantastic work!</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-18452"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/439#comment-18452" class="active">Sheesh! You've done it again Gilles...</a></h3>    <div class="submitted">Submitted by <a href="http://kubuntulover.blogspot.com" rel="nofollow">Bugsbane</a> (not verified) on Sat, 2009-04-25 18:26.</div>
    <div class="content">
     <p>This has to be one of my favourite pieces of mind-blowing image technology. I'm stunned to see it in a photo management app! I love how clear and obvious the masking is (green for preserve, red for maintain). I'd consider myself a power user of Gimp, but I still have to tinker with the liquid rescale interface there for a while until I can get it to do what I want.</p>
<p>5 Stars!</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-18454"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/439#comment-18454" class="active">increadible. been using</a></h3>    <div class="submitted">Submitted by Vladislav (not verified) on Sat, 2009-04-25 18:58.</div>
    <div class="content">
     <p>increadible. been using digikam for 6 months now and this project never ceases to amaze me. great job on the kde4 version, and I look forward to using this transform tool once it comes out for general consumption</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-18456"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/439#comment-18456" class="active">I saw this technology</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Sun, 2009-04-26 16:35.</div>
    <div class="content">
     <p>I saw this technology schowcased two years ago as a tech experiment, and am absolutely amazed that it has been able to be brought up to open-source implementation so soon. I can guarantee you this - no other program outside of the gimp has this feature.<br>
See this video on liquid rescale (here called seam carving):<br>
http://www.youtube.com/watch?v=6NcIJXTlugc</p>
<p>Also, as a side note, the CAPTCHA on this site is awful, after squinting at it and mistyping 4 or 5 times before I could post. Could you do something about that please?</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18459"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/439#comment-18459" class="active">Photoshop CS4 has this</a></h3>    <div class="submitted">Submitted by Julien (not verified) on Mon, 2009-04-27 18:24.</div>
    <div class="content">
     <p>Photoshop CS4 has this feature as well.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18460"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/439#comment-18460" class="active">CS4 open source ?</a></h3>    <div class="submitted">Submitted by digiKam on Mon, 2009-04-27 18:56.</div>
    <div class="content">
     <p>Oh, i never seen than Photoshop is an open source program (:=)))...</p>
<p>digiKam</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18461"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/439#comment-18461" class="active">Sure it is :-)
And free as in</a></h3>    <div class="submitted">Submitted by Andi Clemens on Mon, 2009-04-27 19:26.</div>
    <div class="content">
     <p>Sure it is :-)<br>
And free as in "torrent"... hmmm you know what I mean!</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div></div><a id="comment-18457"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/439#comment-18457" class="active">Looks great :-)</a></h3>    <div class="submitted">Submitted by Gandalf (not verified) on Mon, 2009-04-27 07:53.</div>
    <div class="content">
     <p>Liquid rescaling looks great so far, and will for sure bring more users to digikam. I've been playing with its current svn implementation - Is there an explanation somewhere what the settings like energy function, rigidity etc are good for?<br>
What's not working very conveniently right now is the selecting of the area to be protected or erased. It would be helpful if one could change the size of the pen or draw a closed loop and select the full interior of it. </p>
<p>Already looking forward to the final implementation!</p>
<p>Gandalf</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18458"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/439#comment-18458" class="active">Try SHIFT+F1 over options</a></h3>    <div class="submitted">Submitted by digiKam on Mon, 2009-04-27 07:56.</div>
    <div class="content">
     <p>&gt;Is there an explanation somewhere what the settings like energy function, rigidity etc are good for?</p>
<p>The only documentation is on widgets tooltips. Use SHIFT+F1 for help.</p>
<p>digiKam</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-18462"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/439#comment-18462" class="active">sharpening mask</a></h3>    <div class="submitted">Submitted by <a href="http://vw.homelinux.net" rel="nofollow">Anonymous</a> (not verified) on Mon, 2009-04-27 21:52.</div>
    <div class="content">
     <p>So doesn't this pave the way for using the mask to sharpen?!  This would be a huge improvement!  In your sample photo for example, being able to sharpen the kid on the bike, without sharpening the background.  There are so many times where I want to sharpen the subject but not the background (background becomes too noisy otherwise), such as just about anything against a clear sky or water. </p>
<p>Vern</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-18571"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/439#comment-18571" class="active">That is really nice</a></h3>    <div class="submitted">Submitted by Tttt (not verified) on Tue, 2009-06-02 11:25.</div>
    <div class="content">
     <p>That is really nice program!!!</p>
         </div>
    <div class="links">» </div>
  </div>

</div>
