---
date: "2016-10-21T13:31:00Z"
title: "Spektrum: Preset for Modern Architecture Photography"
author: "Dmitri Popov"
description: "Like many photographers, I have a handful of hand-made favorite presets (most of them are included in the Daily Curves Set) in my photographic toolbox."
category: "news"
aliases: "/node/759"

---

<p>Like many photographers, I have a handful of hand-made favorite presets (most of them are included in the <a href="https://gumroad.com/l/dailycurves">Daily Curves Set</a>) in my photographic toolbox. But there is one preset in particular I use more often than others. I named it Spektrum, as it’s inspired by images from the <a href="http://www.matthias-heiderich.de/spektrum-berlin-book/">Spektrum Berlin</a> photo book by Matthias Heiderich.</p>
<p><a href="https://medium.com/@dmpop/spektrum-preset-for-modern-architecture-photography-69e88c2b042a">Continue reading</a></p>

<div class="legacy-comments">

</div>