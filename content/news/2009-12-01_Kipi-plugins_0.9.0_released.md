---
date: "2009-12-01T17:29:00Z"
title: "Kipi-plugins 0.9.0 released"
author: "digiKam"
description: "Dear all digiKam fans and users! Kipi-plugins 0.9.0 release for KDE4 is out. kipi-plugins tarball can be downloaded from SourceForge at this url Kipi-plugins will"
category: "news"
aliases: "/node/489"

---

<p>Dear all digiKam fans and users!</p>

<p>Kipi-plugins 0.9.0 release for KDE4 is out.</p>

<a href="http://www.flickr.com/photos/digikam/4150664434/" title="gpssync by digiKam team, on Flickr"><img src="http://farm3.static.flickr.com/2752/4150664434_c76fcdb957.jpg" width="500" height="313" alt="gpssync"></a>

<p>kipi-plugins tarball can be downloaded from SourceForge <a href="http://sourceforge.net/projects/kipi/files">at this url</a></p>

<p>Kipi-plugins will be also available for Windows. Precompiled packages can be donwloaded with KDE-Windows installer. See <a href="http://windows.kde.org">KDE-Windows project</a> for details.</p>

<p>See below the list of new features and bugs-fix coming with this release:</p>

<b>GPSSync</b>             : Bookmark locations.<br><br>

001 ==&gt; BatchProcessImages : 213164 : kipi convert images: 16 bit png from 8 bit bmp.<br>
002 ==&gt; BatchProcessImages : 212118 : Rename images: sorting based on the exif date of an image.<br>
003 ==&gt; RawConverter       : 213681 : Batch RAW converter: Activating 16bit allows jpeg.<br>
004 ==&gt; JPEGLossLess       : 213889 : Segmentation fault at splash screen.<br>
005 ==&gt; PrintAssistant     : 124357 : Output resolution of print wizard is very low.<br>
006 ==&gt; PrintAssistant     : 204707 : Printwizard should be able to print uncroped thumbnails.<br>
007 ==&gt; PrintAssistant     : 124357 : Output resolution of print wizard is very low.<br>
008 ==&gt; AdvancedSlideShow  : 214744 : Advanced slideshow crash after pressing ok.<br>
009 ==&gt; GPSSync            : 135387 : Stickers of photos on the world map.<br>
010 ==&gt; JPEGLossLess       : 215644 : Rotate image fails under windows.<br>
011 ==&gt; FlickrExport       : 215646 : Export to flickr fails under windows.<br>
012 ==&gt; GPSSync            : 167279 : Add a list of favorite place to gps locater window (e.g. new drop down menu).<br>
013 ==&gt; GPSSync            : 135406 : History for last used GPS coordinates.<br>
014 ==&gt; GPSSync            : 206080 : Kipi-plugins gpssync no map when entering.<br>
015 ==&gt; General            : 216622 : digiKam bug report data errors.<br>
016 ==&gt; GPSSync            : 214210 : Don't take the correct position from geolocation file.<br>
017 ==&gt; libkipi            : 216790 : Configuration dialogue crashes getting icon for plugins not loaded.<br>
018 ==&gt; GPSSync            : 216803 : GPSSync ability to zoom with mouse wheel.<br>
019 ==&gt; SlideShow          : 215798 : slideshow crashes when starting.<br>

<div class="legacy-comments">

  <a id="comment-18875"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/489#comment-18875" class="active">Is it possible again to</a></h3>    <div class="submitted">Submitted by Jonas (not verified) on Fri, 2009-12-04 12:48.</div>
    <div class="content">
     <p>Is it possible again to select more than one file at a time in the geolocation window and change the gps coordinates for these files at a time. In the last version it was not possible to select more than on file :(</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-18877"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/489#comment-18877" class="active">What is Kipi ?</a></h3>    <div class="submitted">Submitted by AlterEgo (not verified) on Sat, 2009-12-05 16:07.</div>
    <div class="content">
     <p>At least a one line description would be very useful :-)<br>
I don't know what it is and there are no link to find out.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18890"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/489#comment-18890" class="active">Kipi-plugins are originally a</a></h3>    <div class="submitted">Submitted by Tommy (not verified) on Sun, 2009-12-13 17:16.</div>
    <div class="content">
     <p>Kipi-plugins are originally a digiKam's plugins what extends the features with graphical functions. The Kipi-plugins can be used alone without digiKam itself and some apps use it like Gwenview and few others. You have all kind functions with it like red eye removing, format conversions, importing and exporting to/from other places and so on.</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-18878"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/489#comment-18878" class="active">How to find Kipi plugins from Digikam.</a></h3>    <div class="submitted">Submitted by Rob (not verified) on Sat, 2009-12-05 17:50.</div>
    <div class="content">
     <p>Hi,<br>
Could you give us a quick explanation of where Digikam looks for the Kipi Plugins?  I installed Digikam 1.0 rc1 from tarball on Ubuntu Jaunty (works brilliantly thanks) and also the latest KIPI plugins, both from tarball. Unfortunately Digikam does not see the Kipi plugins in its Settings/Digikam/KipiPlugins dialogue. A bit of explanation on how the two link together would be fantastic.<br>
Thanks,<br>
Rob.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18882"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/489#comment-18882" class="active">Working now</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Mon, 2009-12-07 16:00.</div>
    <div class="content">
     <p>After prodding and poking about a bit Digikam is now finding the Kipi plugins. Calling cmake for Kipi with DCMAKE_INSTALL_PREFIX=/usr seemed to be the key.</p>
         </div>
    <div class="links">» </div>
  </div>
</div>
</div>
