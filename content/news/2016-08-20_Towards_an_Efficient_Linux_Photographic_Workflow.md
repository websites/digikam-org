---
date: "2016-08-20T14:11:00Z"
title: "Towards an Efficient Linux Photographic Workflow"
author: "Dmitri Popov"
description: "digiKam is the cornerstone of my photographic workflow. This powerful and versatile photo management application has all tools and features necessary for transferring, organizing, processing,"
category: "news"
aliases: "/node/757"

---

<p>digiKam is the cornerstone of my photographic workflow. This powerful and versatile photo management application has all tools and features necessary for transferring, organizing, processing, and managing photos, RAW files, and videos. But even though digiKam can handle practically any photographic task you throw at it, there is still room for optimizing and improving parts of the Linux-based photographic workflow. <a href="https://medium.com/@dmpop/towards-an-efficient-linux-photographic-workflow-4d9301c08da7">Continue reading</a></p>

<div class="legacy-comments">

</div>