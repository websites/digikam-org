---
date: "2014-05-14T07:23:00Z"
title: "digiKam Software Collection 4.0.0 released..."
author: "digiKam"
description: "Dear all digiKam fans and users! digiKam team is proud to announce the release of digiKam Software Collection 4.0.0. This version, include many new features"
category: "news"
aliases: "/node/713"

---

<a href="https://www.flickr.com/photos/digikam/14099672266"><img src="https://farm8.staticflickr.com/7457/14099672266_ac5b8a60a9_z.jpg" width="640" height="360"></a>


<p>Dear all digiKam fans and users!</p>

<p>
digiKam team is proud to announce the release of digiKam Software Collection 4.0.0.
This version, include many new features introduced by completed <a href="http://community.kde.org/Digikam/GSoC2013#digiKam_Google_Summer_of_Code_2013_Projects_list">GSoC 2013 projects</a>:
</p>

<ul>

<li>
A new tool dedicated to organize whole tag hierarchies. This new feature is relevant of <a href="http://community.kde.org/Digikam/GSoC2013#digiKam_Tag_Manager">GoSC-2013 project</a> from <b>Veaceslav Munteanu</b>. Veaceslav has also implemented multiple selection and multiple drag-n-drop capabilities on Tags Manager and Tags View from sidebars, and the support for writing face rectangles metadata in Windows Live Photo format.
</li>

<li>
A new maintenance tool dedicated to parse image quality and auto-tags items automatically using Pick Labels. This tool is relevant to another <a href="http://community.kde.org/Digikam/GSoC2013#Image_Quality_Sorter_for_digiKam">GoSC-2013 project</a> from <b>Gowtham Ashok</b>.
</li>

<li>
<p>Showfoto thumbbar is now ported to Qt model/view in order to switch later full digiKam code to Qt5. This project is relevant to another <a href="http://community.kde.org/Digikam/GSoC2013#Port_Showfoto_Thumb_bar_to_Qt4_Model.2FView">GoSC-2013 project</a> from <b>Mohamed Anwer</b>.</p>
</li>

<li>
<p>A lots of work have been done into Import tool to fix several dysfunctions reported since a very long time. For example, The status to indicate which item have been already downloaded from the device is back. Thanks to <b>Teemu Rytilahti</b> and <b>Islam Wazery</b> to contribute. All fixes are not yet completed and the game continue until the next beta release.</p>
</li>

<li>
<p>This release is now fully ported to Qt4 model-view and drop last Q3-Support classes. The last pending part was Image Editor Canvas ported and completed by <b>Yiou Wang</b> through this <a href="http://community.kde.org/Digikam/GSoC2013#Port_Image_digiKam_Editor_Canvas_Classes_to_Qt4_Model.2FView">GoSC-2013 project</a>. 
In the future, port to Qt5 will be easy and quickly done, when KDE 5 API will stabilized and released.</p>
</li>

<li>
<p>This release include also the multi-core CPU support for severals tools which require a lots of computing, as Sharpen, LocalContrast, Noise Reduction, and all visual effects tools.</p>
</li>

</ul>

<p>As usual with a major release, we have a long list of <a href="https://bugs.kde.org/buglist.cgi?f1=cf_versionfixedin&amp;o1=equals&amp;query_format=advanced&amp;bug_status=RESOLVED&amp;bug_status=NEEDSINFO&amp;bug_status=VERIFIED&amp;bug_status=CLOSED&amp;v1=4.0.0&amp;product=digikam&amp;product=digikamimageplugins&amp;product=kipiplugins&amp;product=showfoto">files closed</a> with this release into KDE bugzilla.</p>

<p>digiKam software collection tarball can be downloaded from <a href="http://download.kde.org/stable/digikam/digikam-4.0.0.tar.bz2.mirrorlist">KDE repository</a></p>

<p>Have fun to play with your photo using this new main release

</p><p>digiKam Team...</p>
<div class="legacy-comments">

  <a id="comment-20700"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/713#comment-20700" class="active">Great!</a></h3>    <div class="submitted">Submitted by <a href="http://www.londonlight.org/" rel="nofollow">DrSlony</a> (not verified) on Wed, 2014-05-14 12:46.</div>
    <div class="content">
     <p>Well done and thank you to everyone involved!</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-20701"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/713#comment-20701" class="active">Windows Support</a></h3>    <div class="submitted">Submitted by Philippe (not verified) on Wed, 2014-05-14 17:10.</div>
    <div class="content">
     <p>I take advantage of this release to ask if the support of the windows platform has been enhanced? and if this version is available on Windows?</p>
<p>I use the linux version but my parents the Windows one.</p>
<p>But I was unable to use it very well because they are french and I don't have found a way to change the language of digikam :(<br>
I think it is in the "kde settings" but the french is not available :(</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20720"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/713#comment-20720" class="active">When will the windows port of</a></h3>    <div class="submitted">Submitted by Daniel (not verified) on Mon, 2014-05-19 14:58.</div>
    <div class="content">
     <p>When will the windows port of 4.0 be made available?</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-20724"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/713#comment-20724" class="active">french setting</a></h3>    <div class="submitted">Submitted by Philippe (not verified) on Tue, 2014-05-27 09:25.</div>
    <div class="content">
     <p>The easiest way to put it in French is to use a environment variable : LANG=fr.UTF-8</p>
<p>You can add it in config panel --&gt; system &amp; security --&gt; system --&gt; advanced system parameters and click on "environment variable", add a new variable (system or user wide), call it "LANG" and set it to "fr.UTF-8"<br>
(in french : Panneau de configuration --&gt; Système et sécurité --&gt; Système --&gt; Paramètres système avancés,  "Variables d'environement", ajouter une nouvelle variable soit pour tout le système soit pour l'utilisateur courant, la nommer "LANG" et lui donner la valeur : "fr.UTF-8" ).</p>
<p>OR, If you prefer you can also make a bat file to launch your digikam :<br>
----8&lt;--digikam.bat--8&lt;----<br>
SET LANG=fr.UTF-8<br>
"C:\Program Files (x86)\digiKam\bin\digikam.exe"<br>
----8&lt;---------------8&lt;----</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-20702"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/713#comment-20702" class="active">Very excited!</a></h3>    <div class="submitted">Submitted by Rob MacKenzie (not verified) on Wed, 2014-05-14 17:35.</div>
    <div class="content">
     <p>I've been waiting for this release to switch my main workflow.<br>
Really looking forward to it, thanks for all the work! I will contribute some money when I can.</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-20703"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/713#comment-20703" class="active">Thanks to the team for all</a></h3>    <div class="submitted">Submitted by Le Gars (not verified) on Wed, 2014-05-14 17:38.</div>
    <div class="content">
     <p>Thanks to the team for all the hard work! Been looking forward to this release!<br>
Amazing software with a lot of new features to try out</p>
<p>Merci encore !</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-20704"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/713#comment-20704" class="active">installing 4.0.0 in Opensuse 13.01</a></h3>    <div class="submitted">Submitted by antoni (not verified) on Wed, 2014-05-14 19:48.</div>
    <div class="content">
     <p>Thanks for the effort and the new version.</p>
<p>Trying to install in Opensuse 13.01 got an error: libkdcraw.so.23 not available.</p>
<p>antoni</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20707"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/713#comment-20707" class="active">I also use opensuse, I'll try</a></h3>    <div class="submitted">Submitted by umby (not verified) on Thu, 2014-05-15 15:12.</div>
    <div class="content">
     <p>I also use opensuse, I'll try to download by the correct repository, as soon as possible.<br>
do yuo know if the new version maintain the old database without problem?</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20714"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/713#comment-20714" class="active">Opensuse</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Fri, 2014-05-16 19:36.</div>
    <div class="content">
     <p>I have installed on previous 3.5.0 withou problem. In fact works a lot better.</p>
<p>antoni</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-20713"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/713#comment-20713" class="active">Opensuse install</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Fri, 2014-05-16 19:35.</div>
    <div class="content">
     <p>Solved. Using KDE Extra repository.</p>
<p>antoni</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20719"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/713#comment-20719" class="active">hi antoni
How can I add an</a></h3>    <div class="submitted">Submitted by umby (not verified) on Sat, 2014-05-17 16:24.</div>
    <div class="content">
     <p>hi antoni<br>
How can I add an extra dedicated repository in opensuse for new digikam?<br>
can yuo write me the exact typing...<br>
thank<br>
umby</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-20721"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/713#comment-20721" class="active">Hi guys,
I use opensuse</a></h3>    <div class="submitted">Submitted by umby (not verified) on Tue, 2014-05-20 22:40.</div>
    <div class="content">
     <p>Hi guys,<br>
I use opensuse 13.1<br>
I can not upload the new version<br>
I tried to add extra kde on yast but I do not see digikam4.0<br>
anyone has any solution</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20722"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/713#comment-20722" class="active">Opensuse repository</a></h3>    <div class="submitted">Submitted by antoni (not verified) on Fri, 2014-05-23 19:42.</div>
    <div class="content">
     <p>see KDE:Extra / openSUSE_13.1</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div></div><a id="comment-20705"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/713#comment-20705" class="active">Ubuntu Softwarepaket</a></h3>    <div class="submitted">Submitted by wolfgang (not verified) on Wed, 2014-05-14 21:16.</div>
    <div class="content">
     <p>Hello,</p>
<p>In Ubuntu (Ubunt 14:01 LTS) Software Center digikam only the version 3.5 appeared available.<br>
Where can I find the version of digikam 4.0?</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20706"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/713#comment-20706" class="active">You can try my PPA:</a></h3>    <div class="submitted">Submitted by Michal Sylwester (not verified) on Thu, 2014-05-15 09:37.</div>
    <div class="content">
     <p>You can try my PPA: https://launchpad.net/~msylwester/+archive/digikam</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20708"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/713#comment-20708" class="active">works fine for me!</a></h3>    <div class="submitted">Submitted by MRIG (not verified) on Thu, 2014-05-15 19:46.</div>
    <div class="content">
     <p>works fine for me!</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-20709"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/713#comment-20709" class="active">Kubuntu 12.04 LTS</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Fri, 2014-05-16 02:57.</div>
    <div class="content">
     <p>Any chance of a version for 12.04 ?</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20727"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/713#comment-20727" class="active">Sorry, but no time to manage</a></h3>    <div class="submitted">Submitted by Michal Sylwester (not verified) on Mon, 2014-06-02 03:11.</div>
    <div class="content">
     <p>Sorry, but no time to manage several versions, for now I'm happy to have the trusty ppa working...</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div></div><a id="comment-20710"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/713#comment-20710" class="active">Installation of new Digikam 4.0</a></h3>    <div class="submitted">Submitted by Mark (not verified) on Fri, 2014-05-16 03:16.</div>
    <div class="content">
     <p>Hello, I'm new to Linux (about 6 months of use) and I would like to install the newest version of Digikam 4.0 but before I get started I would like to thank the developers for their tremendous effort they put in to make this project happen.  Thanks so much.  Ok that being said I am hoping to get a little instruction and compiling and installing the tarball. So far I've downloaded digikam4.0.0.tar.bz2. I"ve also installed the dependencies, I've gotten away with avoiding Linux applications that require compiling until now. Thanks for your help.</p>
<p>Mark</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20711"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/713#comment-20711" class="active">If you happen to use Ubuntu</a></h3>    <div class="submitted">Submitted by Le Gars (not verified) on Fri, 2014-05-16 18:15.</div>
    <div class="content">
     <p>If you happen to use Ubuntu (and on 14.04), you can look into using the commenter (Michal Sylwester) above's PPA<br>
https://launchpad.net/~msylwester/+archive/digikam</p>
<p>With the following commands:<br>
<code>sudo add-apt-repository ppa:msylwester/digikam<br>
sudo apt-get update<br>
sudo apt-get install digikam<br>
</code></p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20715"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/713#comment-20715" class="active">Installation of new Digikam 4.0</a></h3>    <div class="submitted">Submitted by Mark (not verified) on Fri, 2014-05-16 20:21.</div>
    <div class="content">
     <p>That worked perfectly. Thank you. </p>
<p>BTW It took me a few trys to leave this post on account the captcha was a little to differcult for me to make out. :)</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20716"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/713#comment-20716" class="active">* difficult </a></h3>    <div class="submitted">Submitted by Mark (not verified) on Fri, 2014-05-16 20:26.</div>
    <div class="content">
     <p>*difficult - spell check in Linux Firefox isn't so great either. :)</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-20717"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/713#comment-20717" class="active">digikam 3.1 doesn't update on ubuntu 13.04</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Sat, 2014-05-17 06:24.</div>
    <div class="content">
     <p>I tried with ppa:msylwester/digikam PPA,<br>
but when launch the<br>
<code> sudo apt-get install digicam <code><br>
Terminal answers my digikam is already at last version, but the one I have installed is version 3.1</code></code></p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-20726"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/713#comment-20726" class="active">Easy install</a></h3>    <div class="submitted">Submitted by Ian (not verified) on Fri, 2014-05-30 13:36.</div>
    <div class="content">
     <p>Thanks for the ppa instructions. This is valuable information and should have been included in the article alongside the Tarball link for us non-techies.<br>
Also, the easier way in the 'buntu distros is to add the ppa in Software Sources. E.G. go to the Software Manager, then go "Edit" - "Software Sources", "Other Software" - "Add" then enter ppa:msylwester/digikam. The update will start and Digikam 4.0 will be installed, without needing any text commands. Which are beyond most non IT peoples experience. Such as us photographers.</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-20712"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/713#comment-20712" class="active">Otherwise have a look at the</a></h3>    <div class="submitted">Submitted by Le Gars (not verified) on Fri, 2014-05-16 18:21.</div>
    <div class="content">
     <p>Otherwise have a look at the README.LOCAL file in the tarball. (It will say you need to run .bootstrap.local and some other commands). Remember the dependencies need to be the development files (so you can compile - often the dev files filenames end in '-dev')</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-20718"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/713#comment-20718" class="active">like</a></h3>    <div class="submitted">Submitted by <a href="http://pendaftaran-cpns.blogspot.com/2013/12/informasi-pendaftaran-dan-jadwal-penerimaan-CPNS-tahun.html" rel="nofollow">CPNS 2014</a> (not verified) on Sat, 2014-05-17 07:09.</div>
    <div class="content">
     <p>it's nice software</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-20729"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/713#comment-20729" class="active">local contrast tool</a></h3>    <div class="submitted">Submitted by <a href="http://pirekare.blogspot.com" rel="nofollow">Aykut</a> (not verified) on Wed, 2014-06-04 12:35.</div>
    <div class="content">
     <p>I've been using digikam for many years. The new version seems faster and more responsive. My only complaint is the new algorithm for the local contrast tool. Somehow, I was unable to get the enhancing results of the previous versions. Even at modest levels, the results look dull and unpleasant. Otherwise, many thanks for the nice work.</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-20730"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/713#comment-20730" class="active">what's new?</a></h3>    <div class="submitted">Submitted by umby (not verified) on Wed, 2014-06-04 20:53.</div>
    <div class="content">
     <p>Hi guys,<br>
I'm fully satisfied with digikam, and I say this around to your friends and colleagues, even if I can not perform some simple workflow in batches (do not keep in memory the steps of the workflow ... resizing jpeg-watermark with text) </p>
<p>I installed the new version 4.0, (open suse) but I do not see important changes compared to the 3.5.<br>
what's new in your opinion.<br>
I have two libraries around 100,000 photos, what benefits do you think?<br>
thanks<br>
umberto</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20735"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/713#comment-20735" class="active">look this file</a></h3>    <div class="submitted">Submitted by digiKam on Mon, 2014-06-16 09:29.</div>
    <div class="content">
     <p>https://projects.kde.org/projects/extragear/graphics/digikam/repository/revisions/master/entry/project/NEWS.4.0.0</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20736"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/713#comment-20736" class="active">thank you
that's ok
umby</a></h3>    <div class="submitted">Submitted by umby (not verified) on Mon, 2014-06-16 23:04.</div>
    <div class="content">
     <p>thank you<br>
that's ok<br>
umby</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div><a id="comment-20731"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/713#comment-20731" class="active">shooting yourself with not distributing binary downloads</a></h3>    <div class="submitted">Submitted by Carpur Anomal (not verified) on Wed, 2014-06-11 13:29.</div>
    <div class="content">
     <p>A new release is not a new release if there are no binary downloads for the major distros, so if you are not ready to put up official binary deb or rpm files or, much better, setup repositories for the major distros (debian, ubuntu, redhat) then  new release is not ready. Do not publish a "new release ready" blogpost, if in fact you are not ready.</p>
<p>Preparation of binary packages or distribution repositories is not some "bothersome afterwork", but it is the most important part of your development cycle - if you are not willing to make the software delivery process easier, then why are you doing it at all? </p>
<p>Please rethink your non-existent distribution policy and at put up some official deb files of this new release, thanks.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20732"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/713#comment-20732" class="active">Windows binaries would also be great</a></h3>    <div class="submitted">Submitted by Daniel (not verified) on Wed, 2014-06-11 14:36.</div>
    <div class="content">
     <p>I totally agree and I would love to see the binaries for Windows too.</p>
<p>It's such a pity that there are no binaries for this awesome great software. I'm 100% sure that there would be a lot of people willing to work with this software if there would be an easy installer/compatible binaries.</p>
<p>You could reach a lot bigger audience in providing the binaries and probable could also get some more money for this great piece of software.</p>
<p>I'm still hoping for the windows release...</p>
<p>Thanks for the great development.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20737"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/713#comment-20737" class="active">Windows installer (current code - next 4.1.0)</a></h3>    <div class="submitted">Submitted by digiKam on Tue, 2014-06-17 05:59.</div>
    <div class="content">
     <p>Hi all,</p>

<p>As current Windows packager is very busy this summer, I finally take
few hours to fix code, compile, and run windows packaging script.</p>

<p>I build a 32bits version of my Windows 7 KDE installation including
digiKam and KDE. It compiled with MSVC 2010 + few extra libraries, as
OpenCV. Not all dependencies are here and this want mean that some
extra features can be dropped.</p>

<p>Translations files are missing too. I never found a good way to have
translations files compiled here. digiKam will be in English.</p>

<p>So, this installer come without guaranties to run fine on your system.
On mine, it's start and is usable...</p>

<a href="https://www.flickr.com/photos/digikam/14437923603/"><img src="https://farm4.staticflickr.com/3920/14420281185_69885948a9_z.jpg" width="640" height="360"></a>

<p>At least <a href="https://drive.google.com/file/d/0B7yq-xFihT0_TTZTaXlUaExlaWs/edit?usp=sharing">this installer</a> is better than nothing. It must be used for testing purpose only...</p>


         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-20733"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/713#comment-20733" class="active">I agree. I m Using Digikam</a></h3>    <div class="submitted">Submitted by Alex (not verified) on Mon, 2014-06-16 09:22.</div>
    <div class="content">
     <p>I agree. I m Using Digikam for more than 5 years, supported it, and always the same trouble with "new versions": i have to trust someone i dont know (using the ppa) to get the new version. This is hardly acceptable for me. Please dont ignore this issue!</p>
<p>Anyway, this time i dit it.<br>
Result:<br>
- faster<br>
- no video playing inside digikam -&gt; it crashes (before it did)</p>
<p>All together i m already looking for a alternative, but in the linux-world i found nothing. Ideas?<br>
cheers<br>
Alex</p>
<p>p.s. i still apreciate the good and hard work and still like digikam, but...but...but...s.o.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20734"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/713#comment-20734" class="active">Phonon component.</a></h3>    <div class="submitted">Submitted by digiKam on Mon, 2014-06-16 09:26.</div>
    <div class="content">
     <p>Problem already reported to bugzilla and relevant to KDE Phonon component.</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-20738"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/713#comment-20738" class="active">LightZone</a></h3>    <div class="submitted">Submitted by Grix (not verified) on Wed, 2014-06-25 01:14.</div>
    <div class="content">
     <p>Did you try LightZone ?</p>
<p>http://lightzoneproject.org/</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20739"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/713#comment-20739" class="active">they require registration,</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Fri, 2014-06-27 09:21.</div>
    <div class="content">
     <p>they require registration, not an option</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20740"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/713#comment-20740" class="active">Registration is only for</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Sun, 2014-06-29 19:16.</div>
    <div class="content">
     <p>Registration is only for logging into download page, not to use the program.</p>
<p>anyway it's up to you</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div></div></div>
</div>