---
date: "2013-10-10T15:30:00Z"
title: "digiKam Software Collection 3.5.0 released..."
author: "digiKam"
description: "Dear all digiKam fans and users! digiKam team is proud to announce the release of digiKam Software Collection 3.5.0. This version is a bug-fix release,"
category: "news"
aliases: "/node/706"

---

<a href="http://www.flickr.com/photos/digikam/10153725064"><img src="http://farm3.staticflickr.com/2891/10153725064_2cb695a4e1_o.png" width="400" height="245"></a>

<p>Dear all digiKam fans and users!</p>

<p>digiKam team is proud to announce the release of digiKam Software Collection 3.5.0.
This version is a bug-fix release, as we are currently busy to complete <a href="http://community.kde.org/Digikam/GSoC2013#digiKam_Google_Summer_of_Code_2013_Projects_list">all GSoC 2013 projects</a> for next major release 4.0.0. Look in Release plan <a href="http://community.kde.org/Digikam/GSoC2013#Roadmap_and_Releases_Plan_including_all_GSoC-2013_works">for details...</a></p>

<p>See <a href="https://bugs.kde.org/buglist.cgi?f1=cf_versionfixedin&amp;o1=equals&amp;query_format=advanced&amp;bug_status=RESOLVED&amp;bug_status=NEEDSINFO&amp;bug_status=VERIFIED&amp;bug_status=CLOSED&amp;v1=3.5.0&amp;product=digikam&amp;product=digikamimageplugins&amp;product=kipiplugins&amp;product=showfoto">the list of files closed</a> with this release into KDE bugzilla.</p>

<p>digiKam software collection tarball can be downloaded from <a href="http://download.kde.org/stable/digikam/digikam-3.5.0.tar.bz2.mirrorlist">KDE repository</a></p>

<p>Happy digiKam</p>
<div class="legacy-comments">

  <a id="comment-20645"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/706#comment-20645" class="active">Will all GSoC 2013 projects be included in 4.0.0?</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Thu, 2013-10-10 16:45.</div>
    <div class="content">
     <p>Will all GSoC 2013 projects be included in 4.0.0? Looking at the linked website I couldn't figure out which of these projects were successfull. I wonder in particular about the tag manager, the local adjustment tool and the HDR plugin.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20647"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/706#comment-20647" class="active">changelog</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Tue, 2013-10-15 16:47.</div>
    <div class="content">
     <p>Hi</p>
<p>Please add the changelog links for 3.4.0 and 3.5.0 to the changelog page as well as to the 3.5.0 announcement.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20651"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/706#comment-20651" class="active">Done...</a></h3>    <div class="submitted">Submitted by digiKam on Sun, 2013-10-20 21:57.</div>
    <div class="content">
     <p>Changelog web page updated...</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20652"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/706#comment-20652" class="active">Thank you :)</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Tue, 2013-10-22 01:09.</div>
    <div class="content">
     <p>Thank you :)</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div></div><a id="comment-20648"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/706#comment-20648" class="active">Where to get 3.5.0 from?</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Tue, 2013-10-15 16:52.</div>
    <div class="content">
     <p>Where do Mint/Ubuntu users get 3.5.0 from? Latest we have is 3.3.0.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20649"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/706#comment-20649" class="active">I used to make packages for</a></h3>    <div class="submitted">Submitted by Philip5 (not verified) on Thu, 2013-10-17 11:34.</div>
    <div class="content">
     <p>I used to make packages for Ubuntu on my PPA but since the Kubuntu team started including them on their PPA I stopped. No use to have doublet of packages on PPAs. I see now that they only have Digikam 3.3 packed. I use my own packages of Digikam 3.5 for Ubuntu 13.04 (raring) but they not avalible anyware online for the moment.</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-20650"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/706#comment-20650" class="active">Version number inflation</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Sun, 2013-10-20 20:01.</div>
    <div class="content">
     <p>I find it a bit of exaggeration to introduce a new minor version number for 5 fixed bugs. Why do you specify a three-tier version number ("3.4.0") at all if there will never be a bugfix version release (e.g. "3.4.1")?</p>
<p>I'm not complaining about lack of new features, on the contrary, I'm very happy with Digikam the way it is. But I would expect more from a minor version bump.</p>
<p>Cheers</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-20653"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/706#comment-20653" class="active">losing EXIF? hanging on Mac?</a></h3>    <div class="submitted">Submitted by Wanthalf (not verified) on Mon, 2013-10-28 14:01.</div>
    <div class="content">
     <p>Why does digikam now lose EXIF metadata when batch resizing images?</p>
<p>And why does it hang forever on my Mac since version 3.4 before even showing the thumbnails? Even after a new installation of both MacOS and macports and removing the old database and thumbnail cache the situation is the same... (I cannot find where the main configuration is stored.)</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20654"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/706#comment-20654" class="active">or...?</a></h3>    <div class="submitted">Submitted by Wanthalf (not verified) on Mon, 2013-10-28 14:11.</div>
    <div class="content">
     <p>Ok. I am puzzled. Now it suddenly keeps EXIF. Autorotation works sometime, and sometimes not, however. Strange.</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-20655"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/706#comment-20655" class="active">Mint 15</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Wed, 2013-11-06 14:53.</div>
    <div class="content">
     <p>I still don't know how to get digiKam 3.5.0 on Mint 15, and I need 3.5.0 due to a very important metadata bug being fixed.</p>
         </div>
    <div class="links">» </div>
  </div>

</div>