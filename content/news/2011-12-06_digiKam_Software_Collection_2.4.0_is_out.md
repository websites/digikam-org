---
date: "2011-12-06T14:45:00Z"
title: "digiKam Software Collection 2.4.0 is out..."
author: "digiKam"
description: "Dear all digiKam fans and users! After one month of development, digiKam team is proud to announce the digiKam Software Collection 2.4.0, as bug-fixes release,"
category: "news"
aliases: "/node/635"

---

<a href="http://www.flickr.com/photos/digikam/6465734631/" title="digikam-histogramview by digiKam team, on Flickr"><img src="http://farm8.staticflickr.com/7033/6465734631_e698d66eac_z.jpg" width="640" height="180" alt="digikam-histogramview"></a>

<p>Dear all digiKam fans and users!</p>

<p>After one month of development, digiKam team is proud to announce the digiKam Software Collection 2.4.0, as bug-fixes release, including a total of <a href="https://projects.kde.org/projects/extragear/graphics/digikam/repository/revisions/master/entry/project/NEWS.2.4.0">25 fixed bugs</a>. In this release, histogram view from right sidebar have been improved for better image analyse.</p>

<p>Close companion Kipi-plugins is released along with digiKam 2.4.0. This release features some bugs fixes and translations update.</p>

<p>As usual, digiKam software collection tarball can be downloaded from <a href="http://sourceforge.net/projects/digikam/files">SourceForge Repository</a>.</p>

<p>Happy digiKam...</p>
<div class="legacy-comments">

  <a id="comment-20137"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/635#comment-20137" class="active">Thanks for the</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Tue, 2011-12-06 23:54.</div>
    <div class="content">
     <p>Thanks for the release.</p>
<p>What's the point of the third number in the release version btw? With montly releases it seems to have little to no purpose. It would look more clean if it were to be dropped completely e.g 2.5.0 vs 2.5.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20139"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/635#comment-20139" class="active">no, third release number still used sometime...</a></h3>    <div class="submitted">Submitted by digiKam on Wed, 2011-12-07 09:07.</div>
    <div class="content">
     <p>...for example, 2.1.1 release exist with small translations update...</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20140"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/635#comment-20140" class="active">And 2.4.1 release exists with</a></h3>    <div class="submitted">Submitted by Philip Johnsson (not verified) on Wed, 2011-12-07 12:03.</div>
    <div class="content">
     <p>And 2.4.1 release exists with this release. Not sure what's different in 2.4.1 to 2.4.0 though. Didn't find any reason in the changelog och news files. Maybe just some cleaning and tarball repackage?</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20141"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/635#comment-20141" class="active">Nicolas Lécureuil said in G+...</a></h3>    <div class="submitted">Submitted by digiKam on Wed, 2011-12-07 12:11.</div>
    <div class="content">
     <p>... "2.4.0 didn't had all i18n data, so 2.4.1 just fix localisation install."</p>
<p>digiKam</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div></div><a id="comment-20138"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/635#comment-20138" class="active">Great i like small fix</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Wed, 2011-12-07 07:59.</div>
    <div class="content">
     <p>Great i like small fix realases ... :D ... good job guys for lot of works and fixes ... soon we will get most adwanced photo managment software ...</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-20142"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/635#comment-20142" class="active">KDE4 Dependency </a></h3>    <div class="submitted">Submitted by Woodstock69 (not verified) on Thu, 2011-12-08 00:59.</div>
    <div class="content">
     <p>Love Digikam - both on Linux and that other OS, however its dependency on KDE4 is a little annoying.</p>
<p>Is it possible to make Digikam autonomous like Libreoffice? I'm running KDE3.5.10 (yes, I have ancient hardware) and the latest Libreoffice and GIMP work fine on it. Digikam however is its Achilles heel, stuck at 0.9.6.</p>
<p>Otherwise, keep up the fantastic work on this brilliant piece of software.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20155"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/635#comment-20155" class="active">Hi!
I run KDE4, and Digikam,</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Sat, 2011-12-17 23:01.</div>
    <div class="content">
     <p>Hi!<br>
I run KDE4, and Digikam, on an 2002 Pentium IV. Works fairly well. I will recommend upgrading to KDE4 for several reasons. Performance is much better now than in the earlier KDE4 versions.</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-20143"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/635#comment-20143" class="active">Windows binaries, please? :)</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Thu, 2011-12-08 14:25.</div>
    <div class="content">
     <p>Windows binaries, please? :)</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20144"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/635#comment-20144" class="active">Working on it</a></h3>    <div class="submitted">Submitted by Ananta Palani (not verified) on Fri, 2011-12-09 11:37.</div>
    <div class="content">
     <p>There is a problem with this version I am trying to resolve on Windows before I package it. I hope to get it working this weekend.</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-20145"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/635#comment-20145" class="active">Can't build it under debian</a></h3>    <div class="submitted">Submitted by Leszek (not verified) on Fri, 2011-12-09 13:27.</div>
    <div class="content">
     <p>Hey a great new release but unfortunately I can't build it under Debian Testing/Unstable because of some qjson errors. The libqjson &amp; libqjson-dev version in Debian are at version 0.7.1. But strangely I get those errors when compiling</p>
<p><code></code></p>
<p>/home/leszek/Downloads/work/digikam/digikam-2.4.1/extra/libkvkontakte/libkvkontakte/vkontaktejobs.cpp:121: undefined reference to `QJson::Parser::Parser()'<br>
/home/leszek/Downloads/work/digikam/digikam-2.4.1/extra/libkvkontakte/libkvkontakte/vkontaktejobs.cpp:123: undefined reference to `QJson::Parser::parse(QByteArray const&amp;, bool*)'<br>
/home/leszek/Downloads/work/digikam/digikam-2.4.1/extra/libkvkontakte/libkvkontakte/vkontaktejobs.cpp:136: undefined reference to `QJson::Parser::errorString() const'<br>
/home/leszek/Downloads/work/digikam/digikam-2.4.1/extra/libkvkontakte/libkvkontakte/vkontaktejobs.cpp:121: undefined reference to `QJson::Parser::~Parser()'<br>
/home/leszek/Downloads/work/digikam/digikam-2.4.1/extra/libkvkontakte/libkvkontakte/vkontaktejobs.cpp:121: undefined reference to `QJson::Parser::~Parser()'<br>
collect2: ld returned 1 exit status</p>
<p></p>
<p>Perhaps someone could help. Either its a problem with digikam or the Debian qjson package.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20146"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/635#comment-20146" class="active">linking pb</a></h3>    <div class="submitted">Submitted by digiKam on Fri, 2011-12-09 13:30.</div>
    <div class="content">
     <p>It's a linking problem, not a compilation problem. Sound like linker cannot find all qjson library object on your system.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20147"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/635#comment-20147" class="active">Thx for the info. I guess the</a></h3>    <div class="submitted">Submitted by Leszek (not verified) on Fri, 2011-12-09 17:28.</div>
    <div class="content">
     <p>Thx for the info. I guess the problem is caused by this change here: http://web.archiveorange.com/archive/v/mK9nVL5bLqiRIBYPNtuv</p>
<p>After grabing FindQJson.cmake from an older 0.7.1-3 debian package and copying it to the right location I could successfully compile Digikam 2.4.1.<br>
So I suggest contacting the Debian Maintainer of this package to get a solution or to fix it in the Digikam Code. (As far as I understood the above link it can be fixed both ways, but I am no QJson expert, so I really don't know)</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20148"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/635#comment-20148" class="active">To make Digikam compile, you</a></h3>    <div class="submitted">Submitted by Yannick (not verified) on Sun, 2011-12-11 00:58.</div>
    <div class="content">
     <p>To make Digikam compile, you can add this to extra/libkvkontakte/CMakeLists.txt:</p>
<p> SET_TARGET_PROPERTIES(kvkontakte PROPERTIES LINK_FLAGS -Wl,-lqjson)</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20150"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/635#comment-20150" class="active">Yes! this works for</a></h3>    <div class="submitted">Submitted by Ivan (not verified) on Thu, 2011-12-15 18:07.</div>
    <div class="content">
     <p>Yes! this works for kvkontakte, thanks!</p>
<p>But I still having problem compiling in debian because kdelib 4.7 is not available (only in debian experimental!)</p>
<p>In wich debian version have you compiled? there are some workaround?</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20154"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/635#comment-20154" class="active">Debian packages - 2.4.1</a></h3>    <div class="submitted">Submitted by <a href="http://packages.debian.org/digikam" rel="nofollow">Mark Purcell</a> (not verified) on Sat, 2011-12-17 10:44.</div>
    <div class="content">
     <p>I have now uploaded the Debian packages, again to experimental, due to the other dependencies.</p>
<p>In short, using Debian, you do need to upgrade KDE SC in experimental to use digikam 2.4.1.</p>
<p>Mark</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div></div></div></div>
</div>
