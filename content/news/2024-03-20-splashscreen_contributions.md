---
date: "2024-03-20T00:00:00"
title: "Splashscreen contributions"
author: "digiKam Team"
description: "For the next digiKam releases, digiKam team needs photographs for digiKam and Showfoto splash-screens."
category: "news"
---

[![](https://www.digikam.org/img/photos/splash-digikam/8.3.0.png "digiKam 8.3.0 Splash-screen")](https://www.digikam.org/img/photos/splash-digikam/8.3.0.png)

For the next digiKam releases, the digiKam team needs photographs for digiKam and Showfoto splash-screens.
Proposing photo samples as splash-screens is a simple way for users to contribute to digiKam project. The pictures must be correctly exposed/composed, and the subject must be chosen from a real photographer's inspiration. Note that we will add a horizontal frame to the bottom of the image as in the current splashes.

If somebody wants to contribute with nice photographs, you can take a look at the previous selected items and follow the instructions to contribute [to the dedicated page](https://www.digikam.org/contribute/splashscreens/).

We are waiting your best photo samples to review.
