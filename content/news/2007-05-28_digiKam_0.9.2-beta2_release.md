---
date: "2007-05-28T17:05:00Z"
title: "digiKam 0.9.2-beta2 release"
author: "gerhard"
description: "Dear all digiKam fans! The success story continues with the 0.9.2-beta2 release including a new light table (compare feature). The 0.9.2 final release is on"
category: "news"
aliases: "/node/223"

---

<p>Dear all digiKam fans!<br>
The success story continues with the <a href="http://digikam3rdparty.free.fr/0.9.2_release">0.9.2-beta2</a> release including a new light table (compare feature). The 0.9.2 final release is on track for mid June publishing. This beta2 version is rock-solid, don't hesitate to use it for production. Beta1 and beta2 are rather used to improve usability than debugging.</p>
<p>In order to make digiKam 0.9.2-beta2 compile you need to compile and install libkexiv2 and libkdcraw first (unless you complied them for beta1 already).</p>
<p>The library tarballs can be downloaded from <a href="http://sourceforge.net/project/showfiles.php?group_id=149779">(1) SourceForge</a>.<br>
The digiKam tarball + documentation can be downloaded from <a href="http://sourceforge.net/project/showfiles.php?group_id=42641">(2) SourceForge</a> as well.</p>
<p><em>So what's new on digiKam planet?</em><br>
--------------------------------<br>
General       : digiKam has a powerful new tool to compare similar images side by side: the Light Table. Demos and screenshots are <a href="http://www.digikam.org/?q=node/221">here</a> and <a href="http://www.digikam.org/?q=node/222">here.</a><br>
----------------------------------------------------------------------------</p>
<p>digiKam BUGFIXES FROM KDE BUGZILLA (alias B.K.O | <a href="http://bugs.kde.org">http://bugs.kde.org</a>):</p>
<p>040 ==&gt; 135048 : Easily compare similar images using a light-table.<br>
041 ==&gt; 145159 : Improvements to the light-table.<br>
042 ==&gt; 145204 : Small issues with the light-table.<br>
043 ==&gt; 145227 : Change ratings via short-cuts in the light-table.<br>
044 ==&gt; 145236 : Small wishes for the light-table.<br>
045 ==&gt; 145237 : Small wishes for the light-table (2).<br>
046 ==&gt; 145170 : Always allow zooming in/out in imageeditor.<br>
047 ==&gt; 145078 : Ctrl-Y untied from Redo.<br>
048 ==&gt; 145083 : Space and Shift-Space isn't used for navigation between images.<br>
049 ==&gt; 145077 : Ctrl-W and Ctrl-Q shortcuts not tied to proper actions:fixed<br>
050 ==&gt; 145558 : In menu view: window size/original size: ctrl-shift-z obsolete?<br>
051 ==&gt; 144640 : CTRL-P does nothing in Album GUI: fixed to print action<br>
052 ==&gt; 144643 : Ctrl-Shift-A does not deselect in Album GUI: fixed<br>
053 ==&gt; 144644 : Ctrl-0 does not set zoom to 100% in Preview mode: Alt+Ctrl+0<br>
054 ==&gt; 144650 : Shift-Space doesn't work as PageUp.<br>
055 ==&gt; 145079 : Ctrl-A, Ctrl-Shift-A don't peform proper selection actions.<br>
056 ==&gt; 145627 : Showfoto /path/to/directory doesn't work, while the "open dir" feature exists.<br>
057 ==&gt; 146012 : Dragging an image over a tag in "tag filters" panel crashes digiKam.<br>
058 ==&gt; 146032 : Panning doesn't work in Light Table.</p>

<div class="legacy-comments">

</div>