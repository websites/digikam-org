---
date: "2011-01-17T16:27:00Z"
title: "5 Splash Screens to Spice up digiKam"
author: "Dmitri Popov"
description: "Every new version of digiKam features its own unique splash screen. But you don’t have to wait for the next digiKam release to get a"
category: "news"
aliases: "/node/565"

---

<p>Every new version of digiKam features its own unique splash screen. But you don’t have to wait for the next digiKam release to get a new splash screen. Here are a few ready-to-go designs created by your truly. <a href="http://scribblesandsnaps.wordpress.com/2011/01/17/5-splash-screens-to-spice-up-digikam/">Continue to read</a></p>

<div class="legacy-comments">

</div>