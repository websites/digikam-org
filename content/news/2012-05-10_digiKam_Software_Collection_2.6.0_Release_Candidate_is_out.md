---
date: "2012-05-10T14:48:00Z"
title: "digiKam Software Collection 2.6.0 Release Candidate is out..."
author: "digiKam"
description: "Dear all digiKam fans and users! digiKam team is proud to announce the candidate release of digiKam Software Collection 2.6.0. With this release, digiKam include"
category: "news"
aliases: "/node/653"

---

<a href="http://www.flickr.com/photos/digikam/7170729158/" title="digiKam MACOSX by digiKam team, on Flickr"><img src="http://farm8.staticflickr.com/7244/7170729158_c4feea9276_c.jpg" width="800" height="450" alt="digiKam MACOSX"></a>

<p>Dear all digiKam fans and users!</p>

<p>digiKam team is proud to announce the candidate release of digiKam Software Collection 2.6.0.</p>

<p>With this release, digiKam include a lots of bugs fixes about XMP sidecar file supports. New features have been also introduced to last <a href="http://techbase.kde.org/Projects/Digikam/CodingSprint2011">Coding Sprint from Genoa</a>. You can read a <a href="http://dot.kde.org/2012/02/22/digikam-team-meets-genoa-italy">resume of this event</a> to dot KDE web page. Thanks again to <a href="http://ev.kde.org">KDE-ev</a> to sponsorship digiKam team...</p>

<p>digiKam include now a progress manager to control all parallelized process running in background. This progress manager is also able to follow processing from Kipi-plugins.</p>

<a href="http://www.flickr.com/photos/digikam/6812381420/" title="progressmanager by digiKam team, on Flickr"><img src="http://farm8.staticflickr.com/7055/6812381420_1463394eba_z.jpg" width="640" height="256" alt="progressmanager"></a>

<p>Also, a new Maintenance Tool have been implemented to simplify all maintenance tasks to process on your whole collections.</p>

<a href="http://www.flickr.com/photos/digikam/6812384920/" title="maintenancetool01 by digiKam team, on Flickr"><img src="http://farm8.staticflickr.com/7062/6812384920_cb3424fd22_z.jpg" width="640" height="256" alt="maintenancetool01"></a>

<p>Kipi-plugins includes a new tool to export your collections to <a href="http://imageshack.us/">ImageShack Web Service</a>. Thanks to <b>Victor Dodon</b> to contribute.</p>

<a href="http://www.flickr.com/photos/digikam/6812381534/" title="imageshackexport by digiKam team, on Flickr"><img src="http://farm8.staticflickr.com/7205/6812381534_b1282f3bcb_z.jpg" width="640" height="256" alt="imageshackexport"></a>

<p>digiKam have been ported to <a href="http://www.littlecms.com/">LCMS version 2</a> color management library by <b>Francesco Riosa</b>. Also a new tool dedicated to manage colors from scanned <a href="http://en.wikipedia.org/wiki/Reversal_film">Reversal Films</a> have been implemented by <b>Matthias Welwarsky</b>.

<a href="http://www.flickr.com/photos/digikam/6955685535/" title="negativefimtool by digiKam team, on Flickr"><img src="http://farm8.staticflickr.com/7059/6955685535_48cf2bcdfc_z.jpg" width="640" height="256" alt="negativefimtool"></a>

</p><p>This is pre-production release. Use it carefully. Please report all bugs to KDE bugzilla following indications <a href="http://www.digikam.org/drupal/support">from this page</a>. The release plan can be seen <a href="http://www.digikam.org/about/releaseplan">at this url</a>.</p>

<p>See <a href="https://projects.kde.org/projects/extragear/graphics/digikam/repository/revisions/1230546e9acaebc5e8da1c550f20cc49f8add81d/entry/NEWS">the list of digiKam file closed</a> with this release into KDE bugzilla.</p>

<p>See <a href="https://projects.kde.org/projects/extragear/graphics/kipi-plugins/repository/revisions/6853a3d0c2d05e6e9734922e9d1cd432dee270c0/entry/NEWS">the list of Kipi-plugins file closed</a> with this release into KDE bugzilla.</p>

<p>digiKam software collection tarball can be downloaded from <a href="http://sourceforge.net/projects/digikam/files">SourceForge Repository</a></p>

<p>Happy digiKam testing...</p>
<div class="legacy-comments">

  <a id="comment-20225"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/653#comment-20225" class="active">Links not working</a></h3>    <div class="submitted">Submitted by james (not verified) on Thu, 2012-05-10 19:13.</div>
    <div class="content">
     <p>That's a great news, but as already said for the previous BETA, the links at the end of your article are not working (show "403 Forbidden"):<br>
"See the list of digiKam file closed with this release into KDE bugzilla.<br>
See the list of Kipi-plugins file closed with this release into KDE bugzilla."</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20226"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/653#comment-20226" class="active">Compiling in mac</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Thu, 2012-05-10 23:12.</div>
    <div class="content">
     <p>I see that you've compiled digikam on mac, that's pretty cool, I am trying to build that too, but digikam complains about not finding marble.<br>
Could you point somewhere to look for the instrucctions on getting marble on osx ready for digikam?</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20227"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/653#comment-20227" class="active">look instructions from my tutorial...</a></h3>    <div class="submitted">Submitted by digiKam on Fri, 2012-05-11 14:22.</div>
    <div class="content">
     <p>https://projects.kde.org/projects/extragear/graphics/digikam/digikam-software-compilation/repository/revisions/master/entry/README.MACOSX</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div><a id="comment-20228"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/653#comment-20228" class="active">Thank you</a></h3>    <div class="submitted">Submitted by DrSlony (not verified) on Mon, 2012-05-14 19:22.</div>
    <div class="content">
     <p>Thank you everyone involved for your hard work!</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-20229"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/653#comment-20229" class="active">No Kipi plugins found</a></h3>    <div class="submitted">Submitted by Bernd (not verified) on Thu, 2012-05-17 09:22.</div>
    <div class="content">
     <p>First thanks for the excellent job...</p>
<p>I followed the instructions in README.MACOSX included in digikam-software-compilation-2.6.0-rc.tar.gz<br>
Everything compiled fine on Lion 10.7.4 and I'm able to start digikam but the preferences of "kipi plugins" shows 0 Kipi plugins found<br>
and therefore no thumbnails/images are shown in the main window.</p>
<p>I'm sure that I missed something to install or to configure....</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-20232"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/653#comment-20232" class="active">Great software...</a></h3>    <div class="submitted">Submitted by Daniel (not verified) on Mon, 2012-05-21 11:52.</div>
    <div class="content">
     <p>... but unfortunately the links are still not working.</p>
<p>Is there any hope for a Windows version?</p>
<p>And when will be the final version released? Release plan is not yet updated.</p>
<p>Thanks again for the great work and I'm looking forward using digikam 2.6 on my windows 7 64bit system :-)</p>
<p>Best regards,<br>
 Daniel</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20238"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/653#comment-20238" class="active">Same here - I'd love to test</a></h3>    <div class="submitted">Submitted by Hank (not verified) on Tue, 2012-05-22 14:25.</div>
    <div class="content">
     <p>Same here - I'd love to test a windows version.</p>
<p>Thanks for a great program,</p>
<p>Regards, Hank.</p>
         </div>
    <div class="links">» </div>
  </div>
</div>
</div>
