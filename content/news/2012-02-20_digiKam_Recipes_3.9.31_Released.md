---
date: "2012-02-20T09:54:00Z"
title: "digiKam Recipes 3.9.31 Released"
author: "Dmitri Popov"
description: "Besides a handful of minor tweaks, the new version of the digiKam Recipes ebook includes the following new material: Prevent Photos in digiKam from Disappearing"
category: "news"
aliases: "/node/645"

---

<p>Besides a handful of minor tweaks, the new version of the&nbsp;<a href="http://dmpop.homelinux.com/digikamrecipes/">digiKam Recipes</a>&nbsp;ebook includes the following new material:</p>
<ul>
<li>Prevent Photos in digiKam from Disappearing</li>
<li>Simple Color Toning</li>
<li>Simulate a Washed Out Effect</li>
</ul>
<p><a href="http://dmpop.homelinux.com/digikamrecipes"><img class="alignnone" title="digikamrecipes" src="https://s3-eu-west-1.amazonaws.com/digikamrecipes/digikamrecipes_b.png" alt="" width="300" height="500"></a></p>
<p><a href="http://scribblesandsnaps.wordpress.com/2012/02/20/digikam-recipes-3-9-31-released/">Continue to read</a></p>

<div class="legacy-comments">

</div>