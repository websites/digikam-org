---
date: "2024-05-13T00:00:00"
title: "digiKam Recipes 2024-05-13 released"
author: "Dmitri Popov"
description: "A new revision of the digiKam Recipes book is available"
category: "news"
---

A new revision of [digiKam Recipes](https://dmpop.gumroad.com/l/digikamrecipes) is available for your reading pleasure. The new version covers the auto tagging feature introduced in digiKam 8.3 and explains how to run digiKam in a container.

If you bought the book through Gumroad, you'll find the new revision in the **Library** section. The book purchased through Google Play should be updated automatically to the latest version. If you have problems getting the latest revision of the book, contact the author at _dmpop@cameracode.coffee_

The digiKam Recipes book is available from [Google Play Store](https://play.google.com/store/books/details/Dmitri_Popov_digiKam_Recipes?id=T83DBAAAQBAJ) and [Gumroad](https://gumroad.com/l/digikamrecipes/).