---
date: "2006-09-15T22:20:00Z"
title: "digiKam 0.9.0-beta2 and digiKamImagePlugins 0.9.0-beta2 are out there: catch them!"
author: "Anonymous"
description: "Here we go again: another step beyond on the path to the first stable release of the 0.9 series for digiKam and digikamImagePlugins. After a"
category: "news"
aliases: "/node/134"

---

<p>Here we go again: another step beyond on the path to the first stable release of the 0.9 series for digiKam and digikamImagePlugins.</p>
<p>After a period of hard and intensive work, the digiKam community brings to you all the new <strong>digiKam</strong> and <strong>DigikamImagePlugins 0.9.0-beta2</strong> releases with some <strong>new features</strong>, and others have been improved:</p>
<ul>
<li>New option to start image sequence number index with a custom value.
</li><li>New option to add camera name to target download file name.
</li><li>New option to create download folder per file format (e.g. JPG and CR2)
</li><li>New option to set date format of auto-created albums.
</li><li>New option to process a batch creation of all albums items thumbnails.
</li><li>New option to set image as tag thumbnail on Tag Filters View using D&amp;D.
</li><li>New option to set a matching condition to use between tags in Tag Filters View (OR or AND condition).
</li><li>New color scheme theme 'Digikasa' from Sergio Di Rio Mare
</li></ul>
<p> And a lot of <strong>bugs</strong> have been <strong>fixed</strong> since the 0.9.0-beta1 release:</p>
<ul>
<li>121691 : Problems with Downloading images from Camera.
</li><li>131034 : Display a mini-view of the photo currently transferred, i.e. better visual feedback.
</li><li>131603 : Orientation of RAW-images (especially Canons *.cr2)
</li><li>131532 : Minolta exception code can break EXIF rotation
</li><li>131550 : digikam/showfoto can't show jpeg image under PowerPC
</li><li>131549 : endianess problem under Linux-PowerPC (with png images at least)
</li></ul>
<p>Do you want to know more about all the changes? Have a look at the<a href="http://websvn.kde.org/trunk/extragear/graphics/digikam/NEWS?rev=582129&amp;view=markup"> NEWS </a> file.</p>
<p>Are you interested in some of our plans for the future and in what is still missing for the final 0.9 release? Have a look at the<a href="http://websvn.kde.org/trunk/extragear/graphics/digikam/TODO?rev=579142&amp;view=markup"> TODO</a> file.</p>
<p>As you know, this is not an stable release but a beta one, which means that we are asking users to test this version of digiKam and to report bugs. Now, you have the chance to help us, if you have found a bug, please report it to the bug tracking system at bugs.kde.org. In a first step, make sure that there is not already a bug report for your problem. A list of open bug reports for digikam can be found <a href="http://bugs.kde.org/buglist.cgi?product=digikam&amp;bug_status=UNCONFIRMED&amp;bug_status=NEW&amp;bug_status=ASSIGNED&amp;bug_status=REOPENED"> here</a>, and for digikamimageplugins in this <a href="http://bugs.kde.org/buglist.cgi?product=digikamimageplugins&amp;bug_status=UNCONFIRMED&amp;bug_status=NEW&amp;bug_status=ASSIGNED&amp;bug_status=REOPENED"> URL</a>.</p>
<p>Come on! what are you waiting for? Get your packages <a href="http://sourceforge.net/project/showfiles.php?group_id=42641"> here</a>!</p>

<div class="legacy-comments">

</div>