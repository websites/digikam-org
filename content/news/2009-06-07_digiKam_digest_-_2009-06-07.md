---
date: "2009-06-07T23:39:00Z"
title: "digiKam digest - 2009-06-07"
author: "Anonymous"
description: "Outstanding work this week was done on: First visible effect of porting to Qt4 model list view: overlays over image thumbnails. When hovering with mouse"
category: "news"
aliases: "/node/456"

---

<style type="text/css">
tr.bugs { border-bottom: 1px solid white }
tr.bugs td, tr.bugs th { text-align: center; padding: 3px }

</style><p>Outstanding work this week was done on:</p>
<ul>
<li>
<p>First visible effect of porting to Qt4 model list view: overlays over image thumbnails. When hovering with mouse over "plate" user will see small icons for loss-less rotating of image, rating, selection and probably more in future. Icons are faded in and out with nice looking animation, also rating widget interaction was improved.</p>
<p><a href="http://www.flickr.com/photos/mikmach/3603500415/" title="overlays.png by mikmach, on Flickr"><img src="http://farm4.static.flickr.com/3323/3603500415_70efbc9e93_m.jpg" width="240" height="218" alt="overlays.png"></a></p>
</li>
<li>
<p>Optimizations of database queries.</p>
</li>
<li>
<p>Reorganization of code with moving of 3rd party libs to one place for clarity.</p>
</li>
<li>
<p>Support for <a href="http://www.libpgf.org">PGF</a> wavelet-based image format. This<br>
format will be used in future as internal thumbnail cache.</p>
<p><a href="http://www.flickr.com/photos/mikmach/3604313334/" title="libpgf.png by mikmach, on Flickr"><img src="http://farm4.static.flickr.com/3559/3604313334_082745d295_m.jpg" width="152" height="240" alt="libpgf.png"></a></p>
</li>
<li>
<p>Eraser tool for Liquid Rescale plugin. Now editing of masks will be much simpler.</p>
<p><a href="http://www.flickr.com/photos/mikmach/3604312976/" title="eraser.png by mikmach, on Flickr"><img src="http://farm4.static.flickr.com/3625/3604312976_6eee260a43_m.jpg" width="240" height="185" alt="eraser.png"></a></p>
</li>
<li>
<p>Simplification of UI in printing kipi-plugin.</p>
</li>
<p>Also small improvements on digest itself: less crude table, links to commits in<br>
 <a href="http://websvn.kde.org">WebSVN</a> and discussions<br>
about bugs on <a href="http://bugs.kde.org">BKO</a>.</p>
<hr>
<table summary="Bugs and wishes dealt with in last week">
<thead>
<tr class="bugs">
<th></th>
<th>Opened bugs</th>
<th>Opened last week</th>
<th>Closed last week</th>
<th>Change</th>
<th>Opened wishes</th>
<th>Opened last week</th>
<th>Closed last week</th>
<th>Change</th>
</tr>
</thead>
<tbody><tr class="bugs">
<td>digikam</td>
<td>230</td>
<td>+16</td>
<td>-13</td>
<td>3</td>
<td>268</td>
<td>+6</td>
<td>-6</td>
<td>0</td>
</tr>
<tr class="bugs">
<td>kipiplugins</td>
<td>106</td>
<td>+2</td>
<td>-4</td>
<td>-2</td>
<td>142</td>
<td>+4</td>
<td>-1</td>
<td>3</td>
</tr>
</tbody></table>
<p>
Full tables:
</p>
<ul>
<li><a href="https://bugs.kde.org/component-report.cgi?product=digikam">digiKam</a></li>
<li><a href="https://bugs.kde.org/component-report.cgi?product=kipiplugins">KIPI-plugins</a></li>
</ul>
<hr>
<p><code></code></p>
<p>SVN commit <a href="http://websvn.kde.org/?view=rev&amp;revision=976210">976210</a> by cgilles:</p>
<p>moving DBStatDlg to libdigikam. thank to MSVC 2008 to simplify developers life with weird dll concept...</p>
<p> M  +1 -1      CMakeLists.txt<br>
 A             digikam/dbstatdlg.cpp   libs/database/dbstatdlg.cpp#976209 [License: GPL (v2+)]<br>
 A             digikam/dbstatdlg.h   libs/database/dbstatdlg.h#976209 [License: GPL (v2+)]<br>
 D             libs/database/dbstatdlg.cpp<br>
 D             libs/database/dbstatdlg.h  </p>
<p></p>
<hr>
<code>
<p>SVN commit <a href="http://websvn.kde.org/?view=rev&amp;revision=976164">976164</a> by cgilles:</p>
<p>New overlay buttons over icon view item to rotate left/right images. Transformations are lossless for jpeg :<br>
we use kipi-plugin to rotate.<br>
BUG: <a href="https://bugs.kde.org/show_bug.cgi?id=134308">134308</a></p>
<p> M  +1 -0      CMakeLists.txt<br>
 M  +50 -5     digikam/digikamimageview.cpp<br>
 M  +6 -0      digikam/digikamimageview.h<br>
 AM            digikam/imagerotationoverlay.cpp   [License: GPL (v2+)]<br>
 AM            digikam/imagerotationoverlay.h   [License: GPL (v2+)]</p>
<p></p></code>
<hr>
<code>
<p>SVN commit <a href="http://websvn.kde.org/?view=rev&amp;revision=976287">976287</a> by jnarboux:</p>
<p>Apply patch of Pino Toscano to use external Lqr is available.</p>
<p>CCBUGS: <a href="https://bugs.kde.org/show_bug.cgi?id=149485">149485</a></p>
<p> M  +6 -0      CMakeLists.txt<br>
 M  +34 -21    imageplugins/contentawareresizing/CMakeLists.txt  </p>
<p></p></code>
<hr>
<code>
<p>SVN commit <a href="http://websvn.kde.org/?view=rev&amp;revision=976426">976426</a> by cgilles:</p>
<p>reset selection when rotate buttons are used.</p>
<p> M  +28 -2     imagerotationoverlay.cpp<br>
 M  +10 -2     imagerotationoverlay.h  </p>
<p></p></code>
<hr>
<code>
<p>SVN commit <a href="http://websvn.kde.org/?view=rev&amp;revision=976791">976791</a> by cgilles:</p>
<p>remove RatingBox widget. Use RatingWidget instead everywhere.<br>
RatingWidget has now a fading mode to display progressively widget<br>
This mode is used in new icon-view based on Model View to switch is edit mode when<br>
user move mouse over the widget.<br>
During fading, widget appear and rating cannot be edited. fading duration is 1500ms.<br>
This will prevent unwanted rating assignation to item when user move mouse over the item to<br>
just only select it.<br>
To really want to change rating to item, user need to stay over rating widget more than<br>
fading duration.<br>
This patch only change this behavour in iconview, not with thummbar. Thumbbar is not yet<br>
ported to Model/View, but when it will done, we will use same component than iconview and<br>
problem will be fixed automatically there.<br>
BUG: <a href="https://bugs.kde.org/show_bug.cgi?id=192425">192425</a></p>
<p> M  +0 -1      CMakeLists.txt<br>
 M  +7 -6      digikam/albumiconviewfilter.cpp<br>
 M  +18 -17    digikam/iconview.cpp<br>
 M  +26 -24    digikam/imagepreviewbar.cpp<br>
 M  +10 -6     digikam/imageratingoverlay.cpp<br>
 M  +3 -2      digikam/imageratingoverlay.h<br>
 D             digikam/ratingbox.cpp<br>
 D             digikam/ratingbox.h<br>
 M  +174 -65   digikam/ratingwidget.cpp<br>
 M  +14 -0     digikam/ratingwidget.h<br>
 M  +1 -0      libs/imageproperties/imagedescedittab.cpp  </p>
<p>http://websvn.kde.org/?view=rev&amp;revision=976791</p>
<p></p></code>
<hr>
<code>
<p>SVN commit <a href="http://websvn.kde.org/?view=rev&amp;revision=976887">976887</a> by cgilles:</p>
<p>if image is unrated, draw star properly (not 6 stars)<br>
set fading duration to 600ms<br>
CCBUGS: <a href="https://bugs.kde.org/show_bug.cgi?id=192425">192425</a></p>
<p> M  +5 -5      ratingwidget.cpp  </p>
<p></p></code>
<hr>
<code>
<p>SVN commit <a href="http://websvn.kde.org/?view=rev&amp;revision=977100">977100</a> by jnarboux:</p>
<p>Fix bug: the mask was deleted when window is resized, now it is resized.</p>
<p>CCBUGS: <a href="https://bugs.kde.org/show_bug.cgi?id=149485">149485</a></p>
<p> M  +0 -2      imageplugins/contentawareresizing/TODO<br>
 M  +2 -4      libs/widgets/imageplugins/imageguidewidget.cpp  </p>
<p></p></code>
<hr>
<code>
<p>SVN commit <a href="http://websvn.kde.org/?view=rev&amp;revision=977032">977032</a> by cgilles:</p>
<p>Display rating widget when mouse is over icon view item. By this way:<br>
- user can always see where he can click to change rating, especially with unrated item.<br>
- behavour is the same than select/rotate buttons over icon view item. It's homogeneous.</p>
<p>With previous behavour, rating widget been displayed only when mouse is over rating widget area.<br>
User need to discover this feature by searching area. Now, it's more intuitive.<br>
CCBUGS: <a href="https://bugs.kde.org/show_bug.cgi?id=192425">192425</a></p>
<p> M  +1 -1      imageratingoverlay.cpp<br>
 M  +18 -5     ratingwidget.cpp<br>
 M  +5 -2      ratingwidget.h  </p>
<p></p></code>
<hr>
<code>
<p>SVN commit <a href="http://websvn.kde.org/?view=rev&amp;revision=976979">976979</a> by aclemens:</p>
<p>If the format string is empty, don't add it to the map. Nobody will ever<br>
know what format is listed and therefore it doesn't make much sense to<br>
add it at all.<br>
Anyway an empty format should never appear in the query, but just to be<br>
sure we should continue in this case.</p>
<p> M  +3 -1      albumdb.cpp  </p>
<p></p></code>
<hr>
<code>
<p>SVN commit <a href="http://websvn.kde.org/?view=rev&amp;revision=976973">976973</a> by aclemens:</p>
<p>Instead of creating multiple sub-queries, why not just use GROUP BY in<br>
the query? This will speed up the "Database Statistics" dialog a little<br>
bit.<br>
Thanks to Stefano Rivoir for the tip (damn, used GROUP BY a lot in some<br>
project, but here I forgot it ;-))</p>
<p>Marcel,</p>
<p>I removed the code under FIXME now, since you said it is not needed<br>
anymore.</p>
<p> M  +12 -15    albumdb.cpp  </p>
<p></p></code>
<hr>
<code>
<p>SVN commit <a href="http://websvn.kde.org/?view=rev&amp;revision=977062">977062</a> by jnarboux:</p>
<p>Add circle shaped cursor whose diameter is equal to brush size when painting mask.</p>
<p>CCBUGS: <a href="https://bugs.kde.org/show_bug.cgi?id=149485">149485</a></p>
<p> M  +0 -1      imageplugins/contentawareresizing/TODO<br>
 M  +9 -1      libs/widgets/imageplugins/imageguidewidget.cpp  </p>
<p></p></code>
<hr>
<code>
<p>SVN commit <a href="http://websvn.kde.org/?view=rev&amp;revision=977055">977055</a> by cgilles:</p>
<p>Rating Widget will use fading effect if KDE control center has right options enabled</p>
<p> M  +4 -2      imageratingoverlay.cpp  </p>
<p></p></code>
<hr>
<code>
<p>SVN commit <a href="http://websvn.kde.org/?view=rev&amp;revision=977237">977237</a> by jnarboux:</p>
<p>add icon for gallery plugin</p>
<p> M  +2 -0      CMakeLists.txt<br>
 A             icons (directory)<br>
 A             icons/CMakeLists.txt<br>
 AM            icons/hi16-action-gallery.png<br>
 AM            icons/hi22-action-gallery.png<br>
 AM            icons/hi32-action-gallery.png<br>
 AM            icons/hi48-action-gallery.png<br>
 AM            icons/hisc-action-gallery.svgz<br>
 M  +1 -1      plugin_galleryexport.cpp  </p>
<p></p></code>
<hr>
<code>
<p>SVN commit <a href="http://websvn.kde.org/?view=rev&amp;revision=977301">977301</a> by cgilles:</p>
<p>enable Rating Widget fadding mode in thumbbar</p>
<p> M  +1 -0      imagepreviewbar.cpp  </p>
<p></p></code>
<hr>
<code>
<p>SVN commit <a href="http://websvn.kde.org/?view=rev&amp;revision=977452">977452</a> by cgilles:</p>
<p>move clapack to 3rdparty</p>
<p> M  +31 -31    CMakeLists.txt<br>
 M  +1 -1      digikam/CMakeLists.txt<br>
 A             libs/3rdparty/clapack (directory)   libs/dimg/filters/clapack#977409<br>
 D             libs/dimg/filters/clapack (directory)  </p>
<p></p></code>
<hr>
<code>
<p>SVN commit <a href="http://websvn.kde.org/?view=rev&amp;revision=977451">977451</a> by cgilles:</p>
<p>move sqlite2 to 3rdparty</p>
<p> M  +32 -32    CMakeLists.txt<br>
 M  +2 -2      digikam/CMakeLists.txt<br>
 A             libs/3rdparty/sqlite2 (directory)   libs/database/sqlite2#977409<br>
 D             libs/database/sqlite2 (directory)  </p>
<p></p></code>
<hr>
<code>
<p>SVN commit <a href="http://websvn.kde.org/?view=rev&amp;revision=977442">977442</a> by cgilles:</p>
<p>move libpgf in 3rdparty</p>
<p> A             3rdparty/libpgf (directory)   database/libpgf#977409<br>
 D             database/libpgf (directory)  </p>
<p></p></code>
<hr>
<code>
<p>SVN commit <a href="http://websvn.kde.org/?view=rev&amp;revision=977441">977441</a> by cgilles:</p>
<p>add new sub dir for 3rd party components</p>
<p> A             3rdparty (directory)  </p>
<p></p></code>
<hr>
<code>
<p>SVN commit <a href="http://websvn.kde.org/?view=rev&amp;revision=977437">977437</a> by cgilles:</p>
<p>first implementation of PGF image loader for digiKam image editor and Showfoto.<br>
At this moment, only loading image is supported. Saving still in my todo list.<br>
More info about PGF image format : http://www.libpgf.org<br>
CCMAIL: digikam-devel@kde.org</p>
<p> M  +7 -8      CMakeLists.txt  </p>
<p></p></code>
<hr>
<code>
<p>SVN commit <a href="http://websvn.kde.org/?view=rev&amp;revision=977483">977483</a> by cgilles:</p>
<p>Implement progress callbacks compatible with C++ and thread safe.<br>
This is the same problem than old LibRaw implementation : libpgf only permit to use a static C ansi<br>
procedure and handle progress event. An additional void pointer is now used to pass C++ class<br>
which host static method.</p>
<p>For more informations, we follow this paper :</p>
<p>http://www.newty.de/fpt/callback.html#example1</p>
<p>Julien, this is exactly a good example to do with liblqr implementation to be relly thread safe.<br>
CCMAIL: Julien@narboux.fr</p>
<p> M  +46 -41    PGFimage.cpp<br>
 M  +11 -6     PGFimage.h<br>
 M  +2 -2      PGFplatform.h  </p>
<p>http://websvn.kde.org/?view=rev&amp;revision=977483</p>
<p></p></code>
<hr>
<code>
<p>SVN commit <a href="http://websvn.kde.org/?view=rev&amp;revision=977484">977484</a> by cgilles:</p>
<p>implement progress callback using new libpgf api<br>
CCMAIL: Julien@narboux.fr</p>
<p> M  +28 -1     pgfloader.cpp<br>
 M  +4 -0      pgfloader.h  </p>
<p></p></code>
<hr>
<code>
<p>SVN commit <a href="http://websvn.kde.org/?view=rev&amp;revision=977534">977534</a> by mwiesweg:</p>
<p>Not only select, but set current index as well.<br>
For me it fixes the problem that initially the right arrow need to be pressed twice.</p>
<p> M  +4 -0      imagecategorizedview.cpp  </p>
<p></p></code>
<hr>
<code>
<p>SVN commit <a href="http://websvn.kde.org/?view=rev&amp;revision=977453">977453</a> by cgilles:</p>
<p>move lprof to 3rdparty</p>
<p> M  +12 -12    CMakeLists.txt<br>
 A             libs/3rdparty/lprof (directory)   libs/lprof#977409<br>
 D             libs/3rdparty/lprof/CMakeLists.txt<br>
 D             libs/lprof (directory)  </p>
<p></p></code>
<hr>
<code>
<p>SVN commit <a href="http://websvn.kde.org/?view=rev&amp;revision=977538">977538</a> by mwiesweg:</p>
<p>For me, KDirWatch is broken since KDE 4.2.4. It insists on reporting changes in the db journal file<br>
and fails to watch subdirs. Thankfully we have KDirNotify as a resort.</p>
<p>Filter out reports of the db file and its journal.</p>
<p> M  +4 -0      albummanager.cpp  </p>
<p></p></code>
<hr>
<code>
<p>SVN commit <a href="http://websvn.kde.org/?view=rev&amp;revision=977562">977562</a> by mwiesweg:</p>
<p>For the sorting by tags and by image size many images can in fact have the same sorting value<br>
(imagine an album without any rated image sorted by rating. The last sorting will be used,<br>
 because I think a stablesort is applied. This may be desirable, but there may be no last<br>
 sorting at all, in this case sorting is random / by image id)<br>
In the case of equal values by rating or by image size, use  the file name for sorting.<br>
Need second-order sorting be configurable?</p>
<p> M  +13 -2     imagesortsettings.cpp  </p>
<p></p></code>
<hr>
<code>
<p>SVN commit <a href="http://websvn.kde.org/?view=rev&amp;revision=977584">977584</a> by aclemens:</p>
<p>Add more information to the AboutData dialog, like IRC and general<br>
contact possibilities</p>
<p> M  +1 -1      digikam/main.cpp<br>
 M  +6 -2      digikam/version.h.cmake<br>
 M  +1 -1      showfoto/main.cpp  </p>
<p></p></code>
<hr>
<code>
<p>SVN commit <a href="http://websvn.kde.org/?view=rev&amp;revision=977590">977590</a> by cgilles:</p>
<p>first version of PGF writer</p>
<p> M  +53 -207   pgfloader.cpp  </p>
<p></p></code>
<hr>
<code>
<p>SVN commit <a href="http://websvn.kde.org/?view=rev&amp;revision=977633">977633</a> by jnarboux:</p>
<p>Add a new tool to erase green or red masks.</p>
<p>CCBUGS: <a href="https://bugs.kde.org/show_bug.cgi?id=149485">149485</a></p>
<p> M  +1 -1      imageplugins/contentawareresizing/TODO<br>
 M  +29 -7     imageplugins/contentawareresizing/contentawareresizetool.cpp<br>
 M  +17 -5     libs/widgets/imageplugins/imageguidewidget.cpp<br>
 M  +3 -2      libs/widgets/imageplugins/imageguidewidget.h<br>
 M  +7 -2      libs/widgets/imageplugins/imagewidget.cpp<br>
 M  +2 -1      libs/widgets/imageplugins/imagewidget.h  </p>
<p></p></code>
<hr>
<code>
<p>SVN commit <a href="http://websvn.kde.org/?view=rev&amp;revision=977731">977731</a> by cgilles:</p>
<p>Saving and loading PGF file in RGB 8 bits work fine now.</p>
<p> M  +14 -13    pgfloader.cpp  </p>
<p></p></code>
<hr>
<code>
<p>SVN commit <a href="http://websvn.kde.org/?view=rev&amp;revision=977776">977776</a> by cgilles:</p>
<p>read/write PGF in 16bits work fine now</p>
<p> M  +52 -21    pgfloader.cpp  </p>
<p></p></code>
<hr>
<code>
<p>SVN commit <a href="http://websvn.kde.org/?view=rev&amp;revision=977947">977947</a> by cgilles:</p>
<p>implement loading without image data</p>
<p> M  +25 -18    pgfloader.cpp  </p>
<p></p></code>
<hr>
<code>
<p>SVN commit <a href="http://websvn.kde.org/?view=rev&amp;revision=977976">977976</a> by cgilles:</p>
<p>pgf provide a preview extraction (scaled image version) as JPEG. great</p>
<p> M  +15 -0     thumbnailcreator.cpp  </p>
<p></p></code>
<hr>
<code>
<p>SVN commit <a href="http://websvn.kde.org/?view=rev&amp;revision=978111">978111</a> by cgilles:</p>
<p>add pgf settings panel</p>
<p> M  +1 -2      dimg/loaders/pgfloader.cpp<br>
 AM            dimg/loaders/pgfsettings.cpp   [License: GPL (v2+)]<br>
 AM            dimg/loaders/pgfsettings.h   [License: GPL (v2+)]<br>
 M  +4 -3      widgets/common/filesaveoptionsbox.cpp  </p>
<p></p></code>
<hr>
<code>
<p>SVN commit <a href="http://websvn.kde.org/?view=rev&amp;revision=978048">978048</a> by aclemens:</p>
<p>Use a normal JOIN here, this will greatly improve speed, especially when<br>
called multiple times, like we do in batchthumbnailgenerator.</p>
<p>In my (extreme) test case the thumbnails dialog was filled in only 2<br>
seconds instead of 3 minutes!!! (:-))</p>
<p>I will check further queries in the database model to see if we can<br>
improve speed there.</p>
<p> M  +1 -2      albumdb.cpp  </p>
<p></p></code>
<hr>
<code>
<p>SVN commit <a href="http://websvn.kde.org/?view=rev&amp;revision=978047">978047</a> by cgilles:</p>
<p>preview PGF with scaled image</p>
<p> M  +31 -5     dimg/loaders/pgfloader.cpp<br>
 M  +2 -3      threadimageio/pgfutils.cpp<br>
 M  +1 -0      threadimageio/previewtask.cpp  </p>
<p></p></code>
<hr>
<code>
<p>SVN commit <a href="http://websvn.kde.org/?view=rev&amp;revision=978055">978055</a> by gateau:</p>
<p>More compact ui.</p>
<p> M  +158 -198  printoptionspage.ui  </p>
<p></p></code>
<hr>
<code>
<p>SVN commit <a href="http://websvn.kde.org/?view=rev&amp;revision=978137">978137</a> by aclemens:</p>
<p>a normal JOIN should be enough here, every image will have<br>
ImageInformation in the database, at least the creation date and the<br>
format is always known.<br>
Therefore the images table doesn't have to be always fully returned.</p>
<p> M  +1 -1      imagelister.cpp  </p>
<p></p></code>
<hr>
<code>
<p>SVN commit <a href="http://websvn.kde.org/?view=rev&amp;revision=978169">978169</a> by aclemens:</p>
<p>According to some dev from the sqlite IRC channel, it is always better to<br>
tell sqlite exactly what you want to do, otherwise it might try to<br>
optimize a query in a wrong way. If you want to use a normal INNER JOIN,<br>
tell sqlite explicitly to do so.</p>
<p> M  +7 -7      albumdb.cpp  </p>
<p></p></code>
<hr>
<code>
<p>SVN commit <a href="http://websvn.kde.org/?view=rev&amp;revision=978243">978243</a> by mwiesweg:</p>
<p>Insert initial revision of SQL for thumbnail db.<br>
Nothing in use yet, schema is open for discussion + changes.</p>
<p> M  +76 -4     thumbnaildb.cpp<br>
 M  +33 -2     thumbnaildb.h<br>
 M  +40 -4     thumbnailschemaupdater.cpp  </p>
<p></p></code>
<hr>
<code>
<p>SVN commit <a href="http://websvn.kde.org/?view=rev&amp;revision=978238">978238</a> by mwiesweg:</p>
<p>Factorize ThumbnailCreator code and prepare for addition of another<br>
storage method for thumbnails.</p>
<p> M  +267 -145  thumbnailcreator.cpp<br>
 M  +51 -6     thumbnailcreator.h<br>
 M  +24 -0     thumbnailcreator_p.h<br>
 M  +3 -2      thumbnailloadthread.cpp  </p>
<p>http://websvn.kde.org/?view=rev&amp;revision=978238</p>
<p></p></code>
<hr>
<code>
<p>SVN commit <a href="http://websvn.kde.org/?view=rev&amp;revision=978319">978319</a> by mwiesweg:</p>
<p>Don't forget to rotate the image returned the first time after creation.<br>
Prepare for database storage, where thumbs will be stored unrotated.<br>
Add documentation to ThumbnailInfo.</p>
<p> M  +19 -3     thumbnailcreator.cpp<br>
 M  +15 -6     thumbnailcreator.h<br>
 M  +1 -1      thumbnailcreator_p.h  </p>
<p></p></code>
<hr>
<code>
<p>SVN commit <a href="http://websvn.kde.org/?view=rev&amp;revision=978368">978368</a> by mwiesweg:</p>
<p>A persistent index must be used here as well, else crashes can occur.<br>
Now that the stars are shown whenever the item is entered, we dont need<br>
mouseMoved() anymore, slotEntered() is enough.</p>
<p> M  +4 -21     imageratingoverlay.cpp<br>
 M  +1 -2      imageratingoverlay.h  </p>
<p></p></code>
<hr>
<code>
<p>SVN commit <a href="http://websvn.kde.org/?view=rev&amp;revision=978480">978480</a> by cgilles:</p>
<p>compile under MSVC 9 and MinGW</p>
<p> M  +9 -1      dimg/loaders/pgfloader.cpp<br>
 M  +5 -1      threadimageio/pgfutils.cpp  </p>
<p></p></code>
<hr>
<code>
<p>SVN commit <a href="http://websvn.kde.org/?view=rev&amp;revision=978584">978584</a> by aclemens:</p>
<p>Optimizing the parser a little bit.<br>
It is always a good idea to move variable creation out of loops, to<br>
speed up the algorithm a little and save us from constructing /<br>
destructing the object all the time.</p>
<p> M  +9 -8      manualrenameinput.cpp  </p>
<p></p></code>
<hr>
<code>
<p>SVN commit <a href="http://websvn.kde.org/?view=rev&amp;revision=978638">978638</a> by cgilles:</p>
<p>add PGF as file to load in showfoto</p>
<p> M  +4 -3      imagedialog.cpp  </p>
<p></p></code>
<hr>
<code>
<p>SVN commit <a href="http://websvn.kde.org/?view=rev&amp;revision=978687">978687</a> by aclemens:</p>
<p>small optimizations: if possible, don't generate variables inside of<br>
loops to save instruction calls</p>
<p> M  +2 -1      haariface.cpp  </p>
<p></p></code>
<hr>
<code>
<p>SVN commit <a href="http://websvn.kde.org/?view=rev&amp;revision=978688">978688</a> by aclemens:</p>
<p>Use const refs in foreach loops if possible, to avoid copying of data</p>
<p>I marked one line with a FIXME, not sure if we can use a const ref here,<br>
too.<br>
What do you think?</p>
<p>CCMAIL:digikam-devel@kde.org</p>
<p> M  +1 -1      digikam/albummanager.cpp<br>
 M  +1 -1      digikam/digikamimageview.cpp<br>
 M  +3 -2      libs/threadimageio/thumbnailloadthread.cpp  </p>
<p></p></code>
<hr>
</ul>
<div class="legacy-comments">

  <a id="comment-18580"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/456#comment-18580" class="active">Link broken</a></h3>    <div class="submitted">Submitted by <a href="http://infernal-quack.net" rel="nofollow">Infernal Quack</a> (not verified) on Mon, 2009-06-08 10:38.</div>
    <div class="content">
     <p>The link to http://www.libpgf.org/ is broken</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18581"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/456#comment-18581" class="active">fixed. thanks</a></h3>    <div class="submitted">Submitted by digiKam on Mon, 2009-06-08 10:50.</div>
    <div class="content">
     <p>fixed. thanks</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-18583"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/456#comment-18583" class="active">Nice view!</a></h3>    <div class="submitted">Submitted by Troy Unrau (not verified) on Mon, 2009-06-08 17:14.</div>
    <div class="content">
     <p>That's a nice view - I like the clean positioning of the mouse-over buttons and ratings.  I don't suppose it looks quite as clean for the vertical images, however it's still very nice...  I don't suppose anyone would be interested in porting it to dolphin? .oO(where is ereslibre when I need him...)</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18586"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/456#comment-18586" class="active">Portin</a></h3>    <div class="submitted">Submitted by Fri13 on Tue, 2009-06-09 11:08.</div>
    <div class="content">
     <p>I dont thing that feature would be on Dolphin so usefull because there is more other files than photos what is managed on it and gwenview is for that purpose to view/rotate and small editing of photos and pictures.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18590"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/456#comment-18590" class="active">I'm not referring to the</a></h3>    <div class="submitted">Submitted by Troy Unrau (not verified) on Tue, 2009-06-09 19:44.</div>
    <div class="content">
     <p>I'm not referring to the specific features of rotation, but to the actual appearance and layout of the view.  Of course, in dolphin, there'd be different button overlays, but the idea of having the rating widget on the icon is nice.</p>
<p>Cheers</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div><a id="comment-18584"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/456#comment-18584" class="active">dngconverter plugin</a></h3>    <div class="submitted">Submitted by aj (not verified) on Mon, 2009-06-08 18:04.</div>
    <div class="content">
     <p>I'm using the dngconverter plugin, when opening the resulting dng files with ufraw it tells me there is no camera whitebalance and it's using fallback to auto. Is this a restriction of the dng format or a feature not yet implemented in the converter? I'm converting from canon cr2 raw format using the standalone plugin, no jpeg thumbnail and lossless compression enabled. Starting the plugin from within digikam I get the same results. I'd like to use dng for all my photos as the space savings are quite significant if you sum it up. I use openSUSE 11.1 64bit and packages from KDE:KDE4:Factory:Desktop repository, kipi-plugins version is 0.3-14.4</p>
         </div>
    <div class="links">» </div>
  </div>

</div>