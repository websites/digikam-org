---
date: "2009-09-28T10:13:00Z"
title: "Kipi-plugins 0.7.0 for KDE4 released"
author: "digiKam"
description: "Dear all digiKam fans and users! Kipi-plugins 0.7.0 maintenance release for KDE4 is out. kipi-plugins tarball can be downloaded from SourceForge at this url Kipi-plugins"
category: "news"
aliases: "/node/481"

---

<p>Dear all digiKam fans and users!</p>

<p>Kipi-plugins 0.7.0 maintenance release for KDE4 is out.</p>

<a href="http://www.flickr.com/photos/digikam/3962201828/" title="zoomrtool by digiKam team, on Flickr"><img src="http://farm3.static.flickr.com/2540/3962201828_383dac1fe3.jpg" width="500" height="400" alt="zoomrtool"></a>

<p>kipi-plugins tarball can be downloaded from SourceForge <a href="http://sourceforge.net/projects/kipi/files">at this url</a></p>

<p>Kipi-plugins will be also available for Windows. Precompiled packages can be donwloaded with KDE-Windows installer. See <a href="http://windows.kde.org">KDE-Windows project</a> for details.</p>

<p>See below the list of new features and bugs-fix coming with this release:</p>

<b>FlickrExport</b> : Plugin is now able to export to Zooomr web service (www.zooomr.com).<br>
<b>FlickrExport</b> : User can set photo visibility one by one if necessary (public, friends, family).<br>
<b>TimeAdjust</b>   : A dedicated photo time-stamp can be used to adjust difference of camera clock.<br><br>

001 ==&gt; BatchProcessImages : 205603 : Crash on finishing Batch Unsharp mask.<br>
002 ==&gt; HTMLExport         : 199533 : Enable dateframes theme in kipi-plugins [patch].<br>
003 ==&gt; HTMLExport         : 111509 : Subalbums not supported by HTML export.<br>
004 ==&gt; RAWConverter       : 205833 : Batch raw converter uses first dot in filename to determine file ending.<br>
005 ==&gt; GPSSync            : 199181 : Gps sync unable to use search field.<br>
006 ==&gt; BatchProcessImages : 197377 : Batch -&gt; Image Filtering -&gt; Unsharp Mask: Wrong Thresold.<br>
007 ==&gt; DNGConverter       : 204437 : DNGConverter turns olympus ORF unusable.<br>
008 ==&gt; GPSSync            : 206714 : Can not select multiple images in list.<br>
009 ==&gt; JPEGLossLess       : 188933 : Lossless rotate does not work for this image. Image does not rotate.<br>
010 ==&gt; FlickrExport       : 193685 : Set permissions on a per-photo base [patch].<br>
011 ==&gt; GPSSync            : 193734 : Closing geolocation correlator without saving shows two warning dialogs.<br>
012 ==&gt; TimeAdjust         : 194768 : Use a clock photo to work out the time difference of the camera clock.<br>
013 ==&gt; FlickrExport       : 172359 : Set content type and safety level on flickr upload [patch].<br>
014 ==&gt; RawConverter       : 160272 : Raw images converter changes brightness or exposure of photos.<br>
015 ==&gt; AdvancedSlideshow  : 206976 : Image display time in advanced slideshow.<br>
016 ==&gt; GPSSync            : 206995 : GPSCorrelator: "Remove" removes from all and does not update digikam-database.<br>
017 ==&gt; BatchProcessImages : 207108 : Plural used with a in front (wrong English).<br>
018 ==&gt; JPEGLossLess       : 205602 : Picture can't be display top-bottom, it is always rotated to the left or right.<br>
019 ==&gt; FlickrExport       : 139537 : Option to export to zooomr [patch for FlickrExport].<br>
020 ==&gt; GPSSync            : 183582 : Crash when trying to geolocate photographs after canceling empty map window.<br>
021 ==&gt; MetadataEdit       : 193508 : Crash when saving after editing XMP Metadata for a image.<br>
022 ==&gt; TimeAdjust         : 207620 : Adjust Time &amp; Date: Example not updated when Adjustment Type changed.<br>
023 ==&gt; AcquireImages      : 208107 : corrupted image when saving a scan.<br>
024 ==&gt; MetadataEdit       : 207997 : Let the exif import dialog remember its last location.<br>
025 ==&gt; MetadataEdit       : 200554 : Import metadata: More sensitive choice of directory in file-open dialog [patch].<br>
026 ==&gt; BatchProcessImages : 135411 : Batch resizing: Proportional (2-dim) is not proportional.<br>
027 ==&gt; BatchProcessImages : 206357 : Tools / Resize - two dimensions are not proportionately resized.<br>
028 ==&gt; BatchProcessImages : 208446 : Gwenview crash when click on CLOSE.<br>
029 ==&gt; TimeAdjust         : 204634 : "Date and Time (digitized)" is not adjusted.<br>

<div class="legacy-comments">

</div>
