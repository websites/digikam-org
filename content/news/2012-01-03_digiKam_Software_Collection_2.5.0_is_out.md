---
date: "2012-01-03T13:00:00Z"
title: "digiKam Software Collection 2.5.0 is out..."
author: "digiKam"
description: "Dear all digiKam fans and users! After one month of bugs triage in KDE bugzilla, digiKam team is proud to announce the digiKam Software Collection"
category: "news"
aliases: "/node/638"

---

<a href="http://www.flickr.com/photos/digikam/6627387563/" title="digiKam-geolocation by digiKam team, on Flickr"><img src="http://farm8.staticflickr.com/7155/6627387563_0a40d988b6_b.jpg" width="800" height="225" alt="digiKam-geolocation"></a>

<p>Dear all digiKam fans and users!</p>

<p>After one month of bugs triage in KDE bugzilla, digiKam team is proud to announce the digiKam Software Collection 2.5.0, as bug-fixes release, including a total of <a href="https://projects.kde.org/projects/extragear/graphics/digikam/repository/revisions/master/entry/project/NEWS.2.5.0">101 fixed bugs</a>.</p>

<p>Close companion Kipi-plugins is released along with digiKam 2.5.0. This release features <a href="https://projects.kde.org/projects/extragear/graphics/kipi-plugins/repository/revisions/master/entry/project/NEWS.2.5.0">41 fixed bugs</a> and translations update.</p>

<p>As usual, digiKam software collection tarball can be downloaded from <a href="http://sourceforge.net/projects/digikam/files">SourceForge Repository</a>.</p>

<p>We hope you an happy new year 2012...</p>
<div class="legacy-comments">

  <a id="comment-20158"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/638#comment-20158" class="active">macports does not work</a></h3>    <div class="submitted">Submitted by juergen (not verified) on Tue, 2012-01-03 15:02.</div>
    <div class="content">
     <p>Hi,<br>
I tried to install digikam using macports, but it did not work.</p>
<p>Please see https://trac.macports.org/ticket/32524#comment:14:<br>
"Right. It's not a compiler problem. It's an incompatibility between digikam and the latest version of boost. The developers of digikam will need to fix this. Feel free to alert them to the problem."</p>
<p>Could you please have a look at this?<br>
Best regards<br>
Juergen</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20159"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/638#comment-20159" class="active">problem already reported...</a></h3>    <div class="submitted">Submitted by digiKam on Tue, 2012-01-03 15:14.</div>
    <div class="content">
     <p>Look this bugzilla entry :</p>
<p>https://bugs.kde.org/show_bug.cgi?id=287772</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-20160"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/638#comment-20160" class="active">in</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Wed, 2012-01-04 01:44.</div>
    <div class="content">
     <p>in core/utilities/setup/setupplugins.cpp is a reference to KIPI::ConfigWidget-&gt;clearAll() and checkAll() which seem not to exist. only after removing the lines Digikam 2.5 builts fine.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20161"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/638#comment-20161" class="active">fixed..</a></h3>    <div class="submitted">Submitted by digiKam on Wed, 2012-01-04 06:53.</div>
    <div class="content">
     <p>https://bugs.kde.org/show_bug.cgi?id=290496</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20173"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/638#comment-20173" class="active">Thanks!</a></h3>    <div class="submitted">Submitted by BroX (not verified) on Thu, 2012-01-05 21:41.</div>
    <div class="content">
     <p>Works great!</p>
<p>Happy new year to all developers!</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div><a id="comment-20162"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/638#comment-20162" class="active">Hopefully there will be</a></h3>    <div class="submitted">Submitted by polarbeer (not verified) on Wed, 2012-01-04 13:15.</div>
    <div class="content">
     <p>Hopefully there will be Windows installer too for digiKam 2.5.0!</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20163"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/638#comment-20163" class="active">Windows installer is under</a></h3>    <div class="submitted">Submitted by digiKam on Wed, 2012-01-04 13:19.</div>
    <div class="content">
     <p>Windows installer is under testing here...</p>
<p>https://plus.google.com/u/0/100127992760143250422/posts/ZYA3Rt1Bnig</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20165"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/638#comment-20165" class="active">Can it be downloaded</a></h3>    <div class="submitted">Submitted by polarbeer (not verified) on Wed, 2012-01-04 20:01.</div>
    <div class="content">
     <p>Can it be downloaded somewhere or is Ananta still testing it?</p>
<p>Windows installer can be found for 2.3.0 but not for newer version (2.4.0, 2.4.1 or 2.5.0) here:</p>
<p>http://sourceforge.net/projects/digikam/files/</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20175"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/638#comment-20175" class="active">Windows 2.5.0 Installer Now on SourceForge</a></h3>    <div class="submitted">Submitted by Ananta Palani (not verified) on Fri, 2012-01-06 15:15.</div>
    <div class="content">
     <p>I have uploaded the Windows 2.5.0 installer on SourceForge. If you have any troubles please create a bug report.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20176"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/638#comment-20176" class="active">windows 2.5</a></h3>    <div class="submitted">Submitted by edkiefer (not verified) on Fri, 2012-01-06 20:07.</div>
    <div class="content">
     <p>Any progress on  Bug 277206 -problems exporting RAW (CR2) from editor</p>
<p>Has to do with opening files and saving on win platform .</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div></div></div><a id="comment-20164"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/638#comment-20164" class="active">Excellent work, thank you</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Wed, 2012-01-04 17:43.</div>
    <div class="content">
     <p>Excellent work, thank you guys.<br>
Happy new year to everyone!</p>
<p>btw: Your captcha codes aren't readable, even by humans :)</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-20174"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/638#comment-20174" class="active">Upgrade 1.9 -&gt; 2.5 - Face recognition does not work</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Fri, 2012-01-06 09:57.</div>
    <div class="content">
     <p>After upgrading from digiKam 1.9 to 2.5 I found out, that face recognition does not work, although in clean digiKam 2.5 it works. </p>
<p>Any suggestion how to upgrade without losing previous database?</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20178"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/638#comment-20178" class="active">face recognition finally</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Sat, 2012-01-07 09:31.</div>
    <div class="content">
     <p>face recognition finally works in 2.5? or did you mean face detection?</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20179"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/638#comment-20179" class="active">face detection, of course</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Sat, 2012-01-07 12:56.</div>
    <div class="content">
     <p>But it didn't work when I run digiKam with an old database from version 1.9.<br>
I had to load the old database into digiKam 2.5, save all metadata into XMP sidecars and than delete the database and let digiKam 2.5 read all the metadata from XMP sidecars. Now I have the old metadata information and working face detection.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20184"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/638#comment-20184" class="active">Face recognition</a></h3>    <div class="submitted">Submitted by <a href="http://nicofo.tuxfamily.org" rel="nofollow">nicolas</a> (not verified) on Wed, 2012-01-18 22:00.</div>
    <div class="content">
     <p>For me, face detection works (since several versions of digikam) but not face recognition.<br>
Reading the different messages, I don't understand if it is normal (feature not yet implemented) or not.</p>
<p>=&gt; if it is not yet implemented, PLEASE INDICATE IT CLEARLY IN DIGIKAM WINDOW "Scanning faces" (for instance make the option "Detect and recognize faces" unclickable (grey) and add the text "this option will be available in a future version").  Because as it is now, it just leaves the impression that "digikam is buggy, digikam doesn't work, ..."  That's a pity because besides that, it's a great soft !</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20185"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/638#comment-20185" class="active">I agree with nicolas: disable</a></h3>    <div class="submitted">Submitted by frattale (not verified) on Wed, 2012-01-18 23:13.</div>
    <div class="content">
     <p>I agree with nicolas: disable this feature until it works; otherwise I have to test it in every new release. :)<br>
Is it there a plan to make face recognition work?<br>
I love digikam and it would be nice to have this very useful feature too.</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div></div></div><a id="comment-20181"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/638#comment-20181" class="active">digiKam should be able to rename sidecars too</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Sat, 2012-01-14 16:03.</div>
    <div class="content">
     <p>digiKam should rename its XMP sidecars too. And the best would be if digiKam renamed RawTherapee PP3 files too.<br>
If I want to rename several RAW pictures with their JPGs made by RawTherape and all tagged by digiKam, the best way is to forget about it. If I rename them, I end up with albums full of XMP and PP3 files, that belong to no picture.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20183"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/638#comment-20183" class="active">digikam 2.5.0. on suse 12.1</a></h3>    <div class="submitted">Submitted by <a href="http://linux4photo.wordpress.com" rel="nofollow">wmnd</a> (not verified) on Wed, 2012-01-18 19:41.</div>
    <div class="content">
     <p>hello,<br>
can anyone explain to me how to install digikam 2.5.0. on suse 12.1 ?</p>
         </div>
    <div class="links">» </div>
  </div>
</div>
</div>
