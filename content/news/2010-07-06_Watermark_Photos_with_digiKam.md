---
date: "2010-07-06T21:03:00Z"
title: "Watermark Photos with digiKam"
author: "Dmitri Popov"
description: "While there are many ways to protect your photos from unauthorized use, watermarking still remains the simplest and probably the most effective technique that can"
category: "news"
aliases: "/node/531"

---

<p>While there are many ways to protect your photos from unauthorized use, watermarking still remains the simplest and probably the most effective technique that can help you to identify you as the creator and make it difficult to use your works without permission.</p>
<p>Although digiKam supports watermarking, this feature is hidden so well that you might not even realize that it’s there. This is because the watermarking function in digiKam is tucked under the Batch Queue Manager tool which you can use to watermark multiple photos in one go. Here is how this works in practice.<a href="http://scribblesandsnaps.wordpress.com/2010/07/06/watermark-photos-with-digikam/">Continue to read</a></p>

<div class="legacy-comments">

</div>