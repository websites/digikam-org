---
date: "2015-02-02T10:19:00Z"
title: "digiKam Quick Tip: Using Album Categories"
author: "Dmitri Popov"
description: "Did you know that you can assign categories to albums in digiKam? To do this, right-click on an album, choose Properties from the context menu,"
category: "news"
aliases: "/node/730"

---

<p>Did you know that you can assign categories to albums in digiKam? To do this, right-click on an album, choose <strong>Properties</strong> from the context menu, and the desired category from the <strong>Category</strong> drop-down list.</p>
<p><a href="https://scribblesandsnaps.files.wordpress.com/2015/01/edit_categories.png"><img src="https://scribblesandsnaps.files.wordpress.com/2015/01/edit_categories.png?w=605" alt="edit_categories" width="605" height="413"></a></p>
<p><a href="http://scribblesandsnaps.com/2015/02/02/digikam-quick-tip-using-album-categories/">Continue reading</a></p>

<div class="legacy-comments">

</div>