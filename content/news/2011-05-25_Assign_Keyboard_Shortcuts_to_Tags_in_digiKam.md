---
date: "2011-05-25T09:23:00Z"
title: "Assign Keyboard Shortcuts to Tags in digiKam"
author: "Dmitri Popov"
description: "digiKam makes it relatively easy to tag photos. Select one or several photos, expand the Caption/Tags right sidebar, tick the tags you want, and press"
category: "news"
aliases: "/node/604"

---

<p>digiKam makes it relatively easy to tag photos. Select one or several photos, expand the Caption/Tags right sidebar, tick the tags you want, and press the Apply button. You can also assign keyboard shortcuts to the often-used tags to speed up the tagging process. <a href="http://scribblesandsnaps.wordpress.com/2011/05/25/assign-keyboard-shortcuts-to-tags-in-digikam/">Continue to read</a></p>

<div class="legacy-comments">

</div>