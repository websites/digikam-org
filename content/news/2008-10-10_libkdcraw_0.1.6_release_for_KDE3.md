---
date: "2008-10-10T14:42:00Z"
title: "libkdcraw 0.1.6 release for KDE3"
author: "digiKam"
description: "libkdcraw 0.1.6 for KDE3 has been just released. The tarball can be downloaded from SourceForge at this url. This release is a backport of current"
category: "news"
aliases: "/node/379"

---

<p>libkdcraw 0.1.6 for KDE3 has been just released. The tarball can be downloaded from SourceForge <a href="https://sourceforge.net/project/showfiles.php?group_id=149779&amp;package_id=230894&amp;release_id=632155">at this url</a>.</p>

<p>This release is a backport of current implementation from KDE4 based on <a href="http://www.libraw.org">LibRaw 0.6.1 Raw decoder</a>. For more informations, take a look on <a href="http://www.digikam.org/drupal/node/372">this previous blog entry</a></p>

<p>New cameras are supported with this release: <b>Canon 50D</b>, <b>Sony A900</b>, <b>Nikon D90</b>, <b>Nikon P6000</b>, and <b>Panasonic LX3 FZ28.</b></p>

<p>Have an happy Raw workflow...</p>

<div class="legacy-comments">

  <a id="comment-17890"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/379#comment-17890" class="active">LX3 Raw</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Fri, 2008-10-24 21:49.</div>
    <div class="content">
     <p>Which version of Digikam can handle LX3 raw. Also, Will it be able to get a DNG out of the Raw?</p>
         </div>
    <div class="links">» </div>
  </div>

</div>