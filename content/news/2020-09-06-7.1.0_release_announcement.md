---
date: "2020-09-06T00:00:00"
title: "digiKam 7.1.0 is released"
author: "digiKam Team"
description: "Dear digiKam fans and users, we are proud to announce digiKam 7.1.0."
category: "news"
---

[![](https://i.imgur.com/kyw72ds.png "digikam 7.0.0 and Canon CR3 support")](https://imgur.com/kyw72ds)

Dear digiKam fans and users,

After this long summer, we are now proud to release digiKam 7.1.0. This maintenance version is a result of a long period of bug-triaging on bugzilla. It introduces plenty of fixes and some features. Check out some of the highlights listed below and discover all the changes in detail.

### Better Canon CR3 Metadata Support

digiKam tries to support as many digital cameras' file formats as possible, but support for RAW files is a big challenge. Some applications have been especially created only to support RAW files from specific cameras, as this kind of support is complex, long, and hard to maintain over time.

RAW files are not like JPEG images. Nothing is standardized and camera makers are free to change everything inside these digital containers without ever documenting it. RAW files allow camera makers to reinvent the wheel and implement hidden features, to cache metadata, or encrypt information.

When you buy an expensive camera, such as the latest Canon devices, you should expect the image provided to be seriously pre-processed by the camera firmware and ready to use immediately. This is true for JPEG, but not RAW files, where the format changes for every new camera released, as it depends on the camera's sensor data. This is also the case for the Canon CR3: the RAW format produced by this camera has required intensive reverse-engineering that the digiKam team cannot always support well. This is why we use [the powerful Libraw library](https://www.libraw.org) to post-process the RAW files on the computer. This library includes complex algorithms to support all kinds of different RAW file formats, including the Canon CR3.

[![](https://i.imgur.com/HSpXCDU.png "digikam 7.1.0 and Canon CR3 Metadata support")](https://i.imgur.com/HSpXCDU.png)

With the 7.0.0 release, the Canon CR3 Raw file metadata support was basic, with only a few properties taken from Exif. This information is used to populate the database with the main shot information captured from the camera. The goal is to be able to use some technical criteria in search of an engine later to find items in huge collections.

The CR3 file format is a complex proprietary container based on [ISO/IEC base media file format](https://en.wikipedia.org/wiki/ISO/IEC_base_media_file_format). In digiKam, historically we have used the [Exiv2 library](https://www.exiv2.org) to deal with file metadata, but this library is not yet able to extract information from CR3 images. So we needed to use an alternative, and by chance, libraw provides mechanisms to handle property tags from a client application.

For digiKam 7.1.0, we have written a metadata interface based on libraw for the CR3, and the application is now able to read a large portion of Exif tags, including GPS information, color profile, and, of course. standard IPTC and XMP containers. CR3 Metadata changes are only supported through XMP sidecar, as no write support is possible with this format.

Although the new libraw based interface is less powerful than Exiv2 (which permits a lot of operations on tags), by using libraw. digiKAm can be better compatible today with CR3. Moreever, the interface is not only compatible with CR3 files, but with all RAW files supported by libraw. Other RAW files not yet supported by Exiv2 are de facto supported, sich as Sigma X3F, Panasonix RW2, or Leica RWL for example.

# Application New Features and Improvements

Here's an unsorted list of features and improvements included in this release:

* A new Batch Queue Manager plugin has been introduced to fix Hot Pixels automatically. Most current digital cameras produce images with several brightly colored "bad pixels" when using slow shutter speeds. Night images can be ruined by these "bad pixels" for example, and the tool can fix it using a black frame subtraction method. This tool, already available for a while in Image Editor, has been improved and is now able to manage a collection of black frames from different camera models.

![Batch Queue Manager version of Hot Pixels Fixer](https://i.imgur.com/XH9IS53.png)

* A new Batch Queue Manager plugin has been introduced to apply texture over images. This decorative tool, already available for a while in Image Editor, can be appended to your photo workflow and process target images automatically.

![Batch Queue Manager version of Apply Texture](https://i.imgur.com/EC7WJfP.png)

* Regarding metadata management, we have improved the [IPTC](https://en.wikipedia.org/wiki/IPTC_Information_Interchange_Model) compatibility with [UTF-8 characters encoding](https://en.wikipedia.org/wiki/UTF-8). It's now possible to use extended characters set everywhere in legacy IPTC text containers. The older IPTC norm was originally limited to [ASCII characters](https://en.wikipedia.org/wiki/ASCII) and [XMP](https://en.wikipedia.org/wiki/Extensible_Metadata_Platform) have been published by Adobe in the past to replace IPTC without this limitation. But IPTC is still here for compatibility reasons and has evolved for better internationalization support.
It permits to host text based on non-latin alphabets such as Russian, German, Greek, Japanese, Arabic, etc.

![Entering UTF-8 Caption in IPTC With Metadata Editor](https://i.imgur.com/TD833u4.png)

### Coming Up Next

As you can see, digiKam version 7.1.0 has a lot going for it. [The bugzilla entries closed](https://bugs.kde.org/buglist.cgi?f1=cf_versionfixedin&limit=0&o1=equals&order=bug_id&product=digikam&v1=7.1.0) alone for this release are impressive, with more than 300 files closed this summer.

Next is to prepare for digiKam 7.2.0 and include new improvements for faces management written by the students during the Google Summer of Code event. We plan a few beta releases to check the non-regression and new features.

We would like to thank all users for [your support and donations](https://www.digikam.org/donate/) and all contributors, students and testers who made it possible to improve and publish this release.

digiKam 7.1.0 source code tarball, Linux 32/64 bits AppImage bundles, macOS package and Windows 32/64 bit installers can be downloaded from [this repository](https://download.kde.org/stable/digikam/).

We wish you a happy digiKaming for this autumn!