---
date: "2009-04-28T20:06:00Z"
title: "digiKam digest - introduction"
author: "Anonymous"
description: "Welcome, Because KDE-wide commit digest is a bit erratic lately I thought about replacing it in part of KDE I am especially interested in: digiKam."
category: "news"
aliases: "/node/440"

---

<p>Welcome,</p>
<p>Because KDE-wide commit digest is a bit erratic lately I thought about replacing<br>
it in part of KDE I am especially interested in: digiKam.</p>
<p>It covers development of digiKam, kipi-plugins, libkdcraw and libkexiv2.</p>
<p>Since some of code is shared in KDE framework it may be interesting also for users of other programs but it will be centered around digiKam and any editorial comments will be digiKam-centric.</p>
<p>Form will be kept as simple as possible but <em>may</em> evolve in time.</p>
<p>m.</p>

<div class="legacy-comments">

  <a id="comment-18463"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/440#comment-18463" class="active">Great.</a></h3>    <div class="submitted">Submitted by Lure (not verified) on Wed, 2009-04-29 06:00.</div>
    <div class="content">
     <p>Great.</p>
         </div>
    <div class="links">» </div>
  </div>

</div>