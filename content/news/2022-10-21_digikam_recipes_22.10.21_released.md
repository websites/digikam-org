---
date: "2022-10-21T00:00:00"
title: "digiKam Recipes 2022.10.21 released"
author: "Dmitri Popov"
description: "A new revision of the digiKam Recipes book is available"
category: "news"
---

Time for another revision of the [digiKam Recipes](https://dmpop.gumroad.com/l/digikamrecipes) book. This update features a completely revised chapter covering the versioning functionality in digiKam. It now offers a clear explanation of how versioning works in digiKam, which can help you to get the most out of this versatile feature. The _Add shell scripts to the Import module_ chapter has also been revised to include a better practical example of how to use shell scripts with the Import tool. The new _Enable and configure tooltips_ chapter describes how to put the handy tooltip tool to practical use, while the _Travel in time using advanced search_ tip demonstrates how you can use the advanced search feature to find photos from the past.

As always, all _digiKam Recipes_ readers will receive the updated version of the book automatically and free of charge. The _digiKam Recipes_ book is available from [Google Play Store](https://play.google.com/store/books/details/Dmitri_Popov_digiKam_Recipes?id=T83DBAAAQBAJ) and [Gumroad](https://gumroad.com/l/digikamrecipes/).