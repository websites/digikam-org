---
date: "2008-06-10T15:08:00Z"
title: "digiKam Fuzzy Search Tools Under Construction"
author: "digiKam"
description: "One month ago, with Marcel, we have worked on a new sets of Search Tools in digiKam for KDE4, called Fuzzy Search During my digiKam"
category: "news"
aliases: "/node/321"

---

<p>One month ago, with Marcel, we have worked on a new sets of Search Tools in digiKam for KDE4, called <b>Fuzzy Search</b></p>

<p>During my digiKam presentation at <a href="http://www.digikam.org/drupal/node/315">LGM2008</a> i have introduced the concept to be able to search duplicates items around the whole collection of photos. But the concept is not just limited to find the similars photos by using copy, it even allows user to drawn a sketch of photo what user memories and shows photos what has similar shapes and colors as on sketch.</p>

<p>This is not a new concept in fact. An old program for Linux named <a href="http://www.imgseek.net">ImgSeek</a> provide already this feature. By my opinion, it's time to update old interface of ImgSeek and make it more suitable for end users by implementing the technology into digiKam.</p>

<p>The way how we are be able to search photos, looks great for me. It's very fast, and give pertinent results with my huge collection of photos under ImgSeek (it took over one hour to learn how the old GUI worked). This is why we have decided to study the <a href="ftp://ftp.cs.washington.edu/tr/1995/01/UW-CSE-95-01-06.d/UW-CSE-95-01-06-color.ps.gz">Fast Multiresolution Image Querying</a> paper which describe how to implement these features in digiKam. The core of the method is based on image fingerprints computed with Haar wavelets theory.</p>

<p>During one month, we have backported the fingerprints generator C++ code from ImgSeek to digiKam. For performance reason, only this part is written in pure C++ in ImgSeek, all the rest of the program is written in Python. We reviewed the algorithm and rewritted the code using Qt API and we have included new interface for it, so digiKam is able to record and retrieve finterprints from database. Like ImgSeek do not use a SQL database as digiKam, but a serialized and customized binary file to host finterprint data, we have changed the algorithm to be compatible with an SQL database.</p>

<p>Like you can see on the both videos below, Fuzzy Search is implemented on unstable code for KDE4:</p>

<object width="500" height="404"><param name="movie" value="http://www.youtube.com/v/gvpWMDvDnWU&amp;hl=en"><embed src="http://www.youtube.com/v/gvpWMDvDnWU&amp;hl=en" type="application/x-shockwave-flash" width="500" height="404"></object>

<object width="500" height="404"><param name="movie" value="http://www.youtube.com/v/GH2s0J10gJs&amp;hl=en"><embed src="http://www.youtube.com/v/GH2s0J10gJs&amp;hl=en" type="application/x-shockwave-flash" width="500" height="404"></object>

<p>The first video is a Sketch Search tool in action. The editor itself is very simple to use, you only select the color and size of the brush and then you draw the template to sketch area. The second video is a tool dedicated to find similar photos by using a reference photo. The whole collection is scanned to find dublicates. The results are given in a real time using digiKams search KIO-slave. And both tools gives the results very fast from database (my collection of photos include around 10.000 photos and runs on double core CPU with 10Gb of RAM).</p>

<p>The other tool is under development but not yet suitable to find all duplicates photos from your collections. It will work like the old kipi-plugin "find duplicates" and it gives a list of all candidates. The advantage on current solution is the re-usage of digiKam icon view and the sidebar, and not a separated dialog: so integration and usability is better with the rest of digiKam..</p>

<p>In the future, we will introduce a new config to set the level of relevance of matches from database, and to display these values on the icon view. An undo/redo operation for sketch editor will be greated too.</p>

<div class="legacy-comments">

  <a id="comment-17572"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/321#comment-17572" class="active">tineye integration</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Tue, 2008-06-10 16:09.</div>
    <div class="content">
     <p>Do you think in the future you could implement a plugin to search for usage of your images on the web?  I am thinking something along the lines of what tineye offers</p>
<p>http://blog.ideeinc.com/category/tineye/</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-17573"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/321#comment-17573" class="active">Tineye sound like a great</a></h3>    <div class="submitted">Submitted by digiKam on Tue, 2008-06-10 17:23.</div>
    <div class="content">
     <p>Tineye sound like a great tool, but it's free to use ? There is an api for developpers ?</p>
<p>digiKam</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-17574"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/321#comment-17574" class="active">I was already excited about</a></h3>    <div class="submitted">Submitted by maninalift (not verified) on Tue, 2008-06-10 17:26.</div>
    <div class="content">
     <p>I was already excited about this feature but I had expected it to be initially fairly undeveloped. However, ImgSeek appears to be pretty powerful, I'll be very pleased if all of that functionality finds it's way into DigiKam.</p>
<p>Good work.</p>
<p>Always nice to have videos to watch too.</p>
<p>ps your captchas are too damn hard.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-17575"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/321#comment-17575" class="active">i second that. I wanted to</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Tue, 2008-06-10 23:40.</div>
    <div class="content">
     <p>i second that. I wanted to comment something earlier but didnt manage to get past the captcha and then i lost interest to do so</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-17576"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/321#comment-17576" class="active">Captcha settings</a></h3>    <div class="submitted">Submitted by digiKam on Wed, 2008-06-11 11:26.</div>
    <div class="content">
     <p>Captcha settings fixed...</p>
<p>digiKam</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-17580"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/321#comment-17580" class="active">Captcha problems?</a></h3>    <div class="submitted">Submitted by Fri13 (not verified) on Wed, 2008-06-11 20:44.</div>
    <div class="content">
     <p>The captcha protection is bretty hard but not too hard, tought it could set little easier but I see it's fine now :-)</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div></div>
</div>
