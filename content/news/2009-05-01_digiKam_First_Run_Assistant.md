---
date: "2009-05-01T06:28:00Z"
title: "digiKam First Run Assistant"
author: "digiKam"
description: "This week, i have work for a while on another digiKam subject: a new first run assistant. The goal of this assistant is to group"
category: "news"
aliases: "/node/442"

---

<a href="http://www.flickr.com/photos/digikam/3489922191/"><img src="http://farm4.static.flickr.com/3650/3489922191_0091586541.jpg" width="500" height="313" alt="firstrun01"></a>


<p>This week, i have work for a while on another digiKam subject: a new first run assistant.</p>

<p>The goal of this assistant is to group lead questions to configure some importants rules in digiKam setup. Of course, we won't to reproduce here the huge digiKam setup panel, but welcome users in digiKam and ask for minimal settings.</p>

<p>Assistant provide tips and explainations to guide users for future adjustements. Also, we cannot replace <a href="http://docs.kde.org/development/en/extragear-graphics/digikam/digikam.pdf">digiKam documentation</a> which can be compared to a real book, but at least users are introduced to the digiKam world.</p>

<p>More panels can be added in assistant if necessary, but we must limit configuration screens to not bloat ergonomy. Note that on left side of each page, we can customize image (currently digiKam logo) and use some nice graphic designs in relation with page contents, as you can see with pro softwares...</p>

<p>These screenshots have been taken under Windows Vista, but of course, the same exists under Linux.</p>

<a href="http://www.flickr.com/photos/digikam/3490736792/"><img src="http://farm4.static.flickr.com/3628/3490736792_4638087161.jpg" width="500" height="313" alt="firstrun02"></a>

<a href="http://www.flickr.com/photos/digikam/3490737082/"><img src="http://farm4.static.flickr.com/3391/3490737082_346ca67777.jpg" width="500" height="313" alt="firstrun03"></a>

<a href="http://www.flickr.com/photos/digikam/3489923047/"><img src="http://farm4.static.flickr.com/3544/3489923047_f0d60d9fb8.jpg" width="500" height="313" alt="firstrun04"></a>

<a href="http://www.flickr.com/photos/digikam/3490737726/"><img src="http://farm4.static.flickr.com/3585/3490737726_7dcdd3bfcd.jpg" width="500" height="313" alt="firstrun05"></a>

<a href="http://www.flickr.com/photos/digikam/3489923767/"><img src="http://farm4.static.flickr.com/3354/3489923767_c0565216f0.jpg" width="500" height="313" alt="firstrun06"></a>

<div class="legacy-comments">

  <a id="comment-18495"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/442#comment-18495" class="active">Does this address this bug report?</a></h3>    <div class="submitted">Submitted by <a href="http://solidsmoke.blogspot.com" rel="nofollow">Syam</a> (not verified) on Fri, 2009-05-01 13:50.</div>
    <div class="content">
     <p>https://bugs.kde.org/show_bug.cgi?id=188959</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18496"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/442#comment-18496" class="active">no... but...</a></h3>    <div class="submitted">Submitted by digiKam on Fri, 2009-05-01 13:58.</div>
    <div class="content">
     <p>No, but i think it's possible to implement. I take a look...</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18497"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/442#comment-18497" class="active">Thanks..</a></h3>    <div class="submitted">Submitted by Syam (not verified) on Fri, 2009-05-01 14:22.</div>
    <div class="content">
     <p>Thanks..</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div><a id="comment-18498"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/442#comment-18498" class="active">I had a feeling...</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Mon, 2009-05-04 09:10.</div>
    <div class="content">
     <p>When I've viewed all the config pages my first though have been: Is really needed this wizard? I mean, except for the first page, where the users indicates where the images are, the rest, I think that could be configured with a "good enough" default one. If you read the explanation of every page, you are saying which one is the beast, actually.</p>
<p>For a newbie a wizard is "Blablabla Next &gt; Blablabla Next &gt; ..."  :)</p>
<p>Only an exercice of reflection.</p>
<p>Keep the good work and thanks!</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18501"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/442#comment-18501" class="active">On one hand you are right.</a></h3>    <div class="submitted">Submitted by mikolajmachowski (not verified) on Mon, 2009-05-04 14:42.</div>
    <div class="content">
     <p>On one hand you are right. But on the other hand all those settings have big influence on how digiKam behaves and overall user experience.</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-18502"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/442#comment-18502" class="active">Simpler First Run Assistant</a></h3>    <div class="submitted">Submitted by <a href="http://kadu.net" rel="nofollow">patpi</a> (not verified) on Mon, 2009-05-04 18:29.</div>
    <div class="content">
     <p>Here Wizard is needed because those changes have big influence as already said by mikolajmichowski. However 6 pages is too much. From my personal experience gained from <a href="http://kadu.net">Kadu IM</a> , Kadu has also Configwizard, 3-4 pages are optimal. Having more pages is too frustrating for an end user. </p>
<p>That is why I would like to propose something shorter. Only 4 pages, only most important decision will be made, this should be enough for most users.</p>
<p>Png file: http://img209.imageshack.us/img209/3060/digikam.png<br>
<a href="http://projects.gnome.org/dia/">Dia</a> file: http://www.speedyshare.com/942477938.html</p>
<p>I think we don't loose much and shorter first run is very very important (otherwise user will be just clicking OK, OK, NEXT, NEXT and won't read anything) </p>
<p>Edit:// aaah, i forgot. On the right side of the First run (under digiKam logo, or above. Or at the top) there should be "Page 1 from 4" , "Page 2 from 4" etc. This will be informative and is needed.   </p>
<p>If you have any comments please email me. (But i will try to check this post again later)</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18503"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/442#comment-18503" class="active">You suggest to drop two</a></h3>    <div class="submitted">Submitted by mikolajmachowski (not verified) on Mon, 2009-05-04 19:07.</div>
    <div class="content">
     <p>You suggest to drop two screens: RAW import, previews or real images.</p>
<p>This could be done.<br>
a) move decision of RAW import to first opening of RAW file; when opening of RAW file for first time ask is user would like to use simple import or full tool + immortal "Do you want to see this message in future"<br>
b) don't agree about setting of previews by default; it could be done like in RAW import - first run dialog; or by hardware detection - big RAM and fast CPU means real images</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18508"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/442#comment-18508" class="active">Apart from selecting the</a></h3>    <div class="submitted">Submitted by Bob (not verified) on Tue, 2009-05-05 11:03.</div>
    <div class="content">
     <p>Apart from selecting the location and integrity of the image files, I'm not convinced that any of this is needed. All first-time users really need to do is indicate where their image files are and whether or not they want digiKam to make any modifications to those images. Once they have used digiKam for a while and understand its features, they can change options using the normal settings dialog.</p>
<p>Decisions about settings that affect performance, image quality, RAW file handling, right-click menus, tool-tips, etc. all require users to form opinions that some settings will be better than others. Until they have used digiKam for a while, how can they form such opinions? However, questions like, "Where are your images?" and, "Will you allow digiKam to modify your images?" are questions that first-time users could answer. (If I were a first-time user, my answer to the latter question would be, "No." Until I learn to trust digiKam, I would not let it modify anything.)</p>
<p>The latest "digest" reports that several new "first run" pages have been added recently, which makes me concerned that more useful features or fixes--ones that will affect users every day, not just once in their lifetimes--may be neglected.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18509"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/442#comment-18509" class="active">You are right: those</a></h3>    <div class="submitted">Submitted by mikmach (not verified) on Tue, 2009-05-05 12:06.</div>
    <div class="content">
     <p>You are right: those questions aren't created from newbie point of view but from developers or long time users who are answering again and again for the same questions...</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div></div><a id="comment-18557"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/442#comment-18557" class="active">I think that could be</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Thu, 2009-05-21 05:02.</div>
    <div class="content">
     <p>I think that could be configured with a "good enough" default one. If you read the explanation of every page, you are saying which one is the beast, actually.</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-18499"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/442#comment-18499" class="active">0.11 release schedule</a></h3>    <div class="submitted">Submitted by Mikael (not verified) on Mon, 2009-05-04 12:11.</div>
    <div class="content">
     <p>What's the release schedule for 0.11? I'm working on the Finnish localization and I'd like to have this feature covered in the release..</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18500"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/442#comment-18500" class="active">At the moment there is no</a></h3>    <div class="submitted">Submitted by mikolajmachowski (not verified) on Mon, 2009-05-04 14:40.</div>
    <div class="content">
     <p>At the moment there is no release schedule for 0.11 .</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-18560"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/442#comment-18560" class="active">Just learn something.
thanks.</a></h3>    <div class="submitted">Submitted by <a href="http://www.club-penguin.org/" rel="nofollow">clubpenguin</a> (not verified) on Tue, 2009-05-26 01:58.</div>
    <div class="content">
     <p>Just learn something.<br>
thanks.</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-18565"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/442#comment-18565" class="active">UI of "configure where images and meta-data are stored"</a></h3>    <div class="submitted">Submitted by Matthias Siau (not verified) on Sat, 2009-05-30 16:38.</div>
    <div class="content">
     <p>I don't like the way to "configure where images and meta-data are stored". Now I have to read the text to see which field is for images location and which is for database location (a little too much work when you know what you have to provide, only not in which field). It's also not clear that the text is related to the location field. It would be nice to have two fieldsets (as it is called in html) with titles "images location" and "database location".<br>
Another idea I had, was to add an option (checkbox) "use same location for database as images" and an greyed out (when the checkbox is checked) location field where you can see where the database will be stored (also appending /digikam4.db, instead of only showing a directory).</p>
<p>Thanks for considering my remarks!<br>
Matthias</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-18574"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/442#comment-18574" class="active">I don't think that it is</a></h3>    <div class="submitted">Submitted by Tim (not verified) on Tue, 2009-06-02 11:37.</div>
    <div class="content">
     <p>I don't think that it is really necessary to explain step by step how to install this program, it has it's own friendly interface, so that it not so hard to install it.</p>
         </div>
    <div class="links">» </div>
  </div>

</div>