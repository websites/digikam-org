---
date: "2011-03-28T09:18:00Z"
title: "New Features in digiKam 2.0: Versioning"
author: "Dmitri Popov"
description: "When tweaking photos in digiKam, you probably would want to keep the originals intact. And this is where the Versioning feature can come in rather"
category: "news"
aliases: "/node/589"

---

<p>When tweaking photos in digiKam, you probably would want to keep the originals intact. And this is where the Versioning feature can come in rather handy. It allows you to save each edited version of an original photo as a separate image complete with a list of all applied actions. <a href="http://scribblesandsnaps.wordpress.com/2011/03/28/new-features-in-digikam-2-0-versioning/">Continue to read</a></p>

<div class="legacy-comments">

</div>