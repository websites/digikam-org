---
date: "2011-08-03T08:41:00Z"
title: "Quick Tip: Disable Certain File Types in digiKam"
author: "Dmitri Popov"
description: "digiKam supports an impressive range of file formats, so you can use the application to handle RAW files, movies and everything in between. But what"
category: "news"
aliases: "/node/618"

---

<p>digiKam supports an impressive range of file formats, so you can use the application to handle RAW files, movies and everything in between. But what if you want to explicitly exclude a specific type of files? digiKam offers a simple solution for that.</p>
<p><a href="http://scribblesandsnaps.wordpress.com/2011/08/03/quick-tip-disable-certain-file-types-in-digikam/">Continue to read</a></p>

<div class="legacy-comments">

</div>