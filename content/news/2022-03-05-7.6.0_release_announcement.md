---
date: "2022-03-05T00:00:00"
title: "digiKam 7.6.0 is released"
author: "digiKam Team"
description: "Dear digiKam fans and users, we are proud to announce the stable release of digiKam 7.6.0."
category: "news"
---

[![](https://i.imgur.com/XMsH3Z9.png "digiKam 7.6.0 Running Under Linux Plasma Desktop with Advanced Search Engine")](https://imgur.com/XMsH3Z9)

Dear digiKam fans and users,

After one month of active maintenance and another huge bug triage, the digiKam team is proud to present version 7.6.0
of its open source digital photo manager.
See below the list of most important features coming with this release.

### Bundles packaging improvements

#### ICU support in Linux AppImage Bundle

Long time ago, the AppImage packaging suffered from an important lack of [International Components for Unicode (ICU) support](https://en.wikipedia.org/wiki/International_Components_for_Unicode).
Typically, digiKam and Showfoto deal with the string internally using Qt, but the framework was not compiled with the ICU library to handle
properly all string encoding used over the world.

The main side-effect of this unsupported feature is the lack of ability to use non-latin1 characters in text search fields,
for example while users query the database with specific German section of text used in file-names (aka ä, Ä, ö, Ö, ü, Ü).

With this new 7.6.0 release, ICU support with Qt framework has been fixed.

[![](https://i.imgur.com/iiQjoAi.png "Entering non latin-1 characters in digiKam 7.6.0 tags search field")](https://imgur.com/iiQjoAi)

#### Qt 5.15 LTS used in AppImage bundle

With this release we take care about upgrading the Qt framework with a LTS version. Since Qt 5.15.2, the framework is only published privately to
the registered clients from the Qt Company. By chance, the KDE project deals with the Qt company to provide a rolling release of the whole Qt framework
including all most important patches. This is the [Qt collection patch](https://community.kde.org/Qt5PatchCollection) used from now by the digiKam
AppImage bundle. This allows digiKam to benefit from important fixes as to support the most recent version of Mysql and Mariadb database in QtSql plugin.
Even if Qt 5.15.3 is just released as open-source, one year later 5.15.2, we will continue to use the Qt Collection Patch,
as the last customer Qt5 release is 5.15.8. So there exists again a serious gap between the open-source and the customer versions of Qt.

[![](https://i.imgur.com/4y2jrrJ.png "digiKam 7.6.0 AppImage using Qt 5.15 LTS")](https://imgur.com/4y2jrrJ)

#### Update of KDE framework and Libraw

KDE framework has been updated to the latest 5.90 release including plenty of bugfixes.
Internal Raw processors based on Libraw source code have been updated to the latest snapshot 2022-02-10.
More than 1180 different RAW cameras are now supported with this release.

[![](https://i.imgur.com/OphyZD0.png "digiKam 7.6.0 Running Raw Import Tool From Image Editor")](https://imgur.com/OphyZD0)

#### Manifest of all Rolling Release Components

digiKam bundles use many rolling release components which are not published as stable releases from time to time.
This is the rolling releases publication workflow. So, to know exactly which version of code is used by digiKam bundle during packaging,
we register the component versioning ID in a manifest file that users can show in the Help/Components Info
dialog from digiKam and Showfoto. This allows us to verify quickly if the code used includes bugs or fixes.

[![](https://i.imgur.com/3ZaeWTF.png "digiKam 7.6.0 Showing Manifest of Rolling Release Components")](https://imgur.com/3ZaeWTF)

### Maintenance and improvements

See the small selection below of notable entries fixed in bugzilla:

- 405235: reduce slow startup and new file scan on low latency networks under macOS.
- 426086, 420868, 448645: reduce time loading and fix crash with TIFF files under Windows.
- 435413: icon-view items selection usability fixes under Windows.
- 436533: add JPEG-XL support.
- 440953: fix Gphoto2 camera selection under macOS.
- 444280: fix crash under Windows when opening RAW file with Raw Profile color space.
- 449170: fix import RAW from darktable.
- 449078: add non-intrusive feedback to users with long searches on the database.
- 449315: fix delete items in Sqlite database.
- 443471: long investigations under Windows 10/11 about no startup.
- 399034: usability fixes with geo-location.
- 425886: improve stability of long searches in databases under Windows.

If you want more details, this new version arrives with more than
[840 files triaged and closed in bugzilla](https://bugs.kde.org/buglist.cgi?f1=cf_versionfixedin&limit=0&o1=equals&order=bug_id&product=digikam&v1=7.6.0).

### New Flow View Plugin Using Masonry Layouts

With this release a new 3rd-party plugin was proposed by a [github developer](https://github.com/cathaysia/digikamflowplugin)
to render a list of images using Masonry layout. Masonry is a grid layout based on columns. Unlike other grid layouts,
it doesn’t have fixed height rows. Basically, Masonry layout optimizes the use of space inside the canvas by reducing any
unnecessary gaps. Without this type of layout, certain restrictions are required to maintain the structure of layout,
as with the main icon-view in digiKam album window. This kind of layout is used by the Pinterest social network for example.

The plugin is not yet mature, as it cannot show any metadata or details from items, but it's suitable enough to be included
in all bundles with this release. You can access the plugin through the View menu entry in all digiKam main windows and Showfoto.

[![](https://i.imgur.com/O0rP1tc.png "New Flow-View Plugin for digiKam 7.6.0")](https://imgur.com/O0rP1tc)

If you like this plugin, don't hesitate [to give feedback on github](https://github.com/cathaysia/digikamflowplugin/issues).

### Future Plans

We will continue to maintain the Qt5 version of digiKam [in a dedicated branch of gitlab repository](https://invent.kde.org/graphics/digikam/-/tree/qt5-maintenance).
Only bug-fixes Will be applied to this code.

In parallel, [the git master branch](https://invent.kde.org/graphics/digikam/-/tree/master) will be used to port whole code to the new Qt6 framework. We will use the
partial port done by a student during summer 2021 and make this code compatible also with Qt5.

This port is for the moment done to 80%. Regression tests need to be done with Qt5 and Qt6 as plenty of changes have been
introduced. All new features will also be hosted to this code for a future 8.0.0 release probably published at the end of this year.

[![](https://i.imgur.com/kGohf8p.png "digiKam 8.0.0 Will be the Next Major Version Ported to Qt6")](https://imgur.com/kGohf8p)

### Final Words

Thanks to all users for [your support and donations](https://www.digikam.org/donate/),
and to all contributors, students, testers who allowed us to improve this release.

digiKam 7.6.0 source code tarball, Linux 64 bits AppImage bundles, macOS Intel package, and Windows 64 bits installers
can be downloaded from [this repository](https://download.kde.org/stable/digikam/7.6.0/).

Rendez-vous in a few months for the next digiKam 7.7.0 stable release.

Happy digiKaming.
