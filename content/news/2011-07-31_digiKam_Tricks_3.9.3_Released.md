---
date: "2011-07-31T16:41:00Z"
title: "digiKam Tricks 3.9.3 Released"
author: "Dmitri Popov"
description: "As the number version suggests, this is a minor release which features a handful of tweaks and corrections as well as improved compatibility with the"
category: "news"
aliases: "/node/617"

---

<p>As the number version suggests, this is a minor release which features a handful of tweaks and corrections as well as improved compatibility with the Cool Reader app for Android.</p>
<p>Although attention in this release was focused on tweaks and fixes, the book includes the following new material:</p>
<ul>
<li>Disable Certain File Types</li>
<li>Use the Focal Length Analyzer Script with digiKam</li>
</ul>
<p><a href="http://scribblesandsnaps.wordpress.com/2011/07/31/digikam-tricks-3-9-3-released/">Continue to read</a></p>

<div class="legacy-comments">

</div>