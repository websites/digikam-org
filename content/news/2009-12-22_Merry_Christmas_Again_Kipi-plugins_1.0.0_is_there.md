---
date: "2009-12-22T10:37:00Z"
title: "Merry Christmas Again : Kipi-plugins 1.0.0 is there..."
author: "digiKam"
description: "Merry Christmas digiKam fans and users! In the spirit of the season, we bring you a nice gift to put under your white Christmas tree:"
category: "news"
aliases: "/node/492"

---

<p>Merry Christmas digiKam fans and users!</p>

<p>In the spirit of the season, we bring you a nice gift to put under your <a href="http://www.balsamhill.com/Artificial-Christmas-Trees-s/1.htm">white Christmas tree</a>:</p>

<p>digiKam team is proud to announce Kipi-plugins 1.0.0 !</p>

<a href="http://www.flickr.com/photos/digikam/4202714965/" title="melting_the_ice-final_1024 by digiKam team, on Flickr"><img src="http://farm3.static.flickr.com/2750/4202714965_ff3e1e6d5f.jpg" width="500" height="293" alt="melting_the_ice-final_1024"></a>

<p>kipi-plugins tarball can be downloaded from SourceForge <a href="http://sourceforge.net/projects/kipi/files">at this url</a></p>

<p>Kipi-plugins will be also available for Windows. Precompiled packages can be donwloaded with KDE-Windows installer. See <a href="http://windows.kde.org">KDE-Windows project</a> for details.</p>

<p>See below the list of new features and bugs-fix coming with this release:</p>

001 ==&gt; GPSSync         : 199189 : GPS correlator does not allow offset in seconds.<br>
002 ==&gt; GPSSync         : 165277 : Last gpx directory isn't remembered.<br>
003 ==&gt; GPSSync         : 174086 : GPX time parsing problems.<br>
004 ==&gt; PicasaWebExport : 162993 : picasa album list not refreshed.<br>
005 ==&gt; Facebook        : 202045 : Facebook exports always to same album (the one initially selected).<br>
006 ==&gt; GPSSync         : 217710 : GPSSync unable to modify coordinates.<br>
007 ==&gt; GPSSync         : 217989 : GPSSync randomly no satellite view.<br>
008 ==&gt; DNGConverter    : 217897 : DNGconverter produces invalid DNG files.<br>
009 ==&gt; Libkdcraw       : 218762 : Libraw is too old.<br>
010 ==&gt; Common          : 219207 : Compilation error in PreviewWidget.<br>
011 ==&gt; GPSSync         : 192439 : Geolocation position not remembered or initialised consistently.<br>

<div class="legacy-comments">

  <a id="comment-18914"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/492#comment-18914" class="active">Compiling fails for me with</a></h3>    <div class="submitted">Submitted by Gans (not verified) on Tue, 2009-12-22 13:30.</div>
    <div class="content">
     <p>Compiling fails for me with "class KSaneIface::KSaneWidget’ has no member named ‘closeDevice’" in acquireimages/scandialog.cpp<br>
void ScanDialog::closeEvent(QCloseEvent *e)<br>
{<br>
    d-&gt;saneWidget-&gt;closeDevice();<br>
    saveSettings();<br>
    e-&gt;accept();<br>
}<br>
I have found <a href="http://osdir.com/ml/kde-commits/2009-11/msg04806.html">this</a> commit, so I ask it is alright to revert that line to d-&gt;saneWidget-&gt;scanCancel(); ?</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18915"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/492#comment-18915" class="active">this due to use libksane from trunk API</a></h3>    <div class="submitted">Submitted by digiKam on Tue, 2009-12-22 13:41.</div>
    <div class="content">
     <p>this call use libksane API from trunk.</p>
<p>and there is no libksane CMake rules to control version available to compile.</p>
<p>digiKam</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18921"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/492#comment-18921" class="active">:(</a></h3>    <div class="submitted">Submitted by Ralph (not verified) on Tue, 2009-12-22 23:12.</div>
    <div class="content">
     <p>So, no kipi-plugins 1.0.0 with KDE 4.2?</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18922"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/492#comment-18922" class="active">yes, I will fix it...</a></h3>    <div class="submitted">Submitted by digiKam on Tue, 2009-12-22 23:17.</div>
    <div class="content">
     <p>yes, I will fix it...</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18927"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/492#comment-18927" class="active">So is there an patch to make</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Fri, 2009-12-25 13:41.</div>
    <div class="content">
     <p>So is there an patch to make it compile on KDE4.2 ?<br>
If so please link to it.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18929"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/492#comment-18929" class="active">It compile fine with KDE 4.3.x</a></h3>    <div class="submitted">Submitted by digiKam on Fri, 2009-12-25 21:53.</div>
    <div class="content">
     <p>Just tested. It compile with libksane from KDE 4.3.x</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-18930"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/492#comment-18930" class="active">comment line is enough... but...</a></h3>    <div class="submitted">Submitted by digiKam on Fri, 2009-12-25 22:26.</div>
    <div class="content">
     <p>For KDE 4.2, commenting line d-&gt;saneWidget-&gt;closeDevice() is fine... but...</p>
<p>I trying to do it automatically in code, checking libksane version ID from libksane/version.h... But it doesn't exist (as in libkipi, libkexiv2, libkdcraw... etc...)... </p>
<p>It's a bug in libksane. Please report it to KDE bugzilla...</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18938"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/492#comment-18938" class="active">libksane version macro</a></h3>    <div class="submitted">Submitted by Kåre  (not verified) on Sat, 2009-12-26 22:04.</div>
    <div class="content">
     <p>I have now added the version macro to trunk. I have used the KDE version macro to check the libksane versions.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18939"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/492#comment-18939" class="active">trunk only ?</a></h3>    <div class="submitted">Submitted by digiKam on Sat, 2009-12-26 22:09.</div>
    <div class="content">
     <p>Do you have patched libksane for KDE4.3 and 4.2 too ?</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div></div></div></div></div></div><a id="comment-18919"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/492#comment-18919" class="active">Groovy! Looks like GPSsync</a></h3>    <div class="submitted">Submitted by <a href="http://kubuntulover.blogspot.com" rel="nofollow">Bugsbane</a> (not verified) on Tue, 2009-12-22 20:04.</div>
    <div class="content">
     <p>Groovy! Looks like GPSsync has gotten some love. :) I love the way KIPI integrates between Digikam and Gwenview. So fast and easy. I've been trying to get Krita devs to look into KIPI, but Krita supports 32bit images. Any chance of KIPI supporting 32 bit some time in the future?</p>
<p>Either way, thanks for working on these. I love my KIPI plugins. It keeps amusing me how Gwenview can already do everything the Ubuntu team is looking for in a photo previewing and basic editing app to replace Gimp. Maybe they should start looking outside Gnome... ;-)</p>
<p>I'll stop creating trouble now... (maybe)</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18920"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/492#comment-18920" class="active">agreed</a></h3>    <div class="submitted">Submitted by <a href="http://www.panopixel.org/" rel="nofollow">DrSlony</a> (not verified) on Tue, 2009-12-22 21:41.</div>
    <div class="content">
     <p>Hahaha agreed!</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-18952"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/492#comment-18952" class="active">Kubuntu packages</a></h3>    <div class="submitted">Submitted by xeros (not verified) on Tue, 2009-12-29 08:32.</div>
    <div class="content">
     <p>I wonder when kipi-plugins will be packaged in kubuntu-ppa backports or backports-beta repositories.<br>
I've upgraded digiKam to 1.0 RC from backports-beta with KDE 4.4 Beta 2 but because of dependency problems this upgrade removed kipi-plugins package.</p>
         </div>
    <div class="links">» </div>
  </div>

</div>
