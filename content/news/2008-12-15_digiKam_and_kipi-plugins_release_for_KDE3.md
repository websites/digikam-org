---
date: "2008-12-15T10:48:00Z"
title: "digiKam and kipi-plugins release for KDE3..."
author: "digiKam"
description: "Dear all digiKam fans and users! Before to close this year, a new digiKam beta release and a new stable kipi-plugins release have be done"
category: "news"
aliases: "/node/413"

---

<p>Dear all digiKam fans and users!</p>

<a href="http://www.flickr.com/photos/digikam/3109504061/" title="digikam0.9.5-beta2 by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3217/3109504061_051d70aab7.jpg" width="500" height="375" alt="digikam0.9.5-beta2"></a>

<p>Before to close this year, a new digiKam beta release and a new stable kipi-plugins release have be done for KDE3, especially as bug fix and translations updates. Also, shared libraries libkexiv2 and libkdcraw have been updated.</p>

<p>digiKam 0.9.5-beta2 tarball can be downloaded from SourceForge <a href="https://sourceforge.net/project/showfiles.php?group_id=42641&amp;package_id=34800">at this url</a></p>

<p>Kipi-plugins 0.1.7, libkdcraw 0.1.7, and libkexiv2 0.1.8 can be downloaded from SourceForge <a href="https://sourceforge.net/project/showfiles.php?group_id=149779&amp;package_id=165761">at this url</a></p>

<p>Closed files and new features are listed below:</p>

<p>
<b>digiKam 0.9.5-beta2 - Release date: 2008-12-14</b><br><br>

BUGFIXES FROM KDE BUGZILLA (alias B.K.O | http://bugs.kde.org):<br><br>

001 ==&gt; 164573 : Better support for small screens.<br>
002 ==&gt; 175970 : digitaglinktree merges tags with same name in different subfolders.<br>
003 ==&gt; 108760 : Use collection image (or part of) as Tag/Album icon.<br>
004 ==&gt; 144078 : very slow avi startup.<br>              
005 ==&gt; 146258 : Moving an album into waste basket didn't remove.<br>
006 ==&gt; 149165 : cannot edit anymore - sqlite lock?<br>              
007 ==&gt; 146025 : Wrong image name when using info from EXIF.<br>
008 ==&gt; 167056 : Updating tags is slow when thumbnails are visible.<br>
009 ==&gt; 141960 : Problems with photos without EXIV data when updating me.<br>          
010 ==&gt; 129379 : Renamed Album is shown multiple times although there is only on related picture directory.<br>
011 ==&gt; 171247 : Album creation error when name start by a number.<br>
012 ==&gt; 150906 : digiKam unable to connect to Panasonic LUMIX DMC-TZ3.<br>
013 ==&gt; 148812 : "Auto Rotate/Flip &amp;Using Exif Orientation" fails with some images.<br>
014 ==&gt; 150342 : Camera image window keeps scrolling to the currently downloaded picture.<br>
015 ==&gt; 147475 : digiKam Slideshow - Pause button does not stay sticky / work.<br>
016 ==&gt; 148899 : Image Editor does not get the focus after clicking on an image.<br>
017 ==&gt; 148596 : Empty entries in the "back" drop-down when changing month (date view).<br>
018 ==&gt; 161387 : Unable to import photos with digiKam even though it's detected.<br>
019 ==&gt; 165229 : Thumbnail complete update does not work reliably, even for jpgs.<br>
</p>

<p>
<b>Kipi-plugins 0.1.7 - Release date: 2008-12-14</b><br><br>

BUGFIXES FROM KDE BUGZILLA (alias B.K.O | http://bugs.kde.org):<br><br>

001 ==&gt; 175033 : GPSSync            : GPSSync does not get any map just a grey square.<br>
002 ==&gt; 175304 : MetadataEdit       : db does replicate to iptc or exif.<br>          
003 ==&gt; 137359 : MetadataEdit       : Unclear wording in kipiplugin_metadataedit.<br>
004 ==&gt; 146381 : JPEGLossLess       : Image rotation splits the image with a small strip appearing on the wrong side.<br>
005 ==&gt; 155023 : SendImages         : Crashs when trying to send images via digiKam.<br>
006 ==&gt; 170272 : FlickrExport       : Undefined signature problem prevents uploading to flickr.<br>
007 ==&gt; 161114 : SendImages         : Resizing and emailing from digikam results in no-more readable files.<br>
008 ==&gt; 147685 : General            : Errors in error and "what's this" messages -- No way to select text in an error box.<br>
009 ==&gt; 146293 : MetadataEdit       : Plugins/Images/metadata: untranslated.<br>
010 ==&gt; 140345 : BatchProcess       : kipiplugins do not work due to trying to save the result into the root directory.<br>
011 ==&gt; 163395 : SendImages         : digiKam and encoding errors in flickr export and mail attachments.<br>
012 ==&gt; 136941 : BatchProcess       : Graphical picture ordering and renaming.<br>
013 ==&gt; 149394 : General            : Recover lost/deleted images from flash memory/camera.<br>
014 ==&gt; 162441 : PicasaWebExport    : Unable to create an album.<br>
015 ==&gt; 162993 : PicasaWebExport    : Picasa album list not refreshed.<br>
016 ==&gt; 162994 : PicasaWebExport    : Picasa album list does not contain "not listed" albums, contains only "public" albums.<br>
017 ==&gt; 174353 : GPSSync            : Kipi-plugin geolocation does not work anymore.<br>
018 ==&gt; 132982 : BatchProcess       : Batch rename limited, should allow renaming by patterns.<br>
</p>

<p>
<b>libkdcraw-0.1.7 - Release date: 2008-12-14</b><br><br>

NEW FEATURES:<br><br>

Updated to LibRaw to 0.6.3 : NEF processing code changed (some overflow control added).<br>
Updated to LibRaw to 0.6.2.<br>                          
New cameras: Canon G10 &amp; 5D Mk2, Leaf AFi 7, Leica D-LUX4, Panasonic FX150 &amp; G 1, Fujifilm IS Pro.<br>
</p>

<p>
<b>libkexiv2-0.1.8 - Release date: 2008-12-14</b><br><br>

NEW FEATURES:<br><br>

- API changed: Move all Exiv2 classes acess methods to interanl provate container. ABI number set to 5.0.0.<br>
- Fix broken compilation with Exiv2 0.18.<br>
- Support TIFF metadata writting mode (require Exiv2 &gt;= 0.18).<br>
- Added new methods canWriteComment(), canWriteExif(), canWriteIptc().<br>
- Added new method supportMetadataWritting().<br>       
- Do not change file time-stamp when metadata are changed.<br>

BUGFIXES FROM KDE BUGZILLA (alias B.K.O | http://bugs.kde.org):<br><br>

001 ==&gt; 158989 : digiKam won't start: MakerTagInfo registry full.<br>
002 ==&gt; 157173 : Crash by adding gps data to pictures.<br>
</p>

<p>All digiKam and Kipi-plugins teams wish you a merry Christmas and an happy new year.</p>
<div class="legacy-comments">

  <a id="comment-18044"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/413#comment-18044" class="active">Thank you so much for</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Tue, 2008-12-16 11:05.</div>
    <div class="content">
     <p>Thank you so much for remembering the many kde3 users that are still around. My workplace has decided not to move to kde4 for a couple of years and having updated kde3 applications makes life much easier.</p>
<p>Thanks for you all work</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-18049"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/413#comment-18049" class="active">Build and runs OK on AMD64 box</a></h3>    <div class="submitted">Submitted by chiman (not verified) on Wed, 2008-12-17 03:52.</div>
    <div class="content">
     <p>I tried building the latest digikam on my AMD64 box, with KDE 3.5.9.<br>
<code> uname -smovr<br>
Linux 2.6.25.18-0.2-default #1 SMP 2008-10-21 16:30:26 +0200 x86_64 GNU/Linux<br>
</code></p>
<p>Got in digikam-0.9.5-beta2 and kipi-plugins-0.1.7. They required a bit of extra package loading before they built without complaint, but eventually they were installed. digikam reports:</p>
<p><code><br>
digikam --version<br>
Qt: 3.3.8b<br>
KDE: 3.5.9 "release 49.1"<br>
digiKam: 0.9.5-beta2<br>
</code></p>
<p>Very nice package. The GUI is quite responsive. </p>
<p>I did notice that, in Settings-&gt;Configure Digikam-&gt;Kipi Plugins, the Kipi library version is shown as 0.1.6. A bit confusing. But in the Help sections of the corresponding Tools and Batch menu entries, the Kipi version is shown (properly) as 0.1.7.</p>
<p>Again, thanks for your work.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18050"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/413#comment-18050" class="active">libkipi != kipi-plugins</a></h3>    <div class="submitted">Submitted by digiKam on Wed, 2008-12-17 07:57.</div>
    <div class="content">
     <p>In Settings-&gt;Configure Digikam-&gt;Kipi Plugins, the version show on top/right is libkipi release id, not kipi-plugins. The library version is not the same than kipi-plugins.</p>
<p>digiKam</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18069"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/413#comment-18069" class="active">I figured I was misunderstanding something</a></h3>    <div class="submitted">Submitted by chiman (not verified) on Thu, 2008-12-18 00:41.</div>
    <div class="content">
     <p>Thanks for the clarification.</p>
<p>Joyeux Noel</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div><a id="comment-18070"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/413#comment-18070" class="active">Hi,
we're having a fixed IP</a></h3>    <div class="submitted">Submitted by Corinna (not verified) on Thu, 2008-12-18 12:20.</div>
    <div class="content">
     <p>Hi,</p>
<p>we're having a fixed IP ADSL connection, used by<br>
just two people.  Yesterday I built digikam-0.9.5-beta2<br>
plus kipi-plugins-0.1.7.  Then I started to use the kipi<br>
geotagging plugin for testing purposes and tagged a<br>
couple of my photos with geo information.</p>
<p>When I tagged my 8th photo, I suddenly couldn't view<br>
any satellite information anymore.  It turned out that my IP<br>
was entirely blocked from satellite view by Google and I'm<br>
still blocked today.  Of course I also reported that at<br>
Google, but...</p>
<p>...given that Google does that if they encounter unusual<br>
access, I'm wondering if the new kipi geotagging plugin<br>
isn't using an invalid access method which is treated<br>
by Google as hostile.  The map content of the<br>
geotagging plugin also looks somewhat broken, even when in<br>
normal map view.  The UI elements don't seem to match<br>
correctly, for instance.</p>
<p>Thanks,<br>
Corinna</p>
<p>Btw., the captchas are partly unreadable.  I had to enter<br>
this a couple of times.  What's a vertical stripe with a<br>
dot below?!?</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18072"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/413#comment-18072" class="active">Googles reply</a></h3>    <div class="submitted">Submitted by Corinna (not verified) on Thu, 2008-12-18 19:09.</div>
    <div class="content">
     <p>http://groups.google.com/group/Google-Maps-Troubleshooting/browse_thread/thread/30cb4fb23be18426?</p>
<p>:((((((</p>
<p>Corinna</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18073"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/413#comment-18073" class="active">Google response is wrong...</a></h3>    <div class="submitted">Submitted by digiKam on Thu, 2008-12-18 20:28.</div>
    <div class="content">
     <p>Geolocation plugin do not use GoogleMaps api directly. We use a web browser (konqueror) session embeded in a widget to display map and just an relay url to play with googlemap. </p>
<p>digiKam</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18074"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/413#comment-18074" class="active">Re: Google response is wrong...</a></h3>    <div class="submitted">Submitted by Corinna (not verified) on Thu, 2008-12-18 21:12.</div>
    <div class="content">
     <p>Hi Gilles,</p>
<p>Thanks, I will certainly forward this information to the<br>
google group thread.</p>
<p>But why could that have happened then?  At the time<br>
I did nothing else with google, just using the geotagging<br>
plugin in Digikam.  And all of a sudden I was blocked.<br>
Do you have any idea?</p>
<p>Thanks,<br>
Corinna</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18075"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/413#comment-18075" class="active">Perhaps something has changed...</a></h3>    <div class="submitted">Submitted by digiKam on Thu, 2008-12-18 21:20.</div>
    <div class="content">
     <p>Perhaps something has changed in public googlemap api and using depreciated methods are blocked by web service to force googlemap clients to use new api...</p>
<p>Gilles</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18076"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/413#comment-18076" class="active">Re: Perhaps something has changed...</a></h3>    <div class="submitted">Submitted by Corinna (not verified) on Fri, 2008-12-19 15:02.</div>
    <div class="content">
     <p>Hi Gilles,</p>
<p>I got another reply from the guy at Google, see<br>
http://groups.google.com/group/Google-Maps-Troubleshooting/msg/aa0f3808f7d0935e</p>
<p>There's also this point in the FAQ<br>
http://code.google.com/apis/maps/faq.html#tos_nonweb<br>
which questions what he claims about using the API in<br>
applications like digikam is not covered by the Terms<br>
of Service.</p>
<p>Thanks,<br>
Corinna</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div></div></div></div>
</div>
