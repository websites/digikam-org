---
date: "2014-06-30T18:55:00Z"
title: "digiKam Software Collection 4.1.0 released..."
author: "digiKam"
description: "Dear digiKam fans and users, The digiKam Team is proud to announce the release of digiKam Software Collection 4.1.0. This release includes many fixes since"
category: "news"
aliases: "/node/714"

---

<a href="https://www.flickr.com/photos/digikam/14510651595"><img src="https://farm4.staticflickr.com/3864/14510651595_877b40f208_c.jpg" width="800" height="450"></a>

<p>Dear digiKam fans and users,</p>

<p>
The digiKam Team is proud to announce the release of digiKam Software Collection 4.1.0. This release includes many fixes since the <a href="http://www.digikam.org/node/713">previous stable release 4.0.0</a>:
</p>

<ul>

<li>
The face management feature has seen huge improvements and includes fixes for some problems introduced in recent releases.
</li>

<li>
The detection and recognition of faces is now more robust and suitable for production.
</li>

<li>
All icon views now have a new overlay to identify photos that have geo-location information, making it easier for users to search albums for photos with GPS coordinates.
</li>

<li>
The maximum thumbnail size is now 512 pixels, up from 256 pixels previously, improving their appearance on high-resolution displays.
</li>

</ul>

<p>
As usual, we have worked hard to close your reported issues. A list of the <a href="https://bugs.kde.org/buglist.cgi?f1=cf_versionfixedin&amp;o1=equals&amp;query_format=advanced&amp;bug_status=RESOLVED&amp;bug_status=NEEDSINFO&amp;bug_status=VERIFIED&amp;bug_status=CLOSED&amp;v1=4.1.0&amp;product=digikam&amp;product=digikamimageplugins&amp;product=kipiplugins&amp;product=showfoto">issues closed</a> in digiKam 4.1.0 is available through the KDE Bugtracking System.
</p>

<p>The digiKam software collection tarball can be downloaded from <a href="http://download.kde.org/stable/digikam/digikam-4.1.0.tar.bz2.mirrorlist">KDE repository</a></p>

<p>Have fun playing with your photos using this new release,

</p><p>digiKam Team...</p>
<div class="legacy-comments">

  <a id="comment-20741"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/714#comment-20741" class="active">What's next? Species recognition? </a></h3>    <div class="submitted">Submitted by Alan Pater (not verified) on Mon, 2014-06-30 23:28.</div>
    <div class="content">
     <p>Great job! </p>
<p>But seriously, perhaps in a couple of more releases ...</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20744"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/714#comment-20744" class="active">+1</a></h3>    <div class="submitted">Submitted by <a href="http://www.londonlight.org/" rel="nofollow">DrSlony</a> (not verified) on Tue, 2014-07-01 17:24.</div>
    <div class="content">
     <p>Hehe, as a macro photographer, I'd like that!</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-20742"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/714#comment-20742" class="active">Compilation</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Tue, 2014-07-01 03:36.</div>
    <div class="content">
     <p>As always, the best photographic app around!</p>
<p>However, please make it easier for the idiot to compile to older distro releases. I don't want to upgrade my entire distro just because it's missing Digikam 4.1.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20743"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/714#comment-20743" class="active">Look notice here...</a></h3>    <div class="submitted">Submitted by digiKam on Tue, 2014-07-01 04:38.</div>
    <div class="content">
     <p>http://www.digikam.org/download/tarball</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20745"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/714#comment-20745" class="active">Ubuntu lags with opencv</a></h3>    <div class="submitted">Submitted by Michal Sylwester (not verified) on Wed, 2014-07-02 02:49.</div>
    <div class="content">
     <p>So it seems it can't be built at the moment unless you also build the new opencv.</p>
<p>So if someone actually uses my ppa, you will have to wait for the 4.1 until either Ubuntu catches up to opencv 2.4.9, or I manage to prepare my build of it, which may take a while...</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20748"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/714#comment-20748" class="active">I don't think ubuntu 14.04</a></h3>    <div class="submitted">Submitted by philip5 (not verified) on Wed, 2014-07-02 23:09.</div>
    <div class="content">
     <p>I don't think ubuntu 14.04 will get any official backport of newer opencv so I guess you'll need to backport that too for digikam 4.1 to build. That's what I did with my own packages of opencv 2.4.9 and digikam 4.1 anyway.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20750"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/714#comment-20750" class="active">Are you publishing your</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Thu, 2014-07-03 03:41.</div>
    <div class="content">
     <p>Are you publishing your packages?  I didn't see them in your ppa.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20753"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/714#comment-20753" class="active">Haven't done updates of</a></h3>    <div class="submitted">Submitted by philip5 (not verified) on Thu, 2014-07-03 13:16.</div>
    <div class="content">
     <p>Haven't done updates of digikam on my ppa since the kubuntu team started doing updates (for a while). Didn't want conflicting packages out there to that effort. I have built my own packages of digikam all the time for my self anyway.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20754"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/714#comment-20754" class="active">Any chance of publishing a</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Fri, 2014-07-04 11:05.</div>
    <div class="content">
     <p>Any chance of publishing a how-to without assumptions and jumping around to half a dozen readme docs? Basic compilation I can do, but the Digikam instructions seem too complicated, for me at least, with all the jumping around and sub-compilation of dependancies. The site instructions perhaps make assumptions in some steps that prevent success.</p>
<p>Whilst I'm still plodding along and trying to understand the instructions, when/if I do manage to compile it, I'll attempt a how-to myself. Thought this may take sometime..... :-)</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20755"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/714#comment-20755" class="active">Wow! What a journey compiling</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Sat, 2014-07-05 08:11.</div>
    <div class="content">
     <p>Wow! What a journey compiling from source is. Six hours of solid surfing, reading and trial and error.</p>
<p>Managed to compile libraw-0.16.0 and create a .deb that installs and is recognised, so now have run into opencv issue as above.Compiler cannot find my installed version. </p>
<p>Now, to tackle compiling that one too..... Here goes!</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20756"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/714#comment-20756" class="active">Digikam is really let down by</a></h3>    <div class="submitted">Submitted by Rob Dean (not verified) on Sun, 2014-07-06 12:27.</div>
    <div class="content">
     <p>Digikam is really let down by the fact that it doesn't supply new builds (and all dependences) in a PPA for ubuntu users. Get with the times people, provide easy to install software and people will be more likely to use it. Sure, I could use the ubuntu  PPA and run a much older version (with all the inherent bugs). Or I could spend some time working out how to compile the thing myself (I'm a busy guy and really, in this day and age why should I?). Or you could provide a PPA or at least a DEB package file to install the latest version. Not that difficult I wouldn't have thought?</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20757"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/714#comment-20757" class="active">Digikam is great - with a suitable ppa for ubuntu 14.04  better</a></h3>    <div class="submitted">Submitted by <a href="http://www.freunde-schloss-beuggen.de" rel="nofollow">Schlossfreund</a> (not verified) on Sun, 2014-07-06 15:06.</div>
    <div class="content">
     <p>I like digikam very much and use it intensively. As a volunteer I support a club collecting photos and preparing them for use.<br>
Digikam is a great help because in total I work with some 90'000 files. But there is a number of annoying bugs in digikam 4.0.<br>
So please motivate someone of your great team to prepare a suitable ppa so that I can use digikam 4.1 with ubuntu / Kubuntu 14.04. And enjoy your work to reduce the bugs - please!<br>
Thanks to the team anyway!</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20758"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/714#comment-20758" class="active">I just uploaded my packages</a></h3>    <div class="submitted">Submitted by philip5 (not verified) on Sun, 2014-07-06 21:12.</div>
    <div class="content">
     <p>I just uploaded my packages of both digikam 4.1.0 and opencv 2.4.9 to my PPA for (k)ubuntu 14.04 (trusty). They are waiting for being built but hopefully will be avalible as packages in a couple of hours...</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20759"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/714#comment-20759" class="active">That sounds great Philip,</a></h3>    <div class="submitted">Submitted by Rob Dean (not verified) on Mon, 2014-07-07 05:22.</div>
    <div class="content">
     <p>That sounds great Philip, although I still can't understand why there isn't an official PPA........</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-20760"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/714#comment-20760" class="active">Thanks!</a></h3>    <div class="submitted">Submitted by Michal Sylwester (not verified) on Mon, 2014-07-07 11:00.</div>
    <div class="content">
     <p>It looks than the opencv changes were a little too big relative to my packaging skills.<br>
I copied to built packages to my ppa as well, so if someone is using it already they should get it without changing the ppa.</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div></div></div></div></div></div><a id="comment-20761"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/714#comment-20761" class="active">PPA Ubuntu 14.04?????</a></h3>    <div class="submitted">Submitted by <a href="http://www.freunde-schloss-beuggen.de" rel="nofollow">Schlossfreund</a> (not verified) on Tue, 2014-07-08 08:17.</div>
    <div class="content">
     <p>Hi Philipp, I asked Gilles if there is a possibility to provide a ppa for ubuntu 14.04. You did something like that in the past for earlier versions. I'm lacking the knowledge to create a ppa, but I'm a great fan of digikam, I use digikam daily to work with my photo collection. Could you please do the the whole fan group the favour to create such a ppa?</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20762"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/714#comment-20762" class="active">Install Digikam 4.1 with PPA</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Tue, 2014-07-08 09:58.</div>
    <div class="content">
     <p>The simple way to install Digikam 4.1 and Kipi-plugins for Ubuntu 14.04 :</p>
<p><code><br>
sudo add-apt-repository ppa:msylwester/misc<br>
sudo apt-get update<br>
sudo apt-get install digikam<br>
sudo apt-get install kipi-plugins<br>
</code><br>
Or, without the KDE dependencies for gnome version of Ubuntu :<br>
<code><br>
sudo apt-get install --no-install-recommends digikam<br>
sudo apt-get install --no-install-recommends kipi-plugins<br>
</code><br>
Before doing the installation, be sure to <strong>uninstall</strong> the previous version of Digikam and Kipi-plugins.</p>
<p>That's it ! It works for me !</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20764"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/714#comment-20764" class="active">Happy that it worked for you</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Thu, 2014-07-10 04:27.</div>
    <div class="content">
     <p>Happy that it worked for you in 14.04.</p>
<p>If you're not running 14.04, what then? This is the point of the present discussion.</p>
<p>If you are running windows or 12.04, it is difficult to compile....</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-20781"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/714#comment-20781" class="active">Thx Phillip, maybe that</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Tue, 2014-07-22 12:00.</div>
    <div class="content">
     <p>Thx Phillip, maybe that should be referenced in the install notes of this website.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20782"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/714#comment-20782" class="active">My packages are not official</a></h3>    <div class="submitted">Submitted by philip5 (not verified) on Tue, 2014-07-22 12:46.</div>
    <div class="content">
     <p>My packages are not official packages but something that works until they are avalible from downsteam maintaining packagers. If posted here on the digikam site they would be more or less official and I don't think upstream Digikam have any intention to be package maintainers. That should be handled in each linux distribution.</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div></div></div></div></div></div><a id="comment-20746"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/714#comment-20746" class="active">face recognition</a></h3>    <div class="submitted">Submitted by Kornel (not verified) on Wed, 2014-07-02 14:49.</div>
    <div class="content">
     <p>Is there a way to show all and only these faces that have been detected &amp; recognized by digikam but not yet confirmed by the user?</p>
<p>It would be really helpful for people with large collection of faces of many different people. I couldn't find an easy way to check if dikigam 4.0.0 recognized (i.e. connected a face with a person, not detected a face) faces correctly.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20749"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/714#comment-20749" class="active">+1</a></h3>    <div class="submitted">Submitted by Nico (not verified) on Thu, 2014-07-03 00:32.</div>
    <div class="content">
     <p>this is definitely something I needed to correct those wrongly recognised faces and approve the correct ones - for now, I have been hovering over all images to identify the unconfirmed ones but this is not really practical for larger collections</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-20751"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/714#comment-20751" class="active">+1</a></h3>    <div class="submitted">Submitted by Henrique (not verified) on Thu, 2014-07-03 04:15.</div>
    <div class="content">
     <p>Would it be This right?</p>
<p>https://bugs.kde.org/show_bug.cgi?id=336253</p>
<p>I really need this "unconfrmed faces"</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-20747"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/714#comment-20747" class="active">Windows</a></h3>    <div class="submitted">Submitted by Greg (not verified) on Wed, 2014-07-02 17:41.</div>
    <div class="content">
     <p>Has support for Windows been dropped?  I haven't seen a new windows install package in many releases.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20752"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/714#comment-20752" class="active">There is a beta version of</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Thu, 2014-07-03 07:29.</div>
    <div class="content">
     <p>There is a beta version of Digikam 4.1.0 for Windows in the Mailinglist for Users of Digikam (http://mail.kde.org/pipermail/digikam-users/).</p>
<p>http://mail.kde.org/pipermail/digikam-users/2014-July/019581.html</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20763"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/714#comment-20763" class="active">Thank you for your effort.
(I</a></h3>    <div class="submitted">Submitted by MBB (not verified) on Thu, 2014-07-10 03:16.</div>
    <div class="content">
     <p>Thank you for your effort.<br>
(I am not actually using it right now, but thank you all the same)<br>
There seems to be one in http://download.kde.org/stable/digikam/ too. </p>
<p>I did notice that the version 4.1 installer is far smaller then 3.4 ( 170 MB vs 234 MB). I'm curious about why, is there a lot missing or have you been able to optimize the code? Or are you just using a higher compression for the distribution?<br>
And I read that KDE 5 framework got launched today. What does that mean for Digikam on Windows, do you have to rewrite and retry everything? Or will it make work more easy to cross-compile? </p>
<p>Good luck and keep up to good work.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20765"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/714#comment-20765" class="active">The difference is due to all</a></h3>    <div class="submitted">Submitted by digiKam on Thu, 2014-07-10 07:39.</div>
    <div class="content">
     <p>The difference is due to all translations files missing in this installer. digiKam still in English.<br>
It miss msgfmt program for KDE windows</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-20774"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/714#comment-20774" class="active">The 4.x builds for Windows</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Thu, 2014-07-17 13:59.</div>
    <div class="content">
     <p>The 4.x builds for Windows appear to be missing Lensfun plugin, meaning no auto-correction for distortion/vignetting/CA.</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div></div><a id="comment-20766"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/714#comment-20766" class="active">Clicking face tags in combo list</a></h3>    <div class="submitted">Submitted by Ste (not verified) on Mon, 2014-07-14 10:21.</div>
    <div class="content">
     <p>Still (since 4.0.0) when you want to add a face tag, you're presented a combo list of what matches what you're typing, but you cannot click it because mouse click is ignored. You have to select the right tag with arrow keys and press Enter.</p>
<p>Both in album view and in face detection/recognition view.</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-20767"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/714#comment-20767" class="active">Faces not detected anymore</a></h3>    <div class="submitted">Submitted by Ste (not verified) on Mon, 2014-07-14 19:09.</div>
    <div class="content">
     <p>Since installing 4.1.0 (debian package on sid) faces are not detected anymore on my pictures (I dont use recognition, just detection). Accuracy set to 100%.</p>
<p>More, when you select only ONE ALBUM in the "album" config option (I usually drop new photos in a "New pics" album), face detection will START from that album but will nevertheless continue on the whole rest of the albums in the collection.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20768"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/714#comment-20768" class="active">Faces not detected...</a></h3>    <div class="submitted">Submitted by digiKam on Mon, 2014-07-14 19:33.</div>
    <div class="content">
     <p>Face Detection is typically an OpenCV issue with missing internal components not installed to your distro.</p>
<p>Run digiKam from the console with debug trace enabled and look messages printed when you run face detection.</p>
<p>https://www.digikam.org/contrib</p>
<p>Album select problem is already reported to bugzilla...</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20769"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/714#comment-20769" class="active">This is</a></h3>    <div class="submitted">Submitted by Ste (not verified) on Mon, 2014-07-14 19:43.</div>
    <div class="content">
     <p>This is reported:</p>
<p>digikam([pid])/KFACE: OpenCV Haar Cascade director cannot be found. Did you install OpenCV XML data files?</p>
<p>All packege dependencies are met though...</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20770"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/714#comment-20770" class="active">harr cascade files.</a></h3>    <div class="submitted">Submitted by digiKam on Mon, 2014-07-14 19:48.</div>
    <div class="content">
     <p>These files are typically installed through libkface. Perhaps it's a packaging problem from your distro, as package in incomplete or badly compiled.</p>
<p>https://projects.kde.org/projects/extragear/libs/libkface/repository/revisions/master/show/data</p>
<p>Without these files, detection cannot be processed.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20771"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/714#comment-20771" class="active">Files are actually there</a></h3>    <div class="submitted">Submitted by Ste (not verified) on Mon, 2014-07-14 19:57.</div>
    <div class="content">
     <p>Files are actually there (/usr/share/opencv/haarcascades). libopencv* is 2.4.9 though, may it cause problems?</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20773"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/714#comment-20773" class="active">Need to copy .xml files to: (Debian Sid)</a></h3>    <div class="submitted">Submitted by Am Tresen (not verified) on Thu, 2014-07-17 05:30.</div>
    <div class="content">
     <p>Okay, I managed to make it work. I needed to copy the xml files into the following directory:</p>
<p>/home/YOURUSERNAME/.kde/share/apps/libkface/haarcascades/</p>
<p>Thanks for the support :-)</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20777"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/714#comment-20777" class="active">Great. Just a softlink at the</a></h3>    <div class="submitted">Submitted by Ste (not verified) on Thu, 2014-07-17 19:45.</div>
    <div class="content">
     <p>Great. Just a softlink at the dir and face detection works great again!</p>
<p>Thanksss!</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-20775"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/714#comment-20775" class="active">I have nothing to do with the</a></h3>    <div class="submitted">Submitted by philip5 (not verified) on Thu, 2014-07-17 18:32.</div>
    <div class="content">
     <p>I have nothing to do with the digikam packages in Debian Sid BUT they forcefully remove all haarcascades and lbpcascades files that Digikam provides and make the packages dependent on them as provided from the opencv-data package. the opencv-data package provides them in /usr/share/opencv/haarcascades but I'm not sure if Digikam looks for them in any other place than the self provided files in /usr/share/kde4/apps/libkface/haarcascades? Might be a packaging problem with Digikam or to patch Digikam to also look in /usr/share/opencv/haarcascades for the files...</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20776"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/714#comment-20776" class="active">libkface implementation look this path...</a></h3>    <div class="submitted">Submitted by digiKam on Thu, 2014-07-17 18:58.</div>
    <div class="content">
     <p>... but only from 4.1.0 release. It sound like you have not packaged digiKam 4.1.0 with the libkface provided into the tarball, and used the system based library which is an older version.</p>
<p>Look code here in libkface :</p>
<p>https://projects.kde.org/projects/extragear/libs/libkface/repository/revisions/master/entry/libkface/facedetector.cpp#L65</p>
<p>digiKam</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div><a id="comment-20772"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/714#comment-20772" class="active">This is what debug gave out</a></h3>    <div class="submitted">Submitted by Am Tresen (not verified) on Wed, 2014-07-16 20:26.</div>
    <div class="content">
     <p>This is what debug gave out for me:</p>
<p>digikam(6660)/KFACE KFaceIface::FaceDetector::Private::backend: OpenCV Haar Cascades dir found at  ()<br>
digikam(6660)/KFACE: OpenCV Haar Cascade director cannot be found. Did you install OpenCV XML data files?</p>
<p>I guess the problem is that the Cascades directory isnt found. It is at /usr/share/opencv/haarcascades though, and i downloaded all the XML files from the website you specified. Is there any workaround for this, like specifying the directory for OpenCV or something?</p>
<p>Thanks in advance,</p>
<p>Tresen</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div></div></div><a id="comment-20783"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/714#comment-20783" class="active">make fails</a></h3>    <div class="submitted">Submitted by <a href="http://www.cuestrin.de" rel="nofollow">Andy SteinhaufAnonymous</a> (not verified) on Thu, 2014-07-24 19:35.</div>
    <div class="content">
     <p>Hello,</p>
<p>I've updated cv to 2.4.9 but digikam doesn't compile:</p>
<p>Scanning dependencies of target kfacegui<br>
[  5%] Building CXX object extra/libkface/test/CMakeFiles/kfacegui.dir/kfacegui_automoc.cpp.o<br>
[  5%] Building CXX object extra/libkface/test/CMakeFiles/kfacegui.dir/kfacegui/main.cpp.o<br>
[  5%] Building CXX object extra/libkface/test/CMakeFiles/kfacegui.dir/kfacegui/mainwindow.cpp.o<br>
[  5%] Building CXX object extra/libkface/test/CMakeFiles/kfacegui.dir/kfacegui/faceitem.cpp.o<br>
[  5%] Building CXX object extra/libkface/test/CMakeFiles/kfacegui.dir/kfacegui/button.cpp.o<br>
[  5%] Building CXX object extra/libkface/test/CMakeFiles/kfacegui.dir/kfacegui/marquee.cpp.o<br>
[  5%] Building CXX object extra/libkface/test/CMakeFiles/kfacegui.dir/kfacegui/fancyrect.cpp.o<br>
Linking CXX executable kfacegui<br>
/usr/bin/ld: cannot open output file kfacegui: Ist ein Verzeichnis<br>
collect2: Fehler: ld gab 1 als Ende-Status zurück<br>
make[2]: *** [extra/libkface/test/kfacegui] Fehler 1<br>
make[1]: *** [extra/libkface/test/CMakeFiles/kfacegui.dir/all] Fehler 2<br>
make: *** [all] Fehler 2</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-20784"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/714#comment-20784" class="active">install</a></h3>    <div class="submitted">Submitted by Bob in Spain (not verified) on Sun, 2014-07-27 08:39.</div>
    <div class="content">
     <p>digiKam looks like a great program but way too complicated for me to install and run - I'm only an average user and I'm not stupid but this looks too intimidating for me, big shame.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20785"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/714#comment-20785" class="active">install</a></h3>    <div class="submitted">Submitted by Pedro Rodrigues (not verified) on Wed, 2014-07-30 00:06.</div>
    <div class="content">
     <p>Hi Bob<br>
As any feature rich software, digikam is a big bunch of complex code that depends on, even more, complex codes. Put it to work starting from source files may be a little hard. Yet, it is usualy very easy to install by using the binary packages provided by most linux distributions.</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-20786"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/714#comment-20786" class="active">Readability of Announcements</a></h3>    <div class="submitted">Submitted by Bob (not verified) on Thu, 2014-07-31 12:10.</div>
    <div class="content">
     <p>I know this may come across as a bit petty, but I would be grateful if you could arrange for a team member who is more fluent in English to proofread and edit your release announcements. The announcements are difficult to read due to some errors.</p>
<p>I appreciate all of your hard work on digiKam. As I want to be constructive, I offer you a revision of the above release announcement and I hope that you find it helpful:</p>
<dl>
<dd>
Dear <cite>digiKam</cite> fans and users,
<p>The <cite>digiKam</cite> Team is proud to announce the release of <cite>digiKam Software Collection</cite> 4.1.0. This release includes many fixes since the previous stable release 4.0.0:</p>
<ul>
<li>The face management feature has seen huge improvements and includes fixes for some problems introduced in recent releases.</li>
<li>The detection and recognition of faces is now more robust and suitable for production.</li>
<li>All icon views now have a new overlay to identify photos that have geo-location information, making it easier for users to search albums for photos with GPS coordinates.</li>
<li>The maximum thumbnail size is now 512 pixels, up from 256 pixels previously, improving their appearance on high-resolution displays.</li>
</ul>
<p>As usual, we have worked hard to close your reported issues. A list of the issues closed in <cite>digiKam</cite> 4.1.0 is available through the <cite>KDE Bugtracking System</cite>.</p>
<p>The <cite>digiKam</cite> software collection tarball can be downloaded from the <cite>KDE</cite> repository.</p>
<p>Have fun playing with your photos using this new release,</p>
<p>The <cite>digiKam</cite> Team.<br>
</p></dd>
</dl>
<p>E&amp;OE</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20787"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/714#comment-20787" class="active">fixed</a></h3>    <div class="submitted">Submitted by digiKam on Sat, 2014-08-02 04:41.</div>
    <div class="content">
     <p>fixed and published...</p>
         </div>
    <div class="links">» </div>
  </div>
</div>
</div>
