---
date: "2013-03-13T09:22:00Z"
title: "digiKam Software Collection 3.1.0 is out..."
author: "digiKam"
description: "Dear all digiKam fans and users! After one month since 3.0.0 release, digiKam team is proud to announce the digiKam Software Collection 3.1.0, as bug-fixes"
category: "news"
aliases: "/node/688"

---

<a href="http://www.flickr.com/photos/digikam/8456040242/"><img src="http://farm9.staticflickr.com/8518/8456040242_6884ac2977_o.png" width="400" height="245"></a>

<p>Dear all digiKam fans and users!</p>

<p>After one month since 3.0.0 release, digiKam team is proud to announce the digiKam Software Collection 3.1.0, as bug-fixes release. digiKam 3.x versions include GoSC 2012 projects <a href="http://community.kde.org/Digikam/GSoC2012">listed here</a>.</p>

<p>With this release several internal patches have been applied to increase source code stability. digiKam team has used <a href="http://scan.coverity.com/">Coverity SCAN tool</a> to parse whole implementation and fix in-deep bugs. Also, MAC-OSX support have been improved and crashes under this platform have been hacked.</p> 

<p>See <a href="https://bugs.kde.org/buglist.cgi?f1=cf_versionfixedin&amp;o1=equals&amp;query_format=advanced&amp;bug_status=RESOLVED&amp;bug_status=NEEDSINFO&amp;bug_status=VERIFIED&amp;bug_status=CLOSED&amp;v1=3.1.0&amp;product=digikam&amp;product=digikamimageplugins&amp;product=kipiplugins&amp;product=showfoto">the list of files closed</a> with this release into KDE bugzilla.</p>

<p>digiKam software collection tarball can be downloaded from <a href="http://download.kde.org/stable/digikam/digikam-3.1.0.tar.bz2.mirrorlist">KDE repository</a></p>

<p>Happy digiKaming...</p>
<div class="legacy-comments">

  <a id="comment-20505"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/688#comment-20505" class="active">Crash during tagging</a></h3>    <div class="submitted">Submitted by Vladimir (not verified) on Wed, 2013-03-13 10:34.</div>
    <div class="content">
     <p>Unfortunately <a href="https://bugs.kde.org/show_bug.cgi?id=278049">this bug</a> is still not fixed.</p>
<p>I like DigiKam but this bug actually prevents me from using it. :-(</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-20506"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/688#comment-20506" class="active">Windows release?</a></h3>    <div class="submitted">Submitted by <a href="http://orrison.com" rel="nofollow">rorrison</a> (not verified) on Wed, 2013-03-13 10:45.</div>
    <div class="content">
     <p>How's the windows build coming?</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20518"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/688#comment-20518" class="active">In progress...</a></h3>    <div class="submitted">Submitted by Ananta Palani on Fri, 2013-03-15 23:23.</div>
    <div class="content">
     <p>I haven't had a lot of time, but the main reason 3.0.0 and 3.1.0 haven't been released yet is that a bug has been identified on upgrade that causes digikam to lose all collections, basically disassociating the database from the actual images. The issue seems to lie in the underlying KDE Windows platform, but it may be fixed in the upcoming KDE 4.10.1 windows release. Regardless, I hope to have digiKam released in the next week or so.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20520"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/688#comment-20520" class="active">Thanks</a></h3>    <div class="submitted">Submitted by <a href="http://orrison.com" rel="nofollow">rorrison</a> (not verified) on Sat, 2013-03-16 11:59.</div>
    <div class="content">
     <p>Looking forward to it!</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-20526"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/688#comment-20526" class="active">Any closer?</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Sun, 2013-04-07 20:03.</div>
    <div class="content">
     <p>its been a few weeks now.  Are you any closer to releasing a windows version?</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20528"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/688#comment-20528" class="active">Any progress with the windows</a></h3>    <div class="submitted">Submitted by chrlau5 (not verified) on Thu, 2013-04-18 09:50.</div>
    <div class="content">
     <p>Any progress with the windows build? Really looking forward to try it and to organize all my photos :-)</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div></div><a id="comment-20507"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/688#comment-20507" class="active">"This will be the last 2.x</a></h3>    <div class="submitted">Submitted by Vangelis (not verified) on Wed, 2013-03-13 11:16.</div>
    <div class="content">
     <p>"This will be the last 2.x release." ?</p>
<p>I guess this must be some forgotten text there, right? :)</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20508"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/688#comment-20508" class="active">...</a></h3>    <div class="submitted">Submitted by digiKam on Wed, 2013-03-13 11:19.</div>
    <div class="content">
     <p>Fixed...</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-20509"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/688#comment-20509" class="active">Face Recognition in Digikam</a></h3>    <div class="submitted">Submitted by <a href="http://alicious.com/" rel="nofollow">pbhj</a> (not verified) on Wed, 2013-03-13 13:01.</div>
    <div class="content">
     <p>Face Recognition - Status : Not completed. </p>
<p>:(</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20510"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/688#comment-20510" class="active">work in progress</a></h3>    <div class="submitted">Submitted by digiKam on Wed, 2013-03-13 13:05.</div>
    <div class="content">
     <p>... and planed in later release... as 3.3.0 or 3.4.0...</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-20511"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/688#comment-20511" class="active">Packages</a></h3>    <div class="submitted">Submitted by bastl (not verified) on Wed, 2013-03-13 13:10.</div>
    <div class="content">
     <p>Thanks Gilles,</p>
<p>as i need the 16bit raw conversion feature in batch tool, I was really waiting for this update. You have any idea, when first packages will be available? (Ubuntu)</p>
<p>Greetzs B.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20512"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/688#comment-20512" class="active">no idea....</a></h3>    <div class="submitted">Submitted by digiKam on Wed, 2013-03-13 13:14.</div>
    <div class="content">
     <p>We provide source tarball, not bin package. Ask to your distro team...</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-20513"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/688#comment-20513" class="active">Clone tool</a></h3>    <div class="submitted">Submitted by Alex (not verified) on Fri, 2013-03-15 01:56.</div>
    <div class="content">
     <p>Thanks for producing this excellent software. The metadata management is great! </p>
<p>I have to echo something mentioned by other users: a Clone tool to remove minor artifacts in the Image Editor would be a big bonus.</p>
<p>Waiting for this version to appear in Gentoo's portage tree...</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20516"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/688#comment-20516" class="active">project not retained in 2012</a></h3>    <div class="submitted">Submitted by digiKam on Fri, 2013-03-15 10:13.</div>
    <div class="content">
     <p>This project have been proposed 2 times. One time it's have not been completed by student (2011), second time not retained by KDE mentors.</p>
<p>digiKam</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20519"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/688#comment-20519" class="active">&gt;This project have been</a></h3>    <div class="submitted">Submitted by Jon Piesing (not verified) on Sat, 2013-03-16 11:41.</div>
    <div class="content">
     <p>&gt;This project have been proposed 2 times. One time it's have not been completed by student (2011), second time not retained by KDE mentors.</p>
<p>That's a pity. Clone is the only time I have to go outside DK to gimp, edit the image there and then mess around with exiftool to get the metadata right again. Oh well.</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div><a id="comment-20514"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/688#comment-20514" class="active">Export to html</a></h3>    <div class="submitted">Submitted by imhof jean-pierre (not verified) on Fri, 2013-03-15 10:02.</div>
    <div class="content">
     <p>Thanks digiKam team for your work:)</p>
<p>With digikam 3.1 over kde 4.10.1 and Opensuse 12.2 (and 12.3), I have no more  "export to html" in my menu "Exportation"</p>
<p>greetings</p>
<p>Jean-Pierre Imhof</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20515"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/688#comment-20515" class="active">libkipi problem</a></h3>    <div class="submitted">Submitted by digiKam on Fri, 2013-03-15 10:12.</div>
    <div class="content">
     <p>Fixed in last libkipi (published with next KDE 4.10.x release)</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20517"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/688#comment-20517" class="active">What a speed, superb!
many</a></h3>    <div class="submitted">Submitted by imhof jean-pierre (not verified) on Fri, 2013-03-15 15:33.</div>
    <div class="content">
     <p>What a speed, superb!<br>
many thanks</p>
<p>Jean-Pierre Imhof</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-20521"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/688#comment-20521" class="active">export to html</a></h3>    <div class="submitted">Submitted by Maciej (not verified) on Tue, 2013-03-19 09:32.</div>
    <div class="content">
     <p>Hello</p>
<p>I have the same problem. In opensuse 12.3 and KDE 4.10.1 i don't have option "export to HTML". I have: digikam-3.1.0-163.1.x86_64 and libkipi10-4.10.1-65.1.x86_64. I downgrade libkipi to old version, but it didn't help.</p>
<p>Best regards<br>
Maciej</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20527"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/688#comment-20527" class="active">HTML export</a></h3>    <div class="submitted">Submitted by breakolami (not verified) on Thu, 2013-04-11 21:41.</div>
    <div class="content">
     <p>Hello I use Digikam 3.1.0 with Kubuntu 13.04 and KDE 4.10.2<br>
I do not find HTML export<br>
I have a lot of export function but no HTML</p>
<p>Thanks for help and sorry for my bad english I'm french</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-20529"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/688#comment-20529" class="active">Export HTML missing</a></h3>    <div class="submitted">Submitted by Emmanuel (not verified) on Wed, 2013-04-24 20:34.</div>
    <div class="content">
     <p>Thank you for your work but why is the Export to HTML gone?</p>
<p>Cheers,</p>
<p>Emmanuel</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-20524"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/688#comment-20524" class="active">how to compile on Ubuntu 12.04</a></h3>    <div class="submitted">Submitted by <a href="http://dia-scan.blogspot.com" rel="nofollow">Mirc M</a> (not verified) on Wed, 2013-03-27 17:20.</div>
    <div class="content">
     <p>A big thanks to digiKam team for this great software !!!</p>
<p>I managed to compile version 3.1 on Ubuntu 12.04.<br>
I have published step by step instructions how to do it.<br>
I hope they are easy enough to follow and succeed even for Linux newbies.</p>
<p><a href="http://dia-scan.blogspot.com/2013/03/how-to-compile-digikam-31-on-ubuntu-1204.html">Here is the link</a></p>
<p>Good luck all, Mirc M</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-20525"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/688#comment-20525" class="active">digiKam software collection</a></h3>    <div class="submitted">Submitted by <a href="http://www.maillotdefoot2014.com/" rel="nofollow">maillotdefoot2014</a> (not verified) on Sat, 2013-03-30 15:24.</div>
    <div class="content">
     <p>I am now encountered some problems, my lens in use sometimes crashes, it makes me a headache,<br>
I will take a look at your collection of software, there may be other discoveries</p>
         </div>
    <div class="links">» </div>
  </div>

</div>
