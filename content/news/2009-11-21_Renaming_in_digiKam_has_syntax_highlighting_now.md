---
date: "2009-11-21T14:57:00Z"
title: "Renaming in digiKam has syntax highlighting now"
author: "Andi Clemens"
description: "Just a quick comment that renaming in digiKam is capable of using syntax highlighting now. It should be easier to decipher a renaming string this"
category: "news"
aliases: "/node/485"

---

<p>Just a quick comment that renaming in digiKam is capable of using syntax highlighting now. It should be easier to decipher a renaming string this way :-)<br>
A quick demo video can be found here: </p>
<object width="425" height="344"><param name="movie" value="http://www.youtube.com/v/GN_bMzOo1og&amp;hl=en_US&amp;fs=1&amp;"><param name="allowFullScreen" value="true"><param name="allowscriptaccess" value="always"><embed src="http://www.youtube.com/v/GN_bMzOo1og&amp;hl=en_US&amp;fs=1&amp;" type="application/x-shockwave-flash" allowscriptaccess="always" allowfullscreen="true" width="425" height="344"></object><p>
... as well as some screenshots:</p>
<p><a href="http://www.flickr.com/photos/26732399@N05/4121367571/" title="syntax_highlighting.png von andiclemens bei Flickr"><img src="http://farm3.static.flickr.com/2599/4121367571_852cd83250_m.jpg" width="222" height="240" alt="syntax_highlighting.png"></a> <a href="http://www.flickr.com/photos/26732399@N05/4122656092/" title="history_advancedrename.png von andiclemens bei Flickr"><img src="http://farm3.static.flickr.com/2615/4122656092_5040734e16_m.jpg" width="240" height="147" alt="history_advancedrename.png"></a> <a href="http://www.flickr.com/photos/26732399@N05/4123459488/" title="A slightly more complex example pattern von andiclemens bei Flickr"><img src="http://farm3.static.flickr.com/2536/4123459488_f4db9b9e57_m.jpg" width="240" height="151" alt="A slightly more complex example pattern"></a></p>

<div class="legacy-comments">

  <a id="comment-18834"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/485#comment-18834" class="active">What about saving changes?</a></h3>    <div class="submitted">Submitted by paurullan (not verified) on Sat, 2009-11-21 16:35.</div>
    <div class="content">
     <p>Is there any plan about saving these strings? I always use the "dir_[date:yyyy-MM-dd]_###" and there could be some way to avoid to write it.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18836"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/485#comment-18836" class="active">As you can see, it is a</a></h3>    <div class="submitted">Submitted by Andi Clemens on Sat, 2009-11-21 18:55.</div>
    <div class="content">
     <p>As you can see, it is a combobox widget. Currently it holds a history of the last 20 typed in patterns.<br>
This history is shared among the AlbumUI, CameraUI and Batch Queue Manager, so you have access to the history from everywhere.<br>
For every instance the RenameWidget is used in, the last used pattern is saved and restored individually.<br>
So if you often use a specific pattern in Batch Queue Manager, but a different one in CameraUI or AlbumUI, it i remembered correctly.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18839"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/485#comment-18839" class="active">Great!
Right now on ubuntu</a></h3>    <div class="submitted">Submitted by paurullan (not verified) on Sat, 2009-11-21 23:09.</div>
    <div class="content">
     <p>Great!<br>
Right now on ubuntu karmic with digikam 1.0.0-beta5 it seems the used patters do not appear but no big deal.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18840"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/485#comment-18840" class="active">That's because beta5 doesn't</a></h3>    <div class="submitted">Submitted by Andi Clemens on Sun, 2009-11-22 02:52.</div>
    <div class="content">
     <p>That's because beta5 doesn't have the history... :D<br>
Ubuntu had the stupid idea to add a beta version of digiKam into their stable release.<br>
Unfortunately this version crashes all the time when using camera import.<br>
digiKam-1.0.0-beta6 has been released since two weeks, but Ubuntu doesn't seem to upgrade the package.<br>
I really hope they will not ship the beta5 for 6 months now, this can really shed a false light on digiKam, because<br>
of the crashes... (that's why it is a beta version, they should at least have added it into an experimental repository).</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18841"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/485#comment-18841" class="active">Is there a way to update</a></h3>    <div class="submitted">Submitted by Skippy (not verified) on Sun, 2009-11-22 09:54.</div>
    <div class="content">
     <p>Is there a way to update digikam to the last version for kubuntu without compiling?<br>
A ppa repository? A deb package?</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18842"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/485#comment-18842" class="active">Luka Renko is providing</a></h3>    <div class="submitted">Submitted by Andi Clemens on Sun, 2009-11-22 11:35.</div>
    <div class="content">
     <p>Luka Renko is providing digiKam-beta6 in his PPA:<br>
<a href="https://launchpad.net/~lure/+archive/ppa">PPA of Luka Renko</a></p>
<p>Maybe you can give it a try?</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18844"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/485#comment-18844" class="active">Seems working.
Thanks a lot</a></h3>    <div class="submitted">Submitted by Skippy (not verified) on Sun, 2009-11-22 18:42.</div>
    <div class="content">
     <p>Seems working.<br>
Thanks a lot</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-18886"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/485#comment-18886" class="active">DigiKam update...how?</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Fri, 2009-12-11 00:19.</div>
    <div class="content">
     <p>Hi there,</p>
<p>sorry for not knowing ANYthing...but...i would like to update Digikam but dont even know how....I use Linux Mint and have DigiKam vers. 0.10.0 and i not even know what PPA and other stuff even means.</p>
<p>also: if i update, all my tags are still where they belong. right?</p>
<p>bonus question:<br>
i would like to "print" the data of the tags into the data of the pics itself, they should be a part of every individual picture. since it would be pointless to tag thousands of pics for weeks and than after i format (for example) the pc it was all in vain....<br>
i was told this is possible but the way wich was explained to me doesnt exist in my vers. as it seems</p>
<p>thank you for reading</p>
<p>hope you can help me here </p>
<p>regards</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div></div></div></div><a id="comment-18837"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/485#comment-18837" class="active">Look here:</a></h3>    <div class="submitted">Submitted by Andi Clemens on Sat, 2009-11-21 19:04.</div>
    <div class="content">
     <p>Look here:</p>
<p><a href="http://www.flickr.com/photos/26732399@N05/4122656092/" title="history_advancedrename.png von andiclemens bei Flickr"><img src="http://farm3.static.flickr.com/2615/4122656092_5040734e16_m.jpg" width="240" height="147" alt="history_advancedrename.png"></a></p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-18835"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/485#comment-18835" class="active">It would be cool to have this</a></h3>    <div class="submitted">Submitted by Gogast (not verified) on Sat, 2009-11-21 18:53.</div>
    <div class="content">
     <p>It would be cool to have this as a standalone tool for KDE</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18838"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/485#comment-18838" class="active">Right now this tool is</a></h3>    <div class="submitted">Submitted by Andi Clemens on Sat, 2009-11-21 19:17.</div>
    <div class="content">
     <p>Right now this tool is closely tight into digiKam and I guess it will be even more dependend of the digiKam code in the future, since we want to provide every single peace of information from our databases, if possible.</p>
<p>There are standalone renaming tools like krename, gwenrename and metamorphose2. I don't think another one would make any sense.<br>
I started this tool only to have it integrated in digiKam for importing images, renaming in the main window and for using it in the Batch Queue Manager. I had not found a batch renamer that is just a small widget that can be plugged into digiKam.<br>
So I created my own...</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-18845"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/485#comment-18845" class="active">Only as Good as the Exif Library</a></h3>    <div class="submitted">Submitted by Bob (not verified) on Mon, 2009-11-23 11:54.</div>
    <div class="content">
     <p>I wrote my own renaming script based on Exiftool because no<br>
matter how pretty the renaming tool is in digiKam, it depends<br>
on the capabilities of an Exif library to support the Exif<br>
tags. In this regard, "Exiv2" is sorely lacking.</p>
<p>The information I want in the file name includes the Canon<br>
Maker Note tag "File Number". If the camera's option not<br>
to reset the file number is chosen, then each file gets a<br>
unique, sequential number that is extremely convenient for<br>
identifying them. I like to add the date and a few other<br>
details to the file name, but I always keep the file number<br>
in the name, too. Exiv2 cannot read this tag at all, so the<br>
renamer in digiKam is of no use to me. (The full file number<br>
is not given in the original file name, so parsing it from<br>
there is not an option.)</p>
<p>Exiv2 has had two updates this year, the last one five months<br>
ago. Exiftool had its 41st update of the year last Friday.<br>
Exiv2 is moving at a frustratingly slow pace and is the source<br>
of my major gripe with digiKam: the inability to write tags to<br>
Canon CR2 raw files. That makes the digiKam database a single<br>
point of failure and prevents me from having standalone image<br>
files from which I can reconstruct all needed metadata.</p>
<p>Now, I really like digiKam and use it almost exclusively for<br>
photo management, but the "almost" is because Exiv2 just does<br>
not match the standard of the rest of the package and there<br>
seems to be little hope that it will ever get up to speed.<br>
So, my next little project is to use Exiftool to read from<br>
the digiKam database and write to the CR2 files, as it will<br>
probably be years before Exiv2 can do this, if at all.</p>
<p>Great work on digiKam, but don't forget about the total<br>
package: it's more than just the GUI.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18846"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/485#comment-18846" class="active">Well have you reported this</a></h3>    <div class="submitted">Submitted by Andi Clemens on Mon, 2009-11-23 14:54.</div>
    <div class="content">
     <p>Well have you reported this bug to exiv2?<br>
I can not find anything in their bugtracker about this issue, so how should they know? ;-)</p>
<p>Other Canon RAW files are working fine here, I can extract Canon.ImageNumber.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18855"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/485#comment-18855" class="active">The lack of Canon CR2 write</a></h3>    <div class="submitted">Submitted by Bob (not verified) on Tue, 2009-11-24 20:30.</div>
    <div class="content">
     <p>The lack of Canon CR2 write support is Exiv2 "Feature #635". Is it<br>
an open issue and the target release of "1.0" was deleted six months<br>
ago. There have been no updates on the issue since then. As far as I<br>
can tell it's dead, or at least years away.</p>
<p>The "Canon.ImageNumber" tag does not work for the EOS 40D. I haven't<br>
tried it on the 50D, but I wouldn't hold out much hope for it there.<br>
Exiftool had a fix for this about two years ago.</p>
<p>Exiftool recognises 228 Exif tags in my (now replaced) 40D images,<br>
Exiv2 recognises only 99. For my S3 IS it's 187 vs. 110. For my<br>
(now replaced) A40 it's 175 vs. 106. I haven't got the figures to<br>
hand for my 50D and (now replaced) 400D, but I assume they are<br>
about the same as the 40D. Should I open 500+ bug reports--one for<br>
each tag--or one each for the tags that I am most intested in, or<br>
one for each camera, or just one big one?</p>
<p>There is already one big one: "Feature #481", opened 26 July, 2006,<br>
requesting that Exiv2 support the tags that are supported by<br>
Exiftool. The response (the following day) was:</p>
<p><cite>"To some extent this is work in progress; with the new TIFF<br>
parser Exiv2 will be able to decode more Exif Makernote tags."</cite></p>
<p>That new parser has yet to be released. I'm on my fifth camera since<br>
the time that was reported, so I have come to the conclusion that what<br>
Exiv2 contributes to digiKam--beyond the basics--is irrelevant to me.<br>
The time to wait for fixes and the release cycle are just far too long;<br>
longer than the upgrade cycle for my cameras. Exiftool works today and<br>
fixes for new camera models come fast (there are already fixes for the<br>
Canon 1D Mark IV, and that camera has yet to be released).</p>
<p>DigiKam, on the other hand, I'm sticking with. It is a great application.<br>
The developers handled the KDE 4 transition more professionally than any<br>
other application that I use regularly. I *trust* that it will continue<br>
to improve. Where it falls down--or, more rightly, where Exiv2 lets it<br>
down--I can compensate for with Exiftool.</p>
<p>I suppose my gripe is that the failings of Exiv2 mean that work on<br>
things like syntax highlighting for file renaming are just developer<br>
hours that I would much rather see spent on things like CR2 write support<br>
or image grouping. There are a host of things on the digiKam wish list<br>
with hundreds of votes; syntax highlight in the file renamer is not among<br>
them.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18857"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/485#comment-18857" class="active">Maybe "Syntax Highlighting"</a></h3>    <div class="submitted">Submitted by Andi Clemens on Wed, 2009-11-25 14:32.</div>
    <div class="content">
     <p>Maybe "Syntax Highlighting" was not a vote in BKO, but you know, as a developer I have wishes, too ;-)<br>
And one wish on BKO was that renaming in digiKam is not possible because it lacks features.<br>
Also I didn't spent months on implementing this, overall it was just 2 hours...</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18860"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/485#comment-18860" class="active">That's quite OK. I was only</a></h3>    <div class="submitted">Submitted by Bob (not verified) on Fri, 2009-11-27 12:54.</div>
    <div class="content">
     <p>That's quite OK. I was only expressing my disappointment that my personal<br>
biases did not coincide with yours. C'est la vie.</p>
<p>I fully understand that open source software is developed by those who<br>
have an itch to scratch. You are quite entitled to implement what matters<br>
most to you, as it is your time and effort that goes into it. Were us users<br>
paying you for this, then we could demand that you address the wish list<br>
items, but we are not, so we can't. All we can do is appeal to you to<br>
keep an eye on the wish list and hope that you can find a little time to<br>
use your skills to scratch a few other itches. The "itches" with the most<br>
votes on the wish list would probably make more people happy than those<br>
with fewer votes or none.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18861"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/485#comment-18861" class="active">At least your FileNumber</a></h3>    <div class="submitted">Submitted by Andi Clemens on Fri, 2009-11-27 16:34.</div>
    <div class="content">
     <p>At least your FileNumber issue has been solved and will be available in exiv2-0.19:</p>
<p>[andi@macbook:/mnt/data/fotos/digiKam tests/RAW]$ /home/andi/Programmieren/KDE/exiv2-dev/src/exiv2 -pt IMG_4807.CR2 | grep Num<br>
Exif.Photo.FNumber                           Rational    1  F2.8<br>
Exif.Canon.SerialNumber                      Long        1  6d2426148<br>
<strong>Exif.CanonFi.FileNumber                      Long        1  100-4807</strong><br>
Exif.CanonFi.BracketShotNumber               SShort      1  0</p>
<p>Well at least I hope :-) This example was generated with files from a Canon EOS D400.</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div><a id="comment-18858"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/485#comment-18858" class="active">I'm currently trying to write</a></h3>    <div class="submitted">Submitted by Andi Clemens on Wed, 2009-11-25 20:14.</div>
    <div class="content">
     <p>I'm currently trying to write a patch to support Canon File Information tags. Some fields are already working, FileNumber isn't :-) But I guess it will be done at the end of the day.</p>
<p>I will sent the patch to Andreas for review, I have never ever done anything for exiv2 and I'm still confused a little bit :-)</p>
<p>Andi</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div><a id="comment-18849"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/485#comment-18849" class="active">It is correct that Canon</a></h3>    <div class="submitted">Submitted by <a href="http://www.exiv2.org" rel="nofollow">Andreas</a> (not verified) on Tue, 2009-11-24 04:14.</div>
    <div class="content">
     <p>It is correct that Canon makernote hasn't seen an update in a long time and CR2 write support is not yet implemented. So I can understand the frustration if you are a Canon user and would like to see these features in digiKam. The good news is that both of these changes are not very difficult to implement. So what about this: I add the CR2 write support for the upcoming 0.19 release and you take care of the makernote update?</p>
<p>I have been releasing Exiv2 about four times per year. This year it will be three releases assuming I find the time to finish 0.19. The functionality has been extended greatly over time and there is always development activity, also between releases: close to 100 commits have been made to the source code repository since 0.18.2. Most Linux distros don't get updated more than four times a year, and you can always get the latest development version from the Exiv2 SVN, so this release strategy still seems appropriate.</p>
<p>Technically, implementing this functionality in C++ requires more lines of code and is more time consuming than doing the same in Perl. Performance is much better on the other hand and for many C/C++ applications it is not feasible to use a Perl application/library for the job.</p>
<p>At the end of the day, it all boils down to time. Progress depends on the amount of time invested by the people working on it. Join us to make things happen faster.</p>
<p>Andreas</p>
         </div>
    <div class="links">» </div>
  </div>
</div>
</div>