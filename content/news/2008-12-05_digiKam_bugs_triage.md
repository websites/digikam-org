---
date: "2008-12-05T11:57:00Z"
title: "digiKam bugs triage..."
author: "digiKam"
description: "Since few days, team works on KDE bugzilla to clean bugs database before to release digiKam 0.10.0-beta7 for KDE4 and digiKam 0.9.5-beta2 for KDE3. More"
category: "news"
aliases: "/node/411"

---

<p>Since few days, team works on KDE bugzilla to clean bugs database before to release digiKam 0.10.0-beta7 for KDE4 and digiKam 0.9.5-beta2 for KDE3.</p>

<a href="http://www.flickr.com/photos/digikam/3083776081/" title="setupcollection-kde4 by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3247/3083776081_2ce7f712f6.jpg" width="500" height="400" alt="setupcollection-kde4"></a>

<p>More than 30 files have been closed in 3 days, and less than one quarter of all files have been parsed. Note than digiKam and DigikamImagePlugins component in bugzilla counts around 500 entries.</p>

<p>We need help to review all reports. If you have already file a bug about digiKam, please check if this one still valid to use last KDE3 stable release (0.9.4), or better to try current 0.10.0-beta6 release for KDE4.</p>

<p>Thanks in advance...</p>


<div class="legacy-comments">

  <a id="comment-18031"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/411#comment-18031" class="active">digikam on KDE4 is hot!</a></h3>    <div class="submitted">Submitted by morgan (not verified) on Fri, 2008-12-05 19:53.</div>
    <div class="content">
     <p>Great work!  Latest build on KDE4 doesn't crash on me any more upon startup.  And it's beautiful!  And functional!  Excellent, excellent job!  So pretty.</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-18032"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/411#comment-18032" class="active">Thank you</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Fri, 2008-12-05 23:37.</div>
    <div class="content">
     <p>After my last comment, on the other post, I want to thank you for bringing us back diversity with a picture not from windows.</p>
<p>so, thank you</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-18033"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/411#comment-18033" class="active">Dang, I wanna try this so bad...</a></h3>    <div class="submitted">Submitted by <a href="http://kdenlive.org" rel="nofollow">Bugsbane</a> (not verified) on Sat, 2008-12-06 01:05.</div>
    <div class="content">
     <p>Curse you, slow, outdated repositories! Curse my lack of compiling skills!</p>
<p>May {$DIETY} grant my wish that one day we shall see DigiKam included in project Neon.</p>
<p>*sigh*</p>
<p>Great work Giles. It sounds amazing, and as I'm sure you can guess by now we're eagerly awaiting getting our hands on this delicious sounding new DigiKam.</p>
<p>PS It took 5 attempts to get this message through your CAPTCHA...</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18034"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/411#comment-18034" class="active">&gt; PS It took 5 attempts to</a></h3>    <div class="submitted">Submitted by xeros (not verified) on Sat, 2008-12-06 16:12.</div>
    <div class="content">
     <p>&gt; PS It took 5 attempts to get this message through your CAPTCHA...</p>
<p>Not only you - it's difficult to read the code.</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-18035"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/411#comment-18035" class="active">captcha</a></h3>    <div class="submitted">Submitted by <a href="http://dotancohen.com" rel="nofollow">Dotan Cohen</a> (not verified) on Sat, 2008-12-06 18:23.</div>
    <div class="content">
     <p>I agree, the captcha has prevented me from posting here numerous times. It is simply not worth the effort- that is time that I could be triaging bugs!</p>
<p>Gilles: Might I suggest that the digiKam screenshots use the default KDE theme and colours? While I, like you, prefer to save my eyes and use a dark theme, most of the world see that dark theme and it turns them off. I know this because I've introduced people to digiKam and after seeing the dark screenshots they don't want to use it, even after they've seen it running on my desktop and expressed interest (both Windows and Linux users).</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-18036"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/411#comment-18036" class="active">development</a></h3>    <div class="submitted">Submitted by Stefan Waidheim (not verified) on Sun, 2008-12-07 16:12.</div>
    <div class="content">
     <p>The current development looks nice and stable, beta6 comes around quite good. Sadly my isp screwed up big time so at home I am left without internet for at least until mid january :( I will still try to update my patch but it will take some time.<br>
So I beg one of the devs to have a look at the bug and squash it, I suppose it won't take more than a few lines changed.</p>
<p>ps The captcha is extremely hard to read lately, sometimes I can only guess.</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-18039"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/411#comment-18039" class="active">Can anybody help me???? I'm desperate...</a></h3>    <div class="submitted">Submitted by giancarlo (not verified) on Mon, 2008-12-08 21:52.</div>
    <div class="content">
     <p>I've been almost two weeks surfing on the web but I can't solve an annoying problem with qt/kdelibs.</p>
<p>Let's explain:<br>
- I have a KXmlGuiWindow, but I do not want to use xml config file for his management,<br>
- the window already has a layout, so I can't use another one, or if I try I got some new errors,<br>
- I need to place a QTreeWidget in the center of the window, without using KDockWidget because it feels ugly (if I could have a KDockWidget without the top bar it could be perfect)<br>
- I need to place a terminal (made using kparts) rather than the QTreeWidget, when some event happens<br>
- I need to place other widgets rather than the QTreeWidget and the terminal, when some other event happens...</p>
<p>But:<br>
- Using setCentralWidget() I can put my QTreeWidget into the window but if I try to hide OR delete it before inserting the terminal widget, my app crashes</p>
<p>I've been trying with a new object which inherits from QWidget, placing a QLayout into it and trying to set there my objects.<br>
But:<br>
- I need to delete any widget from the layout, before inserting another one<br>
- On qt4.4.3 there is no such method like the old removeAllItems() (qt4.2)<br>
- If I try to delete any item from the layout, with either removeItem()/removeWidget(), the widget will be deleted but my window will remain dirty</p>
<p>For example, if I delete the terminal and then I set the QTreeWidget, the terminal object won't exist no more, but the terminal will remain on the window! And under the terminal I'll have the QTreeWidget.</p>
<p>I hope anybody could help me, I'm going crazy. Thanks.</p>
<p>(sorry for my bad english, but unfortunately I'm italian)</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18040"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/411#comment-18040" class="active">ops I forgot my mail:</a></h3>    <div class="submitted">Submitted by giancarlo (not verified) on Mon, 2008-12-08 21:53.</div>
    <div class="content">
     <p>ops I forgot my mail:  badblock AT email DOT it<br>
thanks :)</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-18042"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/411#comment-18042" class="active">Great work!
However I have</a></h3>    <div class="submitted">Submitted by zdenek (not verified) on Sun, 2008-12-14 22:39.</div>
    <div class="content">
     <p>Great work!<br>
However I have one question regarding RAW batch conversion. I don't know maybe I'm doing something wrong, but when I use batch conversion, resulting JPEG from Raws with portrait orientation are not rotated correctly. Does batch conversion respect this kind of information from EXIF?</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18043"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/411#comment-18043" class="active">Exif rotation can be written outside Exif standard</a></h3>    <div class="submitted">Submitted by digiKam on Mon, 2008-12-15 06:22.</div>
    <div class="content">
     <p>This is incredible, but true : some Camera constructors use Exif Makernotes to host image orientation informations (as Panasonic Raw file for ex.). If Exiv2 library don't know these informations encoding, digiKam is not able to set the right image orientation during Raw conversion...</p>
<p>digiKam</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-18077"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/411#comment-18077" class="active">Can anyone tell me what</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Fri, 2008-12-19 17:23.</div>
    <div class="content">
     <p>Can anyone tell me what happened to all the batch editing tools, e.g. batch resize? They do not show up anymore even though kipi-plugins is installed. All I'm seeing is DNG converter and one other tool I can't remember right now.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18078"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/411#comment-18078" class="active">It's will not ported (by me)...</a></h3>    <div class="submitted">Submitted by digiKam on Fri, 2008-12-19 17:33.</div>
    <div class="content">
     <p>I will not port this kipi-plugin. For digiKam i prepare a new tool, more powerfull, where we can assign more than one batch action to items: a <b>Batch Queue Manager</b>.</p>

<p>Developement is already started, but not yet complete. I'm not sure if it will done for digiKam 0.10.0. A screenshot is given below:</p>

<a href="http://www.flickr.com/photos/digikam/3065212027/" title="batchqueuemanager-alpha1 by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3060/3065212027_43f4b42fb0.jpg" width="500" height="400" alt="batchqueuemanager-alpha1"></a>

<p>digiKam</p>         </div>
    <div class="links">» </div>
  </div>
</div>
</div>
