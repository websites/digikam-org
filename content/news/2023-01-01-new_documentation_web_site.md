---
date: "2023-01-01T00:00:00"
title: "New digiKam User Documentation"
author: "digiKam Team"
description: "Dear digiKam fans and users, we are proud to announce the availability of the new user documentation web site."
category: "news"
---

[![](https://i.imgur.com/9Mmng9R.png "The New digiKam Documentation Website")](https://imgur.com/9Mmng9R)

Dear digiKam fans and users,

A new year means new goals, and this one is a challenge: migrating the old digiKam documentation based on [DocBook](https://en.wikipedia.org/wiki/DocBook) format to a new architecture, more simple and easy to maintain.
After 20 years, we left the DocBook manual for the modern [Sphinx/ReStructuredText](https://www.sphinx-doc.org/) framework. This one is really a pleasure to use by documentation writers.

The content is published in a [dedicated web site](https://docs.digikam.org/en/index.html), internationalized by the KDE translation teams.
An [EPUB](https://docs.digikam.org/en/epub/DigikamManual.epub) version is also available for the off-line use cases.

The new documentation is now open for contributions, to fix contents and add new sections/chapters. Please look at the dedicated [git repository](https://invent.kde.org/documentation/digikam-doc),
and especially the [README](https://invent.kde.org/documentation/digikam-doc/-/blob/master/README.md) file where you will find all technical details to help us with this manual.

Thanks for your future supports.

The digiKam team wishes you a happy new year 2023.
