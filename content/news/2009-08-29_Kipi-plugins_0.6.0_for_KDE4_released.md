---
date: "2009-08-29T15:44:00Z"
title: "Kipi-plugins 0.6.0 for KDE4 released"
author: "digiKam"
description: "Dear all digiKam fans and users! Kipi-plugins 0.6.0 maintenance release for KDE4 is out. kipi-plugins tarball can be downloaded from SourceForge at this url Kipi-plugins"
category: "news"
aliases: "/node/476"

---

<p>Dear all digiKam fans and users!</p>

<p>Kipi-plugins 0.6.0 maintenance release for KDE4 is out.</p>

<a href="http://www.flickr.com/photos/digikam/3867809090/" title="kipiplugins0.6.0 by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3449/3867809090_75eb0b583c.jpg" width="500" height="313" alt="kipiplugins0.6.0"></a>

<p>kipi-plugins tarball can be downloaded from SourceForge <a href="http://sourceforge.net/projects/kipi/files">at this url</a></p>

<p>Kipi-plugins will be also available for Windows. Precompiled packages can be donwloaded with KDE-Windows installer. See <a href="http://windows.kde.org">KDE-Windows project</a> for details.</p>

<p>See below the list of new features and bugs-fix coming with this release:</p>

<b>AdvancedSlideshow</b> : Soundtracks can be loaded / saved now.<br><br>

001 ==&gt; AdvancedSlideshow  : 142676 : Recursive slideshow option.<br>
002 ==&gt; AdvancedSlideshow  : 195772 : Allow to load playlists rather than having to re-add the songs one by one.<br>
003 ==&gt; AdvancedSlideshow  : 195699 : Ken Burns : it takes minutes before the slideshow actually starts.<br>
004 ==&gt; AdvancedSlideshow  : 182743 : Renders system unresponsive on large image collections.<br>
005 ==&gt; GalleryExport      : 202019 : [PATCH] : mpGallery not initialized, crash on destruction if setup not called.<br>
006 ==&gt; AdvancedSlideshow  : 131619 : Blend transition in slideshow without OpenGL.<br>
007 ==&gt; SendImages         : 147513 : It's non possible to send the file of photo because kmail don't start.<br>
008 ==&gt; BatchProcessImages : 202447 : digiKam crashes after recompressing one or several images.<br>
009 ==&gt; BatchProcessImages : 202706 : digiKam crashes after closing the batch resize window.<br>
010 ==&gt; AdvancedSlideshow  : 172384 : digiKam crashes when exiting advanced slide show from kipi-plugins.<br>
011 ==&gt; BatchProcessImages : 203232 : Batch compress images crash.<br>
012 ==&gt; PrintWizard        : 150479 : Provide export to pdf file.<br>
013 ==&gt; BatchProcessImages : 203379 : Error while converting RAW images to JPEG.<br>
014 ==&gt; FlickrExport       : 203848 : Add ability to upload to flickr photo stream only.<br>
015 ==&gt; FlickrExport       : 203849 : Add option to create a photo set from uploaded photos.<br>
016 ==&gt; BatchProcessImages : 199932 : Batch resize images produces images 1 pixel too short.<br>
017 ==&gt; HTMLExport         : 196968 : Add link-tags to exported HTML-galleries to ease navigation.<br>
018 ==&gt; PrintWizard        : 202480 : PrintWizard doesn't add comments properly but shows them in the preview.<br>
019 ==&gt; AdvancedSlideshow  : 098617 : Drag and drop custom image sort in slide show.<br>
<div class="legacy-comments">

</div>
