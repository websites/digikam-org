---
date: "2015-02-24T22:48:00Z"
title: "digiKam Software Collection 4.8.0 released..."
author: "digiKam"
description: "Dear digiKam fans and users, The digiKam Team is proud to announce the release of digiKam Software Collection 4.8.0. This release includes a new sets"
category: "news"
aliases: "/node/732"

---

<a href="https://www.flickr.com/photos/digikam/16015768494"><img src="https://farm9.staticflickr.com/8634/16015768494_e0a8e7d3cc_c.jpg" width="800" height="225"></a>

<p>Dear digiKam fans and users,</p>

<p>
The digiKam Team is proud to announce the release of digiKam Software Collection 4.8.0. This release includes a new sets of bugs fixes from <a href="https://plus.google.com/u/0/107171232114475191915/about">Maik Qualmann</a> who maintain KDE4 version while <a href="https://techbase.kde.org/Projects/Digikam/CodingSprint2014#KDE_Framework_Port">KF5 port is under progress</a>.
</p>

<p>
See the new list of the <a href="https://bugs.kde.org/buglist.cgi?f1=cf_versionfixedin&amp;o1=equals&amp;query_format=advanced&amp;bug_status=RESOLVED&amp;bug_status=NEEDSINFO&amp;bug_status=VERIFIED&amp;bug_status=CLOSED&amp;v1=4.8.0&amp;product=digikam&amp;product=digikamimageplugins&amp;product=kipiplugins&amp;product=showfoto">issues closed</a> in digiKam 4.8.0 available through the KDE Bugs-tracking System.
</p>

<p>The digiKam software collection tarball and Windows installer can be downloaded from the <a href="http://download.kde.org/stable/digikam/">KDE repository</a>.
</p>

<p>Have fun to use this new digiKam release.</p>

<p>digiKam Team...</p>
<div class="legacy-comments">

  <a id="comment-20982"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/732#comment-20982" class="active">Thank you  for your hard</a></h3>    <div class="submitted">Submitted by Sorin (not verified) on Wed, 2015-02-25 18:09.</div>
    <div class="content">
     <p>Thank you  for your hard work.</p>
<p>Allow me to be the first one to ask this: any plans for the Windows port?<br>
Of course I can understand that most efforts are now shifted to the KF5 port. After that one is out, will it be easier or harder to get the Windows binaries?</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20983"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/732#comment-20983" class="active">I wondered the same thing.</a></h3>    <div class="submitted">Submitted by cc (not verified) on Thu, 2015-02-26 19:07.</div>
    <div class="content">
     <p>I wondered the same thing. Right now, nobody even seems to know how to build the windows version (I wasn't even asking for a binary. I can relate that this might not be the devs focus right now, but i explicitly <a href="https://bugs.kde.org/show_bug.cgi?id=342481">asked for help building it myself</a>.)</p>
<p>I sure hope, digikam 5 will make this easier. If i could build it myself, i'm pretty sure i could also contribute something to the project.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20988"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/732#comment-20988" class="active">Windows Builds have landed!</a></h3>    <div class="submitted">Submitted by cc (not verified) on Sat, 2015-03-28 09:55.</div>
    <div class="content">
     <p>Thanks for the updated windows builds! Very much appreciated!</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20991"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/732#comment-20991" class="active">Yes, thank you very much!</a></h3>    <div class="submitted">Submitted by Calle (not verified) on Sat, 2015-03-28 15:01.</div>
    <div class="content">
     <p>Yes, thank you very much! Much appreciated!</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20993"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/732#comment-20993" class="active">You're very welcome!</a></h3>    <div class="submitted">Submitted by Ananta Palani on Sat, 2015-03-28 20:43.</div>
    <div class="content">
     <p>Sorry for the delay, I hope you enjoy the release!</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20994"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/732#comment-20994" class="active">Thanks - I'm sure I will!
I</a></h3>    <div class="submitted">Submitted by Calle (not verified) on Sun, 2015-03-29 19:05.</div>
    <div class="content">
     <p>Thanks - I'm sure I will!<br>
I will try it in a couple of days. I've just ordered a NAS (my first server) and am planning to put both my images and database(s) there. Hopefully this setup will work well with digiKam (?), and that it won't be too slow...! Looking forward to trying digiKam!</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div></div></div><a id="comment-20984"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/732#comment-20984" class="active">Me too</a></h3>    <div class="submitted">Submitted by Grix (not verified) on Mon, 2015-03-02 21:27.</div>
    <div class="content">
     <p>I'm also a DGK windows user (and linux too) I need win because the printer drivers (a lot of windows users have same problem)<br>
For me, is as stable as in linux and I think it´s very important for development that digikam becomes a cross plattform suite like Gimp, Scribus, Inkscape, Libre Office, Blender, Krita, and so. It makes the program much more visible for the masses and also do with linux.</p>
<p>Thanks, Digikam is great and it's the main program I use for my photografic work</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-20990"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/732#comment-20990" class="active">digiKam 4.8.0 Windows installer available...</a></h3>    <div class="submitted">Submitted by digiKam on Sat, 2015-03-28 10:01.</div>
    <div class="content">
     <p>Last 4.8.0 installer for Windows is online :</p>
<p>http://download.kde.org/stable/digikam/digiKam-installer-4.8.0-win32.exe.mirrorlist</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-20992"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/732#comment-20992" class="active">digiKam 4.8.0 for Windows is now available</a></h3>    <div class="submitted">Submitted by Ananta Palani on Sat, 2015-03-28 20:41.</div>
    <div class="content">
     <p>Sorry for the release delay, you can now download <a href="http://download.kde.org/stable/digikam/digiKam-installer-4.8.0-win32.exe.mirrorlist">digiKam 4.8.0 for Windows</a> on the KDE release site. I hope you all enjoy!</p>
<p>After KF5 I hope the Windows binaries will be easier to release. Part of the problem is due to quite a few changes I had to make to the emerge build system to support long paths on Windows (essentially using dynamic NTFS junctions) which I haven't had time to commit back to the emerge repository. I hope to merge the changes in for KF5.</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-20985"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/732#comment-20985" class="active">PLD graphic format</a></h3>    <div class="submitted">Submitted by Marimo (not verified) on Wed, 2015-03-04 22:06.</div>
    <div class="content">
     <p>It would be nice if future versions of Digikam supported the PLD graphic format (read-only support would be enough).</p>
<p>PLD ist the native format used by PhotoLine, an increasingly popular image editing software for Windows and Macintosh computers and it runs Great with Wine too.</p>
<p>Similar to PSD (Photoshop) or XCF (Gimp) formats, PLD is very complex and can contain lots of layers, bitmap and vector graphic elements. But according to the PhotoLine developers, all PLD documents also contain a preview image, either in JPEG or in PNG format.</p>
<p>Maybe Digikam could recognize PLD as a graphic format and show the preview JPEGs/PNGs.</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-20986"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/732#comment-20986" class="active">Mac version</a></h3>    <div class="submitted">Submitted by Geoff (not verified) on Fri, 2015-03-06 21:33.</div>
    <div class="content">
     <p>Congrats on the new release.  I would love to see a dedicated Mac version. I've tried the macports version (which is currently at 4.0.0) in the past and it was never stable enough.<br>
I used digikam for a few years on linux (around 2008-2010), but have used Aperture since 2011.  I'd gladly come back if there we a good binary for mac or at least a good and maintained macports version.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20989"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/732#comment-20989" class="active">Macports 4.8.0 released...</a></h3>    <div class="submitted">Submitted by digiKam on Sat, 2015-03-28 09:59.</div>
    <div class="content">
     <p>Macports as last version 4.8.0 available. Check to update on your computer...</p>
<p>https://www.macports.org/ports.php?by=name&amp;substr=digikam</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-20995"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/732#comment-20995" class="active">Keyworddatabase</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Mon, 2015-03-30 11:11.</div>
    <div class="content">
     <p>Hi,</p>
<p>after updating from 4.7 to 4.8 the keyworddatabase wont work anymore. It's not possible to display the keywords or add some. What can I do, is there a possible solution?</p>
<p>thanks</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20996"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/732#comment-20996" class="active">no one who can help?</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Sat, 2015-04-04 19:50.</div>
    <div class="content">
     <p>no one who can help?</p>
         </div>
    <div class="links">» </div>
  </div>
</div>
</div>