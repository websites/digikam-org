---
date: "2009-12-21T15:11:00Z"
title: "Merry Christmas : digiKam 1.0.0 is there..."
author: "digiKam"
description: "Dear all digiKam fans and users! digiKam team is proud to announce digiKam 1.0.0 ! digiKam tarball can be downloaded from SourceForge at this url"
category: "news"
aliases: "/node/491"

---

<p>Dear all digiKam fans and users!</p>

<p>digiKam team is proud to announce digiKam 1.0.0 !</p>

<a href="http://www.flickr.com/photos/digikam/4203468578/" title="breaking_the_ice-final_1024 by digiKam team, on Flickr"><img src="http://farm3.static.flickr.com/2531/4203468578_82aefde7c6.jpg" width="500" height="293" alt="breaking_the_ice-final_1024"></a>

<p>digiKam tarball can be downloaded from SourceForge <a href="http://sourceforge.net/projects/digikam/files">at this url</a></p>

<p>digiKam will also be available for Windows. Pre-compiled packages can be downloaded with KDE-Windows installer. See <a href="http://windows.kde.org">KDE-Windows project</a> for details.</p>

<p>See the list of new features below and bugfixes coming with this release (since 0.10.0):</p>

<p>Enjoy digiKam. Have a Merry Christmas and a Happy New Year.</p>
 
<h5>NEW FEATURES:</h5><br>

<b>General</b>        : Exif, Makernotes, Iptc, and Xmp metadata tags filtering can be customized in setup config dialog.<br>
<b>General</b>        : Metadata Template: add support of Contact, Location, and Subjects IPTC information.<br>
<b>General</b>        : Add support of Author properties to multi-language Captions. User can set an author name for each caption value.<br>
<b>General</b>        : <a href="http://www.digikam.org/node/427">Add new Batch Queue Manager</a>.<br>
<b>General</b>        : New tool bar animation banner.<br>                                
<b>General</b>        : Fix compilation under MSVC 9.<br>                                 
<b>General</b>        : <a href="http://www.digikam.org/node/442">New first run assistant</a>.<br>    
<b>General</b>        : New dialog to show Database statistics.<br>                       
<b>General</b>        : Add support of wavelets image file <a href="http://www.libpgf.org">format PGF</a>.<br>  
<b>General</b>        : Added support of Metadata Template to set easily image copyright.<br>
<b>General</b>        : Added support of LightRoom keywords path for interoperability purpose.<br>
<b>General</b>        : Added Multi-languages support to Caption and Tags right side-bar.<br>
<b>General</b>        : digiKam use a new database to cache thumbnails instead ~./thumbnails. 
                        File format used to store image is PGF (http://www.libpgf.org).       
                        PGF is a wavelets based image compression format and give space optimizations.<br>
<b>General</b>        : Add OpenStreetMap support in Geolocation panels.<br>
<b>General</b>        : Showfoto, Editor, Preview, LightTable: Thumbbar is now dockable everywhere.<br>
<b>General</b>        : Complete re-write of Color Management code. Add support of thumbs and preview CM.<br>
<b>General</b>        : AdvancedRename utility is used throughout digiKam now (AlbumUI, CameraUI and BQM).<br>
<b>General</b>        : Updated internal LibPGF to 6.09.44.<br>
<b>General</b>        : Syntax highlighting and history management for AdvancedRename utility.

<br><br>

<b>AlbumGui</b>       : Icon view is now based on pure Qt4 model/view.<br>
<b>AlbumGui</b>       : New overlay button over icon view items to perform lossless rotations.<br>
<b>AlbumGui</b>       : Find Duplicates search can be limited to an album and tag range.

<br><br>

<b>ImageEditor</b>    : Close tools by pressing ESC.<br>
<b>ImageEditor</b>    : <a href="http://www.digikam.org/node/439">New Liquid Rescale tool.</a><br>
<b>ImageEditor</b>    : New menu option to switch image color space quickly.<br>
<b>ImageEditor</b>    : Black and White converter: add standard Blue and Yellow-Green color filters.<br>
<b>ImageEditor</b>    : New Local Contrast tool to simulate HDR images (Tone Mapping).<br>
<b>ImageEditor</b>    : New tool to remove noise using wavelets working in luminance (YCrCb color space).

<br><br>

<b>CameraGUI</b>      : New history of camera actions processed.<br>

<br><br>

<b>BatchQueueMgr</b>  : New tool to remove noise using wavelets working in luminance (YCrCb color space).

<h5>BUGFIXES FROM KDE BUGZILLA:</h5><br>

001 ==&gt; 091742 : Batch process auto levels.<br>
002 ==&gt; 149361 : Not all edit actions are available as batch commands (for example auto-levels and gamma correction).<br>
003 ==&gt; 187523 : digiKam crashes on startup (undefined symbol).<br>
004 ==&gt; 187569 : Crash on star removing when filtering by stars.<br>
005 ==&gt; 186549 : digiKam crashes when hovering the bottom part of its.<br>
006 ==&gt; 187641 : Error while loading shared libraries: libkdcraw.so.5.<br>
007 ==&gt; 187429 : Crash on Launch after disable KIPI plugins rotate.<br>
008 ==&gt; 187937 : Writing tags to files doesn't update modification timestamp.<br>
009 ==&gt; 184687 : Crash after deleting an empty album.<br>
010 ==&gt; 186766 : Crash after finish resize and rename of images in folder.<br>
011 ==&gt; 184953 : Zooming and paning in slideshow / without toolbars.<br>
012 ==&gt; 187508 : Spontaneous crash whilst doing nothing in particular.<br>
013 ==&gt; 188235 : Assigning a color profile to TIFF images does not work anymore with 0.10.0.<br>
014 ==&gt; 187748 : Install application icons to right path for non-KDE desktops.<br>
015 ==&gt; 186255 : digiKam crashes when switching collection database.<br>
016 ==&gt; 178292 : AspectRatioCrop is slow and jumps around in first second.<br>
017 ==&gt; 181892 : AspectRatioCrop: some properties should be disabled.<br>
018 ==&gt; 188573 : RW2 converter broken for low light images.<br>
019 ==&gt; 188642 : GPS data in Digikam shows the wrong places on the map.<br>
020 ==&gt; 188907 : Album view should display subfolders, like tag view does.<br>
021 ==&gt; 183038 : "Select All" action interrupts other widgets.<br>
022 ==&gt; 189022 : Better guessing of proportions with 'inverse transformation' option.<br>
023 ==&gt; 189286 : EXIF info lost while converting from NEF to TIFF.<br>
024 ==&gt; 189336 : Preview of RAW look different than actual image.<br>
025 ==&gt; 189413 : Images without GPS-data are shown on the equator in the geolocation-view.<br>
026 ==&gt; 184954 : Unable to delete items from camera - canon sx10is.<br>
027 ==&gt; 189168 : You do not have write access to your home directory base path.<br>
028 ==&gt; 185617 : Images are invalid / not found when an album is moved.<br>
029 ==&gt; 187265 : Advanced search crashes if a selected album has been deleted.<br>
030 ==&gt; 185177 : Searches are not updated when changes are made to the resulting image set.<br>
031 ==&gt; 189542 : Crash on opening download dialog.<br>
032 ==&gt; 114225 : Free rotation using an horizontal line.<br>
033 ==&gt; 189843 : Crash on ubuntu jaunty beta.<br>
034 ==&gt; 167836 : Image preview is too large.<br>
035 ==&gt; 189250 : Editing a canon camera jpeg adds broken sRGB profile.<br>
036 ==&gt; 187733 : Cannot turn off sidebar after Infrared filter usage ?<br>
037 ==&gt; 188755 : Strange behaviour on selection.<br>
038 ==&gt; 185884 : Empty view after clicking the left thumbnail image in Image Editor filter view.<br>
039 ==&gt; 188062 : Synchronize Images with Database does not save Copyright, Credit, Source, By-line, By-line Title in IPTC.<br>
040 ==&gt; 186823 : Several bugs in lens defect auto-correction plugin.<br>
041 ==&gt; 190612 : digiKam and Showfoto crash.<br>
042 ==&gt; 191092 : No images shown in digiKam albums.<br>
043 ==&gt; 191203 : digiKam crashed on first load, after being installed on ubuntu 9.04.<br>
044 ==&gt; 151321 : Remaining space incorrectly calculated when using symlinks (import from camera).<br>
045 ==&gt; 189742 : Thumbnails not saved between sessions.<br>
046 ==&gt; 180507 : Crash when dragging picture to another app with kwin_composite enabled.<br>
047 ==&gt; 190296 : Can not save images on Windows.<br>
048 ==&gt; 141240 : Camera download: file rename with regexps.<br>
049 ==&gt; 181951 : digiKam crashes (SIGABRT) during the importing of pictures from Kodak V530 camera.<br>
050 ==&gt; 166392 : File-Renaming: custom sequence-number doesn't work.<br>
051 ==&gt; 182332 : Can't stop generating of thumbnails when import from a card reader.<br>
052 ==&gt; 152382 : Unable to view photo.<br>
053 ==&gt; 161744 : Filenames are lower case when imported via sd card reader, upper case when imported via usb cable directly from camera.<br>
054 ==&gt; 187560 : Iptc data for author are missing while downloading images.<br>
055 ==&gt; 192294 : digiKam crash when i try to import photos from my card reader.<br>
056 ==&gt; 192413 : Missing items from right-click search menu.<br>
057 ==&gt; 131551 : Camera port info deleted when editing camera title.<br>
058 ==&gt; 181726 : digiKam collect video, music when I connect usb key.<br>
059 ==&gt; 154626 : Autodetect starting number for "appending numbers" when downloading images.<br>
060 ==&gt; 188316 : Missing NEF files when importing from USB disk.<br>
061 ==&gt; 193163 : digiKam ignores settings on the rating stars.<br>
062 ==&gt; 189168 : You do not have write access to your home directory base path.<br>
063 ==&gt; 193226 : Data loss on downloading photos from camera.<br>
064 ==&gt; 189460 : digiKam crash when goelocating image in the marble tab.<br>
065 ==&gt; 186046 : Crash after calling settings.<br>
066 ==&gt; 193348 : Can't build digiKam on kde 4.2.3.<br>
067 ==&gt; 150072 : Download with digiKam doesn't work with the first start of digiKam.<br>
068 ==&gt; 193738 : Have the image editor status bar show the current color depth.<br>
069 ==&gt; 191678 : Exif rotation for Pentax Pef not working.<br>
070 ==&gt; 146455 : Image rotation and auto-rotate don't work.<br>
071 ==&gt; 152877 : Thumbnails: URI does not follow Thumbnail Managing Standard.<br>
072 ==&gt; 140615 : Pre-Creating of image thumbnails in whole digiKam/all subfolders.<br>
073 ==&gt; 193967 : VERY fast loading of thumbnails [PATCH].<br>
074 ==&gt; 191492 : digiKam hangs on startup "reading database".<br>
075 ==&gt; 171950 : Lost pictures after 'download/delete all' when target dir had same file names.<br>
076 ==&gt; 191774 : digiKam download sorting slow, and wrong with multiple folders.<br>
077 ==&gt; 191842 : digiKam download forgets to download some pictures (multiple big folders).<br>
078 ==&gt; 187902 : Status bar gives wrong picture filename and position in list after "Save As".<br>
079 ==&gt; 193894 : Pop up "Copy Finished!" dialogue when done downloading from camera.<br>
080 ==&gt; 193870 : digiKam freezes after second exif rotation on same file.<br>
081 ==&gt; 194342 : What is the right rename pattern to get names like 2009.02.18_09h56m30s ?<br>
082 ==&gt; 194177 : digiKam crashes always on startup and causes signal 11 (SIGSEGSV) - app not useable.<br>
083 ==&gt; 169213 : digiKam should not enter new album after creating it.<br>
084 ==&gt; 191634 : Statistics of the database, and all pictures ?<br>
085 ==&gt; 193228 : Experimental option "write metadata to RAW files" corrupts Nikon NEFs.<br>
086 ==&gt; 194116 : digiKam crashed for no obvious reason.<br>
087 ==&gt; 179766 : Auto crop does not fully remove black corners created by free rotation.<br>
088 ==&gt; 134308 : Add arrow buttons on thumbnail to perform lossless rotation.<br>
089 ==&gt; 194804 : Tag, rate, rotate and delete via slideshow.<br>
090 ==&gt; 192425 : Star rating setting under thumbnails is a nuisance.<br>
091 ==&gt; 194330 : digiKam crashed when deleting image.<br>
092 ==&gt; 188959 : On first use, digiKam should not scan for images without user confirmation.<br>
093 ==&gt; 195202 : Thumbnail rename fails (cross-device link) and so cache doesn't work.<br>
094 ==&gt; 161749 : Raw Converter needs more options.<br>
095 ==&gt; 150598 : File rename during image upload fails silently.<br>
096 ==&gt; 161865 : Image not placed in New Tag category.<br>
097 ==&gt; 136897 : THM files are not uploaded.<br>
098 ==&gt; 195494 : Deleting and cancelling a picture with the keyboard still delete the picture.<br>
099 ==&gt; 120994 : Print preview generates HUGE postscript files.<br>
100 ==&gt; 191633 : Rename a folder, press F5 and see thumbnails go away.<br>
101 ==&gt; 195565 : Loading next image while in curves tool does not confirm and loads nothing.<br>
102 ==&gt; 189046 : digiKam crashes finding duplicates in fingerprints.<br>
103 ==&gt; 185726 : SIGSERV 11 by choosing an image for fullscreen.<br>
104 ==&gt; 193616 : Location not shown digiKam right sidebar after geolocating in external program.<br>
105 ==&gt; 178189 : Do not create preview automatically for large pictures.<br>
106 ==&gt; 172807 : Warning: JPEG format error, rc = 5.<br>
107 ==&gt; 188936 : Not finding all pictures on camera.<br>                    
108 ==&gt; 195812 : Crash when rotating image in album overlay (rightest rotate icon).<br>
109 ==&gt; 094562 : Renaming an album recreates all thumbnails.<br>                       
110 ==&gt; 193124 : Import image should allow both direct and reverse ordering.<br>
111 ==&gt; 188051 : Sort order of filenames in camera - no way to set or choose.<br>      
112 ==&gt; 183435 : No more file manager at media:/camera.<br>                            
113 ==&gt; 189979 : All displays of the same caption don't show the same text when it contains "éèà".<br>
114 ==&gt; 195902 : digiKam crashes on first startup if album folder is not empty.<br>                   
115 ==&gt; 154606 : Changing the date for multiple pictures simultaneously changes all of the times too.<br>
116 ==&gt; 143932 : Utf-8 displayed with wrong encoding when restoring photo to digiKam.<br>                
117 ==&gt; 179227 : Generation of XMP tags crashes the application.<br>                                     
118 ==&gt; 188988 : digiKam crash while scanning images.<br>                                                
119 ==&gt; 192085 : digiKam suddenly crashed when managing pictures.<br>                                    
120 ==&gt; 196465 : Missing reset button for curve in monochrome conversion tool.<br>                       
121 ==&gt; 196329 : Renaming album causes problems (until re-opening).                                  
122 ==&gt; 191288 : CRC error in chunk zTXt.<br>                                                            
123 ==&gt; 189982 : digiKam crashes during start-up.<br>                                                    
124 ==&gt; 098462 : Support for comments in different languages.<br>                                        
125 ==&gt; 159158 : Tags moved in tree are duplicated in the IPTC metadata.<br>                             
126 ==&gt; 186308 : Moving tags in hierarchy not reflected in XMP metadata "TagsList".<br>                  
127 ==&gt; 141912 : Sync Metadata Batch Tool does not reflect changes in tags properly.<br>                 
128 ==&gt; 175321 : Synchronize tags to iptc keywords adds instead of copying.<br>                          
129 ==&gt; 195663 : Crash while adding new tag.<br>                                                         
130 ==&gt; 195735 : digiKam-svn fails to compile on Mac OS X.<br>                                           
131 ==&gt; 196768 : With 4.2.90 digikam always crashes on startup - worked fine with 4.2.3.<br>             
132 ==&gt; 190719 : Compiling digikam-0.10.0 on gentoo fails.<br>                                           
133 ==&gt; 197129 : Hierarchical setting of keywords.<br>                                                   
134 ==&gt; 195079 : digiKam albums appear empty.<br>                                                        
135 ==&gt; 195494 : Deleting and cancelling a picture with the keyboard still delete the picture.<br>       
136 ==&gt; 195975 : Images are not visible.<br>                                                             
137 ==&gt; 196462 : IconView (Qt4 Model/view based): Navigation broken after delete.<br>                    
138 ==&gt; 192055 : Typo digiKam tip of the day.<br>                                                        
139 ==&gt; 164026 : Wish: use embedded thumbs within jpegs to speedup album display.<br>                    
140 ==&gt; 197152 : Startup wizard has problems creating directories.<br>                                   
141 ==&gt; 196726 : digiKam crashes while creating thumbnails.<br>                                         
142 ==&gt; 182852 : Timeline: click area small an unintuitive.<br>                                          
143 ==&gt; 173899 : Timeline does not show any photos.<br>                                                  
144 ==&gt; 196994 : digiKam 1.0.0-0.1.beta1.f11 from kde-redhat crash on startup.<br>                       
145 ==&gt; 180076 : Moving a big folder to another subfolder folder does ... nothing.<br>                  
146 ==&gt; 191557 : Previous and next image is showing weird results.<br>                                   
147 ==&gt; 197360 : In the "edit captions" sidebar moving a tag to white space crashes digiKam.<br>         
148 ==&gt; 195799 : digiKam takes 100% CPU when exiting and finally crashes.<br>                            
149 ==&gt; 152219 : Impossible to clear comments in metadata.<br>                                           
150 ==&gt; 197198 : Libpng not detected during configure.<br>                                               
151 ==&gt; 152199 : Deleting tags can leave orphaned IPTC keywords inside files.<br>                        
152 ==&gt; 197285 : Understand keyword hierarchies with different delimitors.<br>                           
153 ==&gt; 197640 : Add the "New folder" Option while dragging items to folders.<br>                        
154 ==&gt; 197744 : Lists of languages miss en-US.<br>                                                      
155 ==&gt; 197622 : digiKam crashes on exit.<br>                                                            
156 ==&gt; 197808 : Bad images sort order when starting digiKam.<br>                                        
157 ==&gt; 197641 : Can't delete exif x-default caption once set.<br>                                       
158 ==&gt; 196437 : digiKam crashes at startup.<br>                                                         
159 ==&gt; 197868 : digiKam crashes on startup (digikam-0.10.0-2/x86_64/Archlinux).<br>                     
160 ==&gt; 196686 : Iconview (Qt4 Model/View based) : Select multiple images with Ctrl + Mouse not working anymore in 1.0.0-beta1.<br>
161 ==&gt; 197961 : Cameragui freezes digikam when starting to download images from camera.<br>
162 ==&gt; 197445 : digiKam freezes while attempting to import images from canon powershot a540.<br>
163 ==&gt; 198067 : digiKam does not show any pictures.<br> 
164 ==&gt; 186638 : Canon 450D import issues.<br>                                   
165 ==&gt; 195809 : Radio buttons in "Search Group" "Options" in "Advanced search" dialog do not show selection status.<br>
166 ==&gt; 177686 : Menu text is invisible.<br>
167 ==&gt; 198509 : Tagging by drag and drop not possible anymore.<br>
168 ==&gt; 198531 : Compilation error in libpgf [PATCH].<br>
169 ==&gt; 198530 : Compilation error in imagecategorizedview.cpp.<br>
170 ==&gt; 139361 : Templates for meta data.<br>
171 ==&gt; 175923 : digiKam uses wrong album root path/wrong disk uuid.<br>
172 ==&gt; 185065 : Image editor loads new image without being asked to.<br>
173 ==&gt; 188017 : Video colours inverted after digiKam started.<br>
174 ==&gt; 194950 : digiKam endlessly spewing text to stderr.<br>
175 ==&gt; 189080 : Comments not saved with digikam 0.10.0 on KDE 4.2.2.<br>
176 ==&gt; 198868 : Pictures are not displayed in the main view i just can see them in the batch menu.<br>
177 ==&gt; 199168 : Tags in db but not in IPTC.<br>
178 ==&gt; 184445 : Deleting multiple files in folder view gives me multiple errors.<br>
179 ==&gt; 151749 : External URLs from pictures for related information.<br>
180 ==&gt; 192701 : Application: digiKam (digiKam), signal SIGSEGV --Crash under Gnome (Ubuntu 9.04).<br>
181 ==&gt; 199394 : Request: "regenerate folder thumbnails" menu item.<br>
182 ==&gt; 199420 : Duplication of Albums when naming them using upper case letters.<br>
183 ==&gt; 192535 : digiKam collections on removable media are treated as local collections.<br>
184 ==&gt; 199497 : Open camera rejected with: "Fehler beim Auflisten der Dateien in /store_00010001/DCIM/100CANON.".<br>
185 ==&gt; 199602 : Exif rotation not interpreted the same way in different tools.<br>
186 ==&gt; 191522 : Crash on use default action "Download Photos with digiKam".<br>
187 ==&gt; 196463 : Crash on device action Download Photos with digiKam.<br>
188 ==&gt; 199617 : When clicking on "My Tags", the view switches automatically after 2 seconds.<br>
189 ==&gt; 199076 : Sudden crash of digiKam during work.<br>
190 ==&gt; 195121 : digiKam crashes on view image (exiv2).<br>
191 ==&gt; 142566 : Update metadata doesn't work correctly for creation date.<br>
192 ==&gt; 199251 : digiKam crashes when setting exif orientation tag.<br>
193 ==&gt; 199482 : Caption can't be canged.<br>
194 ==&gt; 174586 : Loading video from camera consumes ll memory.<br>
195 ==&gt; 199667 : Make writing of metadata to picture enabled by default.<br>
196 ==&gt; 199981 : XMP synchronization of digiKam namespace occurs for second tag.<br>
197 ==&gt; 199967 : Creation date newer than last modified date.<br>
198 ==&gt; 200190 : Tag filters: unlike older versions AND-condition doesn't match anymore.<br>
199 ==&gt; 193616 : Location not shown digiKam right sidebar after geolocating in external program.<br>
200 ==&gt; 189156 : digikam crashes on importing directories with some (cyrillic?) file names.<br>
201 ==&gt; 191963 : Calender sort by wrong dates on images without properties.<br>
202 ==&gt; 189072 : Right click popup menu.<br>
203 ==&gt; 200223 : Default language of spellchecker is ignored.<br>
204 ==&gt; 126086 : Besides the basic and full exif info I would like a page with selectable fields.<br>
205 ==&gt; 200637 : Too small fonts in collections setup page.<br>
206 ==&gt; 135476 : Support for multiple comment authors.<br>
207 ==&gt; 200246 : digiKam does not recognize GPS EXIF information.<br>
208 ==&gt; 188999 : XMP tags not imported properly.<br>
209 ==&gt; 199148 : Iptc ascii vs unicode pb.<br>
210 ==&gt; 169685 : Tags lost on copy via context menu.<br>
211 ==&gt; 200162 : EXIF tags not updated if digiKam tag is renamed.<br>
212 ==&gt; 200854 : Option to clear light table on close.<br>
213 ==&gt; 200805 : List of supported file types can not be edited, just extended.<br>
214 ==&gt; 200187 : Save and restore selected filter tags.<br>
215 ==&gt; 196999 : Calendar is ignoring selection of date/week but respecting month.<br>
216 ==&gt; 190795 : USB PTP Class does not work anymore with the KDE4 version.<br>
217 ==&gt; 188905 : Edit view goes 100% instead of fit window.<br>
218 ==&gt; 187810 : In import window images are sorted in reverse order but folders are not.<br>
219 ==&gt; 199619 : Deleting image and switching to next fastly seems to confuse album list.<br>
220 ==&gt; 121804 : Image overwritten with blank file when importing into same folder.<br>
221 ==&gt; 200783 : digiKam crashes while loading JPEG images.<br>
222 ==&gt; 200979 : Trust 715 LCD camera  no driver.<br>
223 ==&gt; 179902 : Picture numbering fails if pics taken at more than 1 image per second.<br>
224 ==&gt; 184541 : Resize error when sharpening in image editor.<br>
225 ==&gt; 134389 : Sort images by date - even when spanning albums.<br>
226 ==&gt; 195109 : Integrate lighttable in digiKam.<br>
227 ==&gt; 199373 : LightTable Crash with Image Drag and Drop.<br>
228 ==&gt; 192563 : Zooming into the earth map buggy.<br>
229 ==&gt; 192534 : Rename a folder, then rename a photo and you get an error.<br>
230 ==&gt; 193522 : First run (with existing v3 database) fails, "No album library path...".<br>
231 ==&gt; 147885 : Update metadata doesn't work correctly for creation date.<br>
232 ==&gt; 196462 : IconView (Qt4 Model/view based): Navigation broken after delete.<br>
233 ==&gt; 195871 : Image tool tip does not show all tags if too long.<br>
234 ==&gt; 201560 : Crash at start up when scanning images.<br>
235 ==&gt; 197508 : Geo-localisation integration missing, but marble is installed.<br>
236 ==&gt; 201640 : Opportunity to compile digiKam without Nepomuk service.<br>
237 ==&gt; 201692 : No thumbnails in album view.<br>
238 ==&gt; 185098 : digiKam craches sometimes on raw files selection.<br>
239 ==&gt; 201624 : Showfoto =&gt; color-management =&gt; input-profile =&gt; search =&gt; crash.<br>
240 ==&gt; 151712 : Adjust Exif orientation tag - wrong permission interpretation.<br>
241 ==&gt; 171983 : Enable display of Openstreetmap maps in the Map Widget.<br>
242 ==&gt; 155097 : Performance problems with recursive view and filtering.<br>
243 ==&gt; 200392 : Slideshow displays "Headline" field as "image caption" and has no way to display "Caption" field.<br>
244 ==&gt; 201302 : Crash due to memory access problem on Mac OS X.<br>
245 ==&gt; 133094 : Exif-changes by external prog has no affect inside digiKam.<br>
246 ==&gt; 201746 : Invalid TIFF output in Editor and Batch queue manager.<br>
247 ==&gt; 196692 : Red and blue channels in images are reversed when saving.<br>
248 ==&gt; 202332 : View mode: Zooming using Ctrl+Scroll is broken.<br>
249 ==&gt; 197254 : digikam crash at startup.<br>
250 ==&gt; 202779 : When pressing 'enter', thumbnails should be enlarged, also when a collection is selected.<br>
251 ==&gt; 203574 : Showfoto crashes when opening jpeg file.<br>
252 ==&gt; 203498 : No drop-down menu for export.<br>
253 ==&gt; 203589 : Crash in the noise reduction plugin when 'Threshold' equals zero.<br>
254 ==&gt; 198006 : digiKam crash after assistant finished.<br>
255 ==&gt; 203083 : Crash starting digikam (after kde 4.3 upgrade).<br>
256 ==&gt; 202954 : digiKam crashes on start while scaning picture directory.<br>
257 ==&gt; 203031 : Album list doesn't show pictures even after tons of new pictures were imported.<br>
258 ==&gt; 201810 : Segfault moving files to trash.<br>
259 ==&gt; 193763 : digiKam doesn'd display any photos.<br>
260 ==&gt; 202532 : Wrong rename sequence when downloading.<br>
261 ==&gt; 163161 : Adding ICC profiles confuses digiKam.<br>
262 ==&gt; 148151 : Problem with icc profle Color Management in digiKam with feisty fawn.<br>
263 ==&gt; 162944 : Cms/editor: converting colorspace doesn't work.<br>
264 ==&gt; 165650 : digiKam appears to add a color profile for printing?<br>
265 ==&gt; 182272 : Color profile and white balance.<br>
266 ==&gt; 204656 : Import: "Device Information": linebreaks not HTMLified in KTextEdits [patch].<br>
267 ==&gt; 189073 : Does not save embedded color profile.<br>
268 ==&gt; 195144 : Editor shows wrong colors when loading image without color profile and color management is on.<br>
269 ==&gt; 152521 : digiKam Edit / Showfoto: Use Color Managed View setting in config page is ignored.<br>
270 ==&gt; 163160 : Remember color management status between sessions.<br>
271 ==&gt; 204797 : Click lock icon crash digiKam.<br>
272 ==&gt; 204871 : digiKam crash when rating a picture on the lighttable.<br>
273 ==&gt; 203594 : Option to display a vertical thumb bar.<br>
274 ==&gt; 195050 : Color management doesn't work in thumbnails and view mode.<br>
275 ==&gt; 179802 : After raw import image not shown in editor.<br>
276 ==&gt; 172196 : Color managed view is inaccurate.<br>
277 ==&gt; 197537 : Program crashes when starting import from usb camera (canon 450d).<br>
278 ==&gt; 194950 : digiKam endlessly spewing text to stderr.<br>
279 ==&gt; 193482 : XMP/IPTC fields in Batch Rename tool.<br>
280 ==&gt; 183435 : No more file manager at media:/camera.<br>
281 ==&gt; 200758 : digiKam, dolphin, dc is not in peripherals list.<br>
282 ==&gt; 206071 : Not reading image properties.<br>
283 ==&gt; 191113 : digiKam doesn't recognize images with colorspace adobergb.<br>
284 ==&gt; 206487 : Unsharp mask algorithm in batch processing should equal the one in kipi.<br>
285 ==&gt; 206570 : Crashed when scrolling through thumbnails in edit mode.<br>
286 ==&gt; 206712 : Raw Import Tool: RGB (linear) provides gamma corrected output and should be named as such.<br>
287 ==&gt; 206857 : Unclear English in sentence.<br>
288 ==&gt; 149485 : Advanced image resize for the digiKam editor.<br>
289 ==&gt; 206426 : Default ICC profile locations reads the same directory twice.<br>
290 ==&gt; 203176 : Deleting 2 files still shows one afterwards.<br>
291 ==&gt; 205059 : The default path for image library is not localizable.<br>
292 ==&gt; 204084 : Showfoto claims that there are no images in directory that contains symlinks to images.<br>
293 ==&gt; 197817 : digiKam should utilize _icc_profile X property.<br>
294 ==&gt; 205756 : Copy and paste fails silently when pasting file back into the same folder.<br>
295 ==&gt; 207239 : When pressing Enter thumbnails should be enlarged, also after changing from Tags to Collection or similar.<br>
296 ==&gt; 200830 : 100 % CPU usage without obvious reason.<br>
297 ==&gt; 204480 : Unable to access albums on drives other than C:.<br>
298 ==&gt; 154463 : Autorename while transferring pictures from cam to PC adds lots of '0'.<br>
299 ==&gt; 207882 : Manual: RAW decoder settings: 16 bit not clearly explained.<br>
300 ==&gt; 208004 : Geolocation tool needs higher zoom levels.<br>
301 ==&gt; 207293 : when showing whole collection status bar filters don't work.<br>
302 ==&gt; 206670 : after deleting an image the image before the deleted image is displayed/selected instead of the image after the deleted one.<br>
303 ==&gt; 208160 : After edition image has different colors in QT nad GTK+ applications.<br>
304 ==&gt; 207332 : editor works on the wrong image if a new one was chosen while processing.<br>
305 ==&gt; 206666 : thumbnail borders are drawn inside instead of outside the images thus hiding parts of the image.<br>
306 ==&gt; 195050 : Color management doesn't work in thumbnails and view mode.<br>
307 ==&gt; 200357 : digiKam, exif rotation, thumbnail not rotated.<br>
308 ==&gt; 203485 : consider using KGlobalSettings::picturesPath() instead of KGlobalSettings::documentPath().<br>
309 ==&gt; 204053 : Remember cursor position in album view after refresh / sync.<br>
310 ==&gt; 197413 : Changing the name of an album does not update album list.<br>
311 ==&gt; 202780 : Channel Mixer: Preview not updated when opening dialog.<br>
312 ==&gt; 191854 : digiKam crashed when scanning newly added folders to local collections.<br>
313 ==&gt; 204237 : Attempting Add HPE427 Photosmart Camera To Import Pictures.<br>
314 ==&gt; 196480 : Crash on starting digiKam.<br>
315 ==&gt; 207053 : Replace current renaming in AlbumUI with AdvancedRename utility.<br>
316 ==&gt; 206094 : Camera GUI: Selection with Shift+Pos1/End does not work.<br>
317 ==&gt; 092098 : Plugin for automatically adding copyright marks to pictures.<br>
318 ==&gt; 192242 : Album rename sometimes doesn't show existing album name in dialog.<br>
319 ==&gt; 208851 : Collections: Detection of duplicate collections too strict.<br>
320 ==&gt; 179370 : Crashed while downloading a video file from camera.<br>
321 ==&gt; 141238 : Album tree lacks horizontal scroll bar.<br>
322 ==&gt; 207486 : With 16-bit images, adjustlevels acts weird when inserting manually values into input fields.<br>
323 ==&gt; 193748 : Crash upon importing images from camera.<br>
324 ==&gt; 206109 : digiKam marble map theme not selectable.<br>
325 ==&gt; 207994 : Refresh meta data view on right side after exif import.<br>
326 ==&gt; 207911 : Crash after trying to re-open Image Editor containing an unsaved RAW image.<br>
327 ==&gt; 204080 : It crashed when it was closing.<br>
328 ==&gt; 198554 : Crash on making thumbnails, every time.<br>
329 ==&gt; 207330 : digiKam crashes if the editor has to open a new image while working on another.<br>
330 ==&gt; 209189 : Make albumview rotation buttons optional.<br>
331 ==&gt; 209343 : Mouse-wheel should jump to next/previous picture if used when viewing a picture.<br>
332 ==&gt; 193879 : Crash while rating pictures.<br>
333 ==&gt; 209437 : Allow to use mouse click and drag to move around the image in View mode.<br>
334 ==&gt; 130942 : digiKam should display the default EXIF rotation currently used.<br>
335 ==&gt; 145442 : Contrast mask image plugin.<br>
336 ==&gt; 209225 : Crash when running batch queue in background.<br>
337 ==&gt; 208598 : Thumbbar out of digiKam.<br>
338 ==&gt; 197609 : When rotating an image via the overlay buttons thumbnail is not updated.<br>
339 ==&gt; 183972 : "Album Path" config variable still necessary?<br>
340 ==&gt; 146097 : Images without filename extensions not displayed.<br>
341 ==&gt; 190131 : Sig SIGABRT when importing from Kodak Camera.<br>
342 ==&gt; 190202 : Crash when transfer image from camera.<br>
343 ==&gt; 187204 : Keeps crashing 6 (SIGABRT) import from camera.<br>
344 ==&gt; 178540 : Zoom in previewwidget has poor behavior.<br>
345 ==&gt; 191397 : digiKam geolocation selection crash.<br>
346 ==&gt; 183753 : digiKam crashes while detecting my camera.<br>
347 ==&gt; 192426 : Tried to open a picture in Gimp  The Picture opens in Gimp and digiKam crashes.<br>
348 ==&gt; 202029 : digiKam crash after downloading and deleting pictures from camera.<br>
349 ==&gt; 204967 : Crash import photo SD (Digikam::IconGroupItem::firstItem, Digikam::CameraUI::slotFileList).<br>
350 ==&gt; 209906 : Unportable test(1) construct in cleanup_digikamdb.<br>
351 ==&gt; 209923 : Crash at right-click image while moving pics in background.<br>
352 ==&gt; 202886 : digiKam crash unexpectedly segmentation fault.<br>
353 ==&gt; 209973 : Mouse wheel inverted in Sharpen Tool.<br>
354 ==&gt; 210053 : digiKam new folder does not see photos inside.<br>
355 ==&gt; 210162 : Lighttable keyboard shortcuts "On Right" do not work.<br>
356 ==&gt; 193347 : Crash while searching in menus.<br>
357 ==&gt; 209207 : Crash when deleting saved searches (GPS and Timeline).<br>
358 ==&gt; 205292 : Error message after deleting images outside of digiKam.<br>
359 ==&gt; 210358 : AND instead of OR in tag selecting.<br>
360 ==&gt; 210520 : DSplitterButton not always expanding the sidebar.<br>
361 ==&gt; 132745 : Can not save image to fish:// protocol.<br>
362 ==&gt; 210462 : digiKam crash select new download.<br>
363 ==&gt; 151972 : Color Management shows repeatedly error messages on resizing.<br>
364 ==&gt; 131948 : Broken "Open File" dialog in Color Management plugin.<br>
365 ==&gt; 210773 : Compiling error on solaris 10 with gcc.<br>
366 ==&gt; 167242 : Thumbnails need a lot of space compared to the pictures.<br>
367 ==&gt; 175923 : digiKam uses wrong album root path/wrong disk uuid.<br>
368 ==&gt; 137386 : Sort images in descending date order in album view.<br>
369 ==&gt; 210845 : Date interval search returns nothing if you specify the same date as interval.<br>
370 ==&gt; 210873 : Renaming in digiKam always uses advanced renaming.<br>
371 ==&gt; 210956 : Zoom slider looks not centered with Oxygen style.<br>
372 ==&gt; 211060 : Advanced rename: keep the pattern.<br>
373 ==&gt; 210839 : "New Album From Selection" should be renamed.<br>
374 ==&gt; 144642 : Ctrl-X does nothing in Album GUI.<br>
375 ==&gt; 132828 : Rotate video file does not work.<br>
376 ==&gt; 192748 : Tree-view thumbnail size changes lead to inconsistent tree-view until restart.<br>
377 ==&gt; 177524 : Album navigator tree disappeared.<br>
378 ==&gt; 193422 : When deleting images the icon view backs to top.<br>
379 ==&gt; 200654 : Image dimensions in albums slows down scrolling dramatically.<br>
380 ==&gt; 148105 : Problems with albums who's name start as the album library path.<br>
381 ==&gt; 198695 : Filtering on albums is difficult to use when albums are sorted by date.<br>
382 ==&gt; 206272 : Failed to overwrite original file.<br>
383 ==&gt; 211356 : Name suggestion in save dialog.<br>
384 ==&gt; 211366 : Right clicking on a thumbnail in light table should not move selection.<br>
385 ==&gt; 210967 : Deleting left image in Light Table in Pair mode jumps to next pair.<br>
386 ==&gt; 196668 : Crash opening some images (possibly EXIF).<br>
387 ==&gt; 211237 : Advanced rename: only one shot.<br>
388 ==&gt; 211423 : Raw import intensity curve is not restored.<br>
389 ==&gt; 188235 : Assigning a color profile to TIFF images does not work anymore with 0.10.0.<br>
390 ==&gt; 152483 : Raw converted images can only be saved once.<br>
391 ==&gt; 211625 : Converting bulb RAW image produces unusable results.<br>
392 ==&gt; 211640 : Status LED looks bad [patch].<br>
393 ==&gt; 207608 : digiKam 1.0beta4 cannot save to mounted CIFS/SAMBA-share.<br>
394 ==&gt; 211707 : Use horizontal thumbbar in the editor options doesn't work.<br>
395 ==&gt; 202142 : Tag list inconsistent size.<br>
396 ==&gt; 197399 : Tree view for tag hierarchy doesn't expand when selecting "tags already assigned".<br>
397 ==&gt; 210907 : Allow immediate deleting of images without diverting through the "trash-folder".<br>
398 ==&gt; 149490 : After uploading new pictures, the album isn't displaying new picture.<br>
399 ==&gt; 211818 : Raw import tool: problem with curvers.<br>
400 ==&gt; 211642 : Subdirectories opening and closing again when pressing on the + in Album View.<br>
401 ==&gt; 211117 : showFoto no filters or decorate menu.<br>
402 ==&gt; 164799 : showFoto: "Sort images by filename" is sorted in reverted order.<br>
403 ==&gt; 211535 : Revert and undo in image editor does not work.<br>
404 ==&gt; 189861 : Keyboard shortcuts for assigning markers.<br>
405 ==&gt; 197622 : digiKam crashes on exit.<br>
406 ==&gt; 198183 : When i try to run digiKam, it crashes.<br>
407 ==&gt; 188334 : Crash when applying free rotation to TIF16 picture.<br>
408 ==&gt; 190593 : digiKam crashes on start-up.<br>
409 ==&gt; 089364 : Change date of album to exif date of first image.<br>
410 ==&gt; 210580 : digiKam crashes on PPC Linux when changing thumbnail size.<br>
411 ==&gt; 201717 : Changing rating filter when images are selected, select all images between first and last selected image.<br>
412 ==&gt; 156146 : Reading data base, digikam don't start.<br>
413 ==&gt; 210823 : digiKam does not respect KDE default toolbar style.<br>
414 ==&gt; 212780 : digiKam crashes when opening large JPG.<br>
415 ==&gt; 212748 : digiKam image editor crashes when browsing too fast.<br>
416 ==&gt; 213111 : Crash on edit image.<br>
417 ==&gt; 213188 : BQM convert to png: setting the "PNG compression" has no impact.<br>
418 ==&gt; 213289 : Record custom date format.<br>
419 ==&gt; 208907 : Rename Images - file extension in uppercase.<br>
420 ==&gt; 208553 : Wrong number of images in a closed recursive tag.<br>
421 ==&gt; 132079 : Support for editing non-local files.<br>
422 ==&gt; 211810 : Raw import tool: black picture after import.<br>
423 ==&gt; 213805 : Changing properties on a tag causes the tag tree to close.<br>
424 ==&gt; 161295 : Allow inclusion of original filename in custom filename.<br>
425 ==&gt; 201661 : More options for file renaming when importing images.<br>
426 ==&gt; 213804 : Batch operation into a new folder, then move them back into this folder, confuses thumbnails.<br>
427 ==&gt; 204640 : Add an option to "Mark images as imported".<br>
428 ==&gt; 214855 : Embedded ICC profiles are not properly read.<br>
429 ==&gt; 212105 : Crash in MarkerClusterHolder destructor.<br>
430 ==&gt; 213758 : Crash when start program.<br>
431 ==&gt; 214718 : digiKam Crashed when trying to Edit photo.<br>
432 ==&gt; 215314 : If initialized the program crashes.<br>
433 ==&gt; 116061 : Possibility to blur chroma without touching luma.<br>
434 ==&gt; 215645 : File save as in image editor crashes digiKam.<br>
435 ==&gt; 212967 : Click on photo in geolocation view should open the photo.<br>
436 ==&gt; 215733 : After move to new disk: all Album-categories and Album-preview missing.<br>
437 ==&gt; 215715 : Wrong numbering when importing photos.<br>
438 ==&gt; 216316 : "Icon view font" setting does not work.<br>
439 ==&gt; 211559 : Batch queue manager crash after run pressed.<br>
440 ==&gt; 200124 : Crashed after clicking cancel for the "Scan for files" device dialog.<br>
441 ==&gt; 216502 : Import errors are not reported.<br>
442 ==&gt; 205001 : MapSearch: Merge overlapping markers to clusters.<br>
443 ==&gt; 216652 : Wrong max calculation in histogram view.<br>
444 ==&gt; 197896 : When importing photos from a camera they are not diplayed in alphabetical order.<br>
445 ==&gt; 216791 : Select next instead of privious picture when deleting a picture.<br>
446 ==&gt; 216726 : Turn off ratings on thumbnails in view mode.<br>
447 ==&gt; 207083 : Image Editor shows full-sized file properties after saving.<br>
448 ==&gt; 208227 : ImagePosition does not always reflect contents of GPS Exif-Tags.<br>
449 ==&gt; 209199 : digiKam crash during fingerprint scan.<br>
450 ==&gt; 214227 : digiKam 0.10.0 not listing photos in chronological order.<br>
451 ==&gt; 144155 : Wrong(?) histogram y-axis when blown-out over/underexposure.<br>
452 ==&gt; 196917 : Problem with "sticky" tooltips in album view.<br>
453 ==&gt; 216998 : Histogram color guide is not shown [patch].<br>
454 ==&gt; 216967 : Marble plugin won't compile and subsequent crashes.<br>
455 ==&gt; 217096 : Imageplugins lenscorrection: lens combobox not sorted by alphabetically order [patch].<br>
456 ==&gt; 214707 : No "picture not saved" warning when some tool active.<br>
457 ==&gt; 214718 : digiKam Crashed when trying to Edit photo.<br>
458 ==&gt; 135845 : Scrolling thumb view is very slow.<br>
459 ==&gt; 217766 : Cannot change file extension in "Rename" dialog.<br>
460 ==&gt; 217768 : "Rename" dialog does not present the original name for editing.<br>
461 ==&gt; 217598 : Detect image type to save according to extension.<br>
462 ==&gt; 205341 : digiKam get mixed up with windows path. need of head '/' before C:/.<br>
463 ==&gt; 214502 : Pictures in new folder are not displayed.<br>
464 ==&gt; 217515 : Empty album after interrupting fingerprint generation.<br>
465 ==&gt; 218456 : Right click on raw image caused unexpected crash.<br>
466 ==&gt; 216392 : digiKam crashed during moving of an empty folder.<br>
467 ==&gt; 214641 : Pictures are displayed partial after editing EXIF comment.<br>
468 ==&gt; 213444 : digiKam crashed after closing a window.<br>
469 ==&gt; 218890 : Histogram max calculation range [patch].<br>
470 ==&gt; 218192 : Image Editor blocks re-saving.<br>
471 ==&gt; 218163 : RAW file import settings are lost after import.<br>
472 ==&gt; 218889 : Inconsistent ui element sizing.<br>
473 ==&gt; 180675 : digiKam-doc 0.9.5 missing doc/pt_digikam &amp; doc/pt_showfoto.<br>
474 ==&gt; 210969 : Crash when saving images in editor.<br>
475 ==&gt; 202278 : BQM: sharpen tool resets EXIF orientation.<br>
476 ==&gt; 210211 : digiKam doesn't update metadata of files already in database, sync only writes db-&gt;file not db&lt;-&gt;file.<br>
477 ==&gt; 213806 : Next image is not displayed after rating an image.<br>
478 ==&gt; 211563 : Batch Queue Manager : only one shot.<br>
479 ==&gt; 216808 : Settings are not saved in NoiseReduction tool.<br>
480 ==&gt; 213798 : Selection after "move selection to folder" retains previous selection.<br>
481 ==&gt; 215743 : Endianness bug loading 16 bits raw images.<br>
482 ==&gt; 215752 : Endianness bug saving 16 bits images [patch].<br>
483 ==&gt; 215486 : collection not found in location on disk with uuid (lvm volume).<br>
484 ==&gt; 215662 : digiKam crash on close beta6.<br>
485 ==&gt; 219477 : Import photos crashes after crash due to import of large number of fotos.<br>

<div class="legacy-comments">

  <a id="comment-18893"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/491#comment-18893" class="active">Kongrats on a great</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Mon, 2009-12-21 15:28.</div>
    <div class="content">
     <p>Kongrats on a great achievement.</p>
<p>presumably this depends on the as yet unannounced kipi plugins 1.0?</p>
<p>regards</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18894"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/491#comment-18894" class="active">soon</a></h3>    <div class="submitted">Submitted by digiKam on Mon, 2009-12-21 15:35.</div>
    <div class="content">
     <p>kipi-plugins 1.0 will be released soon...</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-18895"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/491#comment-18895" class="active">Thanks.</a></h3>    <div class="submitted">Submitted by <a href="http://vw.homelinux.net" rel="nofollow">rbr28</a> (not verified) on Mon, 2009-12-21 15:47.</div>
    <div class="content">
     <p>I'm running 1.0.0 on Gentoo Linux, with Kipi-plugins .9.0 and it works fine.  Haven't noticed a whole lot of changes since the RC, but I wouldn't expect to.  I don't use noise reduction much, since my workflow usually includes converting with ufraw first and using it's noise reduction.  I did look at Digikam's noise reduction settings for the first time in a while and I'm impressed with the flexibility. Anxious to go back and see what I can do with some of my RAW photos.  No complaints so far, just happy to see another great Digikam release.</p>
<p>Vern</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-18896"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/491#comment-18896" class="active">General : New tool bar animation banner.</a></h3>    <div class="submitted">Submitted by Stefan (not verified) on Mon, 2009-12-21 15:47.</div>
    <div class="content">
     <p>What is 'tool bar animation banner'?</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18916"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/491#comment-18916" class="active">In top-left corner is digiKam</a></h3>    <div class="submitted">Submitted by mikmach (not verified) on Tue, 2009-12-22 14:33.</div>
    <div class="content">
     <p>In top-left corner is digiKam name and logo. During longer operations it performs subtle, elegant animation as in addition to regular progress bars. Nice effect.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18917"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/491#comment-18917" class="active">I believe you mean the</a></h3>    <div class="submitted">Submitted by Fri13 on Tue, 2009-12-22 16:35.</div>
    <div class="content">
     <p>I believe you mean the top-right corner ;-)</p>
<p>The animation can be removed from toolbar, but it just helps on long time taking operations (like converting a 8Gb RAW photos to PNG) showing that the Application is not crashed because the progressbar might not gain one % while even multiple photos are processed.</p>
<p>The digiKam (O) -logo is as well a link to the this website when clicking it.</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div><a id="comment-18897"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/491#comment-18897" class="active">Cool!</a></h3>    <div class="submitted">Submitted by Tommy.S (not verified) on Mon, 2009-12-21 17:03.</div>
    <div class="content">
     <p>Digikam realy shines and really breaks the ice *Hrrr*! Cool splash you got and concratulations for 1.0.0 release! =D<br>
You guys are doing so marvelous job that this can be greatest christmas present what we can get!! </p>
<p>Now horry up distributors to package and update Digikam for us!</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-18898"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/491#comment-18898" class="active">Congratulations for this highly symbolic milestone</a></h3>    <div class="submitted">Submitted by <a href="http://serendipity.ruwenzori.net/" rel="nofollow">Jean-Marc Liotier</a> (not verified) on Mon, 2009-12-21 17:11.</div>
    <div class="content">
     <p>I take this opportunity to thank all the people involved in this project, and in particular the core team who has over a long time done a tireless job of producing a great integrated JPEG photography processing tool. I was always impressed how responsive you have been whenever a defect was identified. I'm happy that Digikam has reached this highly symbolic milestone that will make the product more visible to all the users who still don't take advantage of such an efficient tool.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18899"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/491#comment-18899" class="active">Same from me! Great people,</a></h3>    <div class="submitted">Submitted by Sven (not verified) on Mon, 2009-12-21 17:15.</div>
    <div class="content">
     <p>Same from me! Great people, great app! I'm curious about the future!</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18902"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/491#comment-18902" class="active">aha... The future is already there...</a></h3>    <div class="submitted">Submitted by digiKam on Mon, 2009-12-21 17:59.</div>
    <div class="content">
     <p>yes, in trunk, kipi-plugins has a new tool to make Pseudo HDR images.  It's based on Align_Images_Stack and Enfuse programs...</p>

<a href="http://www.flickr.com/photos/digikam/4202593315/" title="expoblendingtool04 by digiKam team, on Flickr"><img src="http://farm3.static.flickr.com/2629/4202593315_09cb549940.jpg" width="500" height="400" alt="expoblendingtool04"></a>

<a href="http://www.flickr.com/photos/digikam/4202593625/" title="expoblendingtool08 by digiKam team, on Flickr"><img src="http://farm3.static.flickr.com/2761/4202593625_9c327c2806.jpg" width="500" height="400" alt="expoblendingtool08"></a>

<p>I will prepare a new blog entry about...</p>


         </div>
    <div class="links">» </div>
  </div>
</div></div><a id="comment-18900"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/491#comment-18900" class="active">Congratulations!</a></h3>    <div class="submitted">Submitted by <a href="http://kubuntulover.blogspot.com" rel="nofollow">Bugsbane</a> (not verified) on Mon, 2009-12-21 17:25.</div>
    <div class="content">
     <p>I want to say *thankyou* to Gilles and the Digi Crew for the huge amount of work hinted at above. It's a beautiful thing to see the first "final release" of an open source tool that is truly "professional ready". Many of the features (with Kipi) are well beyond what I would have thought reasonable to expect such as a visual image search and liquid rescale. I'm just very greatful that you continue to support Linux and Open SOurce everywhere as you have.</p>
<p>Enjoy the release of DigiKam 1.0. You all deserve it!</p>
<p>Bugsbane</p>
<p>PS That new splashscreen ROCKS! :D</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18906"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/491#comment-18906" class="active">Just a promo picture for 1.0.0</a></h3>    <div class="submitted">Submitted by Fri13 on Mon, 2009-12-21 20:02.</div>
    <div class="content">
     <p>&gt; <cite>That new splashscreen ROCKS! :D</cite></p>
<p>Thanks, but it is not a splashscreen for this release, just a promo picture for 1.0.0 release for this and other sites/forums etc. ;-)<br>
The digiKam will be getting new splash for next releases and there will be again a competition for digiKam users/fans. So taking and editing photographs for that is suggested. The splash template can be found if needed from SVN.</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-18901"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/491#comment-18901" class="active">Wow!</a></h3>    <div class="submitted">Submitted by mat69 (not verified) on Mon, 2009-12-21 17:47.</div>
    <div class="content">
     <p>Great job! Really amazing how much you all did! :)</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-18903"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/491#comment-18903" class="active">How do we install the latest</a></h3>    <div class="submitted">Submitted by Paul (not verified) on Mon, 2009-12-21 19:02.</div>
    <div class="content">
     <p>How do we install the latest version on windows? Stable latest only has beta 5, and unstable latest only has beta 1. Do we have to wait for the next major KDE release for it to be available? </p>
<p>What would the nightly build be like, would that be an unstable development version which I should avoid, or if I pick last night's version would that pretty much be this release?</p>
<p>Thanks!</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-18904"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/491#comment-18904" class="active">Beautiful</a></h3>    <div class="submitted">Submitted by <a href="http://narnia.homeunix.com/~robert/" rel="nofollow">Robert Marmorstein</a> (not verified) on Mon, 2009-12-21 19:16.</div>
    <div class="content">
     <p>A beautiful, beautiful piece of software.  Clearly THE killer app for KDE and open source.</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-18905"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/491#comment-18905" class="active">Congratulations</a></h3>    <div class="submitted">Submitted by Gandalf (not verified) on Mon, 2009-12-21 19:50.</div>
    <div class="content">
     <p>and many thanks for this nice Christmas present to everyone!</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-18907"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/491#comment-18907" class="active">Congratulations</a></h3>    <div class="submitted">Submitted by <a href="http://spaceoasis.gotdns.org/lolo" rel="nofollow">FrenchHope</a> (not verified) on Mon, 2009-12-21 20:04.</div>
    <div class="content">
     <p>That's a beautiful piece of software and picture ! Keep it up !</p>
<p>Can't wait for a Windows version too.</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-18908"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/491#comment-18908" class="active">Great job and</a></h3>    <div class="submitted">Submitted by paurullan (not verified) on Mon, 2009-12-21 20:59.</div>
    <div class="content">
     <p>Great job and congratulations!</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-18909"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/491#comment-18909" class="active">What a changelog!</a></h3>    <div class="submitted">Submitted by <a href="http://michalm.wordpress.com" rel="nofollow">Micha? Ma?ek</a> (not verified) on Mon, 2009-12-21 21:29.</div>
    <div class="content">
     <p>Now this is an impressive changelog! Thank you for all your work and for this great application.</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-18910"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/491#comment-18910" class="active">Amazying guys!!!!</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Mon, 2009-12-21 23:17.</div>
    <div class="content">
     <p>Amazying guys!!!!</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-18911"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/491#comment-18911" class="active">Amazing, truly amazing :)</a></h3>    <div class="submitted">Submitted by <a href="http://pawel.rumian.net" rel="nofollow">gorky</a> (not verified) on Mon, 2009-12-21 23:45.</div>
    <div class="content">
     <p>This is a wonderful example how OS should be done.<br>
One of the most useful programs I use, properly developed and with very good community support.</p>
<p>You could give some lessons to the Amarok team ;)</p>
<p>Thanks and good luck!<br>
And Merry Christmas ;)</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-18912"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/491#comment-18912" class="active">beuatifull</a></h3>    <div class="submitted">Submitted by FLOSSlover (not verified) on Tue, 2009-12-22 00:20.</div>
    <div class="content">
     <p>I want to express my thanks to the developers. It is really a pleasure to see such a Free, open source greatness. Thank you</p>
<p>... btw this CAPTCHA is sucking life energy from me...</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-18913"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/491#comment-18913" class="active">I love it...</a></h3>    <div class="submitted">Submitted by <a href="http://www.aryuna.de" rel="nofollow">Lukas</a> (not verified) on Tue, 2009-12-22 00:28.</div>
    <div class="content">
     <p>... not just the application. I thank all the developers and users contributing to Digikam for all their effort. I love you guys. Digikam is THE killer application on Linux. If I wouldn't have a fried, I like to do love with you all. Thank God I have. So thank you once more.</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-18918"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/491#comment-18918" class="active">Thank you for one of the best KDE applications!</a></h3>    <div class="submitted">Submitted by <a href="http://www.andmarios.com" rel="nofollow">Marios</a> (not verified) on Tue, 2009-12-22 19:07.</div>
    <div class="content">
     <p>Thank you! Some years ago if you asked me about KDE's flagship application I would say amarok but for the last couple of years the answer is first digiKam and second amarok!</p>
<p>The only thing I would change in current digiKam is the viewer. I would prefer its controls to be more like gwenview's.</p>
<p>Thank you again!</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-18923"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/491#comment-18923" class="active">Thank you...</a></h3>    <div class="submitted">Submitted by Ari (not verified) on Wed, 2009-12-23 05:34.</div>
    <div class="content">
     <p>Being a user of digikam since 3 years ago and having followed all its recent developments... I can't help myself joining the crowd and saying THANK YOU Gilles, THANK YOU digikam core team!!!! for such fine and well engineered piece of software</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-18924"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/491#comment-18924" class="active">Awesome!! Thanks a lot!! Btw,</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Wed, 2009-12-23 22:09.</div>
    <div class="content">
     <p>Awesome!! Thanks a lot!! Btw, is there some way of running this version on Windows, or do we have to wait for a new KDE version? I hope some volunteer can post a precompiled binary, even if it has to be installed in a messy way, because I really wish to use this soon in Windows!! :-)</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-18926"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/491#comment-18926" class="active">Both RC and Final are</a></h3>    <div class="submitted">Submitted by Stefan (not verified) on Thu, 2009-12-24 20:15.</div>
    <div class="content">
     <p>Both RC and Final are crashing on Kubuntu K.K. with KDE 4.4 Beta 2...</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18931"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/491#comment-18931" class="active">repo for karmic?</a></h3>    <div class="submitted">Submitted by qpal (not verified) on Fri, 2009-12-25 23:55.</div>
    <div class="content">
     <p>Hi.</p>
<p>How did you manage to install Digikam 1.0.0.? Is there any repository for Ubuntu Karmic? Currently I have Digikam Beta 5 and it is still crashing. I have KDE 4.4 Beta 2 as well, so I am losing idea of stable digikam on 1.0.0 version.</p>
<p>I have been even thinking about changing my distro since I found digikam is unstable on Kubuntu :-)</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18932"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/491#comment-18932" class="active">digiKam 1.0.0 package for ubuntu &amp; co...</a></h3>    <div class="submitted">Submitted by digiKam on Sat, 2009-12-26 08:40.</div>
    <div class="content">
     <p>http://packages.ubuntu.com/lucid/digikam</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-18933"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/491#comment-18933" class="active">And look this interresting comment about Ubuntu...</a></h3>    <div class="submitted">Submitted by digiKam on Sat, 2009-12-26 08:52.</div>
    <div class="content">
     <p>https://bugs.kde.org/show_bug.cgi?id=220043#c13</p>
<p>This is why i will never use this linux box. (I always use Mandriva there...)</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-18934"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/491#comment-18934" class="active">I'm not using ubuntu, but I</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Sat, 2009-12-26 09:04.</div>
    <div class="content">
     <p>I'm not using ubuntu, but I have the same problem. With kde 4.3 digikam-rc works well, but updating kde to 4.3.85, digikam (and showfoto) crashes frequently, both rc and final release.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18935"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/491#comment-18935" class="active">binary compatibility...</a></h3>    <div class="submitted">Submitted by digiKam on Sat, 2009-12-26 09:16.</div>
    <div class="content">
     <p>It's due to binary compatibility broken with KDE packages used to compile digiKam...</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18936"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/491#comment-18936" class="active">I recompiled digikam of</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Sat, 2009-12-26 09:25.</div>
    <div class="content">
     <p>I recompiled digikam of course, but that's makes no difference.</p>
<p>As a matter of fact, most kde programs (like amarok, konversation, etc) work well without recompiling.</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-18942"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/491#comment-18942" class="active">Great work anyway</a></h3>    <div class="submitted">Submitted by qpal (not verified) on Sun, 2009-12-27 16:49.</div>
    <div class="content">
     <p>Hi Gilles, according to http://www.kde-apps.org/usermanager/search.php?username=frenchpass you had birthday yesterday so happy birthday!</p>
<p>I must say Digikam is really amazing application. I am using Kubuntu for few years, but only as a user, not an administrator. I suppose if there would be any repository with newest builds, many amateur protographers non-developers would report bugs and accelerate this feedback.</p>
<p>Wish you good luck.</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div><a id="comment-18940"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/491#comment-18940" class="active">RS is from Kubuntu</a></h3>    <div class="submitted">Submitted by Stefan (not verified) on Sun, 2009-12-27 04:08.</div>
    <div class="content">
     <p>RS is from Kubuntu backports.</p>
<p>Final - compiled it myself. (Both digikam and kipi). I didn't compile it properly I guess.......</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div><a id="comment-18948"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/491#comment-18948" class="active">Thanks a lot for the totally</a></h3>    <div class="submitted">Submitted by pilki (not verified) on Mon, 2009-12-28 16:01.</div>
    <div class="content">
     <p>Thanks a lot for the totally amazing job! Now, the dream will be absolutely complete if it existed a package of digiKam for MacOS (my laptop being a macbook). Does anyone know about such a plan?</p>
<p>Thanks again</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18949"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/491#comment-18949" class="active">Ask to KDE-mac project...</a></h3>    <div class="submitted">Submitted by digiKam on Mon, 2009-12-28 16:07.</div>
    <div class="content">
     <p>We don't package binary version. As for windows where a dedicated team package KDE programs, ask to KDE-mac project : http://mac.kde.org</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18954"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/491#comment-18954" class="active">Indeed, it does</a></h3>    <div class="submitted">Submitted by pilki (not verified) on Tue, 2009-12-29 11:16.</div>
    <div class="content">
     <p>Indeed, it does exist!<br>
http://www.macports.org/ports.php?by=name&amp;substr=digikam</p>
<p>Awesome :)</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18955"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/491#comment-18955" class="active">ask for 1.0.0 final...</a></h3>    <div class="submitted">Submitted by digiKam on Tue, 2009-12-29 11:26.</div>
    <div class="content">
     <p>Yes. Contact KDE-Mac team to port 1.0.0 final release, and leave RC for production. A lots of files have been fixed between RC and final.</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div><a id="comment-18958"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/491#comment-18958" class="active">1.0.0 is in MacPorts...</a></h3>    <div class="submitted">Submitted by digiKam on Tue, 2009-12-29 23:22.</div>
    <div class="content">
     <p>http://trac.macports.org/ticket/23063</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-18961"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/491#comment-18961" class="active">IPTC copy and paste</a></h3>    <div class="submitted">Submitted by <a href="http://ctseven.co.uk" rel="nofollow">Jarek</a> (not verified) on Wed, 2009-12-30 18:38.</div>
    <div class="content">
     <p>First of all I have to say that I am really impressed by DigiKam. I used to use FotoStation to manage my archives but as I'm using Linux I would like to use a native application.</p>
<p>DigiKam is almost perfect for me. The only thing that is really missing for me is an easy way to copy and paste whole IPTC data between files (in "Edit IPTC Metadata" window) and edit it.</p>
<p>For example I have a file with Caption, keywords, headline, location, credit, copyright and I would like to use the same IPTC in next image but with different headline and updated caption. I don't want to typing all of the fields again. I simply want to open "Edit IPTC Metadata", copy whole content, close it. Than open "Edit IPTC Metadata" in different image and paste content from previous, in all fields at ones.</p>
<p>Is not a problem to do it manually or use export-import (which does not work properly for me)if is just one file but when I need to do it with 200+ daily is to complicated.</p>
<p>Do you think it would be possible soon in DigiKam? </p>
<p>Happy New Year!!</p>
<p>Jarek</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18963"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/491#comment-18963" class="active">There is a file in Bugzilla about...</a></h3>    <div class="submitted">Submitted by digiKam on Wed, 2009-12-30 19:18.</div>
    <div class="content">
     <p>yes, it's planed in the future...</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-18967"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/491#comment-18967" class="active">097 ==&gt; 136897</a></h3>    <div class="submitted">Submitted by Anders (not verified) on Mon, 2010-01-04 14:22.</div>
    <div class="content">
     <p>Hi,</p>
<p>I have a question about the row that says:<br>
097 ==&gt; 136897 : THM files are not uploaded.</p>
<p>When looking in bugzilla for this bug it's status is Resolved and Resolution is Fixed. But what does this mean? As the THM files are not uploaded from my memory card, even with final 1.0.0.</p>
<p>The last post in the bug says:<br>
"Exiftool seems to show the "missing" data in the CRW that is in the THM, so<br>
there is no need to copy the THM.  Thanks!"</p>
<p>Does this mean that the THM-files shouldn't be uploaded?</p>
<p>Just a little confused here.</p>
<p>/Anders</p>
         </div>
    <div class="links">» </div>
  </div>

</div>
