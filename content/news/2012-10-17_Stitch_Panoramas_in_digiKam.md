---
date: "2012-10-17T10:20:00Z"
title: "Stitch Panoramas in digiKam"
author: "Dmitri Popov"
description: "Being a do-it-all kind of application, digiKam is suited not only for performing the mundane tasks of organizing and editing photos. The application also offers"
category: "news"
aliases: "/node/669"

---

<p>Being a do-it-all kind of application, digiKam is suited not only for performing the mundane tasks of organizing and editing photos. The application also offers a specialized tools for more advanced operations: from blending bracketed photos to stitching panoramas. The latter functionality in digiKam is implemented as a Kipi plugin which relies on <a href="http://hugin.sourceforge.net/">Hugin</a>, a set of powerful tools for processing photos and stitching them into panorama images. The plugin wraps Hugin features into a user-friendly interface which dramatically simplifies the process of turning multiple photos into a panorama.</p>
<p><img src="http://scribblesandsnaps.files.wordpress.com/2012/09/digikam-panoramatool.png?w=500" height="422" width="500"></p>
<p><a href="http://scribblesandsnaps.com/2012/10/17/stitch-panoramas-in-digikam/">Continue to read</a></p>

<div class="legacy-comments">

</div>