---
date: "2010-06-15T18:30:00Z"
title: "GSoC: update on digiKam's non-destructive image editing"
author: "Martin Klapetek"
description: "Hi all, little update about the progress that has been made. So far, there are no visible changes to the user yet (if you don't"
category: "news"
aliases: "/node/524"

---

<p>Hi all,</p>
<p>little update about the progress that has been made. So far, there are no visible changes to the user yet (if you don't count tons of new debug stuff on console), all work has been done under the hood. The good news is, that the basics for non-destructive editing has already landed in the codebase. You can now use Brightness/Contrast/Gamma tool and the Autolevels tool in such way, that it will automatically create new version of the edited image, apply the changes on the new version, move the original version to "Originals" folder located in original image's path (will be fully configurable) and save the new version as {originalImageName}<strong><em>_v1</em></strong>.{originalImageFormatExtension}.</p>
<p>There is no GUI yet, which would be able to manipulate with versions and used filters on them, but that's all on plan and I believe, that some basic GUI for reverting the image to its original state could be done at the end of next week. You will find it as a next tab in the right sidebar in digiKam's Image Editor, although the exact appereance of this UI is still a work-in-progress. But as soon as there will be something usable, I promise to bring you some screenshots ;)</p>
<p>Stay tuned,<br>
Marty</p>
<p>Oh yeah and to answer the question "What about editing done with other programs?" - well, this is kinda tricky. It will definitely be possible to revert back to the original image when you edit some of the versions and not the original itself, it should also be possible to just revert this one change. But if you edit the image, that has no version present (and thus not backuped the original, ie. you edit THE original), there's no way to revert back unless we duplicate all of your images, which is rather unwanted. It could be possible to do so, when you open the image in some external app <strong><em>from</em></strong> digiKam, but most probably not if you open it directly.</p>

<div class="legacy-comments">

  <a id="comment-19273"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/524#comment-19273" class="active">non-destructive editing</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Tue, 2010-06-15 19:23.</div>
    <div class="content">
     <p>&gt; "What about editing done with other programs?"</p>
<p>Will it at least work seamless for other native KDE-Programs like Krita?</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19274"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/524#comment-19274" class="active">That would probably need some</a></h3>    <div class="submitted">Submitted by Martin Klapetek on Tue, 2010-06-15 20:48.</div>
    <div class="content">
     <p>That would probably need some changes on their side, digiKam is not able to monitor what other applications do. And there is no common framework, that could be used by all KDE apps, either. So, the answer is (most probably) no. Besides, that's out of scope of my GSoC work (time is limited and I need to focus primarily on my project) =/ But I agree it would be cool to have such feature alongside whole KDE :)</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-19275"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/524#comment-19275" class="active">Great</a></h3>    <div class="submitted">Submitted by Leavejes (not verified) on Tue, 2010-06-15 21:48.</div>
    <div class="content">
     <p>In my opinion this is a very important feature for digikam, great that you are working on it and are already making such good progress!</p>
<p>I have a few questions about this feature.</p>
<p>1: Does this mean that when a user edits a photo, digikam does not have to ask anymore to save the changes? But just automatically copies the current photo and saves the changes? That would be a great usability improvement to the current situation, I think. Also, the save button can disappear I think?</p>
<p>2: Why is there a originals folder? Couldn't you use a hidden folder in the Pictures directory?</p>
<p>somefoto.jpg (newest version of the photo)<br>
.somefoto.jpg/somefoto_v0.jpg (origional of the photo)<br>
.somefoto.jpg/somefoto_v1.jpg (first edit)<br>
.somefoto.jpg/somefoto_v2.jpg (second edit)<br>
.somefoto.jpg/somefoto_v3.jpg (third edit, since this is the newest , it is a copy of somefoto.jpg)</p>
<p>I think the advantage of this method are:<br>
- The origional file name is always the most recent photo, this is what the user expects (this is how a program without history would do it).<br>
- You only keep a duplicate of the most recent version around if the user did edit the photo. (most user do not edit most photos).<br>
- Since you have a duplicate of the most recent it is easy to find out that another program edited somefoto.jpg<br>
- The copies are stored in the Pictures folder, of which you can expect that it has enough space.<br>
- The copies are close to the location of the current photo. (easy for backups)</p>
<p>3: Is it also in the right side bar in the albums view? I think the answer is yes, because this code is shared?</p>
<p>4: Will this feature completely replace the current undo redo system in digikam? Or will it only copy a photo when you stop editing the current photo?</p>
<p>Sorry for asking this much questions, maybe I should just wait until you finish implementing this... :-)</p>
<p>Leavejes</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19276"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/524#comment-19276" class="active">Quite good and important</a></h3>    <div class="submitted">Submitted by Martin Klapetek on Tue, 2010-06-15 22:39.</div>
    <div class="content">
     <p>Quite good and important questions, thanks for them :)</p>
<p>1: Yes. Automatically copies the current photo and saves the changes. About the save button - hm, probably. But if this feature is going to be optional, then it would have to stay. That's not decided yet.</p>
<p>2: This will be customizable and it will be possible to keep the original image where it is with its newer versions along, or move the original to specified folder ("Originals" for default) or keep only the latest version and move all the rest. This is not entirely clear either, I'm still focusing on the foundations. But another setting should allow you to either see all image's versions or see only the latest version in digiKam's album view.<br>
To your points:<br>
 - <cite>The origional file name is always the most recent photo, this is what the user expects (this is how a program without history would do it).</cite></p>
<p>that's true, although with non-destruct. editing this doesn't hold I guess, because the original is THE original, edited images should be distinguishable IMHO</p>
<p> - <cite>You only keep a duplicate of the most recent version around if the user did edit the photo. (most user do not edit most photos).</cite></p>
<p> basically there will be only one duplicate (created when you apply some filter), but as soon as you apply some filter, which uses some random stuff (like raindrops), you need to create another copy of this and all other version base on this as you won't be able to reproduce it with the exact same result as you would reproduce brightness setting for example</p>
<p>- <cite>Since you have a duplicate of the most recent it is easy to find out that another program edited somefoto.jpg </cite></p>
<p> yes, I'm counting with something like that</p>
<p>- <cite>The copies are stored in the Pictures folder, of which you can expect that it has enough space. </cite></p>
<p>the Originals folder is supposed to be created also in the picture's folder</p>
<p> - <cite>The copies are close to the location of the current photo. (easy for backups) </cite></p>
<p> yeah, same with Originals folder, but having a hidden folder is not too good for backup, think about copy/paste when you have hidden files hidden. This wouldn't be a problem if you copied the parent folder, but if you enter that folder, press ctrl+a and ctrl+c, then you would paste them somewhere else with ctrl+v, you won't copy the hidden folders. I was also thinking about this solution, but I figured, that folder like "Originals" will better let the user know, that he has the original files there. I have to put some more thoughts on this process.</p>
<p>3: Most probably it will :)</p>
<p>4: That's also not clear yet. But I guess the undo/redo could integrate with the image history, this will be possible. And probably also done.</p>
<p>Again, thanks for the questions. It also helped me to think more about those things ;)</p>
<p>Marty</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19284"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/524#comment-19284" class="active">&gt;basically there will be only</a></h3>    <div class="submitted">Submitted by JulienN (not verified) on Wed, 2010-06-16 11:30.</div>
    <div class="content">
     <p>&gt;basically there will be only one duplicate (created when you apply some filter), but as soon as you apply &gt;some filter, which uses some random stuff (like raindrops), you need to create another copy of this and all &gt;other version base on this as you won't be able to reproduce it with the exact same result as you would &gt;reproduce brightness setting for example</p>
<p>I think it would be better to make all the filters deterministic, I have already done that in the noise generation plugin because I had in mind non destructive editing! It is easy to do we just delete the code for the initialization of the random number generator. </p>
<p>Julien</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19288"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/524#comment-19288" class="active">This sounds great! I have to</a></h3>    <div class="submitted">Submitted by Martin Klapetek on Thu, 2010-06-17 10:12.</div>
    <div class="content">
     <p>This sounds great! I have to check out how the non-deterministic filters work, but I think, that we could just adjust the filters, so it can either go by random or you can have some value on the input and you'll get the same output. In other words, when you apply this, it will be random, but the value would get stored and with this exact value you could later reproduce it exactly. What do you think about that? (but then again, I haven't checked yet how the filters work in their code, so I'm not sure if that will be even possible)</p>
<p>Marty</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div><a id="comment-19279"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/524#comment-19279" class="active">How about TIFF?</a></h3>    <div class="submitted">Submitted by Fri13 on Wed, 2010-06-16 09:45.</div>
    <div class="content">
     <p>1. The save button should be changed to "save as" button. Because many use JPEG files and they mistakenly press the "save" when the original file is overwritten and so on the quality is lost and the changes are done. But in this case, we could get somekind notification to thumbbar where the green mark tells what photo is saved in background.<br>
check the page 3 the showFoto thumbbar under the photo: http://saukonpaa.com/documents/digikam_showfoto.pdf</p>
<p>2. I dont like the idea that original is moved away from the current album. I use ISO album naming when importing and the MIME type as well.<br>
So I get albums like 2010-06-15/orf/ and 2010-06-15/jpg. Now I would get more albums to manage: 2010-06-15/orf/original and 2010-06-15/jpeg/original.<br>
But that would still be much better than hiding albums or photos! We can not hide files because when we manage albums with file management, we easily would end up to delete album thinking it is empty, while it would not.<br>
And that would especially be _very bad_ if the originals are hidden. I think now we need to get good versioning UI (coming later it seems).<br>
The original and edited photos can exist in same album but renamed just different way when viewed by file manager. But in digiKam, it should notice automatically what is original and what is edited and group them.<br>
The UI is then important part. I have somewhere few mockups of versioning editing UI's what I could send to the author if he can use anything from them for his own idea.<br>
But I think that if we add somekind marking over photo when we hover it (or shown all the times) and then we would get somekind list of versions what could be opened, it could make things easier.</p>
<p>And how about having non-destructive file for the edited photo with versioning but still having a single file (original + edited)?<br>
Like having a TIFF whats features are used for versioning. When user edits photo, add new layer and place the editings to that top layer. Then save automatically in background the file.<br>
More editings, the TIFF would grow. And when watched with normal viewer application, we would get top newest edit in view (this would not support crops :-()<br>
And Krita, Photoshop etc would see as well the layers and you could use them. (Crop and resize are the things what are missing or does the TIFF support such?)</p>
<p>Is there any open image format what would allow layers, but show only the top layer as the one, even it would be different size?<br>
TIFF allows to have layers different size, the biggest jus is the one. So the bottom layer could be the original, cropped/resize and edited versions could be used.<br>
But would it be bad idea to allow digiKam/showFoto to read TIFF images like they would show only the top layer?<br>
TIFF would be non destructive, compressed image.</p>
<p>I think this discussion should be gone in devel mailing list.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19289"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/524#comment-19289" class="active">2. I dont like the idea that</a></h3>    <div class="submitted">Submitted by Martin Klapetek on Thu, 2010-06-17 10:26.</div>
    <div class="content">
     <p><cite>2. I dont like the idea that original is moved away from the current album. I use ISO album naming when importing and the MIME type as well.<br>
So I get albums like 2010-06-15/orf/ and 2010-06-15/jpg. Now I would get more albums to manage: 2010-06-15/orf/original and 2010-06-15/jpeg/original.<br>
But that would still be much better than hiding albums or photos! We can not hide files because when we manage albums with file management, we easily would end up to delete album thinking it is empty, while it would not.</cite></p>
<p>I agree with the hiding. But as I said, this option to move to some other folder will be fully customizable ;) There are use-cases for the moving, which I found myself in by the way.</p>
<p><cite>The original and edited photos can exist in same album but renamed just different way when viewed by file manager. But in digiKam, it should notice automatically what is original and what is edited and group them.</cite></p>
<p>Yes, that's the original idea and most probably will be default. It will indeed somehow group the photos and indicate, that there are more versions inside digiKam.</p>
<p><cite>The UI is then important part. I have somewhere few mockups of versioning editing UI's what I could send to the author if he can use anything from them for his own idea.<br>
But I think that if we add somekind marking over photo when we hover it (or shown all the times) and then we would get somekind list of versions what could be opened, it could make things easier.</cite></p>
<p>Yes, that would be great, be sure to send them martin.klapetek at gmail dot com thanks! :) I'm thinking about all-the time visible indicator in the album view, maybe something like few images on a pile but it can't be mistaken with "album" meaning or something like that.</p>
<p>The TIFF stuff..well, yeah, I think this should go to devel ML, I'll post it there later.</p>
<p>Marty</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div><a id="comment-19277"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/524#comment-19277" class="active">Why are we moving files all over the place?</a></h3>    <div class="submitted">Submitted by Gareth (not verified) on Wed, 2010-06-16 02:14.</div>
    <div class="content">
     <p>I am excited that you are building parametric image editing into Digikam, but it sounds as if you are trying to reinvent the wheel. I wonder if you have read The DAM Book by Peter Krogh? There are already established methods for doing what you are proposing. I'm not saying that digikam should work exactly like Lightroom or Bibble, I'm just concerned that the way you are going about this will make digital asset management more awkward.</p>
<p>My main concern is that you propose to move my master files into a separate folder. I already have a system for managing originals and derivatives. Before you go moving my files around, please try to understand that I have them organized a certain way already.</p>
<p>Anyway, I good luck with this project. This is a badly needed addition to Digikam, and I am excited that it is coming along so quickly.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19280"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/524#comment-19280" class="active">I have as well.</a></h3>    <div class="submitted">Submitted by Fri13 on Wed, 2010-06-16 09:54.</div>
    <div class="content">
     <p>I have as well. year-month-day/MIME -format.</p>
<p>And I have only few kind sub-albums after Y-M-D album. ORF for olympus RAW, CR2 for Canon RAW, JPEG for all cameras and then a PNG where I have all the edited versions.<br>
When I select the main album, I have set to group photos by type. So I get PNG's in own box and I can see straight away what I have edited.</p>
<p>What I have been thinking is that when you edit photo, it should save it to automatically to mime typed sub-album. Like if I have album/orf and album/jpg albums already and I edit photo. When I save it, it creates album/png and moves the file there. But we need this non descructive way and the file renaming. </p>
<p>Question really is how many savings there comes among editing? Like is it per save (Ctrl+S) or edit (levels, curves)? Like I usually do about 10-15 edits for every photo. Resize, Crop, Whitebalance, Sharpening, noise reduction, Gamma, Levels/curves, Saturation, Vivid. Those are my very basic tools. And I might use them multiple times. So if the versioning is per edit, I would end up having dozens of versions of the same photo and then when storing in non-descrutive version, it could be 500Mb for one photo (PNG format, 30-60Mb each when on 10Mpix camera, more when 16Mpix camera)</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19282"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/524#comment-19282" class="active">Me too!</a></h3>    <div class="submitted">Submitted by Jeff (not verified) on Wed, 2010-06-16 10:35.</div>
    <div class="content">
     <p>I have my own system for managing originals (which I keep outside of<br>
DigiKam) and the master copies and edits that I keep within DigiKam.<br>
I won't bore you with the details. I would like to be able to continue<br>
doing things "the hard way" (i.e., my way) if the new changes do not<br>
suit my established workflow.</p>
<p>A very common operation I perform for family "snaps" is to take an<br>
original image in 3:2 aspect ratio and crop it several ways for better<br>
composition. I'll have one new 3:2 image that I'll print at 6"x4" and<br>
another new 7:5 image for a 7"x5" print, etc. Sometimes, I'll want yet<br>
another at 5:4 to print to 10"x8". Usually, I'll start by doing colour<br>
and level corrections to get, say, "*-01" and then create all the<br>
different crops from this edited image. There really isn't any "current"<br>
image when I am done; I want to see all (or most) of the cropped images.<br>
I suspect that the proposed versioning system will make this unworkable.<br>
My alternative is to hit "Save As..." and just add "-02-7x5", or<br>
whatever, to the end of the image file name. That isn't particularly<br>
hard to do, so if that is still possible, I'll be content.</p>
<p>I assume that many (most?) users would appreciate the Picasa-like system<br>
that is proposed, but please do not force us to use it. Leave the<br>
"Save As..." button alone and don't have a UI that makes it impossible<br>
to prevent the creation of copies of images. This might be as simple as<br>
allowing "Save" to trigger the new system and "Save As..." to remain as<br>
it is.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19291"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/524#comment-19291" class="active">Save As will stay, because</a></h3>    <div class="submitted">Submitted by Martin Klapetek on Thu, 2010-06-17 10:49.</div>
    <div class="content">
     <p>Save As will stay, because you may also want to save to some other format, which would be impossible without it. Also, the versioning system that is planned should allow you exactly this. You do several edits (crops) and they will be like forks of the original and of course you will be able see them all. In fact, I'd like to do it in such way, that after some clicking you could see them all along, like it would be another album (this is great if you do some other edits than crop, eg. brightness/contrast, color balance, adjusting curve etc)</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-19290"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/524#comment-19290" class="active">Gareth, Fri13: My main</a></h3>    <div class="submitted">Submitted by Martin Klapetek on Thu, 2010-06-17 10:42.</div>
    <div class="content">
     <p>Gareth, Fri13: <cite>My main concern is that you propose to move my master files into a separate folder. I already have a system for managing originals and derivatives. Before you go moving my files around, please try to understand that I have them organized a certain way already.</cite></p>
<p>No, you understood it wrong. This is the way it currently works (for many reasons), but as I said, it <strong>will be fully customizable</strong>, so you can just simply turn it off and no moving will be done. And default setting will be leaving the files where they are. So don't worry.</p>
<p><cite>Question really is how many savings there comes among editing?</cite><br>
You understood this wrong too :) There will be saving with every edit yeah, BUT it won't create another file for every edit you make (this idea is bad itself). There will be only _one_ created file and this one created file is for thumbnailing and for editing with other apps than digiKam. Another file will be created if you do some edit, which is not simply reproducible (some random values effect for example). There will be option for leaving all files between the nondeterministic effets used, or keep just the last one. For example you do this: autolevels, brightness, raindrops, black&amp;white, film grain. So the new file would only get created if you use raindrops and film grain, because these are not-same-reproducible. And you'll have an option to keep all these version (would speed up removing some changes etc) or to keep only the last one.</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div><a id="comment-19278"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/524#comment-19278" class="active">If editing is done in</a></h3>    <div class="submitted">Submitted by Dmytro (not verified) on Wed, 2010-06-16 07:26.</div>
    <div class="content">
     <p>If editing is done in external apps, it's user/external app responsibility not to destroy the original file,<br>
but digikam to pick up the modified versions.</p>
<p>It would be great if digiKam would not force his own history/layout/etc but would be able to work with existing one,<br>
manually or even better by user defined rules, with possibility of manual override in selected cases.</p>
<p>For example I have raw files and converted by external raw developer jpegs in converted/ directory but with the same name.<br>
It would be really nice if digikam automatically merged the raw file and one or multiple converted jpegs one one related image,<br>
with the last image displayed.</p>
<p>I have the similar situation with film scans, I keep both raw negatives as tiffs and converted jpegs,<br>
I'd much prefer digikam not to show me the negatives.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19281"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/524#comment-19281" class="active">As far digiKam has tried to</a></h3>    <div class="submitted">Submitted by Fri13 on Wed, 2010-06-16 09:58.</div>
    <div class="content">
     <p>As far digiKam has tried to show all the photos what is stored in albums. More like file manager shows all files (expect hidden).</p>
<p>Is there time when now we would need to get digiKam possibility to hide specific files/photos? Like having a tag/filename what would get hidden automatically? This way we could make edited versions, hide them and show only newest. But we would have problem to get user easily to see all the photos in that album.</p>
<p>Lightroom has nice idea about virtual copies. When you make edits, it stores only the edits to it database. Then when you export photo, it creates real file. Or when you create the virtual copy, it allows user to set new set of edits to that but it is only in database, not in the file itself until you export it. So you can have multiple versions with one file, while saving disk space.</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-19283"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/524#comment-19283" class="active">So how will this appear to</a></h3>    <div class="submitted">Submitted by maninalift (not verified) on Wed, 2010-06-16 11:00.</div>
    <div class="content">
     <p>So how will this appear to the user. I'm guessing something like:</p>
<p>* user searches for for "dog", there is one photo in his library<br>
* the user adjusts contrast to +15<br>
* user closes application - there is no warning about saving changes<br>
* user searches for "dog" there, there is still only one photo in the library<br>
* when the user opens the image he sees the adjusted image, and a notification or visual indication of some sort that "previous versions of this image exist"<br>
* the user decides the contrast is too high, he looks at the contrast setting, which is at +15 (ie the application knows that this image is just the original image +15 contrast), he turns it back to 0 (the original value)<br>
* the image now is exactly as it was initially (no applying contrast +15 then -15) </p>
<p>Questions:</p>
<p>* What you seem to be talking about is keeping a catalogue of images to record the history of edits to a given original. This is not my understanding of what non-destructive image editing is:<br>
* The central part of non-destructive image editing is, keeping the original image and a history of edits to that image. This way any point in the history can be reconstructed without any loss of information and a record is kept of the relationship between the images. Keeping snapshots of the image at different points in time (i.e. actual image files) is necessary only to speed up the process and should be balanced against the disk space used, perhaps pruned over time.<br>
* It is not necessarily best to save the current image (and any other "snapshots") in the original image format (and definitely not if it is to be relied upon as a true record of the state of the image at that point in the editing). The snapshots are an internal matter for digiKam, they are there to speed up the process of reconstructing a point in the history and should be encoded in the way that best suits that purpose.</p>
<p>OK as I wrote this it became less like questions and more like a lecture. Please forgive the tone, I'm sure you do actually understand this far better than I do and this is really meant as a question because I want to understand what you are doing. thanks</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19292"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/524#comment-19292" class="active">That's pretty much how it</a></h3>    <div class="submitted">Submitted by Martin Klapetek on Thu, 2010-06-17 11:03.</div>
    <div class="content">
     <p>That's pretty much how it will appear to the user, yeah.</p>
<p>To your questions:<br>
<cite>What you seem to be talking about is keeping a catalogue of images to record the history of edits to a given original.</cite></p>
<p>No (Damn I should work on my explaining skills). That's wrong from so many views. My answer continues with your next question..</p>
<p><cite>The central part of non-destructive image editing is, keeping the original image and a history of edits to that image.</cite></p>
<p>Yes, that's how it will be done. Except that the history will be stored in another file (and in digiKam databse too), which will be there for thumbnaling reasons, for additional editing with external apps (eg. you do some edits in digiKam and then you want to do some edits in GIMP. That would be impossible of you have only the list of changes in database, you would need to export it first). And the snapshots will be used to speed up processing, although some users would like to keep all the snapshots. There will be this possibility.</p>
<p><cite> It is not necessarily best to save the current image in the original image format </cite></p>
<p>Hm, that's pretty good point. I will talk about it with my mentor and we'll see what we'll come up with.</p>
<p>That's ok, thanks for asking :)</p>
<p>Marty</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-19285"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/524#comment-19285" class="active">'Originals' Folder Path</a></h3>    <div class="submitted">Submitted by NoCoolNamesRemain (not verified) on Wed, 2010-06-16 14:11.</div>
    <div class="content">
     <p>It would be nice to be able to have the 'Originals' folder created relative to the root album path. Instead of a separate 'Originals' folder in each folder I have an edited picture in I would have one 'Originals' folder for all of the pictures in digiKam. This of course should be optional.</p>
<p>TLDR: let me change the full path of the 'Originals' folder not just the folder name</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19293"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/524#comment-19293" class="active">I don't think that's a good</a></h3>    <div class="submitted">Submitted by Martin Klapetek on Thu, 2010-06-17 11:07.</div>
    <div class="content">
     <p>I don't think that's a good idea. Think about when you copy some folder (album) to some other place, other computer let's say, where there isn't digiKam installed. That way you have also all the originals with you. If you would have all the originals in one place, they wouldn't get copied with the rest of your album, plus it would be very hard to pick those you actually want (think about 5000 pictures in there and think about naming conflicts...).</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19304"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/524#comment-19304" class="active">This is why this feature</a></h3>    <div class="submitted">Submitted by NoCoolNamesRemain (not verified) on Thu, 2010-06-17 19:15.</div>
    <div class="content">
     <p>This is why this feature needs to be optional. There are pros and cons to each method of storage. You mention that moving the edited version should also move the original along with it -- this way you always have your original. I agree, but it could also be that you don't want the original! If I am moving the album to a removable storage device -- let's say I am taking the files to the printer -- then I only want the latest version and not the originals. It is important to offer users the option of choosing the settings which best meet his needs. In order to avoid an overly complex interface sensible defaults should be chosen and the ability to change them can be hidden away in a 'settings' dialogue somewhere.</p>
<p>Having read your latest post it seem you agree (http://www.digikam.org/drupal/node/526); I did not look to see who I was replying to before making this post : ) I will leave this post here for anyone who is reading this thread later on.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19310"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/524#comment-19310" class="active">...this way you always have</a></h3>    <div class="submitted">Submitted by Martin Klapetek on Fri, 2010-06-18 01:16.</div>
    <div class="content">
     <p><cite> ...this way you always have your original. I agree, but it could also be that you don't want the original! If I am moving the album to a removable storage device -- let's say I am taking the files to the printer -- then I only want the latest version and not the originals.</cite></p>
<p>Yeah, but I think it's less "evil" to just exclude the Originals folder in this case (or copy the whole folder and then just delete the originals folder) than to make it hard for users which would like to simply preserve the originals in the album folder (Picasa also does it this way) and not having to go through thousands of originals in one folder to find the ones they want :) And also, I personaly think, that having options is really great and very useful, but it's just not easy to do options for everything and usable for every one and I also personaly think that having this option is useless as there is simple solution to it. But if there will be a strong demand, I don't see why it shouldn't be included ;)</p>
<p>Marty</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div></div><a id="comment-19286"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/524#comment-19286" class="active">hardlinks?</a></h3>    <div class="submitted">Submitted by <a href="http://blog.russellharrison.com/" rel="nofollow">Russell Harrison</a> (not verified) on Wed, 2010-06-16 15:28.</div>
    <div class="content">
     <p>What about creating the _v1 version as a hardlink of the original on import?  It wouldn't require any more space on the disk and would allow recovery of the original if it were edited in an external application.</p>
<p>It should also be possible, at least on Linux, to use inotify to watch the collection directories for updates to the files and create new versions whenever the files were changed by external applications.  I guess for other OSs and network collections there would need to be a manual way to scan for changes.  Maybe when the user hit the refresh button for an album?</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19294"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/524#comment-19294" class="active">What about creating the _v1</a></h3>    <div class="submitted">Submitted by Martin Klapetek on Thu, 2010-06-17 11:09.</div>
    <div class="content">
     <p><cite>What about creating the _v1 version as a hardlink of the original on import?</cite></p>
<p>This would get unusable on other platforms. I guess.</p>
<p><cite>use inotify to watch the collection directories for updates to the files and create new versions whenever the files were changed by external applications</cite></p>
<p>You would have to have digiKam running all the time to pick up on those changes.</p>
<p>Marty</p>
         </div>
    <div class="links">» </div>
  </div>
</div>
</div>