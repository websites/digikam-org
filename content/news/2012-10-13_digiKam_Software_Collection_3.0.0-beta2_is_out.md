---
date: "2012-10-13T22:28:00Z"
title: "digiKam Software Collection 3.0.0-beta2 is out..."
author: "digiKam"
description: "Dear all digiKam fans and users! After one summer of active development with Google Summer of Code 2012 students, and one first beta release, digiKam"
category: "news"
aliases: "/node/668"

---

<a href="http://www.flickr.com/photos/digikam/7362043208/" title="splash-showfoto by digiKam team, on Flickr"><img src="http://farm8.staticflickr.com/7215/7362043208_dbdb150261.jpg" width="500" height="307" alt="splash-showfoto"></a>

<p>Dear all digiKam fans and users!</p>

<p>After one summer of active development with Google Summer of Code 2012 students, and one first beta release, digiKam team is proud to announce the second beta of digiKam Software Collection 3.0.0.
This version is currently under development, following GoSC 2012 projects <a href="http://community.kde.org/Digikam/GSoC2012">listed here</a>.</p>

<p>See <a href="https://bugs.kde.org/buglist.cgi?f1=cf_versionfixedin&amp;o1=equals&amp;query_format=advanced&amp;bug_status=RESOLVED&amp;bug_status=NEEDSINFO&amp;bug_status=VERIFIED&amp;bug_status=CLOSED&amp;v1=3.0.0&amp;product=digikam&amp;product=digikamimageplugins&amp;product=kipiplugins&amp;product=showfoto">the list of files closed</a> with this release into KDE bugzilla.</p>

<p>digiKam software collection tarball can be downloaded from <a href="http://download.kde.org/unstable/digikam/digikam-3.0.0-beta2.tar.bz2.mirrorlist">KDE repository</a></p>

<p>This version is for testing purposes. <b>Please do not use it yet in production!</b></p>

<p>Happy digiKam testing...</p>
<div class="legacy-comments">

  <a id="comment-20412"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/668#comment-20412" class="active">Compiles </a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Sun, 2012-10-14 08:56.</div>
    <div class="content">
     <p>Compiles on macports with custom portfile.</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-20414"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/668#comment-20414" class="active">The download link is wrong</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Sun, 2012-10-14 19:46.</div>
    <div class="content">
     <p>The download link is wrong ?</p>
<p>http://download.kde.org/unstable/digikam/digikam-3.0.0-beta1a.tar.bz2.mirrorlist</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20415"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/668#comment-20415" class="active">fixed...</a></h3>    <div class="submitted">Submitted by digiKam on Mon, 2012-10-15 14:05.</div>
    <div class="content">
     <p>fixed...</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-20416"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/668#comment-20416" class="active">It works.</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Mon, 2012-10-15 14:05.</div>
    <div class="content">
     <p>The download link<br>
<a src="http://download.kde.org/unstable/digikam/digikam-3.0.0-beta1a.tar.bz2.mirrorlist">http://download.kde.org/unstable/digikam/digikam-3.0.0-beta1a.tar.bz2.mirrorlist</a><br>
works for me. You just need to scroll down a little on the page it takes you to.</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-20419"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/668#comment-20419" class="active">Showfoto function - Enhance</a></h3>    <div class="submitted">Submitted by rawer (not verified) on Sun, 2012-10-21 12:54.</div>
    <div class="content">
     <p>Showfoto function - Enhance -&gt; in-painting , suspend the program (as in version 2.9.0)</p>
         </div>
    <div class="links">» </div>
  </div>

</div>
