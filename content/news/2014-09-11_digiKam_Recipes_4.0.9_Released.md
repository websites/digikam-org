---
date: "2014-09-11T09:20:00Z"
title: "digiKam Recipes 4.0.9 Released"
author: "Dmitri Popov"
description: "Hot on the heels of the 3.17.01 release comes a new version of the digiKam Recipes ebook. As the version number indicates, this is a"
category: "news"
aliases: "/node/717"

---

<p>Hot on the heels of the 3.17.01 release comes a new version of the <a href="http://dmpop.dhcp.io/digikamrecipes/">digiKam Recipes</a> ebook. As the version number indicates, this is a major release that features several significant improvements.</p>
<p><img src="http://scribblesandsnaps.files.wordpress.com/2014/08/digikam-31701.png" alt="digikam-31701" width="375" height="500"></p>
<p><a href="http://scribblesandsnaps.com/2014/09/11/digikam-recipes-4-0-9-released/">Continue to read</a></p>

<div class="legacy-comments">

</div>