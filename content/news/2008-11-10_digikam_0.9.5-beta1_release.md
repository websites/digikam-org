---
date: "2008-11-10T10:07:00Z"
title: "digikam 0.9.5-beta1 release"
author: "digiKam"
description: "Dear all digiKam fans and users! The digiKam development team is happy to start a new serie of KDE3 releases. The digiKam 0.9.5-beta1 tarball can"
category: "news"
aliases: "/node/383"

---

<p>Dear all digiKam fans and users!</p>

<p>The digiKam development team is happy to start a new serie of KDE3 releases. The digiKam 0.9.5-beta1 tarball can be downloaded from <a href="http://sourceforge.net/project/showfiles.php?group_id=42641">SourceForge</a>.
Noteworthy features added since the last stable release include a new RAWImport tool, embedding all image plugin dialogs in editor window, as usual a lot of bug fixes - see below for a complete list of new features and fixed bugs.</p>

<br>
<a href="http://www.flickr.com/photos/digikam/3018933558/" title="digikam0.9.5-beta1 by digiKam team, on Flickr"><img src="http://farm4.static.flickr.com/3197/3018933558_545bb57eac.jpg" width="500" height="400" alt="digikam0.9.5-beta1"></a>
<br>

<h5>NEW FEATURES (since 0.9.4):</h5><br>

<b>General</b>        : libkdcraw dependency updated to 0.1.6.<br>
<b>Image Editor</b>   : All image plug-in tool settings provide default buttons to reset values.<br>
<b>Image Editor</b>   : New Raw import tool to handle Raw pictures with customized decoding settings.<br>
<b>Image Editor</b>   : All image plug-in dialogs are removed. All tools are embedded in editor window.<br>

<h5>BUG FIXES:</h5><br>

001 ==&gt; 166867 : digiKam crashes when starting.<br>
002 ==&gt; 167026 : Crash on startup Directory ImageSubIfd0 not valid.<br>
003 ==&gt; 146870 : Don't reduce size of image when rotating.<br>
004 ==&gt; 167528 : Remove hotlinking URL from tips.<br>
005 ==&gt; 167343 : Albums view corrupted and unusable.<br>
006 ==&gt; 146033 : When switching with Page* image jumps.<br>
007 ==&gt; 127242 : Flashing 'histogram calculation in progress' is a bit annoying.<br>
008 ==&gt; 150457 : More RAW controls in color management pop-up please.<br>
009 ==&gt; 155074 : Edit Image should allow chance to adjust RAW conversion parameters.<br>
010 ==&gt; 155076 : RAW Conversion UI Needs to be more generic.<br>
011 ==&gt; 142975 : Better support for RAW photo handling.<br>
012 ==&gt; 147136 : When selecting pictures from folder showfoto got closed.<br>
013 ==&gt; 168780 : Loose the raw import.<br>
014 ==&gt; 160564 : No refresh of the number of pictures assigned to a tag after removing the tag from some pictures.<br>
015 ==&gt; 158144 : Showfoto crashes in settings window.<br>
016 ==&gt; 162845 : 'Ctrl+F6' Conflict with KDE global shortcut.<br>
017 ==&gt; 159523 : Digikam crashes while downloading images from Olympus MJU 810/Stylus 810 camera no xD card.<br>
018 ==&gt; 161369 : Quick filter indicator lamp is not working properly in recursive image folder view mode.<br>
019 ==&gt; 147314 : Renaming like crazy with F2 slows down my system.<br>
020 ==&gt; 164622 : Crash using timeline when switching from month to week to month view.<br>
021 ==&gt; 141951 : Blank empty context right-click menus.<br>
022 ==&gt; 116886 : Proposal for adding a new destripe/denoise technique.<br>
023 ==&gt; 163602 : Race condition during image download from Camera/ USB device: image corruption and/or loss.<br>
024 ==&gt; 165857 : Unreliable import when photo root is on network share.<br>
025 ==&gt; 147435 : Resize slider in sharpness dialog doesn't work correct.<br>
026 ==&gt; 168844 : Make the selection rectangle draggable over the image (not only resizable).<br>
027 ==&gt; 147151 : Compile error: multiple definition of `jpeg_suppress_tables'.<br>
028 ==&gt; 170758 : High load when tagging.<br>
029 ==&gt; 168003 : Drag&amp;dropping a photo to a folder in Dolphin begins copying the whole system:/media.<br>
030 ==&gt; 142457 : Temp files not cleaned up after crashes.<br>

<div class="legacy-comments">

  <a id="comment-17945"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/383#comment-17945" class="active">All image plug-in dialogs are</a></h3>    <div class="submitted">Submitted by scroogie (not verified) on Mon, 2008-11-10 10:51.</div>
    <div class="content">
     <p><cite>All image plug-in dialogs are removed. </cite></p>
<p>This is a blessing! I always thought that digikam used way to many dialogs. Thanks!</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-17946"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/383#comment-17946" class="active">final version</a></h3>    <div class="submitted">Submitted by <a href="http://bluemoon.nazory.cz" rel="nofollow">bluemoon</a> (not verified) on Mon, 2008-11-10 11:13.</div>
    <div class="content">
     <p>yes, 0.9.5, i look forward to final version!!!</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-17947"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/383#comment-17947" class="active">Just a small question</a></h3>    <div class="submitted">Submitted by houba (not verified) on Mon, 2008-11-10 11:53.</div>
    <div class="content">
     <p>I was just wondering why do you prefer continuing with the qt3 version, rather than focusing on the kde4 version ?</p>
<p>Houba</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-17948"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/383#comment-17948" class="active">KDE3 is the more stable version of KDE desktop</a></h3>    <div class="submitted">Submitted by digiKam on Mon, 2008-11-10 12:14.</div>
    <div class="content">
     <p>KDE3 is the most popular and stable version of KDE desktop published around the world. KDE4 cannot be yet considerated as finalized. It lack a lots of features to be able to replace KDE3. This will be better with KDE 4.2 i think.</p>
<p>We cannot forget KDE3 version for the moment. Bug fixes and new important features has been added (as RAWImport and Editor Tools). It use the same implementation than KDE4 and all bug reports done on these parts are fine for digiKam 0.10.0.</p>
<p>Also, 0.9.5 compile fine with last libkdcraw 0.1.6 (which use libraw core implementation as RAW decoder). We cannot ignore this point.</p>
<p>But i think 0.9.5 release will be the last one for KDE3...</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-17949"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/383#comment-17949" class="active">blah blah blah</a></h3>    <div class="submitted">Submitted by Aaron Seigo (not verified) on Mon, 2008-11-10 12:22.</div>
    <div class="content">
     <p>You know, you continue to say these things and yet the distros have by and large already moved on. Most of the questions in #kde on irc these days are about KDE4.</p>
<p>Meanwhile you spend resources on the KDE3 version while no KDE4 version is available.</p>
<p>I get it that you are conservative when it comes to these things, yet your choice of where to put resources for the next version of digikam is really puzzling to me. What happens when, if as you say, KDE 4.2 comes out and it does indeed meet your standards and there is no digikam for KDE4 yet? I try the digikam for KDE4 on fairly regular basis and continue to be concerned with how long its taking to get to a usable state; it's not surprising given the decision to continue piling work into the KDE3 version.</p>
<p>And you do realize that KDE4 apps work just fine in a KDE3 env? (Just as the reverse is also true?)</p>
<p>For all your hesitations about KDE4, I really think you misjudged this one.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-17950"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/383#comment-17950" class="active">Thanks Gilles for your commitment to end users</a></h3>    <div class="submitted">Submitted by <a href="http://jjorge.free.fr" rel="nofollow">Zézinho</a> (not verified) on Mon, 2008-11-10 13:32.</div>
    <div class="content">
     <p>Yes, thanks a lot for the still best KDE3 version, that I just packaged for latest Mandriva  distribution. Why? Simply because I need it ;-)</p>
<p>I know that all those who work hard on KDE4 can feel badly people working on KDE3, but KDE 3.5 is just too good to be thrown away. I've got a testing machine with KDE 4, but just can't say to any end user "Go with latest KDE." Because I know he will have to fight with things that "no geeks" just can't do with. In fact, I've got 20 friends and relatives that use daily KDE3.5, and will not tell them to change before I see KDE4 with an amazing suite of applications which have received enough love from geeks to be cool for non-geeks.</p>
<p>So thanks again Gilles, this 0.9.5 release will go into all major distributions : Debian and Mandriva. Oups, troll inside ;-)</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-17954"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/383#comment-17954" class="active">You don't understand. Noone</a></h3>    <div class="submitted">Submitted by scroogie (not verified) on Mon, 2008-11-10 14:39.</div>
    <div class="content">
     <p>You don't understand. Noone talks about throwing away KDE 3.5. You can install KDE4 dependencies alongside KDE3 and thus are able to use them in KDE3. A digikam for KDE4 would ergo please all users.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-17956"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/383#comment-17956" class="active">I don't know where the</a></h3>    <div class="submitted">Submitted by Andi Clemens on Mon, 2008-11-10 15:00.</div>
    <div class="content">
     <p>I don't know where the problem is. We provide a KDE4 version of digiKam which is under heavy development. The KDE3 version is not touched that much these days, only backports from trunk are committed into it. So no time is "wasted" on the old project... all new features go into the KDE4 version.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-17960"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/383#comment-17960" class="active">There no problem besides</a></h3>    <div class="submitted">Submitted by scroogie (not verified) on Mon, 2008-11-10 17:34.</div>
    <div class="content">
     <p>There no problem besides misunderstanding. The sentence "KDE 3.5 is just too good to be thrown away" just shows that there is a wrong perception of things. It's dangerous, as it splits the community where it is needless.</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div></div></div><a id="comment-17951"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/383#comment-17951" class="active">Thanks a lot</a></h3>    <div class="submitted">Submitted by Kay (not verified) on Mon, 2008-11-10 14:15.</div>
    <div class="content">
     <p>Hello,</p>
<p>just to say that I fully agree with your choice. We appreciate your caring for the users.</p>
<p>Did I get it right though, that you do have a 0.10 release in the making that is already ported? Or is that only a plan?</p>
<p>Yours,<br>
Kay</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-17952"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/383#comment-17952" class="active">digiKam is already ported: 0.10.0</a></h3>    <div class="submitted">Submitted by digiKam on Mon, 2008-11-10 14:28.</div>
    <div class="content">
     <p>yes, it's already ported. KDE3 (0.9.5) and KDE4 (0.10.0) source code are fully synchronized...</p>
<p>Of course, 0.10.0 is not only a simple port. A lots of new features have been added. This is why it's long to stabilize code. We will trying to complete KDE4 version for KDE 4.2.</p>
<p>digiKam</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-17988"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/383#comment-17988" class="active">Great to hear that stable</a></h3>    <div class="submitted">Submitted by xeros (not verified) on Tue, 2008-11-18 20:43.</div>
    <div class="content">
     <p>Great to hear that stable DigiKam release should be ready for KDE 4.2 :-)<br>
I'm testing KDE4 since 4.0 beta release and with 4.1.2 (and now 4.1.3) I've decided to migrate all my friends desktops to KDE4. I'm looking towards for all KDE3 applications being ported to KDE4. And DigiKam is the one of the most needed because using stable DigiKam 0.9.x releases in KDE4 with KDE3 dependencies gives many problems related to KDE3 desktop configuration without tools (from KDE3) to configure it (KDE3 apps). It's mostly the video player (and codecs) problem on albums in DigiKam. So I'm glad to hear that the most effort in DigiKam development is KDE4 version.</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div><a id="comment-17967"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/383#comment-17967" class="active">Very good, full support for this from me too</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Wed, 2008-11-12 12:36.</div>
    <div class="content">
     <p>When I read in the new kubuntu change descriptions the details about the new KDE4 which still lacks a lot of functionality 3.5.10 has, I simply refrained from upgrading at this time, like many others, too.</p>
<p>So your new KDE version is certainly most welcome for many people.</p>
<p>You're churning out new versions and functionalities so fast, it's a real firework of programming! Congratulations!</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-18091"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/383#comment-18091" class="active">Thanks</a></h3>    <div class="submitted">Submitted by Julien Narboux (not verified) on Sun, 2008-12-28 09:09.</div>
    <div class="content">
     <p>I am glad you still backport some changes to KDE3, I installed latest mandriva on my laptop with KDE4 and I am ot happy with it. They even package the KDE4 version of Digikam which is far from stable, and not translated properly. I think it is very nice to have a stable version. I think the Digikam team is right : they backport features when it is easy to do in KDE3 but still keep working on the KDE4 version mainly.</p>
<p>Thanks,</p>
<p>Julien</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-17958"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/383#comment-17958" class="active">Not (all) tags written...</a></h3>    <div class="submitted">Submitted by Philippe (not verified) on Mon, 2008-11-10 15:10.</div>
    <div class="content">
     <p>I don't actually want to submit because I'm not entirely sure of me (I have to reproduce and be sure of the bug and when it happens) but some could perhaps answer to me...</p>
<p>I'm a new user of digikam (before I was using jbrout)...</p>
<p>I want to write the tags in the jpeg files so I select the right option in the settings. But when I tag photos, some tags are written to the jpg files and other photos are not modified. The database seems quite good that's why I didn't see the problem for a while.</p>
<p>Is it possible for a reason (no place for IPTC metadata , filesystem --ntfs--,...) that the tags are not written??</p>
<p>I'm working with the 0.9.4 version.</p>
<p>Thanks for all</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-17964"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/383#comment-17964" class="active">correction...</a></h3>    <div class="submitted">Submitted by Philippe (not verified) on Tue, 2008-11-11 01:25.</div>
    <div class="content">
     <p>I have verified carefully what I've experienced.<br>
In fact, the tags are well written in the jpg files. So Digikam does its job well.</p>
<p>In fact, it's gwenview which is not able to read the iptc and exif data for some images. I don't know why :(</p>
<p>I was looking in a third software (Microsoft gallery) to know which one failed in its job and the last time the tags didn't appear. It was because this soft was slow to read all the datas. I can see them now!</p>
<p>But I have this time a real bug. When I was applying some tags (I don't remember exactly if that was what I'm doing but I'm sure it was not risked), 3 photos have disapeared of digikam (I was enable to make them reappear) even if they are present on the filesystem.</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-17963"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/383#comment-17963" class="active">Thanks!</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Mon, 2008-11-10 19:28.</div>
    <div class="content">
     <p>Thanks for providing a new version of Digikam 0.9.x!</p>
<p>(Btw.: Digikam 0.9.x runs nicely under KDE3, KDE4, Gnome, XFCE, ...)</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-18023"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/383#comment-18023" class="active">Matching kipi-plugins update?</a></h3>    <div class="submitted">Submitted by Corinna (not verified) on Wed, 2008-12-03 15:22.</div>
    <div class="content">
     <p>Will there be a matching kipi-plugins update as well?<br>
In 0.1.6 there's still at least the crash when trying<br>
to use the geolocalization feature, so it would be<br>
nice to get a 0.1.7 release here...</p>
<p>Thanks,<br>
Corinna</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-18024"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/383#comment-18024" class="active">planned...</a></h3>    <div class="submitted">Submitted by digiKam on Wed, 2008-12-03 15:50.</div>
    <div class="content">
     <p>Yes, it's planed to do a bug fix release</p>
<p>digiKam</p>
         </div>
    <div class="links">» </div>
  </div>
</div>
</div>
