---
date: "2010-05-31T12:42:00Z"
title: "Coding Sprint 2010..."
author: "digiKam"
description: "As in 2008 and 2009, a new digiKam developers reunion is planned in Europe. This year, i proud to announce that i will"
category: "news"
aliases: "/node/518"

---

<a href="http://www.flickr.com/photos/digikam/4620818767/" title="kdecodingsprint2010 by digiKam team, on Flickr"><img src="http://farm5.static.flickr.com/4031/4620818767_5fb7450bc0_m.jpg" width="240" height="105" alt="kdecodingsprint2010"></a>

<p>As in <a href="http://www.digikam.org/drupal/node/354">2008 and 2009</a>, a new digiKamdevelopers reunion is planned in Europe. This year, i proud to announce that i will organize the event in south of France, in the nice city of <a href="http://www.aixenprovencetourism.com">Aix en Provence</a>.</p>

<p><img src="http://album-photo.geo.fr/pict_geofr/thumb/5498/510x510/thumb_479cf0e5fbcd77d8e029a73633da9c74.jpg" width="510" height="383" alt=""></p>

<p>This year, we will meet the first group of <a href="http://code.google.com/soc">Google Summer of Code</a> developers working on digiKam project. Planned works will be heterogeneous as image versioning, face recognition, reverse geo-coding, and scripting interface. For more details, look the complete <a href="http://techbase.kde.org/Projects/Digikam/CodingSprint2010#KDE_Graphics_Coding_Sprint_2010">wiki page dedicated to this event</a>.</p>

<p>As usual, the event will be sponsored by <a href="http://ev.kde.org">KDE-ev</a>, to take in charge a part of travels costs and hotel. We have receive also a small donation from a Chinese photo agency named <a href="http://www.sinopictures.com/">Sino Pictures</a>. To see this event become a real success, we need more money to organize journeys with peoples who contribute to open-source through digiKam project, and share free time in real life to works with us.</p>

<p>So, if you want to sponsorship this event, please <a href="http://www.digikam.org/drupal/donation">follow the instructions given at this page</a>. Thanks in advance for your help...</p>
<div class="legacy-comments">

</div>
