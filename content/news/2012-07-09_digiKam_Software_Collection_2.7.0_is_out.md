---
date: "2012-07-09T13:58:00Z"
title: "digiKam Software Collection 2.7.0 is out..."
author: "digiKam"
description: "Dear all digiKam fans and users! After one month of bugs triage in KDE bugzilla, digiKam team is proud to announce the digiKam Software Collection"
category: "news"
aliases: "/node/661"

---

<a href="http://www.flickr.com/photos/digikam/7500372516/"><img src="http://farm8.staticflickr.com/7273/7500372516_eb64744890_c.jpg" width="800" height="320"></a>

<p>Dear all digiKam fans and users!</p>

<p>After one month of bugs triage in KDE bugzilla, digiKam team is proud to announce the digiKam Software Collection 2.7.0, as bug-fixes release.</p>

<p>In this release, for a better RAW files processing support, <a href="http://www.libraw.org/node/1602">Libraw library</a> have been imported to libkdcraw with last 0.14.7 release. New cameras are now supported: Canon 5D Mark III, G1 X, 1D X and Powershot SX200; Nikon D4,D800/D800E and D3200; Fuji X-S1 and HS30EXR; Casio EX-Z8; Olympus E-M5; Panasonic GF5; Sony NEX-F3, SLT-A37 and SLT-A57; Samsung NX20 and NX210.</p>

<p>Internal <a href="http://lensfun.berlios.de/article.php?story=20120616104824936">LensFun library</a> have been updated to last 0.2.6 release. This one include new camera profiles to apply automatically optical lens corrections.</p>

<p>Internal <a href="http://sourceforge.net/projects/libpgf/files/libpgf/6.12.24-latest/">LibPGF library</a> have been updated to last 6.12.24 release, to improve speed up of thumbnails processing in icon-view.</p>

<p>See <a href="https://bugs.kde.org/buglist.cgi?f1=cf_versionfixedin&amp;o1=equals&amp;query_format=advanced&amp;bug_status=RESOLVED&amp;bug_status=NEEDSINFO&amp;bug_status=VERIFIED&amp;bug_status=CLOSED&amp;v1=2.7.0&amp;product=digikam&amp;product=digikamimageplugins&amp;product=kipiplugins&amp;product=showfoto">the list of files closed</a> with this release into KDE bugzilla.</p>

<p>digiKam software collection tarball can be downloaded from <a href="http://sourceforge.net/projects/digikam/files">SourceForge Repository</a></p>

<p>Happy digiKaming...</p>
<div class="legacy-comments">

  <a id="comment-20273"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/661#comment-20273" class="active">Astonishing work ...  but...</a></h3>    <div class="submitted">Submitted by Mike Scholz (not verified) on Mon, 2012-07-09 20:01.</div>
    <div class="content">
     <p>.. I look very sad after so many improvements in your fantastic work that the Distributions are way after the latest releases and I dont have a clue how to install the latest Digikam Version. Why ? : because I'm not familiar with compiling all the sources and packages to one working program. I think thats really the "dark" side of Linux... not being able to get all needed Libraries compiled in one package. That would make it so much easier and you dont have to hassle around with broken dependencies after trying to update all needed new libraries...;  Yeah maybe I'm a dumb ass for a linux crack.. but I use Linux for over a year and I never ( luckily) needed to compile anything. So what am I trying to say is that I appreciate your work veeeery much - it's absolutely fantastic !  But sadly... the "normal" user cant get up with your latest release... still using Version 2.5 until a Debian Package will be released... :-( </p>
<p>Anyway.. keep up the georgeous work !  </p>
<p>Mike</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20274"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/661#comment-20274" class="active">PPA...</a></h3>    <div class="submitted">Submitted by digiKam on Mon, 2012-07-09 20:09.</div>
    <div class="content">
     <p>From digiKam users mailing list, Philip Johnsson said that digiKam digiKam 2.7.0 is now available for Ubuntu 12.04 (precise) in "extra" PPA...</p>
<p>https://launchpad.net/~philip5</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-20275"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/661#comment-20275" class="active">It's very easy to do what you</a></h3>    <div class="submitted">Submitted by Pawlo (not verified) on Mon, 2012-07-09 21:59.</div>
    <div class="content">
     <p>It's very easy to do what you want, but you have to drop Windows stupid way of doing things. In Kubuntu it's enough to add PPA and if Debian doesn't allow you to do the same it's only Debian's fault.</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-20276"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/661#comment-20276" class="active">Mike Scholz, you should check</a></h3>    <div class="submitted">Submitted by risoto (not verified) on Mon, 2012-07-09 23:23.</div>
    <div class="content">
     <p>Mike Scholz, you should check this link : http://packages.qa.debian.org/d/digikam.html </p>
<p>For Fedora users, you can get the latest version of digikam (especially 2.7.0) through to Testing-Updates repo with this command : yum upgrade digikam --enablerepo=updates-testing</p>
<p>For Debian users, a similar command may exist.</p>
<p>And thank to the developers for this fabulous software :)</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-20277"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/661#comment-20277" class="active">The way out</a></h3>    <div class="submitted">Submitted by Jag59 (not verified) on Tue, 2012-07-10 13:32.</div>
    <div class="content">
     <p>I was waiting for mint13kde(LTS), but as it is not available yet, installed kubuntu 12.04 LTS before one week. By default there is dk 2.5.0 but i am thinking of upgrading to 2.7.0 by adding Philip's PPI.<br>
The trouble with kubuntu is- by default hibernation is disabled. If 'sudo pm-hibernate' (in konsole) hibernates the system, enable it by referring the forum post<br>
https://help.ubuntu.com/12.04/ubuntu-help/power-hibernate.html<br>
Then I added plasma widgets<br>
plasma-widget-quickaccess<br>
plasma-widget-lancelot<br>
Now i am very satisfied with it and may not install mint13kde.</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-20279"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/661#comment-20279" class="active">Mike,
to clarify previous</a></h3>    <div class="submitted">Submitted by ben (not verified) on Wed, 2012-07-11 19:19.</div>
    <div class="content">
     <p>Mike,<br>
to clarify previous posts, installing (and keeping up to date) the latest digikam should be as simple as (supposing you're using a flavour of ubuntu) :<br>
- open a terminal (super -&gt; term)<br>
- type : "sudo add-apt-repository https://launchpad.net/~philip5" + enter<br>
annnnd, you're done ! That is, if you already have installed digikam (it shoud update itself to the latest version of the repo). If this is not the case, you just have to install digikam using the way you find best (ubuntu software center, synaptic, muon, terminal,..)</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20280"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/661#comment-20280" class="active">(follow-up)
correction</a></h3>    <div class="submitted">Submitted by ben (not verified) on Wed, 2012-07-11 19:25.</div>
    <div class="content">
     <p>(follow-up)<br>
correction :<br>
https://launchpad.net/~philip5 must be replaced by "ppa:philip5/extra"</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-20318"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/661#comment-20318" class="active">You must change your linux "distro"</a></h3>    <div class="submitted">Submitted by urcindalo (not verified) on Tue, 2012-08-07 11:59.</div>
    <div class="content">
     <p>What you point out is not the dark side of Linux, but the dark side of your particular Linux "distro".</p>
<p>I'm a proud Gentoo user and, right now, I'm installing the v2.7.0, even though the stable version for amd64 Gentoo Linux is v2.6.0. The best part is I just needed to add a digikam-2.7.0 text line to a special file, ran a command afterwards, and my beloved Gentoo is doing its magic :)</p>
<p>To sum it up, change your Linux distro. Source-based "distros" like Gentoo or Arch are always up-to-date, highly reliable and very customizable. Your life will change. For Gentoo you might start with Sabayon, a Gentoo-based distro wich also installs binary packages.</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-20278"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/661#comment-20278" class="active">Mac OS</a></h3>    <div class="submitted">Submitted by MacUSER (not verified) on Wed, 2012-07-11 17:01.</div>
    <div class="content">
     <p>Good job! My favourite OSS.<br>
I just miss binaries for MAC OSX...</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20303"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/661#comment-20303" class="active">Digikam for Mac OS X</a></h3>    <div class="submitted">Submitted by <a href="http://www.mikeg.de" rel="nofollow">mikeg</a> (not verified) on Wed, 2012-07-25 10:09.</div>
    <div class="content">
     <p>I'm using MacPorts which is quite simple:</p>
<ul>
<li>Install MacPorts: <a>http://www.macports.org/install.php</a></li>
<li>Login to terminal with super user (&gt;su) –&nbsp;If you havent enabled su check <a href="http://support.apple.com/kb/ht1528">Enable Superuser on Mac</a></li>
<li>After successfully login with su type "port search digikam" to check the version</li>
<li>then type "port install digikam" and wait until the process has finnished</li>
<li>I recommend to reboot since Digikam won't start on my machine after a fresh install</li>
<li>If you can't connect to the database check this <a href="https://bugs.kde.org/show_bug.cgi?id=299988">ticket</a></li>
</ul>
<p>Et voilà –&nbsp;I recommend to use a local db (SQLite) instead of a remote MySQL since i ran into serval issues (performance and x-plattform stuff) and not to store the images on a remote file mount as well. My best system is a SQLite and a local folder (or USB-Drive).</p>
<p>Bye<br>
Mike</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20304"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/661#comment-20304" class="active">Well …&nbsp;since i ran into a</a></h3>    <div class="submitted">Submitted by <a href="http://www.mikeg.de" rel="nofollow">mikeg</a> (not verified) on Wed, 2012-07-25 14:57.</div>
    <div class="content">
     <p>Well …&nbsp;since i ran into a couple of issues first i made a clean install again as described above (with the db trick at least). Unfortunately this won't do it since:</p>
<ul>
<li>No Thumbs were shown</li>
<li>Using Maintenance Tools won't help</li>
<li>No difference between using remote or local folder</li>
<li>Filemanager isn't installed with the above shown procedure</li>
<li>Compiling step by step won't help either</li>
</ul>
<p>So … Digikam on OSX wold be nice but at present it's wasted time. Stay at a native installation without using 3rd party software like MacPorts. If anyone got a running version, his/her how to would be nice to try.</p>
<p>Bye<br>
Mike</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div><a id="comment-20284"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/661#comment-20284" class="active">Thanks a lot for the windows</a></h3>    <div class="submitted">Submitted by Hank (not verified) on Sat, 2012-07-14 21:47.</div>
    <div class="content">
     <p>Thanks a lot for the windows update to 2.6.0. - it's much appreciated. However, it doesn't even open. I see the splash-screen, and then - nothing more. Any idea anyone?</p>
<p>The last version that worked for me (WinXP SP3) was 2.3.0. But I'm not giving up yet, wating for version 2.7.0.</p>
<p>B.t.w. I tried to add a reply under the windows 2.6.0. announcement, but that isn't possible.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20285"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/661#comment-20285" class="active">I've tried Digikam 2.6.0</a></h3>    <div class="submitted">Submitted by Stef (not verified) on Sun, 2012-07-15 14:00.</div>
    <div class="content">
     <p>I've tried Digikam 2.6.0 under Windows 7 64bit. Unfortunately I got only a bluescreen while installing...</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20286"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/661#comment-20286" class="active">I've also tried to install it</a></h3>    <div class="submitted">Submitted by Daniel (not verified) on Mon, 2012-07-16 10:57.</div>
    <div class="content">
     <p>I've also tried to install it under Windows 7 64bit.<br>
Installation was fine, but I only get the splash screen but the application terminates after this.<br>
ShowFoto for example starts completely.<br>
I also don't give up to wait for a working windows version.</p>
<p>Digikam is a great software and should be provided also for windows to increase it's popularity.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20287"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/661#comment-20287" class="active">Check if you have any USB</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Mon, 2012-07-16 13:58.</div>
    <div class="content">
     <p>Check if you have any USB devices plugged in. If I have, then digiKam behaves like you describe. Also it crashes when starting it without a USB device at the moment I´m plugging in a USB device. Without a USB device, it works.</p>
<p>With USB device I mean a Stick, HDD, or any other Storage device (probably called USB Mass Storage Device). No problems with my USB mouse :-)</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20288"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/661#comment-20288" class="active">Please Confirm</a></h3>    <div class="submitted">Submitted by Ananta Palani on Wed, 2012-07-18 00:05.</div>
    <div class="content">
     <p>Can all of you confirm if unplugging USB devices solves your problems?</p>
<p>Before doing the following, make a backup of your digikam database!</p>
<p>You might also try resetting your settings (after making a backup) by removing the following file:</p>
<p>   C:\Users\Ananta\AppData\Roaming\.kde\share\config\digikamrc</p>
<p>Where ~YourUserName~ is your user name in Windows Vista or Windows 7. I'm not sure where the .kde folder is stored in Windows XP in a user profile, but have a look under:</p>
<p>   C:\Documents and Settings\~YourUserName~\Application Data\</p>
<p>for the 'digikamrc', and remove it (back it up, eh?).</p>
<p>Now run digiKam. It will have no idea where your collection is located, so tell it and see if it loads. If it does load, send me your newly generated digikamrc file and the original one (~MyFirstName~~MyLastName~@gmail.com, replace as appropriate)</p>
<p>Also, during the time you see the splash-screen, is your hard drive busy? And how big are your collections (how many files and of what type)?</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20296"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/661#comment-20296" class="active">Confirmed</a></h3>    <div class="submitted">Submitted by Bruce Dillahunty (not verified) on Thu, 2012-07-19 19:00.</div>
    <div class="content">
     <p>Resetting my settings didn't help, but as soon as I unmounted my USB hard drive (used for backups), Digikam started right up.</p>
<p>At least once in the status bar at the top of the splash screen (where it shows "loading main view..." etc., the last thing I saw referred to mounting USB devices/cameras (sorry, didn't get the exact text and it disappears rapidly.</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-20298"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/661#comment-20298" class="active">Apparently the same issue</a></h3>    <div class="submitted">Submitted by T Middleton (not verified) on Sat, 2012-07-21 20:22.</div>
    <div class="content">
     <p>Apparently the same issue with SD Cards (which could be a USB device under the covers I suppose?). I've had an SD card plugged into my laptop, which I forgot about. Digikam 2.6 on windows never worked. After reading this I recalled the SD card and removed it... and digikam started up.</p>
<p>A huge relief! I hate being stuck on this windows machine without digikam. (Even with it's odd windows quirks.)</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-20300"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/661#comment-20300" class="active">confirm</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Sun, 2012-07-22 14:42.</div>
    <div class="content">
     <p>I can confirm that it only runs when no usb harddrive is mounted. With harddrive mounted, it does not start beyond the splash screen resp. crashes and shuts down.</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-20315"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/661#comment-20315" class="active">Thank you and bug report</a></h3>    <div class="submitted">Submitted by Ananta Palani on Sun, 2012-08-05 22:11.</div>
    <div class="content">
     <p>Thank you for taking the time to report this. There is a <a href="https://bugs.kde.org/show_bug.cgi?id=303850">bug report</a> related to this now if you would take the time to add any missing details that would be very helpful.</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div></div><a id="comment-20289"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/661#comment-20289" class="active">Please elaborate</a></h3>    <div class="submitted">Submitted by Ananta Palani on Wed, 2012-07-18 00:07.</div>
    <div class="content">
     <p>What do you mean, like a BSOD that crashed your computer? And are you talking about actually installing the software, or the first time your run digiKam? I realize you said 'installing' but just want to make sure. How much hard drive space did you have at the time you were trying to install?</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-20290"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/661#comment-20290" class="active">digiKam 2.7.0 for Windows is now released</a></h3>    <div class="submitted">Submitted by Ananta Palani on Wed, 2012-07-18 00:12.</div>
    <div class="content">
     <p>I just released digiKam 2.7.0 for Windows <a href="http://sourceforge.net/projects/digikam/files/digikam/2.7.0/digiKam-installer-2.7.0-win32.exe/download">here</a>. It uses the same build environment as 2.6.0, so if you are having trouble starting digiKam there is a good chance you won't be able to start it with this version either. But if you don't mind spending the time, please give it a try.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20291"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/661#comment-20291" class="active">This is an amazing news.
I</a></h3>    <div class="submitted">Submitted by <a href="http://www.morolandia.wordpress.com" rel="nofollow">furlan</a> (not verified) on Wed, 2012-07-18 10:35.</div>
    <div class="content">
     <p>This is an amazing news.<br>
I switched from linux to windows some times ago and the program that i missing was digikam (indeed another was kmymoney).<br>
I appreciate it.<br>
Thank you.</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-20292"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/661#comment-20292" class="active">Unfortunately, I realized the</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Wed, 2012-07-18 11:51.</div>
    <div class="content">
     <p>Unfortunately, I realized the same troubles mentioned before with digikam 2.7.0 under Windows 7. Are these difficulties to be expected with Windows Vista as well? On my Windows Vista computer, digikam 2.5.0 is running without any troubles.</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-20294"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/661#comment-20294" class="active">2.7 windows</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Wed, 2012-07-18 16:35.</div>
    <div class="content">
     <p>Installed 2.7 for windows . no USB device hooked up . Start Digikam and it finds my pic path<br>
C:\Documents and Settings\Ed\My Documents\My Pictures\ (saids it was not in C:\ ).<br>
click ok and it starts and loads fine .</p>
<p>Issue it seems to find all pics up to 2012 , as soon at it looks in a folder in 2012 an up it seems empty no pictures thumbs load even after refresh .</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20295"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/661#comment-20295" class="active">ok, rebooted system and ran</a></h3>    <div class="submitted">Submitted by Ed (not verified) on Wed, 2012-07-18 17:41.</div>
    <div class="content">
     <p>ok, rebooted system and ran maintenance under tools (clicked all options except face detection and now it found/see's other pics in my newer folders in the album</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-20299"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/661#comment-20299" class="active">Doesn't open</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Sat, 2012-07-21 22:48.</div>
    <div class="content">
     <p>Nope, it doesn't open - WinXP. The splash screen comes and goes, and that's it.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20313"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/661#comment-20313" class="active">USB or MySQL</a></h3>    <div class="submitted">Submitted by Ananta Palani on Sun, 2012-08-05 22:03.</div>
    <div class="content">
     <p>Are you using a USB hard drive or do you have any SD Cards plugged in (in other words, any external drive of any kind)? Or are you using MySQL for your database?</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-20320"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/661#comment-20320" class="active">no love for network drive on windows 7</a></h3>    <div class="submitted">Submitted by wayne (not verified) on Thu, 2012-08-09 07:01.</div>
    <div class="content">
     <p>I have terabytes of raw images on a samba server (raid mirror, I'd hate to lose anything).  I tried setting up a mysql server for the database, and it works but take literally ten to twenty minutes for digikam to start.  Thats with scanning turned off.</p>
<p>I tried using a local database but it doesn't seem to work.</p>
<p>Has anyone else got any experience with this issue?</p>
<p>My wife loves digikam, and needs to use windows on her laptop.  So keeping 2TB of raw images on there is out.  However I can't think of a better way to do this than Digikam if I can get it to work properly.</p>
<p>For now I have her set up using X11 forwarding over ssh, to run Digikam on the Debian server.  It's a little slow as you can imagine.</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div><a id="comment-20293"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/661#comment-20293" class="active">Windows and themes</a></h3>    <div class="submitted">Submitted by <a href="http://www.morolandia.wordpress.com" rel="nofollow">furlan</a> (not verified) on Wed, 2012-07-18 13:35.</div>
    <div class="content">
     <p>There is any procedure to change the used theme on Windows(7)?</p>
<p>Thanks</p>
<p>Gianluca</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-20297"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/661#comment-20297" class="active">Windows7 64 issues a plenty</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Sat, 2012-07-21 09:52.</div>
    <div class="content">
     <p>I am an avid Linux user but my photography machine is windows 7 based... so, this is comment is related to the Windows platform specifically: I have so looked forward to this release but for windows 7 64 bit it's just an frustrating bug-ville :( nothings stable, RAW won't convert, cannot open any image it fails, edits sometimes but then can't save as it just crashes. I can see that it holds so much promise (sigh), but sadly for windows users running 64 its a fine box of chocolates on the outside, but with half eaten chocolates inside, which is such a shame.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20310"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/661#comment-20310" class="active">Windows 64 issues</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Sat, 2012-08-04 16:06.</div>
    <div class="content">
     <p>I didn't even make it as far as you.  I get the message "Error while opening the database" even after an unistall/reinstall.  I'm in the same boat - multiple computers (Window 7-64, Linux Mint, Windows XP) - hardwired network at home and the desire to have a shared program.  Sadly it looks like Digikam still isn't ready for prime time despite all the hoopla.  If it can't even work "out of the box" it's hard to place your trust in it - especially if you have to do it cross-platform.  </p>
<p>What a wonderful concept - I keep checking back and hoping, but at times it seems like the Linux community just doesn't get it. Anyone who would say that opening a terminal and typing Sudo...Anything is easier (or makes more sense) than hitting an install button is out of touch with reality and the modern world.</p>
<p>And the Captcha on this page is just plain stupid and barely readable!!!  On my 4th attempt!!!</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20312"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/661#comment-20312" class="active">Internal or MySQL Database</a></h3>    <div class="submitted">Submitted by Ananta Palani on Sun, 2012-08-05 22:00.</div>
    <div class="content">
     <p>Are either of you using an external MySQL database? Did you have trouble with any version prior to 2.6.0/2.7.0?</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20319"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/661#comment-20319" class="active">Windows 7 64 Bit</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Tue, 2012-08-07 15:46.</div>
    <div class="content">
     <p>This is the first install on the Windows 7 PC.  I have tested in on an XP machine before, but was concerned about stability.  </p>
<p>And yes I have used MySQL for a WordPress website - I was using xampp on a USB drive which was not inserted and it may be installed on the PC.  After the error, I was able to go into properties and add my image library which seemed to end the error message.  However the image sub-folders do not open and there are no thumbnail anywhere.  I can't open or see anything.   The bottom line is that I'll just continue using my Canon software strictly on the Windows 7 PC which is fast and stable.  I really don't want to put endless hours into this only to find that I wasted my time because of crashes, etc. and wind up back at square 1.</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div></div><a id="comment-20302"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/661#comment-20302" class="active">Automatic grouping?</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Tue, 2012-07-24 10:06.</div>
    <div class="content">
     <p>How is the automatic grouping of JPG and RAW files coming along? I'm *really* looking forward to that feature!</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-20305"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/661#comment-20305" class="active">no Thumbnails visible after adding collection</a></h3>    <div class="submitted">Submitted by Wolfgang (not verified) on Wed, 2012-07-25 20:01.</div>
    <div class="content">
     <p>Hello,</p>
<p>I have installed Digikam 2.7 under Windows XP.<br>
I can Import some Folders as Collections to Digikam and the Thumbnails ar visible.<br>
When i am add a new Collection to digikam Albums, after the Import in all Albums ar no Thumbnails visible.<br>
I can open all Folders in all Albums, but no Thumbnails ar visible.<br>
The Function Rebuild Thumbnails has also no Effect.<br>
This Effect was also uder Digikam 2.5 and Digikam 2.6.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20314"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/661#comment-20314" class="active">Thank you</a></h3>    <div class="submitted">Submitted by Ananta Palani on Sun, 2012-08-05 22:05.</div>
    <div class="content">
     <p>Thanks for taking the time to report this. How many images are in your collection (the one you try to add)? Do you have video files in your collection as well?</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-20317"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/661#comment-20317" class="active">no Thumbnails visible after adding collection</a></h3>    <div class="submitted">Submitted by Wolfgang (not verified) on Mon, 2012-08-06 20:00.</div>
    <div class="content">
     <p>At the first Time i have importet folders with 700, 798, 1420, 587, 4203, 2869 Photos as Albums.<br>
After import the next Folder with 5115 Photos, in aal Albums the Windows with the Thumbnails ar empty.<br>
I am only import Pictures, no Videos.<br>
Is possible to test the Import of the Thumbnails from the commandline to become some helpful Back messages?</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div><a id="comment-20311"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/661#comment-20311" class="active">installation on Mac OSX via Macports</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Sat, 2012-08-04 21:29.</div>
    <div class="content">
     <p>Hello,</p>
<p>I installed DK via macports, following the directions here:</p>
<p>https://projects.kde.org/projects/extragear/graphics/digikam/digikam-software-compilation/repository/revisions/master/entry/README.MACOSX</p>
<p>then I start it up and it gets through checking the ICC repository, then goes onto loading the kipi plugins, at which point it crashes. The crash automatically brings up the bug reporting interface but when I go through the bug reporting steps, it get's hung up doing the backtrace. After waiting for a long time to see if it would actually make it through the backtrace, I finally forced it to quit. </p>
<p>Any ideas on how to complete the backtrace or to get more info out of the backtrace to find where it's failing?</p>
<p>thanks!</p>
         </div>
    <div class="links">» </div>
  </div>

</div>