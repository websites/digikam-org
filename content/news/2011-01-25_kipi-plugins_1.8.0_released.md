---
date: "2011-01-25T09:23:00Z"
title: "kipi-plugins 1.8.0 released..."
author: "digiKam"
description: "Dear all digiKam fans and users! digiKam team is proud to announce Kipi-plugins 1.8.0 ! kipi-plugins tarball can be downloaded from SourceForge at this url"
category: "news"
aliases: "/node/568"

---

<p>Dear all digiKam fans and users!</p>

<p>digiKam team is proud to announce Kipi-plugins 1.8.0 !</p>

<a href="http://www.flickr.com/photos/digikam/5386516261/" title="digiKam1.8.0 by digiKam team, on Flickr"><img src="http://farm6.static.flickr.com/5214/5386516261_8cf2ce2c99.jpg" width="500" height="200" alt="digiKam1.8.0"></a>

<p>kipi-plugins tarball can be downloaded from SourceForge <a href="http://sourceforge.net/projects/kipi/files">at this url</a></p>

<p>See below the list of new features and bugs-fix coming with this release.</p>

<p>Enjoy digiKam.</p>

<h5>BUGFIXES FROM KDE BUGZILLA:</h5><br>

001 ==&gt; 261275 : General           : [PATCH] kipi-plugins 1.6.0 fails to build (final).<br>
002 ==&gt; 253257 : SmugExport        : Program uploads only part of file or smaller version of file and match error occurs.<br>
003 ==&gt; 257514 : PrintImages       : Scaling of Pictures for print.<br>
004 ==&gt; 196855 : GPSSync           : Remember last postition, remember last view mode, cache the last view.<br>
005 ==&gt; 262125 : DngConverter      : DNG Conversion can't be inverted. Embedded file is corrupted.<br>
006 ==&gt; 245956 : PicasaWeb         : Upload video from Digikam to Picasa.<br>
007 ==&gt; 262642 : AdvancedSlideshow : Better looking of "captions" during slideshow [patch].<br>
008 ==&gt; 263545 : PicasaWeb         : Crashed, when trying to import photo from picasaweb.<br>


<div class="legacy-comments">

  <a id="comment-19788"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/568#comment-19788" class="active">005 ==&gt; 262125 : DngConverter</a></h3>    <div class="submitted">Submitted by RPG_Master (not verified) on Tue, 2011-01-25 22:21.</div>
    <div class="content">
     <p>005 ==&gt; 262125 : DngConverter : DNG Conversion can't be inverted. Embedded file is corrupted.</p>
<p>FINALLY!!!<br>
Also, I REALLY hope I still have all my originals hanging around...</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-19789"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/568#comment-19789" class="active">export flickr</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Wed, 2011-01-26 16:22.</div>
    <div class="content">
     <p>the option of setting jpeg compress level is strange. will files always be recompressed? so is impossible to upload original files without changes? what is on png files?</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-19793"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/568#comment-19793" class="active">I would like to see an</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Thu, 2011-01-27 00:04.</div>
    <div class="content">
     <p>I would like to see an option, to auto generate a link and send it to klipper when I upload a picture using, for example picasa.<br>
The main usage for me is to quickly show someone my desktop, meaning take a ksnapshot, uplaod it somewhere and give them the link. That would mean one less action for me :)</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-19794"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/568#comment-19794" class="active">Gallery2 Export</a></h3>    <div class="submitted">Submitted by Sunil Khiatani (not verified) on Thu, 2011-01-27 03:46.</div>
    <div class="content">
     <p>Any chance of updating the gallery export plugin.  It's very basic and a pain to use compared to the Facebook or PicasaWeb export plugins.</p>
         </div>
    <div class="links">» </div>
  </div>

</div>
