---
date: "2011-03-22T09:58:00Z"
title: "New Features in digiKam 2.0: Color Labels and Picks"
author: "Dmitri Popov"
description: "Besides numerous improvements, digiKam 2.0 brings a handful of new features, including Color Labels and Picks. As the name suggests, the Color Labels feature allows"
category: "news"
aliases: "/node/587"

---

<p>Besides numerous improvements, digiKam 2.0 brings a handful of new features, including Color Labels and Picks. As the name suggests, the Color Labels feature allows you to assign color codes to your photos. <a href="http://scribblesandsnaps.wordpress.com/2011/03/22/new-features-in-digikam-2-0-color-labels-and-picks/">Continue to read</a></p>

<div class="legacy-comments">

</div>