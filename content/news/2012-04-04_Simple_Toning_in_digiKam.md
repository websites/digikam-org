---
date: "2012-04-04T08:39:00Z"
title: "Simple Toning in digiKam"
author: "Dmitri Popov"
description: "Adding a dash of color tint to a black and white photo can often produce a dramatic effect, and digiKam does have a handful of"
category: "news"
aliases: "/node/651"

---

<p>Adding a dash of color tint to a black and white photo can often produce a dramatic effect, and digiKam does have a handful of toning filters for you to try.</p>
<p><a href="http://scribblesandsnaps.files.wordpress.com/2012/02/digikam_colortoning.png"><img class="alignnone size-medium wp-image-2274" title="digikam_colortoning" src="http://scribblesandsnaps.files.wordpress.com/2012/02/digikam_colortoning.png?w=500" alt="" width="500" height="393"></a></p>
<p>But you are not limited to the built-in filters: using digiKam's editing tools, you can easily tint photos using whatever color you like.</p>
<p><a href="http://scribblesandsnaps.wordpress.com/2012/04/04/simple-toning-in-digikam/">Continue to read</a></p>

<div class="legacy-comments">

</div>