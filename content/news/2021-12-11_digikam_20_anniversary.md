---
date: "2021-12-11T00:00:00"
title: "Celebrating 20 Years of digiKam"
author: "digiKam Team"
description: "digiKam have been published 2 decades ago"
category: "news"
---

[![](https://i.imgur.com/WZ1zMzQ.png "digiKam 7.4.0 running under Apple macOS")](https://imgur.com/WZ1zMzQ)

20 years ago we built digiKam to give you an open-source choice. This [first version published on Christmas 2001](https://www.digikam.org/documentation/changelog/)
was a simple Gphoto2 graphic interface for Linux. 2 decades later, the application become a main Photo Management Program, working on Linux, Windows,
and macOS with plenty of important features to view, edit, search, publish your image collections.

20 years in, you know where we stand. digiKam is a non-profit, global community, and our values are clear:
choice and control for our users, openness and innovation for the Libre Photo that has become so central to our lives.
On values, all photo management programs are not created equal, and your choices make a big difference.
On our 20th birthday, we want to thank [all contributors](https://invent.kde.org/graphics/digikam/-/raw/master/AUTHORS),
and all users for your support and your trust. When you use digiKam, you make our voice louder;
you become part of a community building our shared values into the fabric of this application. We couldn’t do it without you.

You can support digiKam project with a generous donation for this celebrating moment following instructions [from the donation page](https://www.digikam.org/donate/).

Thanks in advance.

digiKam team.