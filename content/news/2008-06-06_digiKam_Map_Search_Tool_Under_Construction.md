---
date: "2008-06-06T07:22:00Z"
title: "digiKam Map Search Tool Under Construction"
author: "digiKam"
description: "After to have improved the geo-location view from right sidebar in digiKam for KDE4, following this blog entry, the story continue with a new powerfull"
category: "news"
aliases: "/node/318"

---

<p>After to have improved the geo-location view from right sidebar in digiKam for KDE4, following <a href="http://www.digikam.org/drupal/node/306">this blog entry</a>, the story continue with a new powerfull search tool based on <a href="http://edu.kde.org/marble">Marble Widget</a> to be able to find pictures using a map.</p>

<p>The new digiKam search backend implemented by Marcel is already able to perform a search based on a rectangle area defined by GPS positions. Now, we need a suitable graphical interface for end users.</p>

<p>Following this <a href="http://bugs.kde.org/show_bug.cgi?id=153070">bug entry</a> KDE bugzilla, i'm in contact with Marble team to implement a selection tool for the next release. Implementation is not yet published on subversion, but it start to work like you can see on this video:</p>

<object width="500" height="404"><param name="movie" value="http://www.youtube.com/v/2zuByR9IGIE&amp;hl=fr"><embed src="http://www.youtube.com/v/2zuByR9IGIE&amp;hl=fr" type="application/x-shockwave-flash" width="500" height="404"></object>

<div class="legacy-comments">

  <a id="comment-17557"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/318#comment-17557" class="active">OMG WOW O_O!</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Fri, 2008-06-06 12:25.</div>
    <div class="content">
     <p>Just when I thought it couldn't be awesomer<br>
*shed's a tear of joy*</p>
<p>PS:your captcha's font is microscopic.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-17558"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/318#comment-17558" class="active">That looks great</a></h3>    <div class="submitted">Submitted by <a href="http://blog.cryos.net/" rel="nofollow">Marcus D. Hanwell</a> (not verified) on Fri, 2008-06-06 15:39.</div>
    <div class="content">
     <p>That really does look great and I can't wait to try it out. One question though - have you thought about<br>
time synchronisation? In the OSM community they would often recommend taking a picture of your GPS with<br>
your camera when it is showing the time, this allows any time drift to be easily compensated. I have this<br>
unfortunate problem with some images I tagged - I think there is roughly a 20 minute time difference<br>
between camera and GPS.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-17559"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/318#comment-17559" class="active">&gt;That really does look great</a></h3>    <div class="submitted">Submitted by digiKam on Sun, 2008-06-08 19:58.</div>
    <div class="content">
     <p>This is not a problem under digiKam. You can adjust time of your images in batch or synchronize your pictures with your GPS device using a GPX trace. There are 2 kipi-plugins dedicated for that.</p>
<p>digiKam</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-17560"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/318#comment-17560" class="active">Another interesting function</a></h3>    <div class="submitted">Submitted by Peer (not verified) on Mon, 2008-06-09 10:26.</div>
    <div class="content">
     <p>Another interesting function would be an auto-adjust of the timeline with respect to the picture shot times.</p>
<p>Such an option could work like this:</p>
<p>When taking a picture, you generally (or let's say: "at least often") don't move around but stand still, because you stop to walk/get of your bicycle/have a short break with your car (don't know for airplanes though) in order to get your photo.</p>
<p>So when examining a GPS trace, there should be definite speed minima (zero speed) linked to GPS log entries with a non-moving position which should somehow conincide with the times of the photo shots. These might be useable for automatic or semi-automatic (e.g. manually choosing a fixedpoint-photo pair) timeline-correction.</p>
<p>If this is not possible automatically, it might be useful to display a speed-time trace below a map. This trace should be sizeable and movable (like an audio file's representation in audacity) to select a region in a manageable way. Then you could use the mouse pointer on this speed-time-trace and have a point/circle/cross move around according to the GPS trance in the map picture. Thus you could easily reference the map with respect to the photo, and then manually correct the time differences and link a photo to a trace point (or even some interpolated trace point).</p>
<p>Is it clear what I described? (Otherwise contact me by mail for details.)</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-18233"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/318#comment-18233" class="active">digikam</a></h3>    <div class="submitted">Submitted by john (not verified) on Tue, 2009-02-10 19:04.</div>
    <div class="content">
     <p>The map search tool looks awesome. It is going to be a great alternate to Google Earth for me. Hopefully the tool works out well.</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-17579"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/318#comment-17579" class="active">I have a friend who takes</a></h3>    <div class="submitted">Submitted by mxttie (not verified) on Wed, 2008-06-11 13:03.</div>
    <div class="content">
     <p>I have a friend who takes loads of pictures from special flowers, all kinda in the same area. Sometimes when he looks for a photo, he doesn't remember the date, but he does know the exact place where he took it. This feature sure could come in handy for him.. :)</p>
<p>I'm amazed by the truly innovating features that get developed within digikam!</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-17581"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/318#comment-17581" class="active">Ceeeeeeeeeeeeewl !!!</a></h3>    <div class="submitted">Submitted by yellowshark (not verified) on Thu, 2008-06-12 08:10.</div>
    <div class="content">
     <p>This is what I was talking about to friends since ages! Can't wait for it. Showing a pic on Google Earth is fun. Filtering pictures by location is a killer!</p>
<p>Next steps:<br>
* Not a rectangle but a free polygon for selection<br>
* Existing vector features for selection: All pics less than 500 meters from the banks along the river rhine. All pics in the area of Texas. ...<br>
* All pics shot at sundown at Hawai (From time and position you can derive the sun set time)<br>
* etc pp</p>
<p>Thanks<br>
Waiting for it to be on my system!</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-17619"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/318#comment-17619" class="active">Demo Material</a></h3>    <div class="submitted">Submitted by <a href="http://www.gps-kamera.eu" rel="nofollow">yellowshark</a> (not verified) on Tue, 2008-07-01 11:32.</div>
    <div class="content">
     <p>Hello,</p>
<p>can you provide me more demo material about "map search" I would like to place it as an example for the future of GPS-Photo - geotagging on our website www.gps-kamera.eu. Just to show people why they should start to geotag their pictures now.</p>
<p>I can't wait for it to come to the ubuntu-packages!!</p>
<p>I would enjoy to keep contact with this project!</p>
<p>Cheers</p>
<p>Seb</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-17921"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/318#comment-17921" class="active">This is what I was talking</a></h3>    <div class="submitted">Submitted by red (not verified) on Sat, 2008-11-01 11:20.</div>
    <div class="content">
     <p>This is what I was talking about to friends since ages! Can't wait for it. Showing a pic on Google Earth is fun. Filtering pictures by location is a killer!</p>
<p>Next steps:<br>
* Not a rectangle but a free polygon for selection<br>
* Existing vector features for selection: All pics less than 500 meters from the banks along the river rhine. All pics in the area of Texas. ...<br>
* All pics shot at sundown at Hawai (From time and position you can derive the sun set time)<br>
* etc pp<br>
 <a href="http://www.trsohbet.name">sohbet</a><br>
Thanks<br>
Waiting for it to be on my system!</p>
         </div>
    <div class="links">» </div>
  </div>

</div>
