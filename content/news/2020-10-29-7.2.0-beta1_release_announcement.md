---
date: "2020-10-29T00:00:00"
title: "digiKam 7.2.0-beta1 is released"
author: "digiKam Team"
description: "Dear digiKam fans and users, we are proud to announce the first beta of digiKam 7.2.0."
category: "news"
---

[![](https://i.imgur.com/X2FB6sjh.png "digikam 7.2.0-beta1")](https://imgur.com/X2FB6sj)

Dear digiKam fans and users,

Just a few words to inform the community that 7.2.0-beta1 is out and ready to test one month later
the [7.1.0 stable release](https://www.digikam.org/news/2020-09-06-7.1.0_release_announcement/).

After a long time to integrate the student codes working on faces management while this summer, we are now ready to propose a first beta with
the the new improvements planned since a very long time about the usability and the performances of faces tagging, faces detection, and faces
recognition, already presented in July with [7.0.0 release announcement](https://www.digikam.org/news/2020-07-19-7.0.0_release_announcement/).

We also fight against plenty of bugs, and after a long triaging stage, this new version come with
[more than 140 bug-fixes since last stable release 7.1.0](https://bugs.kde.org/buglist.cgi?f1=cf_versionfixedin&limit=0&o1=equals&order=bug_id&product=digikam&v1=7.2.0)
and look very promising. Nothing is completed yet, as we plan one more beta version before Christmas, when we will publish officially the [stable version](https://www.digikam.org/documentation/releaseplan/).
It still bugs to fix while this beta campaign and all help will be welcome from the community to stabilize codes.

Thanks to all users for [your support and donations](https://www.digikam.org/donate/),
and to all contributors, students, testers who allowed us to improve this release.

digiKam 7.2.0-beta1 source code tarball, Linux 32/64 bits AppImage bundles, MacOS
package and Windows 32/64 bits installers can be downloaded from [this repository](https://download.kde.org/unstable/digikam/).
Don't forget to not use this beta in production yet and thanks in advance for your feedback in Bugzilla.

Rendez-vous in a few weeks for the next stage.

Thanks in advance for your help.
