---
date: "2015-01-02T09:53:00Z"
title: "digiKam Recipes 4.3.1 Released"
author: "Dmitri Popov"
description: "I ring in the new year with a digiKam Recipes update. This version features two new recipes: Remove Keywords from Photos and Add Web Interface"
category: "news"
aliases: "/node/727"

---

<p>I ring in the new year with&nbsp;a&nbsp;<a href="http://dmpop.dhcp.io/#!digikamrecipes.md">digiKam Recipes</a>&nbsp;update. This version features&nbsp;two new recipes:&nbsp;<em>Remove Keywords from Photos</em> and&nbsp;<em>Add Web Interface to digiKam with digiKamWebUi</em>. In addition to that, the updated and expanded&nbsp;<em>Deal with Bugs in digiKam</em>&nbsp;recipe now explains how to generate backtraces.</p>
<p><a href="http://dmpop.dhcp.io/#!digikamrecipes.md"><img src="https://scribblesandsnaps.files.wordpress.com/2015/01/digikamrecipes-4-3-1.png" alt="digikamrecipes-4.3.1" width="375"></a></p>
<p><a href="http://scribblesandsnaps.com/2015/01/02/digikam-recipes-4-3-1-released/">Continue to read</a></p>

<div class="legacy-comments">

</div>