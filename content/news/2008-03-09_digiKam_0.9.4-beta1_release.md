---
date: "2008-03-09T11:32:00Z"
title: "digiKam 0.9.4-beta1 release"
author: "gerhard"
description: "Dear all digiKam fans and users! The digiKam development team is happy to release 0.9.4-beta1. The digiKam tarball can be downloaded from SourceForge as well."
category: "news"
aliases: "/node/303"

---

<p>Dear all digiKam fans and users!</p>
<p>The digiKam development team is happy to release 0.9.4-beta1. The digiKam tarball can be downloaded from <a href="http://sourceforge.net/project/showfiles.php?group_id=42641">SourceForge</a> as well.</p>
<p><strong>Note!</strong> This release works only with the latest libkdcraw and libkexiv2. You either have to compile them freshly from svn or wait a few days until the tarballs become available.</p>
<p>NEW FEATURES:</p>
<p>General   :<br>
1. Color theme scheme are pervasively applied in graphical interfaces, giving digiKam a real <a href="http://www.digikam.org/?q=node/302">Pro-look</a> when dark schemes are selected.<br>
1a. Color theme scheme can be changed from either Editor or LightTable.<br>
2. Updated internal CImg library to last stable 1.2.7 (released on 2008/01/23).<br>
3. Add capability to display <a href="http://www.digikam.org/?q=node/283">items count</a> in all Album, Date, Tags, and Tags Filter folder views. The number of items contained in virtual or physical albums can be displayed next to its name. If a tree branch is collapsed, parent views sum-up the number of items from all undisplayed children views. Items count is performed in background by digiKam KIO-Slaves.<br>
A new option from Setup/Album dialog page can toggle on/off this feature.</p>
<p>AlbumGUI   :<br>
4. Add a new tool to perform Date search around whole albums collection: <a href="http://www.digikam.org/?q=node/293">Time-Line</a>.<br>
Timeline is a new left sidebar tab. It's a great tool complementary to the calendar. Try it out!<br>
5. In Calendar View, selecting Year album shows all images of the full year.<br>
6. New <a href="http://www.digikam.org/?q=node/296">status-bar indicator</a> to report album icon view filtering status.</p>
<p>digiKam BUGFIXES FROM KDE BUGZILLA (alias B.K.O | http://bugs.kde.org):</p>
<p>001 ==&gt;  96388 : Show number of images in the album.<br>
002 ==&gt; 155271 : Configure suggests wrong parameter for libjasper.<br>
003 ==&gt; 155105 : Broken png image present in albums folder causes digikam to crash when starting.<br>
004 ==&gt; 146760 : Providing a Timeline-View for quickly narrowing down the date of photos.<br>
005 ==&gt; 146635 : Ratio crop doesn't remember orientation.<br>
006 ==&gt; 144337 : There should be no "empty folders".<br>
007 ==&gt; 128293 : Aspect ratio crop does not respect aspect ratio.<br>
008 ==&gt; 157149 : digiKam crash at startup.<br>
009 ==&gt; 141037 : Search using Tag Name does not work<br>
010 ==&gt; 158174 : Precise aspect ratio crop feature<br>
011 ==&gt; 142055 : Which whitebalance is used<br>
012 ==&gt; 158558 : Delete Function in Tag Filters panel needs to make sure that the tag is unselected when the tag is deleted.<br>
013 ==&gt; 120309 : Change screen backgroundcolor of image tools.<br>
014 ==&gt; 153775 : Download from camera: Renaming because of already existing file does not work.<br>
015 ==&gt; 154346 : Can't rename in digiKam.<br>
016 ==&gt; 154625 : Image-files are not renamed before they are saved to disk.<br>
017 ==&gt; 154746 : Album selection window's size is wrong.</p>

<div class="legacy-comments">

</div>