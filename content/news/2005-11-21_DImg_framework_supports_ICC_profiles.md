---
date: "2005-11-21T18:05:00Z"
title: "DImg framework supports ICC profiles"
author: "site-admin"
description: "The new digikam image library DImg now supports ICC profiles using littlecms. littlecms is an open source color management engine which is used by krita"
category: "news"
aliases: "/node/41"

---

The new digikam image library DImg now supports <a href="http://www.color.org/">ICC profiles</a>
using <a href="http://www.littlecms.com">littlecms</a>. littlecms is
an open source color management engine which is used by <a href="http://koffice.kde.org/krita/">krita</a> as well and installed by
default on most linux distributions. It was added by the new digiKam contributor Paco.
<p></p>
We will add a dedicated ICC profile setup page to the configuration dialog and a new
image plugin to apply ICC profiles manually later.
<p></p>
It is necessary to use the ICC profiles with 16 bit RAW files. Dcraw doesn't
provide automatic color correction, so the RAW image will appear black in the image editor
if no ICC profile is used.
<p></p>
It is currently not possible using ICC profiles coming from RAW files.
<p></p>
Compare the images loaded <a href="/?q=system/files&amp;file=withoutICC.png" alt="without ICC">without ICC</a> and <a href="/?q=system/files&amp;file=withICC.png" alt="with ICC">with ICC</a>.
<div class="legacy-comments">

</div>