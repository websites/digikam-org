---
date: "2015-09-07T09:31:00Z"
title: "digiKam Software Collection 4.13.0 released..."
author: "digiKam"
description: "Dear digiKam fans and users, The digiKam Team is proud to announce the release of digiKam Software Collection 4.13.0. This release is the result of"
category: "news"
aliases: "/node/743"

---

<a href="https://www.flickr.com/photos/digikam/21020168858/in/photostream/" title="digiKam-4.13.0"><img src="https://farm6.staticflickr.com/5667/21020168858_497c6ab6df_c.jpg" width="800" height="225" alt="digiKam-4.13.0"></a>

<p>Dear digiKam fans and users,</p>

<p>
The digiKam Team is proud to announce the release of digiKam Software Collection 4.13.0. 
This release is the result of another huge bugs triage on KDE bugzilla where more than 30 files have been closed.
Thanks to <a href="https://plus.google.com/u/0/107171232114475191915/about">Maik Qualmann</a> who maintain KDE4 version while 
<a href="https://techbase.kde.org/Projects/Digikam/CodingSprint2014#KDE_Framework_Port">KF5 port</a> and <a href="https://community.kde.org/GSoC/2015/Ideas#digiKam">GSoC 2015 projects</a> are in progress. Both are planed to be completed while sprint 2016.
</p>

<p>
As with 4.12.0, this new release comes with OSX PKG installers for Maverick, and Yosemite. Note that you must considerate digiKam under OSX as beta stage, not stable as under Linux. Some dysfunctions have been identified and fixed with this release.
PKG installer includes last 
<a href="http://www.exiv2.org/changelog.html">Exiv2 0.25</a>, 
<a href="http://www.libraw.org/download#stable">LibRaw 0.17.0</a>, 
and <a href="http://lensfun.sourceforge.net/changelog/2015/05/10/Release-0.3.1-Changelog/">LensFun 0.3.1</a> 
which do not exist yet in Macports repository. Exiv2 includes huge fixes about video support which can crash digiKam, and Libraw/Lensfun add new recent camera supports. In the future, we plan to include Hugin CLI tools too required at run-time by Panorama and ExpoBlending tools.
</p>

<p>
The OSX digiKam PKG is not an Apple signed bundle installer. To do it we must pay 99$ by year which is very expensive for an Open Source project. If your computer is protected by Apple GateKeeper, you can bypass safety this protection at install time without to touch your computer settings. Just follow simple instructions given <a href="http://www.bu.edu/infosec/howtos/bypass-gatekeeper-safely/">in this tutorial</a>.
</p>

<p>
For more details, you can consult the huge list of the <a href="https://bugs.kde.org/buglist.cgi?f1=cf_versionfixedin&amp;o1=equals&amp;query_format=advanced&amp;bug_status=RESOLVED&amp;bug_status=NEEDSINFO&amp;bug_status=VERIFIED&amp;bug_status=CLOSED&amp;v1=4.13.0&amp;product=digikam&amp;product=digikamimageplugins&amp;product=kipiplugins&amp;product=showfoto">issues closed</a> in digiKam 4.13.0 available through the KDE Bugs-tracking System.
</p>

<p>The digiKam software collection can be downloaded from the <a href="http://download.kde.org/stable/digikam/">KDE mirrors</a>.
</p>

<p>Have fun to use this new important digiKam stable release.</p>

<p>digiKam Team...</p>
<div class="legacy-comments">

  <a id="comment-21097"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/743#comment-21097" class="active">Generic plugin</a></h3>    <div class="submitted">Submitted by Christoph (not verified) on Wed, 2015-09-16 23:21.</div>
    <div class="content">
     <p>Is there a generic (KIPI) plugin or a digikam batch module, which just invokes any given UNIX command with given arguments (preferably also with options like in the batch queue rename settings, e.g. [dir], [file])?</p>
<p>I would like to call e.g. gimp for FFT processing of a set of images or other non-digikam processing of images.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-21098"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/743#comment-21098" class="active">digiKam BQM</a></h3>    <div class="submitted">Submitted by digiKam on Thu, 2015-09-17 06:46.</div>
    <div class="content">
     <p>The BQM (Batch Queue Manager) inside digiKam core (not Kipi) do not yet support to call an external tool, but somebody post a patch in this way and we must review this code for a future integration. Look here for details :</p>
<p>https://bugs.kde.org/show_bug.cgi?id=220402</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-21099"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/743#comment-21099" class="active">Digikam starts memory run on Ubuntu 14.04</a></h3>    <div class="submitted">Submitted by Matt (not verified) on Tue, 2015-09-22 09:39.</div>
    <div class="content">
     <p>I am having trouble to run digikam on stock Ubuntu 14.04. I tried different approaches to install and run it using different ppa combinations. I installed it with the philip5 ppa. originally only with the extras ppa, then both ppas, then with the kubuntu backports ppa. Uninstalled all, tried different combinations etc... I always get the same problem. Digikam starts fine and seems to work normal, but in the background a memory run starts and goes on until it crashes after 5 minutes or so. Towards the end I get three times a window that says: "Errors occurred while parsing the .pn2 file!". Then the application gets killed. Below is what I get in the terminal. Its the same with digikam 4:3.5 to 4:4.13.</p>
<p>Would be great if someone could help me to troubleshoot this. Best, Matthias</p>
<p><code><br>
Object::connect: No such signal org::freedesktop::UPower::DeviceAdded(QDBusObjectPath) Object::connect: No such signal org::freedesktop::UPower::DeviceRemoved(QDBusObjectPath) void DBusMenuExporterPrivate::fillLayoutItem(DBusMenuLayoutItem*, QMenu*, int, int, const QStringList&amp;): No id for action void DBusMenuExporterPrivate::fillLayoutItem(DBusMenuLayoutItem*, QMenu*, int, int, const QStringList&amp;): No id for action void ....<br>
...~30x<br>
DBusMenuExporterPrivate::fillLayoutItem(DBusMenuLayoutItem*, QMenu*, int, int, const QStringList&amp;): No id for action void DBusMenuExporterPrivate::fillLayoutItem(DBusMenuLayoutItem*, QMenu*, int, int, const QStringList&amp;): No id for action File Parsing error "Errors occurred while parsing the .pn2 file!" File Parsing error "Errors occurred while parsing the .pn2 file!" File Parsing error KCrash: Application 'digikam' crashing... QSocketNotifier: Invalid socket 25 and type 'Read', disabling... QSocketNotifier: Invalid socket 29 and type 'Read', disabling... QSocketNotifier: Invalid socket 76 and type 'Read', disabling... KCrash: Attempting to start /usr/lib/kde4/libexec/drkonqi from kdeinit KCrash: Connect sock_file=/home/masi/.kde/socket-masi-macbuntu/kdeinit4__0 digikam: Fatal IO error 9 (Bad file descriptor) on X server :0.Killed<br>
<code></code></code></p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-21100"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/743#comment-21100" class="active">Open a bug for that crash in</a></h3>    <div class="submitted">Submitted by philip5 (not verified) on Wed, 2015-09-23 22:07.</div>
    <div class="content">
     <p>Open a bug for that crash in digikam at https://bugs.kde.org and make a full bug report as described here: https://www.digikam.org/contrib</p>
<p>A full back trace as described in that link will give more information about your problem. It's  not unlikely that your problem is within exiv2 that digikam use and not really in digikam. If so you might be directed to report the crash to exiv2 that is the tool that digikam use to parse many files. If the bug is in exiv2 and there is a bug fix for it that's not too invasive then notify me so I can patch exiv2 on my PPA and update the package so you don't have to wait until april 2016 for the next official update of exiv2. :)</p>
<p>/Philip</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-21101"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/743#comment-21101" class="active">Hi thanks for your help. I</a></h3>    <div class="submitted">Submitted by Matt (not verified) on Sat, 2015-09-26 03:13.</div>
    <div class="content">
     <p>Hi thanks for your help. I was able to trouble shoot the problem with the back trace. It turned out to be a problem with some marble packages. I removed and reinstalled them and now it works like a charm. Thanks a lot! Seems to be great software!</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-21102"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/743#comment-21102" class="active">Good to know that it worked</a></h3>    <div class="submitted">Submitted by philip5 (not verified) on Sun, 2015-09-27 16:11.</div>
    <div class="content">
     <p>Good to know that it worked out for you. Have fun and happy digikaming! :)</p>
<p>/Philip</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div></div><a id="comment-21103"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/743#comment-21103" class="active">Macports still 4.9.0?</a></h3>    <div class="submitted">Submitted by Christoph (not verified) on Mon, 2015-09-28 17:20.</div>
    <div class="content">
     <p>Looks like the Macports version is still 4.9.0? The problem with this is, the compile fails at my end, apparently because Macport's opencv is version 3 but digikam 4.9.0 seems to require opencv v2. </p>
<p>Or is there a trick that I need to know in order to see 4.13.0 in the Macports repository? Perhaps some "enable beta channel" option? </p>
<p>(Downloading the .pkg from kde.org is not an option as the pkg is unsigned and the SHA/MD5/hashes are posted on the same insecure page as the link to the pkg.)</p>
<p>I love digikam on my little netbook and want to have it on my Mac too (larger screen, more CPU power :)</p>
<p>Thanks,<br>
Christoph</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-21104"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/743#comment-21104" class="active">There is nothing magic...</a></h3>    <div class="submitted">Submitted by digiKam on Mon, 2015-09-28 18:34.</div>
    <div class="content">
     <p>digiKam 4.9.0 to 4.13.0 still use OpenCV2. The way to build OSX PKG is to switch Macport opencv package to right version using subversion history.</p>
<p>digiKam 4.14.0 will support OpenCV 2 and 3.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-21105"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/743#comment-21105" class="active">Thanks for the quick</a></h3>    <div class="submitted">Submitted by Christoph (not verified) on Mon, 2015-09-28 18:40.</div>
    <div class="content">
     <p>Thanks for the quick feedback! I'll try to figure out this subversion history thingy - can't be too hard I guess :)</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-21106"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/743#comment-21106" class="active">But wait... even when I</a></h3>    <div class="submitted">Submitted by Christoph (not verified) on Mon, 2015-09-28 18:49.</div>
    <div class="content">
     <p>But wait... even when I change the opencv version, Macports still has digiKam 4.9.0 only...</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div></div><a id="comment-21107"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/743#comment-21107" class="active">Windows port</a></h3>    <div class="submitted">Submitted by Manu (not verified) on Sun, 2015-10-04 19:20.</div>
    <div class="content">
     <p>Hello, is there no windows port this time?</p>
<p>Thanks</p>
         </div>
    <div class="links">» </div>
  </div>

</div>