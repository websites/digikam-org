---
date: "2011-01-31T09:50:00Z"
title: "digiKam Software Collection 2.0.0 beta2 is out..."
author: "digiKam"
description: "Dear all digiKam fans and users! digiKam team is proud to announce the second digiKam Software Collection 2.0.0 beta release! With this release, digiKam include"
category: "news"
aliases: "/node/571"

---

<p>Dear all digiKam fans and users!</p>

<p>digiKam team is proud to announce the second digiKam Software Collection 2.0.0 beta release!</p>

<a href="http://www.flickr.com/photos/digikam/5389657319/" title="tagshortcuts2 by digiKam team, on Flickr"><img src="http://farm6.static.flickr.com/5218/5389657319_edb75c8ccb_z.jpg" width="640" height="256" alt="tagshortcuts2"></a>

<p>With this release, digiKam include <b>Tags Keyboard Shorcuts</b> feature to simplify tagging operations in your photograph workflow.</p>

<p>digiKam software collection 2.0.0 include all Google Summer of Code 2010 projects, as <b>XMP sidecar support</b>, <b>Face Recognition</b>, <b>Image Versioning</b>, and <b>Reverse Geocoding</b>. All this works have been processed during <a href="http://techbase.kde.org/Projects/Digikam/CodingSprint2010">Coding Sprint 2010</a>. You can find a resume of this event <a href="http://www.digikam.org/drupal/node/538">at this page</a>.</p>

<p>This beta release is not stable. Do not use yet in production. Please report all bugs to KDE bugzilla following indications <a href="http://www.digikam.org/drupal/support">from this page</a>. The release plan can be seen <a href="http://www.digikam.org/drupal/about/releaseplan">at this url</a>. See also <a href="http://websvn.kde.org/*checkout*/branches/extragear/graphics/digikam/core/NEWS?revision=1217743">the list of file closed</a> with this release into KDE bugzilla.</p>

<p>digiKam software collection tarball can be downloaded from <a href="http://sourceforge.net/projects/digikam/files">SourceForge Repository</a></p>

<p>Happy digiKam testing...</p>
<div class="legacy-comments">

  <a id="comment-19813"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/571#comment-19813" class="active">Tar file</a></h3>    <div class="submitted">Submitted by Leo Noordhuizen (not verified) on Mon, 2011-01-31 11:00.</div>
    <div class="content">
     <p>Im very anxious to check out the new beta, however the compressed file seems to be corrupt, OR I am doing something wrong...<br>
Anyway, this is the result of attempting to decompress:<br>
-----------------------------<br>
bzip2: Compressed file ends unexpectedly;<br>
	perhaps it is corrupted?  *Possible* reason follows.<br>
bzip2: Inappropriate ioctl for device<br>
	Input file = (stdin), output file = (stdout)</p>
<p>It is possible that the compressed file(s) have become corrupted.<br>
You can use the -tvv option to test integrity of such files.</p>
<p>You can use the `bzip2recover' program to attempt to recover<br>
data from undamaged sections of corrupted files.</p>
<p>tar: Unexpected EOF in archive<br>
tar: Error is not recoverable: exiting now<br>
-----------------------------------<br>
(I tried twice....)</p>
<p>Thanks for the marvellous work !</p>
<p>[lcn]</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19818"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/571#comment-19818" class="active">Sourceforge woes</a></h3>    <div class="submitted">Submitted by <a href="http://www.akhuettel.de/" rel="nofollow">Andreas K. Hüttel</a> (not verified) on Mon, 2011-01-31 21:52.</div>
    <div class="content">
     <p>Yep, indeed. Right now it's pretty much impossible to download the file, since every single sourceforge mirror resets the connection after a few hundred kilobytes.</p>
<p>Either the file has not propagated yet (and the master is now overloaded), or something else is going badly wrong at sourceforge.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19819"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/571#comment-19819" class="active">Temporary alternative URL</a></h3>    <div class="submitted">Submitted by Andreas K. Hüttel (not verified) on Tue, 2011-02-01 09:54.</div>
    <div class="content">
     <p><a href="http://distfiles.gentoo.org/distfiles/digikam-2.0.0-beta2.tar.bz2">http://distfiles.gentoo.org/distfiles/digikam-2.0.0-beta2.tar.bz2</a></p>
<p>&gt; $ md5sum digikam-2.0.0-beta2.tar.bz2<br>
&gt; 687cd3fa5609d877d5cdd81d1d386863  digikam-2.0.0-beta2.tar.bz2</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19820"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/571#comment-19820" class="active">Really appreciated :)</a></h3>    <div class="submitted">Submitted by Henrik Tjäder (not verified) on Tue, 2011-02-01 22:33.</div>
    <div class="content">
     <p>Really appreciated :)</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div><a id="comment-19821"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/571#comment-19821" class="active">fixed...</a></h3>    <div class="submitted">Submitted by digiKam on Wed, 2011-02-02 07:05.</div>
    <div class="content">
     <p>Sound like a SF.net mirror mechanism problem. I just tested and all is fine now...</p>
<p>digiKam</p>
         </div>
    <div class="links">» </div>
  </div>
</div><a id="comment-19814"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/571#comment-19814" class="active">Side-car Files, Docs and Face "Recognition"</a></h3>    <div class="submitted">Submitted by Jeff (not verified) on Mon, 2011-01-31 11:36.</div>
    <div class="content">
     <p>I'm excited about the XMP side-car support, though. I think that will be a really useful feature, particularly as it adds a degree of data redundancy and makes backups easier. I lots the tags for thousands of images in DigiKam in the past and will never trust it completely again (though I have no plans to use anything else). I think side-car files might also make it less problematic when tagging and geo-locating, as the image file will not be touched and won't be automatically reloaded and re-thumbnailed, which slows things down. I often shoot RAW+JPEG (Canon CR2 RAW) and cannot write the metadata to the CR2 files, so side-car files will help a lot. Shooting this way means I have less to do in post than if I shoot only CR2, but I end up with two files with the same name and different extensions. Having only one side-car file is OK, as images with the same name represent the same scene regardless of file extension (the way I name them, anyway). However, if I were to tag the JPEG file and DigiKam updated the side-car file, would tagging the CR2 also update the same file? If the set of tags were different, would say adding two tags to the JPEG and then (accidentally, maybe) adding only one tag to the CR2 overwrite the side-car file with the single tag version of the metadata? Would tagging the JPEG and updating the side-car automatically make the CR2 file appear to have the same tags? Would I have to re-scan the metadata to update the database to make this work? Also, will there be a separate side-car file per image version, now that image versioning is supported? Different versions of an image could very well require different tags. In my usage, different versions (currently just different names) can have different crop ratios and I'll tag them as such.</p>
<p>One thing I noticed is that the documentation seems to be falling further and further behind the releases. There is very little information available about new features, so they are hard to discover and understand. Keyboard shortcuts for tags sounds very interesting, but where can I learn more. Often, the documentation just describes that a feature exists, but for something like side-car files, it would be important to know in detail exactly how the files are updated and synchronised with the database under various conditions (e.g., versioning, RAW+JPEG shooting, etc.). Is there any work planned on the documentation front? Is there an online copy of the 2.0 guide (or even something later than the 1.2 copy that was all I could find last week)?</p>
<p>I think it was clarified at the time of the last beta release that it is "face detection", not "face recognition". I'm still wondering what the point of face detection is, as I can detect faces perfectly well myself. Judging from the few screenshots with the last release, it looks like tagging people in photographs will still be much easier and more efficient the old way (i.e., look at a photo and click on a few "People" tags on the right panel, rather than going through the whole tag-hunting process separately for each detected face). Maybe face detection would be more useful if it were applied to tasks like automatic red-eye removal and the like.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19815"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/571#comment-19815" class="active">Face detection</a></h3>    <div class="submitted">Submitted by olin (not verified) on Mon, 2011-01-31 16:10.</div>
    <div class="content">
     <p>I didn't try it yet, but it would be good when the tag could be dropped onto the detected face. I would like to try the new Digikam and use it for longer time, but the description with the "beta" status discourages me.</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19816"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/571#comment-19816" class="active">Face Detection in a Workflow</a></h3>    <div class="submitted">Submitted by Jeff (not verified) on Mon, 2011-01-31 18:09.</div>
    <div class="content">
     <p>Can someone explain to me how face detection could make my workflow more efficient? This is how I apply my "People" tags:</p>
<p> 1. I view lots of photos in the album/thumbnail view.</p>
<p> 2. I select lots of those photos that contain a specific person or that have several people in common.</p>
<p> 3. I click the tags for those people.</p>
<p> 4. I return to step 2 until all people in the set of photos are tagged, paging down first if the visible thumbnails are all done.</p>
<p>I can apply tags for two or three people to twenty to thirty photos (one page of thumbnails) in a few seconds and tag several hundred photos in a few minutes. I really don't see how face detection--as applied to tagging--makes that any easier. It seems to me that I would need to do step 2 one photo at a time and step 3 one person at a time once the faces in that one photo have been detected. It sounds like the sort of feature that I might use once just to see what it looks like and then never use it again.</p>
<p>Are there any plans to put the face detection capability to other uses? Automatically fixing red-eye or smoothing skin tones in the editor, perhaps? Any possibility that face recognition might be added?</p>
         </div>
    <div class="links">» </div>
  </div>
</div></div><a id="comment-19817"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/571#comment-19817" class="active">Tags Keyboard Shorcuts - nice one</a></h3>    <div class="submitted">Submitted by Zed (not verified) on Mon, 2011-01-31 21:49.</div>
    <div class="content">
     <p>I am very pleased to read that you gonna implement a "Tags Keyboard Shorcuts" feature. I just started to tag my images and this will certainly be a big help.<br>
Great work you are doing here!<br>
Cheers, Zed.</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-19822"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/571#comment-19822" class="active">Face Recognition</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Thu, 2011-02-03 11:51.</div>
    <div class="content">
     <p>How to use Face Recognition ? Use DigiKam 2.0.0-beta3 from svn, but could not find how to run "Face Recognition"...</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-19823"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/571#comment-19823" class="active">For those worried about touching original files.</a></h3>    <div class="submitted">Submitted by <a href="http://b.joaoubaldo.com/?p=277" rel="nofollow">João C.</a> (not verified) on Fri, 2011-02-04 11:58.</div>
    <div class="content">
     <p>Hi.</p>
<p>I've read a comment on this site about concerns when it comes to write metadata to original photo files. I'm in the same boat, but I also have a suggestion that may relieve some of you digiKam users.</p>
<p>You can read the details at http://b.joaoubaldo.com/?p=277.</p>
<p>Please share any comments, suggestions you may have.</p>
<p>Cheers</p>
         </div>
    <div class="links">» </div>
  </div>
<a id="comment-19832"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/571#comment-19832" class="active">How to test on Macosx ?</a></h3>    <div class="submitted">Submitted by Anonymous (not verified) on Wed, 2011-02-16 11:45.</div>
    <div class="content">
     <p>Hello,</p>
<p>Is there a way to use digikam 2.0.0 beta on macosx ?</p>
<p>- ports for macports ?<br>
- .dmg ?<br>
- compile myself ???</p>
<p>Thanks in advance</p>
<p>BTW: is the scripting interface ( stscript integration) available on this beta ??</p>
         </div>
    <div class="links">» </div>
  </div>
<div class="indented"><a id="comment-19833"></a>
  <div class="comment comment-published">
    <h3 class="title"><a href="/node/571#comment-19833" class="active">macport + source code...</a></h3>    <div class="submitted">Submitted by digiKam on Wed, 2011-02-16 11:59.</div>
    <div class="content">
     <p>I compiled myself digiKam 2.0 under my macbook. I installed all depencies using macport before.</p>
<p>2.0 doesn't start properly. The is no iconview items available. 1.x work fine on the same computer and same dependencies.</p>
<p>It sound like a wrong database initialization under MacOSX.</p>
<p>digiKam</p>
         </div>
    <div class="links">» </div>
  </div>
</div>
</div>
