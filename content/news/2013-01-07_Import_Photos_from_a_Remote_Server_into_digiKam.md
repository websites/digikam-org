---
date: "2013-01-07T10:38:00Z"
title: "Import Photos from a Remote Server into digiKam"
author: "Dmitri Popov"
description: "Using commands under the Import menu, you can pull photos from a variety of sources, including remote servers. The latter functionality in digiKam is provided"
category: "news"
aliases: "/node/679"

---

<p>Using commands under the <strong>Import</strong> menu, you can pull photos from a variety of sources, including remote servers. The latter functionality in digiKam is provided through the <strong>KioExportImport</strong> Kipi plugin which supports common protocols like FTP, SSH, and SMB.</p>
<p><img class="size-medium wp-image-3200" alt="Importing photos from a remote server" src="http://scribblesandsnaps.files.wordpress.com/2012/12/digikam-importremoteserver.png?w=500" width="500" height="308"></p>
<p><a href="http://scribblesandsnaps.com/2013/01/07/import-photos-from-a-remote-server-into-digikam/">Continue to read</a></p>

<div class="legacy-comments">

</div>